<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Front Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application frontend.
| These routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** @var Router $routes */

$routes->get(
    '/',
    function () {
        return redirect(route('admin.dashboard'));
    }
)->name('index');

$routes->get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard');
$routes->get('/profile', 'Users\UsersController@profile')->name('users.profile');
$routes->get('/profile/edit', 'Users\UsersController@editProfile')->name('users.profile.edit');

$this->patch('/change-password', 'Users\UsersController@changePassword')->name('users.changePassword');
$routes->get('users/{status}/data-table', 'Users\UsersController@getPaginatedUsers')->name('users.table');
$routes->patch('users/{user}/status/{change}', 'Users\UsersController@changeStatus')->name('users.change.status');
$routes->resource('users', 'Users\UsersController');

$routes->get('roles/data-table', 'Users\RolesController@getPaginatedRoles')->name('roles.table');
$routes->resource('roles', 'Users\RolesController');

$routes->get('parties/data-table', 'Users\PartyController@getPaginatedParties')->name('parties.table');
$routes->resource('parties', 'Users\PartyController');

$routes->get('invoices/data-table', 'Invoice\InvoiceController@getPaginatedInvoices')->name('invoices.table');
$routes->resource('invoices', 'Invoice\InvoiceController');


$routes->get('customs/data-table', 'Custom\CustomController@getPaginatedCustoms')->name('customs.table');
$routes->resource('customs', 'Custom\CustomController');

$routes->get('permissions/data-table', 'Users\Permissions\PermissionsController@getPaginatedPermissions')->name('permissions.table');
$routes->get('/permissions/manage', 'Users\Permissions\PermissionsController@showRolesAndPermissions')->name('permissions.manage');
$routes->get('/permissions/{permission}/role/{role}/toggle', 'Users\Permissions\PermissionsController@togglePermission')->name('permissions.toggle');
$routes->resource('/permissions', 'Users\Permissions\PermissionsController');
