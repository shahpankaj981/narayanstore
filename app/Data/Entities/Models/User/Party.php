<?php

namespace App\Data\Entities;

use App\Data\Entities\Models\Custom\Custom;
use App\Data\Entities\Models\Invoice\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Party
 * @property mixed address
 * @package App\Entities
 */
class Party extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'address',
        'phone',
        'mobile',
        'contact_person',
        'bank',
        'bank_account',
        'ifsc_code',
        'bank_area',
        'gst_number',
        'pan',
        'iec_number',
        'cin_number',
        'lut',
        'other_details'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'address'      => 'object',
    ];

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * Returns the formatted address.
     *
     * @return string
     */
    public function getFormattedAddressAttribute()
    {
        $address = $this->address;

        return sprintf('%s %s, %s %s', $address->street, $address->city, $address->country, $address->zip);
    }

    public function customs()
    {
        return $this->belongsToMany(Custom::class);
    }
}
