<?php

namespace App\Data\Entities\Models\Invoice;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InvoiceRecord
 * @package App\Data\Entities\Models\Invoice
 */
class InvoiceRecord extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'bale_number',
        'description',
        'hsn_code',
        'quantity',
        'product_type',
        'u_m',
        'rate',
        'taxable_amount'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}