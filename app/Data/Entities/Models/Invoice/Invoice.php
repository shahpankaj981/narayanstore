<?php

namespace App\Data\Entities\Models\Invoice;

use App\Data\Entities\Models\Custom\Custom;
use App\Data\Entities\Party;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Invoice
 * @property mixed invoice_date
 * @package App\Entities
 */
class Invoice extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'invoice_number',
        'invoice_date',
        'payment',
        'payment_date',
        'party_id',
        'agent_name',
        'agent_mobile',
        'agent_charge',
        'lr_number',
        'lr_date',
        'transport_payment',
        'transport_payment_date',
        'transport_name',
        'dispatched_date',
        'received_date',
        'dispatched_from',
        'received_at',
        'discount_rate',
        'postage',
        'insurance',
        'igst_rate',
        'transport_amount',
        'miscellaneous_amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany(InvoiceRecord::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function party()
    {
        return $this->belongsTo(Party::class);
    }

    /**
     * @return string
     */
    public function getFormattedInvoiceDateAttribute()
    {
        return Carbon::parse($this->invoice_date)->toFormattedDateString();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customs()
    {
        return $this->belongsToMany(Custom::class);
    }
}
