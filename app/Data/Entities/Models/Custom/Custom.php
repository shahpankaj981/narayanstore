<?php

namespace App\Data\Entities\Models\Custom;

use App\Data\Entities\Models\Invoice\Invoice;
use App\Data\Entities\Party;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Custom
 * @property mixed clearance_date
 * @package App\Data\Entities\Models\Custom
 */
class Custom extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $with = ['invoices', 'parties'];

    /**
     * @var array
     */
    protected $fillable = [
        'custom_ref_no',
        'vehicle_no',
        'quality',
        'custom_amount',
        'vat_amount',
        'clearance_charge',
        'clearance_date',
        'bale_quantity'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parties()
    {
        return $this->belongsToMany(Party::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function invoices()
    {
        return $this->belongsToMany(Invoice::class);
    }

    /**
     * @return string
     */
    public function getFormattedClearanceDateAttribute()
    {
        return $this->clearance_date ? Carbon::parse($this->clearance_date)->toFormattedDateString() : 'N/A';
    }
}
