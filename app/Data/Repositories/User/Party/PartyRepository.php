<?php

namespace App\Data\Repositories\User\Party;

use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Interface PartyRepository
 * @package App\Data\Repositories\User\Party
 */
interface PartyRepository extends RepositoryInterface
{
    /**
     * @param array $filters
     * @return LengthAwarePaginator
     */
    public function getPaginatedPartiesWith(array $filters): LengthAwarePaginator;
}