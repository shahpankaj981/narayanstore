<?php

namespace App\Data\Repositories\User\Party;

use App\Data\Entities\Party;
use App\StartUp\BaseClasses\Repository\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PartyEloquentRepository
 * @package App\Data\Repositories\User\Party
 */
class PartyEloquentRepository extends BaseRepository implements PartyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Party::class;
    }

    /**
     * @param array $filters
     * @return LengthAwarePaginator
     */
    public function getPaginatedPartiesWith(array $filters): LengthAwarePaginator
    {
        $query       = $this->model->orderBy($filters['column'], $filters['order']);
        $searchQuery = $filters['search'];

        if (!empty($searchQuery)) {
            $query = $query->where(
                function ($query) use ($searchQuery) {
                    $query->orWhere('name', 'like', "%{$searchQuery}%")
                        ->orWhere('mobile', 'like', "%{$searchQuery}%")
                        ->orWhere('address', 'like', "%{$searchQuery}%")
                        ->orWhere('contact_person', 'like', "%{$searchQuery}%");
                }
            );
        }

        return $query->paginate($filters['per_page']);
    }
}