<?php

namespace App\Data\Repositories\User;

use App\Constants\StatusType;
use App\Data\Entities\Models\User\User;
use App\StartUp\BaseClasses\Repository\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class UserEloquentRepository
 * @package App\Data\Repositories\User
 */
class UserEloquentRepository extends BaseRepository implements UserRepository
{
    /**
     * @var array
     */
    protected $filterFields = [
        'id'   => 'id',
        'name' => 'full_name->first_name',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get the paginated users with filters.
     * @param $filters
     * @return mixed
     */
    public function getPaginatedUsersWith(array $filters): LengthAwarePaginator
    {
        $sortField = array_get($this->filterFields, $filters['column']) ?? $filters['column'];
        if ($filters['status'] === 'inactive') {
            $query = $this->model->where('status', StatusType::DEACTIVATED);
        } else {
            $query = $this->model->where('status', '!=' ,StatusType::DEACTIVATED);
        }
        $query = $query->orderBy($sortField, $filters['order']);

        $searchQuery = $filters['search'];

        if (!empty($searchQuery)) {
            $query = $query->where(
                function ($query) use ($searchQuery) {
                    $query->orWhere('full_name->first_name', 'like', "%{$searchQuery}%")
                        ->orWhere('full_name->last_name', 'like', "%{$searchQuery}%")
                        ->orWhere('email', 'like', "%{$searchQuery}%")
                        ->orWhere('status', 'like', "%{$searchQuery}%")
                        ->orWhere('username', 'like', "%{$searchQuery}%");
                }
            );
        }

        return $query->paginate($filters['per_page']);
    }

    /**
     * Changes password of a user.
     *
     * @param int    $userId
     * @param string $password
     * @return mixed
     */
    public function changePassword(int $userId, string $password)
    {
        return $this->model
            ->where('id', $userId)
            ->update([ 'password' => bcrypt($password) ]);
    }
}
