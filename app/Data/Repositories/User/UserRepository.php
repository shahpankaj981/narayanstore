<?php

namespace App\Data\Repositories\User;

use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package App\Data\Repositories\User
 */
interface UserRepository extends RepositoryInterface
{
    /**
     * Get the paginated users with filters.
     * @param array $filters
     * @return mixed
     */
    public function getPaginatedUsersWith(array $filters): LengthAwarePaginator;

    /**
     * Changes password of a user.
     *
     * @param int    $userId
     * @param string $password
     * @return mixed
     */
    public function changePassword(int $userId, string $password);
}
