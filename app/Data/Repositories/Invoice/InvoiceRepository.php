<?php

namespace App\Data\Repositories\Invoice;

use App\Data\Entities\Models\Invoice\Invoice;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InvoiceRepository
 * @package App\Data\Repositories\Invoice
 */
interface InvoiceRepository extends RepositoryInterface
{
    /**
     * Creates an invoice with records.
     *
     * @param array $inputData
     * @return mixed
     */
    public function createInvoiceWithRecords(array $inputData);

    /**
     * Returns the paginated invoices.
     *
     * @param $filters
     * @return LengthAwarePaginator
     */
    public function getPaginatedInvoicesWith($filters);

    /**
     * Finds invoice with records.
     *
     * @param $id
     * @return mixed
     */
    public function findInvoiceWithRecords(int $id);

    /**
     * Updates an invoice with its records.
     *
     * @param $id
     * @param $updateData
     * @return Invoice
     */
    public function updateInvoiceWithRecords(int $id, array $updateData);
}