<?php

namespace App\Data\Repositories\Invoice;

use App\Data\Entities\Models\Invoice\Invoice;
use App\StartUp\BaseClasses\Repository\BaseRepository;
use Illuminate\Container\Container as Application;
use Illuminate\Database\DatabaseManager;
use Illuminate\Pagination\LengthAwarePaginator;

class InvoiceEloquentRepository extends BaseRepository implements InvoiceRepository
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * InvoiceEloquentRepository constructor.
     * @param Application     $app
     * @param DatabaseManager $databaseManager
     */
    public function __construct(Application $app, DatabaseManager $databaseManager)
    {
        parent::__construct($app);
        $this->databaseManager = $databaseManager;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Invoice::class;
    }

    /**
     * Returns the paginated invoices.
     *
     * @param $filters
     * @return LengthAwarePaginator
     */
    public function getPaginatedInvoicesWith($filters)
    {
        $query       = $this->model->orderBy($filters['column'], $filters['order']);
        $searchQuery = $filters['search'];

        if (!empty($searchQuery)) {
            $query = $query->where(
                function ($query) use ($searchQuery) {
                    $query->orWhere('invoice_number', 'like', "%{$searchQuery}%")
                        ->orWhere('invoice_date', 'like', "%{$searchQuery}%")
                        ->orWhere('agent_name', 'like', "%{$searchQuery}%")
                        ->orWhereHas('party', function ($query) use ($searchQuery) {
                            return $query->where('name', 'like', "%{$searchQuery}%");
                        });
                }
            );
        }

        return $query->paginate($filters['per_page']);
    }

    /**
     * Creates an invoice with records.
     *
     * @param array $inputData
     * @return mixed
     * @throws \Exception
     */
    public function createInvoiceWithRecords(array $inputData)
    {
        $this->databaseManager->beginTransaction();
        try {
            $invoice = $this->create($inputData['invoice']);
            foreach ($inputData['items'] as $item) {
                $invoice->records()->create($item);
            }
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
        $this->databaseManager->commit();

        return $invoice;
    }

    /**
     * Finds invoice with records.
     *
     * @param $id
     * @return mixed
     */
    public function findInvoiceWithRecords(int $id)
    {
        return $this->with('records')->find($id);
    }

    /**
     * Updates an invoice with its records.
     *
     * @param $id
     * @param $updateData
     * @return Invoice
     * @throws \Exception
     */
    public function updateInvoiceWithRecords(int $id, array $updateData)
    {
        $this->databaseManager->beginTransaction();
        try {
            $invoice = $this->update($updateData['invoice'], $id);
            foreach ($updateData['items'] as $record) {
                $invoice->records()->updateOrCreate(['id' => $record['id'] ?? ''], $record);
            }
            foreach($updateData['deletedRows'] as $rowId){
                $invoice->records()->find($rowId)->delete();
            }
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();

        return $invoice;
    }
}