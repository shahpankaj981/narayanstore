<?php

namespace App\Data\Repositories\Custom;

use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CustomRepository
 * @package App\Data\Repositories\Custom
 */
interface CustomRepository extends RepositoryInterface
{
    /**
     * Returns the paginated invoices.
     *
     * @param $filters
     * @return LengthAwarePaginator
     */
    public function getPaginatedCustomsWith($filters);

    /**
     * @param $inputData
     * @return mixed
     */
    public function createCustomWithInvoices(array $inputData);

    /**
     * @param $id
     * @param $updateData
     * @return mixed
     */
    public function updateCustomWithInvoices(int $id, array $updateData);

}