<?php

namespace App\Data\Repositories\Custom;

use App\Data\Entities\Models\Custom\Custom;
use App\StartUp\BaseClasses\Repository\BaseRepository;
use Illuminate\Container\Container as Application;
use Illuminate\Database\DatabaseManager;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class CustomEloquentRepository
 * @package App\Data\Repositories\Custom
 */
class CustomEloquentRepository extends BaseRepository implements CustomRepository
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * CustomEloquentRepository constructor.
     * @param Application     $app
     * @param DatabaseManager $databaseManager
     */
    public function __construct(Application $app, DatabaseManager $databaseManager)
    {
        parent::__construct($app);
        $this->databaseManager = $databaseManager;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Custom::class;
    }

    /**
     * Returns the paginated invoices.
     *
     * @param $filters
     * @return LengthAwarePaginator
     */
    public function getPaginatedCustomsWith($filters)
    {
        $query       = $this->model->orderBy($filters['column'], $filters['order']);
        $searchQuery = $filters['search'];

        if (!empty($searchQuery)) {
            $query = $query->where(
                function ($query) use ($searchQuery) {
                    $query->orWhere('custom_ref_no', 'like', "%{$searchQuery}%")
                        ->orWhere('vehicle_no', 'like', "%{$searchQuery}%")
                        ->orWhere('custom_amount', 'like', "%{$searchQuery}%")
                        ->orWhere('quality', 'like', "%{$searchQuery}%")
                        ->orWhere('bale_quantity', 'like', "%{$searchQuery}%")
                        ->orWhere('clearance_date', 'like', "%{$searchQuery}%");
                }
            );
        }

        return $query->paginate($filters['per_page']);
    }

    /**
     * @param $inputData
     * @return Custom
     * @throws \Exception
     */
    public function createCustomWithInvoices(array $inputData): Custom
    {
        $this->databaseManager->beginTransaction();
        try {
            $custom = $this->create($inputData);
            foreach ($inputData['invoices'] as $invoice) {
                $custom->invoices()->attach($invoice['id']);
            }
            foreach ($inputData['parties'] as $party) {
                $custom->parties()->attach($party['id']);
            }
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        return $custom;
    }

    /**
     * @param $id
     * @param $updateData
     * @return Custom
     * @throws \Exception
     */
    public function updateCustomWithInvoices(int $id, array $updateData): Custom
    {
        $this->databaseManager->beginTransaction();
        try {
            $custom     = $this->update($updateData, $id);
            $invoiceIds = array_map(function ($invoice) {
                return $invoice['id'];
            }, $updateData['invoices']);

            $partyIds   = array_map(function ($party) {
                return $party['id'];
            }, $updateData['parties']);

            $custom->parties()->sync($partyIds);
            $custom->invoices()->sync($invoiceIds);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        return $custom;
    }
}
