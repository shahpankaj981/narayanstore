<?php

namespace App\Infrastructure\Helpers;

use App\Data\Entities\Models\Invoice\Invoice;
use App\Data\Entities\Models\User\Permission;
use App\Data\Entities\Models\User\Role;
use App\Data\Entities\Party;
use Illuminate\Support\Collection;

/**
 * Class SelectListHelper
 * @package App\Infrastructure\Helpers
 */
class SelectListHelper
{
    /**
     * Returns all the roles.
     *
     * @return Collection
     */
    public function getAllRoles(): Collection
    {
        return app(Role::class)->all();
    }

    /**
     * Returns all the permissions.
     *
     * @return Collection
     */
    public function getAllPermissions(): Collection
    {
        return app(Permission::class)->all();
    }

    /**
     * Get all parties' id and name.
     */
    public function getAllParties(): array
    {
        return app(Party::class)->pluck('name', 'id')->toArray();
    }

    /**
     * Get all parties for drop down.
     */
    public function getAllPartiesForDropDown(): array
    {
        return app(Party::class)
            ->select('id', 'name')
            ->get()
            ->toArray();
    }

    /**
     * Get all invoices for drop down.
     */
    public function getAllInvoicesForDropDown(): array
    {
        return app(Invoice::class)
            ->select('id', 'invoice_number')
            ->get()
            ->toArray();
    }
}
