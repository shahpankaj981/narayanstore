<?php

use App\Constants\StatusType;

/**
 * @return string
 */
function getCopyrightYearFormatted(): string
{
    $startYear   = config('config.copyright.start_year');
    $currentYear = date('Y');

    return $currentYear > $startYear ? sprintf("%s - %s", $startYear, $currentYear) : sprintf("%s", $startYear);
}

/**
 * Get the user details for the delete button.
 * @param $user
 * @return array
 */
function getUserDetailsForDeleteButton($user): array
{
    if ($user->status === StatusType::DEACTIVATED) {
        return [
            'change_action'   => 'activate',
            'title'           => 'Activate User',
            'icon'            => 'fa fa-check',
            'success_message' => 'Successfully activated user.',
        ];
    } else {
        return [
            'change_action'   => 'deactivate',
            'title'           => 'Deactivate User',
            'icon'            => 'fa fa-trash',
            'success_message' => 'Successfully deactivated user.',
        ];
    }
}

/**
 * Get the module details for the delete button.
 * @param $user
 * @return array
 */
function getModuleDetailsForDeleteButton(): array
{
        return [
            'change_action'   => 'deactivate',
            'title'           => 'Delete Module',
            'icon'            => 'fa fa-trash',
            'success_message' => 'Successfully Deleted Module.',
        ];
}

/**
 * Check if the submenu of tenant is active or not, checking the URI.
 * @param string $subMenu
 * @param null   $additionalParameter
 * @return string
 */
function isSubMenuActive(string $subMenu, $additionalParameter = null): string
{
    return (request()->routeIs($subMenu) && !request()->segment(4)) ? 'm-menu__item--active' : '';
}

/**
 * Check if the menu is active or not, checking the URI.
 * @param string $menu
 * @return string
 */
function isSideBarActive(string $menu): string
{
    $urlParams = explode('/', url()->current());

    return in_array($menu, $urlParams) ? 'm-menu__item--open m-menu__item--expanded' : '';
}
