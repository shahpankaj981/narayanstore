<?php

namespace App\Infrastructure\Providers;

use App\Data\Repositories\Custom\CustomEloquentRepository;
use App\Data\Repositories\Custom\CustomRepository;
use App\Data\Repositories\Invoice\InvoiceEloquentRepository;
use App\Data\Repositories\Invoice\InvoiceRepository;
use App\Data\Repositories\User\Party\PartyEloquentRepository;
use App\Data\Repositories\User\Party\PartyRepository;
use App\Data\Repositories\User\Permission\PermissionEloquentRepository;
use App\Data\Repositories\User\Permission\PermissionRepository;
use App\Data\Repositories\User\Role\RoleEloquentRepository;
use App\Data\Repositories\User\Role\RoleRepository;
use App\Data\Repositories\User\UserEloquentRepository;
use App\Data\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * @package App\Infrastructure\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $repositories = [
        UserRepository::class       => UserEloquentRepository::class,
        PermissionRepository::class => PermissionEloquentRepository::class,
        RoleRepository::class       => RoleEloquentRepository::class,
        PartyRepository::class      => PartyEloquentRepository::class,
        InvoiceRepository::class    => InvoiceEloquentRepository::class,
        CustomRepository::class     => CustomEloquentRepository::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        collect($this->repositories)->each(
            function (string $implementation, string $interface) {
                $this->app->bind($interface, $implementation);
            }
        );
    }
}
