<?php

namespace App\Domain\Admin\Resources\Party;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PartyResource
 * @property string name
 * @property string title
 * @package App\Domain\Admin\Resources\Party
 */
class PartyResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $viewPartyLink = route('admin.parties.show', $this);
        $editDetails   = route('admin.parties.edit', $this);
        $deleteUrl     = route('admin.parties.destroy', $this);

        return [
            'name'           => $this->name,
            'title'          => $this->formatted_address,
            'mobile'         => $this->mobile ?? "N/A",
            'contact_person' => $this->contact_person ?? "N/A",
            'action'         => [
                'view'   => $viewPartyLink,
                'edit'   => $editDetails,
                'delete' => [
                    'method'          => 'delete',
                    'url'             => $deleteUrl,
                    'title'           => 'Delete Party',
                    'icon'            => 'fa fa-trash',
                    'success_message' => 'Successfully deleted the client.',
                ],
            ],
        ];
    }
}
