<?php

namespace App\Domain\Admin\Resources\Custom;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CustomResource
 * @property mixed custom_ref_no
 * @package App\Domain\Admin\Resources\Custom
 */
class CustomResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $viewPartyLink = route('admin.customs.show', $this);
        $editDetails   = route('admin.customs.edit', $this);
        $deleteUrl     = route('admin.customs.destroy', $this);

        return [
            'custom_ref_no'  => $this->custom_ref_no,
            'vehicle_no'     => $this->vehincle_no ?? 'N/A',
            'custom_amount'  => $this->custom_amount,
            'clearance_date' => $this->formatted_clearance_date,
            'action'         => [
                'view'   => $viewPartyLink,
                'edit'   => $editDetails,
                'delete' => [
                    'method'          => 'delete',
                    'url'             => $deleteUrl,
                    'title'           => 'Delete Custom',
                    'icon'            => 'fa fa-trash',
                    'success_message' => 'Successfully deleted the Custom.',
                ],
            ],
        ];
    }
}
