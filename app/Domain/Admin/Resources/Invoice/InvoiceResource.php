<?php

namespace App\Domain\Admin\Resources\Invoice;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class InvoiceResource
 * @property string name
 * @property string title
 * @package App\Domain\Admin\Resources\Party
 */
class InvoiceResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $viewPartyLink = route('admin.invoices.show', $this);
        $editDetails   = route('admin.invoices.edit', $this);
        $deleteUrl     = route('admin.invoices.destroy', $this);

        return [
            'invoice_number' => $this->invoice_number,
            'invoice_date'   => Carbon::parse($this->invoice_date)->toFormattedDateString(),
            'party'          => ucwords($this->party->name),
            'agent_name'     => $this->agent_name ?? "N/A",
            'action'         => [
                'view'   => $viewPartyLink,
                'edit'   => $editDetails,
                'delete' => [
                    'method'          => 'delete',
                    'url'             => $deleteUrl,
                    'title'           => 'Delete Client',
                    'icon'            => 'fa fa-trash',
                    'success_message' => 'Successfully deleted the client.',
                ],
            ],
        ];
    }
}
