<?php

namespace App\Domain\Admin\Services\Invoice;

use App\Data\Repositories\Invoice\InvoiceRepository;
use App\Domain\Admin\Resources\Invoice\InvoiceResource;

/**
 * Class InvoiceService
 * @package App\Domain\Admin\Services\Invoice
 */
class InvoiceService
{
    /**
     * @var InvoiceRepository
     */
    protected $invoiceRepository;

    /**
     * InvoiceService constructor.
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @param array $inputData
     * @return mixed
     */
    public function store(array $inputData)
    {
        return $this->invoiceRepository->createInvoiceWithRecords($inputData);
    }

    /**
     * @param int   $id
     * @param array $updateData
     * @return \App\Data\Entities\Invoice
     */
    public function update(int $id, array $updateData)
    {
        return $this->invoiceRepository->updateInvoiceWithRecords($id, $updateData);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id)
    {
        return $this->invoiceRepository->findInvoiceWithRecords($id);
    }

    /**
     * @param int $id
     * @return int
     */
    public function destroy(int $id)
    {
        return $this->invoiceRepository->delete($id);
    }

    /**
     * @param array $filters
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getPaginatedInvoicesForTable(array $filters)
    {
        $parties = $this->invoiceRepository->getPaginatedInvoicesWith($filters);

        return InvoiceResource::collection($parties);
    }
}