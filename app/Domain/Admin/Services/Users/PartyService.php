<?php

namespace App\Domain\Admin\Services\Users;

use App\Data\Entities\Party;
use App\Data\Repositories\User\Party\PartyRepository;
use App\Domain\Admin\Resources\Party\PartyResource;


/**
 * Class PartyService
 * @package App\Services
 */
class PartyService
{
    /**
     * @var PartyRepository
     */
    protected $partyRepository;

    /**
     * PartyService constructor.
     * @param PartyRepository $partyRepository
     */
    public function __construct(PartyRepository $partyRepository)
    {
        $this->partyRepository = $partyRepository;
    }

    /**
     * @param array $inputData
     * @return mixed
     */
    public function store(array $inputData)
    {
        return $this->partyRepository->create($inputData);
    }

    /**
     * Returns a party by its id.
     *
     * @param int $id
     * @return Party
     */
    public function findById(int $id): Party
    {
        return $this->partyRepository->find($id);
    }

    /**
     * updates a party.
     *
     * @param array $updateData
     * @param int   $id
     * @return Party
     */
    public function update(array $updateData, int $id): Party
    {
        return $this->partyRepository->update($updateData, $id);
    }

    public function getPaginatedPartiesForTable(array $filters)
    {
        $parties = $this->partyRepository->getPaginatedPartiesWith($filters);

        return PartyResource::collection($parties);
    }

    /**
     * Deletes a party.
     *
     * @param int $id
     * @return bool
     */
    public function destroy(int $id): bool
    {
        return $this->partyRepository->delete($id);
    }
}