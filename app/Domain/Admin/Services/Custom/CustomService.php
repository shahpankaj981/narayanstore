<?php

namespace App\Domain\Admin\Services\Custom;
use App\Data\Repositories\Custom\CustomRepository;
use App\Domain\Admin\Resources\Custom\CustomResource;


/**
 * Class CustomService
 * @package App\Domain\Admin\Services\Custom
 */
class CustomService
{
    /**
     * @var CustomRepository
     */
    protected $customRepository;

    /**
     * CustomService constructor.
     * @param CustomRepository $customRepository
     */
    public function __construct(CustomRepository $customRepository)
    {
        $this->customRepository = $customRepository;
    }

    /**
     * @param array $inputData
     * @return mixed
     */
    public function store(array $inputData)
    {
        return $this->customRepository->createCustomWithInvoices($inputData);
    }

    /**
     * @param int   $id
     * @param array $updateData
     * @return mixed
     */
    public function update(int $id, array $updateData)
    {
        return $this->customRepository->updateCustomWithInvoices($id, $updateData);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id)
    {
        return $this->customRepository->find($id);
    }

    /**
     * @param int $id
     * @return int
     */
    public function destroy(int $id)
    {
        return $this->customRepository->delete($id);
    }

    /**
     * @param array $filters
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getPaginatedCustomForTable(array $filters)
    {
        $parties = $this->customRepository->getPaginatedCustomsWith($filters);

        return CustomResource::collection($parties);
    }
}
