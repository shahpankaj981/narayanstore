<?php

namespace App\Domain\Admin\Controllers\Custom;

use App\Domain\Admin\Requests\Custom\CustomRequest;
use App\Domain\Admin\Services\Custom\CustomService;
use App\Infrastructure\Helpers\SelectListHelper;
use App\StartUp\BaseClasses\Controller\AdminController;
use Illuminate\Http\Response;

/**
 * Class CustomController
 * @package App\Domain\Admin\Controllers\Custom
 */
class CustomController extends AdminController
{
    /**
     * @var SelectListHelper
     */
    protected $selectListHelper;
    /**
     * @var CustomService
     */
    private $customService;

    /**
     * CustomController constructor.
     * @param CustomService    $customService
     * @param SelectListHelper $selectListHelper
     */
    public function __construct(CustomService $customService, SelectListHelper $selectListHelper)
    {
        $this->customService    = $customService;
        $this->selectListHelper = $selectListHelper;
    }

    public function index()
    {
        return view('admin.modules.custom.index');
    }

    public function getPaginatedCustoms()
    {
        return $this->customService->getPaginatedCustomForTable(request()->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parties = $this->selectListHelper->getAllPartiesForDropDown();
        $invoices = $this->selectListHelper->getAllInvoicesForDropDown();

        return view('admin.modules.custom.create', compact('productTypes', 'parties', 'invoices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CustomRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CustomRequest $request)
    {
        try {
            $this->customService->store($request->all());
            flash('Successfully Added Custom', 'success');
        } catch (\Exception $exception) {
            logger()->error($exception->getMessage());
            flash('Something went wrong', 'error');

            return $this->sendError('Unable to create new Custom.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully created new Custom.');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $custom = $this->customService->findById((int)$id);

        return view('admin.modules.custom.show', compact('custom'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $custom = $this->customService->findById((int)$id);
        $parties = $this->selectListHelper->getAllPartiesForDropDown();
        $invoices = $this->selectListHelper->getAllInvoicesForDropDown();

        return view('admin.modules.custom.edit', compact('custom', 'parties', 'invoices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param               $id
     * @param CustomRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, CustomRequest $request)
    {
        try {
            $this->customService->update((int)$id, $request->all());
            flash('Successfully Updated Custom', 'success');
        } catch (\Exception $exception) {
            logger()->error($exception->getMessage());
            flash('Something went wrong', 'error');

            return $this->sendError('Unable to update Custom.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully updated Custom.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->customService->destroy((int)$id);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to delete the Custom', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully deleted the Custom.');
    }
}
