<?php

namespace App\Domain\Admin\Controllers\Invoice;

use App\Domain\Admin\Requests\Invoice\InvoiceRequest;
use App\Domain\Admin\Services\Invoice\InvoiceService;
use App\Infrastructure\Helpers\SelectListHelper;
use App\StartUp\BaseClasses\Controller\AdminController;
use Illuminate\Http\Response;

/**
 * Class InvoiceController
 * @package App\Http\Controllers\Admin
 */
class InvoiceController extends AdminController
{
    /**
     * @var InvoiceService
     */
    protected $invoiceService;
    /**
     * @var SelectListHelper
     */
    protected $selectListHelper;

    /**
     * InvoiceController constructor.
     * @param InvoiceService   $invoiceService
     * @param SelectListHelper $selectListHelper
     */
    public function __construct(InvoiceService $invoiceService, SelectListHelper $selectListHelper)
    {
        $this->invoiceService = $invoiceService;
        $this->selectListHelper = $selectListHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.modules.invoice.index');
    }

    /**
     * Returns the paginated invoices for data table.
     */
    public function getPaginatedInvoices()
    {
        return $this->invoiceService->getPaginatedInvoicesForTable(request()->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productTypes = config('productTypes');
        $parties = $this->selectListHelper->getAllParties();

        return view('admin.modules.invoice.create', compact('productTypes', 'parties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InvoiceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(InvoiceRequest $request)
    {
        try {
            $this->invoiceService->store($request->all());
            flash('Successfully Added Invoice', 'success');
        } catch (\Exception $exception) {
            logger()->error($exception->getMessage());
            flash('Something went wrong', 'error');

            return $this->sendError('Unable to create new invoice.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully created new client.');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = $this->invoiceService->findById((int) $id);

        return view('admin.modules.invoice.show', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = $this->invoiceService->findById((int)$id);
        $productTypes = config('productTypes');
        $parties = $this->selectListHelper->getAllParties();
        $records = $invoice->records->all();

        return view('admin.modules.invoice.edit', compact('invoice', 'productTypes', 'parties', 'records'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param                $id
     * @param InvoiceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, InvoiceRequest $request)
    {
        try {
            $this->invoiceService->update((int)$id, $request->all());
            flash('Successfully Updated Invoice', 'success');
        } catch (\Exception $exception) {
            logger()->error($exception->getMessage());
            flash('Something went wrong', 'error');

            return $this->sendError('Unable to update invoice.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully updated client.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->invoiceService->destroy((int)$id);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to delete the invoice', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully deleted the invoice.');
    }
}
