<?php

namespace App\Domain\Admin\Controllers\Dashboard;

use App\StartUp\BaseClasses\Controller\AdminController;

/**
 * Class DashboardController
 * @package App\Domain\Admin\Controllers\Dashboard
 */
class DashboardController extends AdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.modules.dashboard.index');
    }
}
