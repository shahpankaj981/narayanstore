<?php

namespace App\Domain\Admin\Controllers\Users;

use App\Domain\Admin\Requests\User\UsersRequest;
use App\Domain\Admin\Services\Users\UserService;
use App\StartUp\BaseClasses\Controller\AdminController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class UsersController
 * @package App\Domain\Admin\Controllers\Users
 */
class UsersController extends AdminController
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * UsersController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Index page for users page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.modules.users.index');
    }

    /**
     * @param string $status
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getPaginatedUsers(string $status)
    {
        $filters = request()->merge(['status' => $status])->all();
        $users   = $this->userService->getPaginatedUsersForTable($filters);

        return $users;
    }

    /**
     * @return string
     */
    public function create()
    {
        return view('admin.modules.users.create');
    }

    /**
     * @param UsersRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UsersRequest $request)
    {
        try {
            $this->userService->createUser($request->all());
        } catch (\Exception $exception) {
            logger()->error($exception);
            flash('Unable to create new user.', 'error');

            return $this->sendError('Unable to create new user.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully created new user.');
    }

    /**
     * Update the status of the user.
     * @param string $userId
     * @param string $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(string $userId, string $status)
    {
        try {
            $this->userService->changeStatus($userId, $status);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to update the status.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully changed the status of the user.');
    }

    /**
     * Display the details of the user.
     * @param string $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $userId)
    {
        $user          = $this->userService->findById($userId);
        $buttonDetails = getUserDetailsForDeleteButton($user);

        return view('admin.modules.users.show', compact('user', 'buttonDetails'));
    }

    /**
     * Load the form for editing the user details.
     * @param string $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $userId)
    {
        $user = $this->userService->findById($userId);

        return view('admin.modules.users.edit', compact('user'));
    }

    /**
     * Update the user.
     * @param string       $userId
     * @param UsersRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(string $userId, UsersRequest $request)
    {
        try {
            $this->userService->updateUser($request->all(), $userId);
        } catch (\Exception $exception) {
            logger()->error($exception);
            flash('Unable to update user.', 'error');

            return $this->sendError('Unable to update user.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully updated new user.');
    }

    /**
     * Displays the profile page of logged in user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        $user = currentUser();

        return view('admin.modules.users.profile.show', compact('user'));
    }

    /**
     * Changes password.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password'     => 'required|min:6|max:25|confirmed',
        ]);

        try {
            if ($this->userService->changePassword($request->all())) {
                return $this->sendSuccessResponse([], 'Password Changed.');
            } else {
                return $this->sendError('Your Old Password was Incorrect.', Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Something went wrong.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
