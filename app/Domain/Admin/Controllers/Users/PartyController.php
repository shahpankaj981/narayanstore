<?php

namespace App\Domain\Admin\Controllers\Users;

use App\Domain\Admin\Requests\PartyRequest;
use App\Domain\Admin\Services\Users\PartyService;
use App\StartUp\BaseClasses\Controller\AdminController;
use Illuminate\Http\Response;

/**
 * Class PartyController
 * @package App\Http\Controllers\Admin
 */
class PartyController extends AdminController
{
    /**
     * @var PartyService
     */
    protected $partyService;

    /**
     * PartyController constructor.
     * @param PartyService $partyService
     */
    public function __construct(PartyService $partyService)
    {
        $this->partyService = $partyService;
    }

    public function index()
    {
        return view('admin.modules.party.index');
    }

    public function getPaginatedParties()
    {
        return $this->partyService->getPaginatedPartiesForTable(request()->all());
    }

    public function create()
    {
        return view('admin.modules.party.create');
    }

    public function store(PartyRequest $request)
    {
        try {
            $this->partyService->store($request->all());
            flash('Client Created Successfully')->success();
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            flash('Something went wrong')->error();

            return $this->sendError('Unable to create new client.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully created new client.');
    }

    public function edit($id)
    {
        $party = $this->partyService->findById((int) $id);

        return view('admin.modules.party.edit', compact('party'));
    }

    public function update($id, PartyRequest $request)
    {
        try {
            $this->partyService->update($request->all(), (int) $id);
        } catch (\Exception $exception) {
            logger()->error($exception);
            flash('Unable to update client.', 'error');

            return $this->sendError('Unable to update client.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully updated new client.');
    }

    public function show($id)
    {
        $party = $this->partyService->findById((int) $id);
        $buttonDetails = [
            'change_action'   => 'Delete',
            'title'           => 'Delete Client',
            'success_message' => 'Successfully activated client.',
        ];

        return view('admin.modules.party.show', compact('party', 'buttonDetails'));
    }

    public function destroy($id)
    {
        try {
            $this->partyService->destroy((int)$id);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to delete the client', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendSuccessResponse([], 'Successfully deleted the client.');
    }
}