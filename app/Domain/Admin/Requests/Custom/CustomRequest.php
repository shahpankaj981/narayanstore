<?php

namespace App\Domain\Admin\Requests\Custom;

use App\StartUp\Contracts\IFormRequest;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CustomRequest
 * @package App\Domain\Admin\Requests\Custom
 */
class CustomRequest extends FormRequest implements IFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'custom_ref_no'    => 'required',
            'custom_amount'    => 'required',
            'clearance_charge' => 'required',
            'clearance_date'   => 'nullable|date',
            'bale_quantity'    => 'nullable|integer|min:0',
            'invoices'         => 'required|array',
            'parties'          => 'array',
        ];
    }
}
