<?php

namespace App\Domain\Admin\Requests\User;

use App\Constants\DBTable;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UsersRequest
 * @package App\Domain\Admin\Requests\User
 */
class UsersRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $userId = request()->segment(2) ?? '1';

        return [
            'full_name.first_name' => 'required|min:3|max:255',
            'full_name.last_name'  => 'required|min:3|max:255',
            'email'                => 'required|unique:'.DBTable::AUTH_USERS.',email,'.$userId,
            'username'             => 'required|unique:'.DBTable::AUTH_USERS.',username,'.$userId,
        ];
    }

    public function attributes(): array
    {
        return [
            'full_name.first_name' => 'First Name',
            'full_name.last_name'  => "Last Name",
        ];
    }
}