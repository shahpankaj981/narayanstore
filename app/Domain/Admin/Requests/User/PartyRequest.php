<?php

namespace App\Domain\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PartyRequest
 * @package App\Domain\Admin\Requests
 */
class PartyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            => 'required|min:3|max:255|unique:parties,name,'.request('party').',id',
            'address.street'  => 'required|min:3|max:255',
            'address.city'    => 'required|min:3|max:255',
            'address.country' => 'required|min:3|max:255',
            'address.zip'     => 'required|min:3|max:255',
            'mobile'          => 'nullable|min:3|max:25',
            'phone'           => 'nullable|min:3|max:25',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'address.street'  => 'street',
            'address.city'    => 'city',
            'address.country' => 'country',
            'address.zip'     => 'zip',
        ];
    }
}
