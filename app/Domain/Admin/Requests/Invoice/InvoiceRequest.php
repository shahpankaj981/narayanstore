<?php

namespace App\Domain\Admin\Requests\Invoice;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice.invoice_number'      => 'required',
            'invoice.invoice_date'        => 'required',
            'invoice.party_id'            => 'required',
            'items'               => 'required|array',
            'items.*.description' => 'required',
            'items.*.product_type' => 'required',
            'items.*.rate'        => 'required',
            'items.*.quantity'    => 'required',
//            'agent_name'      => 'required',
//            'agent_mobile'    => 'required',
//            'agent_charge'    => 'required',
//            'lr_number'       => 'required',
//            'transport_name'  => 'required',
//            'dispatched_date' => 'required',
//            'received_date'   => 'required',
//            'dispatched_from' => 'required',
//            'received_at'     => 'required',
//            'discount_rate'   => 'required',
//            'gst_rate'        => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'items.*.bale_number.required' => 'Required',
            'items.*.description.required' => 'Required',
            'items.*.hsn_code.required'    => 'Required',
            'items.*.rate.required'        => 'Required',
            'items.*.quantity.required'    => 'Required',
            'items.required'               => 'At least one invoice record is required.',
        ];
    }
}
