webpackJsonp([3],{

/***/ "./resources/assets/domain/common/theme/js/demo/default/custom/components/forms/widgets/form-repeater.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, jQuery) {

//== Class definition
var FormRepeater = function () {

    //== Private functions
    var demo1 = function demo1() {
        $('#m_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function show() {
                $(this).slideDown();
            },

            hide: function hide(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var demo2 = function demo2() {
        $('#m_repeater_2').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function show() {
                $(this).slideDown();
            },

            hide: function hide(deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    };

    var demo3 = function demo3() {
        $('#m_repeater_3').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function show() {
                $(this).slideDown();
            },

            hide: function hide(deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    };

    var demo4 = function demo4() {
        $('#m_repeater_4').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function show() {
                $(this).slideDown();
            },

            hide: function hide(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var demo5 = function demo5() {
        $('#m_repeater_5').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function show() {
                $(this).slideDown();
            },

            hide: function hide(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var demo6 = function demo6() {
        $('#m_repeater_6').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function show() {
                $(this).slideDown();
            },

            hide: function hide(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };
    return {
        // public functions
        init: function init() {
            demo1();
            demo2();
            demo3();
            demo4();
            demo5();
            demo6();
        }
    };
}();

jQuery(document).ready(function () {
    FormRepeater.init();
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js"), __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/domain/common/theme/js/demo/default/custom/components/forms/widgets/form-repeater.js");


/***/ })

},[2]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZGVtby9kZWZhdWx0L2N1c3RvbS9jb21wb25lbnRzL2Zvcm1zL3dpZGdldHMvZm9ybS1yZXBlYXRlci5qcyJdLCJuYW1lcyI6WyJGb3JtUmVwZWF0ZXIiLCJkZW1vMSIsIiQiLCJyZXBlYXRlciIsImluaXRFbXB0eSIsImRlZmF1bHRWYWx1ZXMiLCJzaG93Iiwic2xpZGVEb3duIiwiaGlkZSIsImRlbGV0ZUVsZW1lbnQiLCJzbGlkZVVwIiwiZGVtbzIiLCJjb25maXJtIiwiZGVtbzMiLCJkZW1vNCIsImRlbW81IiwiZGVtbzYiLCJpbml0IiwialF1ZXJ5IiwiZG9jdW1lbnQiLCJyZWFkeSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBLElBQUlBLGVBQWUsWUFBVzs7QUFFMUI7QUFDQSxRQUFJQyxRQUFRLFNBQVJBLEtBQVEsR0FBVztBQUNuQkMsVUFBRSxlQUFGLEVBQW1CQyxRQUFuQixDQUE0QjtBQUN4QkMsdUJBQVcsS0FEYTs7QUFHeEJDLDJCQUFlO0FBQ1gsOEJBQWM7QUFESCxhQUhTOztBQU94QkMsa0JBQU0sZ0JBQVk7QUFDZEosa0JBQUUsSUFBRixFQUFRSyxTQUFSO0FBQ0gsYUFUdUI7O0FBV3hCQyxrQkFBTSxjQUFVQyxhQUFWLEVBQXlCO0FBQzNCUCxrQkFBRSxJQUFGLEVBQVFRLE9BQVIsQ0FBZ0JELGFBQWhCO0FBQ0g7QUFidUIsU0FBNUI7QUFlSCxLQWhCRDs7QUFrQkEsUUFBSUUsUUFBUSxTQUFSQSxLQUFRLEdBQVc7QUFDbkJULFVBQUUsZUFBRixFQUFtQkMsUUFBbkIsQ0FBNEI7QUFDeEJDLHVCQUFXLEtBRGE7O0FBR3hCQywyQkFBZTtBQUNYLDhCQUFjO0FBREgsYUFIUzs7QUFPeEJDLGtCQUFNLGdCQUFXO0FBQ2JKLGtCQUFFLElBQUYsRUFBUUssU0FBUjtBQUNILGFBVHVCOztBQVd4QkMsa0JBQU0sY0FBU0MsYUFBVCxFQUF3QjtBQUMxQixvQkFBR0csUUFBUSwrQ0FBUixDQUFILEVBQTZEO0FBQ3pEVixzQkFBRSxJQUFGLEVBQVFRLE9BQVIsQ0FBZ0JELGFBQWhCO0FBQ0g7QUFDSjtBQWZ1QixTQUE1QjtBQWlCSCxLQWxCRDs7QUFxQkEsUUFBSUksUUFBUSxTQUFSQSxLQUFRLEdBQVc7QUFDbkJYLFVBQUUsZUFBRixFQUFtQkMsUUFBbkIsQ0FBNEI7QUFDeEJDLHVCQUFXLEtBRGE7O0FBR3hCQywyQkFBZTtBQUNYLDhCQUFjO0FBREgsYUFIUzs7QUFPeEJDLGtCQUFNLGdCQUFXO0FBQ2JKLGtCQUFFLElBQUYsRUFBUUssU0FBUjtBQUNILGFBVHVCOztBQVd4QkMsa0JBQU0sY0FBU0MsYUFBVCxFQUF3QjtBQUMxQixvQkFBR0csUUFBUSwrQ0FBUixDQUFILEVBQTZEO0FBQ3pEVixzQkFBRSxJQUFGLEVBQVFRLE9BQVIsQ0FBZ0JELGFBQWhCO0FBQ0g7QUFDSjtBQWZ1QixTQUE1QjtBQWlCSCxLQWxCRDs7QUFvQkEsUUFBSUssUUFBUSxTQUFSQSxLQUFRLEdBQVc7QUFDbkJaLFVBQUUsZUFBRixFQUFtQkMsUUFBbkIsQ0FBNEI7QUFDeEJDLHVCQUFXLEtBRGE7O0FBR3hCQywyQkFBZTtBQUNYLDhCQUFjO0FBREgsYUFIUzs7QUFPeEJDLGtCQUFNLGdCQUFXO0FBQ2JKLGtCQUFFLElBQUYsRUFBUUssU0FBUjtBQUNILGFBVHVCOztBQVd4QkMsa0JBQU0sY0FBU0MsYUFBVCxFQUF3QjtBQUMxQlAsa0JBQUUsSUFBRixFQUFRUSxPQUFSLENBQWdCRCxhQUFoQjtBQUNIO0FBYnVCLFNBQTVCO0FBZUgsS0FoQkQ7O0FBa0JBLFFBQUlNLFFBQVEsU0FBUkEsS0FBUSxHQUFXO0FBQ25CYixVQUFFLGVBQUYsRUFBbUJDLFFBQW5CLENBQTRCO0FBQ3hCQyx1QkFBVyxLQURhOztBQUd4QkMsMkJBQWU7QUFDWCw4QkFBYztBQURILGFBSFM7O0FBT3hCQyxrQkFBTSxnQkFBVztBQUNiSixrQkFBRSxJQUFGLEVBQVFLLFNBQVI7QUFDSCxhQVR1Qjs7QUFXeEJDLGtCQUFNLGNBQVNDLGFBQVQsRUFBd0I7QUFDMUJQLGtCQUFFLElBQUYsRUFBUVEsT0FBUixDQUFnQkQsYUFBaEI7QUFDSDtBQWJ1QixTQUE1QjtBQWVILEtBaEJEOztBQWtCQSxRQUFJTyxRQUFRLFNBQVJBLEtBQVEsR0FBVztBQUNuQmQsVUFBRSxlQUFGLEVBQW1CQyxRQUFuQixDQUE0QjtBQUN4QkMsdUJBQVcsS0FEYTs7QUFHeEJDLDJCQUFlO0FBQ1gsOEJBQWM7QUFESCxhQUhTOztBQU94QkMsa0JBQU0sZ0JBQVc7QUFDYkosa0JBQUUsSUFBRixFQUFRSyxTQUFSO0FBQ0gsYUFUdUI7O0FBV3hCQyxrQkFBTSxjQUFTQyxhQUFULEVBQXdCO0FBQzFCUCxrQkFBRSxJQUFGLEVBQVFRLE9BQVIsQ0FBZ0JELGFBQWhCO0FBQ0g7QUFidUIsU0FBNUI7QUFlSCxLQWhCRDtBQWlCQSxXQUFPO0FBQ0g7QUFDQVEsY0FBTSxnQkFBVztBQUNiaEI7QUFDQVU7QUFDQUU7QUFDQUM7QUFDQUM7QUFDQUM7QUFDSDtBQVRFLEtBQVA7QUFXSCxDQTlIa0IsRUFBbkI7O0FBZ0lBRSxPQUFPQyxRQUFQLEVBQWlCQyxLQUFqQixDQUF1QixZQUFXO0FBQzlCcEIsaUJBQWFpQixJQUFiO0FBQ0gsQ0FGRCxFIiwiZmlsZSI6Ii9qcy9mb3JtLXJlcGVhdGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy89PSBDbGFzcyBkZWZpbml0aW9uXG52YXIgRm9ybVJlcGVhdGVyID0gZnVuY3Rpb24oKSB7XG5cbiAgICAvLz09IFByaXZhdGUgZnVuY3Rpb25zXG4gICAgdmFyIGRlbW8xID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJyNtX3JlcGVhdGVyXzEnKS5yZXBlYXRlcih7ICAgICAgICAgICAgXG4gICAgICAgICAgICBpbml0RW1wdHk6IGZhbHNlLFxuICAgICAgICAgICBcbiAgICAgICAgICAgIGRlZmF1bHRWYWx1ZXM6IHtcbiAgICAgICAgICAgICAgICAndGV4dC1pbnB1dCc6ICdmb28nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgIFxuICAgICAgICAgICAgc2hvdzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICQodGhpcykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBoaWRlOiBmdW5jdGlvbiAoZGVsZXRlRWxlbWVudCkgeyAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnNsaWRlVXAoZGVsZXRlRWxlbWVudCk7ICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0gICBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdmFyIGRlbW8yID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJyNtX3JlcGVhdGVyXzInKS5yZXBlYXRlcih7ICAgICAgICAgICAgXG4gICAgICAgICAgICBpbml0RW1wdHk6IGZhbHNlLFxuICAgICAgICAgICBcbiAgICAgICAgICAgIGRlZmF1bHRWYWx1ZXM6IHtcbiAgICAgICAgICAgICAgICAndGV4dC1pbnB1dCc6ICdmb28nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgIFxuICAgICAgICAgICAgc2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5zbGlkZURvd24oKTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBoaWRlOiBmdW5jdGlvbihkZWxldGVFbGVtZW50KSB7ICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZihjb25maXJtKCdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgZWxlbWVudD8nKSkge1xuICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnNsaWRlVXAoZGVsZXRlRWxlbWVudCk7XG4gICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9ICAgICAgXG4gICAgICAgIH0pO1xuICAgIH1cblxuXG4gICAgdmFyIGRlbW8zID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJyNtX3JlcGVhdGVyXzMnKS5yZXBlYXRlcih7ICAgICAgICAgICAgXG4gICAgICAgICAgICBpbml0RW1wdHk6IGZhbHNlLFxuICAgICAgICAgICBcbiAgICAgICAgICAgIGRlZmF1bHRWYWx1ZXM6IHtcbiAgICAgICAgICAgICAgICAndGV4dC1pbnB1dCc6ICdmb28nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgIFxuICAgICAgICAgICAgc2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5zbGlkZURvd24oKTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBoaWRlOiBmdW5jdGlvbihkZWxldGVFbGVtZW50KSB7ICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZihjb25maXJtKCdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgZWxlbWVudD8nKSkge1xuICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnNsaWRlVXAoZGVsZXRlRWxlbWVudCk7XG4gICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0gICAgICBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdmFyIGRlbW80ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJyNtX3JlcGVhdGVyXzQnKS5yZXBlYXRlcih7ICAgICAgICAgICAgXG4gICAgICAgICAgICBpbml0RW1wdHk6IGZhbHNlLFxuICAgICAgICAgICBcbiAgICAgICAgICAgIGRlZmF1bHRWYWx1ZXM6IHtcbiAgICAgICAgICAgICAgICAndGV4dC1pbnB1dCc6ICdmb28nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgIFxuICAgICAgICAgICAgc2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5zbGlkZURvd24oKTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBoaWRlOiBmdW5jdGlvbihkZWxldGVFbGVtZW50KSB7ICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnNsaWRlVXAoZGVsZXRlRWxlbWVudCk7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0gICAgICBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdmFyIGRlbW81ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJyNtX3JlcGVhdGVyXzUnKS5yZXBlYXRlcih7ICAgICAgICAgICAgXG4gICAgICAgICAgICBpbml0RW1wdHk6IGZhbHNlLFxuICAgICAgICAgICBcbiAgICAgICAgICAgIGRlZmF1bHRWYWx1ZXM6IHtcbiAgICAgICAgICAgICAgICAndGV4dC1pbnB1dCc6ICdmb28nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgIFxuICAgICAgICAgICAgc2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5zbGlkZURvd24oKTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBoaWRlOiBmdW5jdGlvbihkZWxldGVFbGVtZW50KSB7ICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnNsaWRlVXAoZGVsZXRlRWxlbWVudCk7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0gICAgICBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdmFyIGRlbW82ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJyNtX3JlcGVhdGVyXzYnKS5yZXBlYXRlcih7ICAgICAgICAgICAgXG4gICAgICAgICAgICBpbml0RW1wdHk6IGZhbHNlLFxuICAgICAgICAgICBcbiAgICAgICAgICAgIGRlZmF1bHRWYWx1ZXM6IHtcbiAgICAgICAgICAgICAgICAndGV4dC1pbnB1dCc6ICdmb28nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgIFxuICAgICAgICAgICAgc2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5zbGlkZURvd24oKTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBoaWRlOiBmdW5jdGlvbihkZWxldGVFbGVtZW50KSB7ICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5zbGlkZVVwKGRlbGV0ZUVsZW1lbnQpOyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSAgICAgIFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgICAgLy8gcHVibGljIGZ1bmN0aW9uc1xuICAgICAgICBpbml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGRlbW8xKCk7XG4gICAgICAgICAgICBkZW1vMigpO1xuICAgICAgICAgICAgZGVtbzMoKTtcbiAgICAgICAgICAgIGRlbW80KCk7XG4gICAgICAgICAgICBkZW1vNSgpO1xuICAgICAgICAgICAgZGVtbzYoKTtcbiAgICAgICAgfVxuICAgIH07XG59KCk7XG5cbmpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgRm9ybVJlcGVhdGVyLmluaXQoKTtcbn0pO1xuXG4gICAgXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2RlbW8vZGVmYXVsdC9jdXN0b20vY29tcG9uZW50cy9mb3Jtcy93aWRnZXRzL2Zvcm0tcmVwZWF0ZXIuanMiXSwic291cmNlUm9vdCI6IiJ9