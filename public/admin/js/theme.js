webpackJsonp([1],{

/***/ "./resources/assets/domain/admin/js/theme.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

var _util = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/util.js");

var _app = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/app.js");

var _layout = __webpack_require__("./resources/assets/domain/common/theme/js/demo/default/base/layout.js");

var _quickSidebar = __webpack_require__("./resources/assets/domain/common/theme/js/snippets/base/quick-sidebar.js");

// require("./../../common/theme/js/framework/components/general/animate");
__webpack_require__("./resources/assets/domain/common/theme/js/framework/components/general/dropdown.js");
// require("./../../common/theme/js/framework/components/general/example");
__webpack_require__("./resources/assets/domain/common/theme/js/framework/components/general/header.js");
__webpack_require__("./resources/assets/domain/common/theme/js/framework/components/general/menu.js");
// require("./../../common/theme/js/framework/components/general/messenger");
__webpack_require__("./resources/assets/domain/common/theme/js/framework/components/general/offcanvas.js");
// require("./../../common/theme/js/framework/components/general/portlet");
__webpack_require__("./resources/assets/domain/common/theme/js/framework/components/general/quicksearch.js");
__webpack_require__("./resources/assets/domain/common/theme/js/framework/components/general/scroll-top.js");
__webpack_require__("./resources/assets/domain/common/theme/js/framework/components/general/toggle.js");
// require("./../../common/theme/js/framework/components/general/wizard");

$(document).ready(function () {
    _util.mUtil.init();
    _app.mApp.init();
    _layout.mLayout.init();
    _quickSidebar.mQuickSidebar.init();
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/admin/sass/app.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/domain/admin/sass/vendor.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/demo/default/base/layout.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
var mLayout = exports.mLayout = function () {
    var horMenu;
    var asideMenu;
    var asideMenuOffcanvas;
    var horMenuOffcanvas;

    var initStickyHeader = function initStickyHeader() {
        var header = $('.m-header');
        var options = {
            offset: {},
            minimize: {}
        };

        if (header.data('minimize-mobile') == 'hide') {
            options.minimize.mobile = {};
            options.minimize.mobile.on = 'm-header--hide';
            options.minimize.mobile.off = 'm-header--show';
        } else {
            options.minimize.mobile = false;
        }

        if (header.data('minimize') == 'hide') {
            options.minimize.desktop = {};
            options.minimize.desktop.on = 'm-header--hide';
            options.minimize.desktop.off = 'm-header--show';
        } else {
            options.minimize.desktop = false;
        }

        if (header.data('minimize-offset')) {
            options.offset.desktop = header.data('minimize-offset');
        }

        if (header.data('minimize-mobile-offset')) {
            options.offset.mobile = header.data('minimize-mobile-offset');
        }

        header.mHeader(options);
    };

    // handle horizontal menu
    var initHorMenu = function initHorMenu() {
        // init aside left offcanvas
        horMenuOffcanvas = $('#m_header_menu').mOffcanvas({
            class: 'm-aside-header-menu-mobile',
            overlay: true,
            close: '#m_aside_header_menu_mobile_close_btn',
            toggle: {
                target: '#m_aside_header_menu_mobile_toggle',
                state: 'm-brand__toggler--active'
            }
        });

        horMenu = $('#m_header_menu').mMenu({
            // submenu modes
            submenu: {
                desktop: 'dropdown',
                tablet: 'accordion',
                mobile: 'accordion'
            },
            // resize menu on window resize
            resize: {
                desktop: function desktop() {
                    var headerNavWidth = $('#m_header_nav').width();
                    var headerMenuWidth = $('#m_header_menu_container').width();
                    var headerTopbarWidth = $('#m_header_topbar').width();
                    var spareWidth = 20;

                    if (headerMenuWidth + headerTopbarWidth + spareWidth > headerNavWidth) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        });
    };

    // handle vertical menu
    var initLeftAsideMenu = function initLeftAsideMenu() {
        var menu = $('#m_ver_menu');

        // init aside menu
        var menuOptions = {
            // submenu setup
            submenu: {
                desktop: {
                    // by default the menu mode set to accordion in desktop mode
                    default: menu.data('menu-dropdown') == true ? 'dropdown' : 'accordion',
                    // whenever body has this class switch the menu mode to dropdown
                    state: {
                        body: 'm-aside-left--minimize',
                        mode: 'dropdown'
                    }
                },
                tablet: 'accordion', // menu set to accordion in tablet mode
                mobile: 'accordion' // menu set to accordion in mobile mode
            },

            //accordion setup
            accordion: {
                autoScroll: true,
                expandAll: false
            }
        };

        asideMenu = menu.mMenu(menuOptions);

        // handle fixed aside menu
        if (menu.data('menu-scrollable')) {
            var initScrollableMenu = function initScrollableMenu(obj) {
                if (mUtil.isInResponsiveRange('tablet-and-mobile')) {
                    // destroy if the instance was previously created
                    mApp.destroyScroller(obj);
                    return;
                }

                var height = mUtil.getViewPort().height - $('.m-header').outerHeight() - ($('.m-aside-left .m-aside__header').length != 0 ? $('.m-aside-left .m-aside__header').outerHeight() : 0) - ($('.m-aside-left .m-aside__footer').length != 0 ? $('.m-aside-left .m-aside__footer').outerHeight() : 0);
                //- $('.m-footer').outerHeight(); 

                // create/re-create a new instance
                mApp.initScroller(obj, { height: height });
            };

            initScrollableMenu(asideMenu);

            mUtil.addResizeHandler(function () {
                initScrollableMenu(asideMenu);
            });
        }
    };

    // handle vertical menu
    var initLeftAside = function initLeftAside() {
        // init aside left offcanvas
        var asideOffcanvasClass = $('#m_aside_left').hasClass('m-aside-left--offcanvas-default') ? 'm-aside-left--offcanvas-default' : 'm-aside-left';

        asideMenuOffcanvas = $('#m_aside_left').mOffcanvas({
            class: asideOffcanvasClass,
            overlay: true,
            close: '#m_aside_left_close_btn',
            toggle: {
                target: '#m_aside_left_offcanvas_toggle',
                state: 'm-brand__toggler--active'
            }
        });
    };

    // handle sidebar toggle
    var initLeftAsideToggle = function initLeftAsideToggle() {
        var asideLeftToggle = $('#m_aside_left_minimize_toggle').mToggle({
            target: 'body',
            targetState: 'm-brand--minimize m-aside-left--minimize',
            togglerState: 'm-brand__toggler--active'
        }).on('toggle', function () {
            horMenu.pauseDropdownHover(800);
            asideMenu.pauseDropdownHover(800);
        });

        //== Example: minimize the left aside on page load
        //== asideLeftToggle.toggleOn();

        $('#m_aside_left_hide_toggle').mToggle({
            target: 'body',
            targetState: 'm-aside-left--hide',
            togglerState: 'm-brand__toggler--active'
        }).on('toggle', function () {
            horMenu.pauseDropdownHover(800);
            asideMenu.pauseDropdownHover(800);
        });
    };

    var initTopbar = function initTopbar() {
        $('#m_aside_header_topbar_mobile_toggle').click(function () {
            $('body').toggleClass('m-topbar--on');
        });

        // Animated Notification Icon 
        setInterval(function () {
            $('#m_topbar_notification_icon .m-nav__link-icon').addClass('m-animate-shake');
            $('#m_topbar_notification_icon .m-nav__link-badge').addClass('m-animate-blink');
        }, 3000);

        setInterval(function () {
            $('#m_topbar_notification_icon .m-nav__link-icon').removeClass('m-animate-shake');
            $('#m_topbar_notification_icon .m-nav__link-badge').removeClass('m-animate-blink');
        }, 6000);
    };

    // handle quick search
    var initQuicksearch = function initQuicksearch() {
        var qs = $('#m_quicksearch');

        qs.mQuicksearch({
            type: qs.data('search-type'), // quick search type
            source: 'inc/api/quick_search.php',
            spinner: 'm-loader m-loader--skin-light m-loader--right',

            input: '#m_quicksearch_input',
            iconClose: '#m_quicksearch_close',
            iconCancel: '#m_quicksearch_cancel',
            iconSearch: '#m_quicksearch_search',

            hasResultClass: 'm-list-search--has-result',
            minLength: 1,
            templates: {
                error: function error(qs) {
                    return '<div class="m-search-results m-search-results--skin-light"><span class="m-search-result__message">Something went wrong</div></div>';
                }
            }
        });
    };

    var initScrollTop = function initScrollTop() {
        $('[data-toggle="m-scroll-top"]').mScrollTop({
            offset: 300,
            speed: 600
        });
    };

    return {
        init: function init() {
            this.initHeader();
            this.initAside();
        },

        initHeader: function initHeader() {
            initStickyHeader();
            initHorMenu();
            initTopbar();
            initQuicksearch();
            initScrollTop();
        },

        initAside: function initAside() {
            initLeftAside();
            initLeftAsideMenu();
            initLeftAsideToggle();

            this.onLeftSidebarToggle(function (e) {
                var datatables = $('.m-datatable');
                $(datatables).each(function () {
                    $(this).mDatatable('redraw');
                });
            });
        },

        getAsideMenu: function getAsideMenu() {
            return asideMenu;
        },

        onLeftSidebarToggle: function onLeftSidebarToggle(func) {
            $('#m_aside_left_minimize_toggle').mToggle().on('toggle', func);
        },

        closeMobileAsideMenuOffcanvas: function closeMobileAsideMenuOffcanvas() {
            if (mUtil.isMobileDevice()) {
                asideMenuOffcanvas.hide();
            }
        },

        closeMobileHorMenuOffcanvas: function closeMobileHorMenuOffcanvas() {
            if (mUtil.isMobileDevice()) {
                horMenuOffcanvas.hide();
            }
        }
    };
}();

// $(document).ready(function() {
//     if (mUtil.isAngularVersion() === false) {
//         mLayout.init();
//     }
// });
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/base/app.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.mApp = undefined;

var _util = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/util.js");

var mApp = exports.mApp = function () {

    /**
     * Initializes bootstrap tooltip
     */
    var _initTooltip = function _initTooltip(el) {
        var skin = el.data("skin") ? "m-tooltip--skin-" + el.data("skin") : "";
        var width = el.data("width") == "auto" ? "m-tooltop--auto-width" : "";

        el.tooltip({
            trigger: "hover",
            template: "<div class=\"m-tooltip " + skin + " " + width + " tooltip\" role=\"tooltip\">\
                <div class=\"arrow\"></div>\
                <div class=\"tooltip-inner\"></div>\
            </div>"
        });
    };

    /**
     * Initializes bootstrap tooltips
     */
    var _initTooltips = function _initTooltips() {
        // init bootstrap tooltips
        $("[data-toggle=\"m-tooltip\"]").each(function () {
            _initTooltip($(this));
        });
    };

    /**
     * Initializes bootstrap popover
     */
    var _initPopover = function _initPopover(el) {
        var skin = el.data("skin") ? "m-popover--skin-" + el.data("skin") : "";
        var triggerValue = el.data("trigger") ? el.data("trigger") : "hover";

        el.popover({
            trigger: triggerValue,
            template: "\
            <div class=\"m-popover " + skin + " popover\" role=\"tooltip\">\
                <div class=\"arrow\"></div>\
                <h3 class=\"popover-header\"></h3>\
                <div class=\"popover-body\"></div>\
            </div>"
        });
    };

    /**
     * Initializes bootstrap popovers
     */
    var _initPopovers = function _initPopovers() {
        // init bootstrap popover
        $("[data-toggle=\"m-popover\"]").each(function () {
            _initPopover($(this));
        });
    };

    /**
     * Initializes bootstrap file input
     */
    var initFileInput = function initFileInput() {
        // init bootstrap popover
        $(".custom-file-input").on("change", function () {
            var fileName = $(this).val();
            $(this).next(".custom-file-control").addClass("selected").html(fileName);
        });
    };

    /**
     * Initializes metronic portlet
     */
    var _initPortlet = function _initPortlet(el, options) {
        // init portlet tools
        el.mPortlet(options);
    };

    /**
     * Initializes metronic portlets
     */
    var _initPortlets = function _initPortlets() {
        // init portlet tools
        $("[data-portlet=\"true\"]").each(function () {
            var el = $(this);

            if (el.data("portlet-initialized") !== true) {
                _initPortlet(el, {});
                el.data("portlet-initialized", true);
            }
        });
    };

    /**
     * Initializes scrollable contents
     */
    var initScrollables = function initScrollables() {
        $("[data-scrollable=\"true\"]").each(function () {
            var maxHeight;
            var height;
            var el = $(this);

            if (_util.mUtil.isInResponsiveRange("tablet-and-mobile")) {
                if (el.data("mobile-max-height")) {
                    maxHeight = el.data("mobile-max-height");
                } else {
                    maxHeight = el.data("max-height");
                }

                if (el.data("mobile-height")) {
                    height = el.data("mobile-height");
                } else {
                    height = el.data("height");
                }
            } else {
                maxHeight = el.data("max-height");
                height = el.data("max-height");
            }

            if (maxHeight) {
                el.css("max-height", maxHeight);
            }
            if (height) {
                el.css("height", height);
            }

            mApp.initScroller(el, {});
        });
    };

    /**
     * Initializes bootstrap alerts
     */
    var initAlerts = function initAlerts() {
        // init bootstrap popover
        $("body").on("click", "[data-close=alert]", function () {
            $(this).closest(".alert").hide();
        });
    };

    /**
     * Initializes bootstrap collapse for Metronic's accordion feature
     */
    var initAccordions = function initAccordions(el) {};

    var hideTouchWarning = function hideTouchWarning() {
        jQuery.event.special.touchstart = {
            setup: function setup(_, ns, handle) {
                if (typeof this === "function") if (ns.includes("noPreventDefault")) {
                    this.addEventListener("touchstart", handle, { passive: false });
                } else {
                    this.addEventListener("touchstart", handle, { passive: true });
                }
            }
        };
        jQuery.event.special.touchmove = {
            setup: function setup(_, ns, handle) {
                if (typeof this === "function") if (ns.includes("noPreventDefault")) {
                    this.addEventListener("touchmove", handle, { passive: false });
                } else {
                    this.addEventListener("touchmove", handle, { passive: true });
                }
            }
        };
        jQuery.event.special.wheel = {
            setup: function setup(_, ns, handle) {
                if (typeof this === "function") if (ns.includes("noPreventDefault")) {
                    this.addEventListener("wheel", handle, { passive: false });
                } else {
                    this.addEventListener("wheel", handle, { passive: true });
                }
            }
        };
    };

    return {
        /**
         * Main class initializer
         */
        init: function init() {
            mApp.initComponents();
        },

        /**
         * Initializes components
         */
        initComponents: function initComponents() {
            hideTouchWarning();
            initScrollables();
            _initTooltips();
            _initPopovers();
            initAlerts();
            _initPortlets();
            initFileInput();
            initAccordions();
        },

        /**
         *
         * @param {object} el jQuery element object
         */
        // wrJangoer function to scroll(focus) to an element
        initTooltips: function initTooltips() {
            _initTooltips();
        },

        /**
         *
         * @param {object} el jQuery element object
         */
        // wrJangoer function to scroll(focus) to an element
        initTooltip: function initTooltip(el) {
            _initTooltip(el);
        },

        /**
         *
         * @param {object} el jQuery element object
         */
        // wrJangoer function to scroll(focus) to an element
        initPopovers: function initPopovers() {
            _initPopovers();
        },

        /**
         *
         * @param {object} el jQuery element object
         */
        // wrJangoer function to scroll(focus) to an element
        initPopover: function initPopover(el) {
            _initPopover(el);
        },

        /**
         *
         * @param {object} el jQuery element object
         */
        // function to init portlet
        initPortlet: function initPortlet(el, options) {
            _initPortlet(el, options);
        },

        /**
         *
         * @param {object} el jQuery element object
         */
        // function to init portlets
        initPortlets: function initPortlets() {
            _initPortlets();
        },

        /**
         * Scrolls to an element with animation
         * @param {object} el jQuery element object
         * @param {number} offset Offset to element scroll position
         */
        scrollTo: function scrollTo(el, offset) {
            var pos = el && el.length > 0 ? el.offset().top : 0;
            pos = pos + (offset ? offset : 0);

            jQuery("html,body").animate({
                scrollTop: pos
            }, "slow");
        },

        /**
         * Scrolls until element is centered in the viewport
         * @param {object} el jQuery element object
         */
        // wrJangoer function to scroll(focus) to an element
        scrollToViewport: function scrollToViewport(el) {
            var elOffset = el.offset().top;
            var elHeight = el.height();
            var windowHeight = _util.mUtil.getViewPort().height;
            var offset = elOffset - (windowHeight / 2 - elHeight / 2);

            jQuery("html,body").animate({
                scrollTop: offset
            }, "slow");
        },

        /**
         * Scrolls to the top of the page
         */
        // function to scroll to the top
        scrollTop: function scrollTop() {
            mApp.scrollTo();
        },

        /**
         * Initializes scrollable content using mCustomScrollbar plugin
         * @param {object} el jQuery element object
         * @param {object} options mCustomScrollbar plugin options(refer: http://manos.malihu.gr/jquery-custom-content-scroller/)
         */
        initScroller: function initScroller(el, options) {
            if (_util.mUtil.isMobileDevice()) {
                el.css("overflow", "auto");
            } else {
                //el.mCustomScrollbar("destroy");
                el.mCustomScrollbar({
                    scrollInertia: 0,
                    autoDraggerLength: true,
                    autoHideScrollbar: true,
                    autoExpandScrollbar: false,
                    alwaysShowScrollbar: 0,
                    axis: el.data("axis") ? el.data("axis") : "y",
                    mouseWheel: {
                        scrollAmount: 120,
                        preventDefault: true
                    },
                    setHeight: options.height ? options.height : "",
                    theme: "minimal-dark"
                });
            }
        },

        /**
         * Destroys scrollable content's mCustomScrollbar plugin instance
         * @param {object} el jQuery element object
         */
        destroyScroller: function destroyScroller(el) {
            el.mCustomScrollbar("destroy");
        },

        /**
         * Shows bootstrap alert
         * @param {object} options
         * @returns {string} ID attribute of the created alert
         */
        alert: function alert(options) {
            options = $.extend(true, {
                container: "", // alerts parent container(by default placed after the page breadcrumbs)
                place: "append", // "append" or "prepend" in container 
                type: "success", // alert's type
                message: "", // alert's message
                close: true, // make alert closable
                reset: true, // close all previouse alerts first
                focus: true, // auto scroll to the alert after shown
                closeInSeconds: 0, // auto close after defined seconds
                icon: "" // put icon before the message
            }, options);

            var id = _util.mUtil.getUniqueID("App_alert");

            var html = "<div id=\"" + id + "\" class=\"custom-alerts alert alert-" + options.type + " fade in\">" + (options.close ? "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"></button>" : "") + (options.icon !== "" ? "<i class=\"fa-lg fa fa-" + options.icon + "\"></i>  " : "") + options.message + "</div>";

            if (options.reset) {
                $(".custom-alerts").remove();
            }

            if (!options.container) {
                if ($(".page-fixed-main-content").size() === 1) {
                    $(".page-fixed-main-content").prepend(html);
                } else if (($("body").hasClass("page-container-bg-solid") || $("body").hasClass("page-content-white")) && $(".page-head").size() === 0) {
                    $(".page-title").after(html);
                } else {
                    if ($(".page-bar").size() > 0) {
                        $(".page-bar").after(html);
                    } else {
                        $(".page-breadcrumb, .breadcrumbs").after(html);
                    }
                }
            } else {
                if (options.place == "append") {
                    $(options.container).append(html);
                } else {
                    $(options.container).prepend(html);
                }
            }

            if (options.focus) {
                mApp.scrollTo($("#" + id));
            }

            if (options.closeInSeconds > 0) {
                setTimeout(function () {
                    $("#" + id).remove();
                }, options.closeInSeconds * 1000);
            }

            return id;
        },

        /**
         * Blocks element with loading indiciator using http://malsup.com/jquery/block/
         * @param {object} target jQuery element object
         * @param {object} options
         */
        block: function block(target, options) {
            var el = $(target);

            options = $.extend(true, {
                opacity: 0.03,
                overlayColor: "#000000",
                state: "brand",
                type: "loader",
                size: "lg",
                centerX: true,
                centerY: true,
                message: "",
                shadow: true,
                width: "auto"
            }, options);

            var skin;
            var state;
            var loading;

            if (options.type == "spinner") {
                skin = options.skin ? "m-spinner--skin-" + options.skin : "";
                state = options.state ? "m-spinner--" + options.state : "";
                loading = "<div class=\"m-spinner " + skin + " " + state + "\"></div";
            } else {
                skin = options.skin ? "m-loader--skin-" + options.skin : "";
                state = options.state ? "m-loader--" + options.state : "";
                size = options.size ? "m-loader--" + options.size : "";
                loading = "<div class=\"m-loader " + skin + " " + state + " " + size + "\"></div";
            }

            if (options.message && options.message.length > 0) {
                var classes = "m-blockui " + (options.shadow === false ? "m-blockui-no-shadow" : "");

                html = "<div class=\"" + classes + "\"><span>" + options.message + "</span><span>" + loading + "</span></div>";
                options.width = _util.mUtil.realWidth(html) + 10;
                if (target == "body") {
                    html = "<div class=\"" + classes + "\" style=\"margin-left:-" + options.width / 2 + "px;\"><span>" + options.message + "</span><span>" + loading + "</span></div>";
                }
            } else {
                html = loading;
            }

            var params = {
                message: html,
                centerY: options.centerY,
                centerX: options.centerX,
                css: {
                    top: "30%",
                    left: "50%",
                    border: "0",
                    padding: "0",
                    backgroundColor: "none",
                    width: options.width
                },
                overlayCSS: {
                    backgroundColor: options.overlayColor,
                    opacity: options.opacity,
                    cursor: "wait",
                    zIndex: "10"
                },
                onUnblock: function onUnblock() {
                    if (el) {
                        el.css("position", "");
                        el.css("zoom", "");
                    }
                }
            };

            if (target == "body") {
                params.css.top = "50%";
                $.blockUI(params);
            } else {
                var el = $(target);
                el.block(params);
            }
        },

        /**
         * Un-blocks the blocked element
         * @param {object} target jQuery element object
         */
        unblock: function unblock(target) {
            if (target && target != "body") {
                $(target).unblock();
            } else {
                $.unblockUI();
            }
        },

        /**
         * Blocks the page body element with loading indicator
         * @param {object} options
         */
        blockPage: function blockPage(options) {
            return mApp.block("body", options);
        },

        /**
         * Un-blocks the blocked page body element
         */
        unblockPage: function unblockPage() {
            return mApp.unblock("body");
        },

        /**
         * Enable loader progress for button and other elements
         * @param {object} target jQuery element object
         * @param {object} options
         */
        progress: function progress(target, options) {
            var skin = options && options.skin ? options.skin : "light";
            var alignment = options && options.alignment ? options.alignment : "right";
            var size = options && options.size ? "m-spinner--" + options.size : "";
            var classes = "m-loader " + "m-loader--" + skin + " m-loader--" + alignment + " m-loader--" + size;

            mApp.unprogress(target);

            $(target).addClass(classes);
            $(target).data("progress-classes", classes);
        },

        /**
         * Disable loader progress for button and other elements
         * @param {object} target jQuery element object
         */
        unprogress: function unprogress(target) {
            $(target).removeClass($(target).data("progress-classes"));
        }
    };
}();

//== Initialize mApp class on document ready
// $(document).ready(function() {
//     mApp.init();
// });
/**
 * @class mApp  Metronic App class
 */
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js"), __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/base/util.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery, $) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * @class mUtil  Metronic base utilize class that privides helper functions
 */

var mUtil = exports.mUtil = function () {
    var resizeHandlers = [];

    /** @type {object} breakpoints The device width breakpoints **/
    var breakpoints = {
        sm: 544, // Small screen / phone           
        md: 768, // Medium screen / tablet            
        lg: 992, // Large screen / desktop        
        xl: 1200 // Extra large screen / wide desktop
    };

    /** @type {object} colors State colors **/
    var colors = {
        brand: '#716aca',
        metal: '#c4c5d6',
        light: '#ffffff',
        accent: '#00c5dc',
        primary: '#5867dd',
        success: '#34bfa3',
        info: '#36a3f7',
        warning: '#ffb822',
        danger: '#f4516c'
    };

    /**
    * Handle window resize event with some 
    * delay to attach event handlers upon resize complete 
    */
    var _windowResizeHandler = function _windowResizeHandler() {
        var resize;
        var _runResizeHandlers = function _runResizeHandlers() {
            // reinitialize other subscribed elements
            for (var i = 0; i < resizeHandlers.length; i++) {
                var each = resizeHandlers[i];
                each.call();
            }
        };

        jQuery(window).resize(function () {
            if (resize) {
                clearTimeout(resize);
            }
            resize = setTimeout(function () {
                _runResizeHandlers();
            }, 250); // wait 50ms until window resize finishes.
        });
    };

    return {
        /**
        * Class main initializer.
        * @param {object} options.
        * @returns null
        */
        //main function to initiate the theme
        init: function init(options) {
            if (options && options.breakpoints) {
                breakpoints = options.breakpoints;
            }

            if (options && options.colors) {
                colors = options.colors;
            }

            _windowResizeHandler();
        },

        /**
        * Adds window resize event handler.
        * @param {function} callback function.
        */
        addResizeHandler: function addResizeHandler(callback) {
            resizeHandlers.push(callback);
        },

        /**
        * Trigger window resize handlers.
        */
        runResizeHandlers: function runResizeHandlers() {
            _runResizeHandlers();
        },

        /**
        * Get GET parameter value from URL.
        * @param {string} paramName Parameter name.
        * @returns {string}  
        */
        getURLParam: function getURLParam(paramName) {
            var searchString = window.location.search.substring(1),
                i,
                val,
                params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }

            return null;
        },

        /**
        * Checks whether current device is mobile touch.
        * @returns {boolean}  
        */
        isMobileDevice: function isMobileDevice() {
            return this.getViewPort().width < this.getBreakpoint('lg') ? true : false;
        },

        /**
        * Checks whether current device is desktop.
        * @returns {boolean}  
        */
        isDesktopDevice: function isDesktopDevice() {
            return mUtil.isMobileDevice() ? false : true;
        },

        /**
        * Gets browser window viewport size. Ref: http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
        * @returns {object}  
        */
        getViewPort: function getViewPort() {
            var e = window,
                a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }

            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        },

        /**
        * Checks whether given device mode is currently activated.
        * @param {string} mode Responsive mode name(e.g: desktop, desktop-and-tablet, tablet, tablet-and-mobile, mobile)
        * @returns {boolean}  
        */
        isInResponsiveRange: function isInResponsiveRange(mode) {
            var breakpoint = this.getViewPort().width;

            if (mode == 'general') {
                return true;
            } else if (mode == 'desktop' && breakpoint >= this.getBreakpoint('lg') + 1) {
                return true;
            } else if (mode == 'tablet' && breakpoint >= this.getBreakpoint('md') + 1 && breakpoint < this.getBreakpoint('lg')) {
                return true;
            } else if (mode == 'mobile' && breakpoint <= this.getBreakpoint('md')) {
                return true;
            } else if (mode == 'desktop-and-tablet' && breakpoint >= this.getBreakpoint('md') + 1) {
                return true;
            } else if (mode == 'tablet-and-mobile' && breakpoint <= this.getBreakpoint('lg')) {
                return true;
            } else if (mode == 'minimal-desktop-and-below' && breakpoint <= this.getBreakpoint('xl')) {
                return true;
            }

            return false;
        },

        /**
        * Generates unique ID for give prefix.
        * @param {string} prefix Prefix for generated ID
        * @returns {boolean}  
        */
        getUniqueID: function getUniqueID(prefix) {
            return prefix + Math.floor(Math.random() * new Date().getTime());
        },

        /**
        * Gets window width for give breakpoint mode.
        * @param {string} mode Responsive mode name(e.g: xl, lg, md, sm)
        * @returns {number}  
        */
        getBreakpoint: function getBreakpoint(mode) {
            if ($.inArray(mode, breakpoints)) {
                return breakpoints[mode];
            }
        },

        /**
        * Checks whether object has property matchs given key path.
        * @param {object} obj Object contains values paired with given key path
        * @param {string} keys Keys path seperated with dots
        * @returns {object}  
        */
        isset: function isset(obj, keys) {
            var stone;

            keys = keys || '';

            if (keys.indexOf('[') !== -1) {
                throw new Error('Unsupported object path notation.');
            }

            keys = keys.split('.');

            do {
                if (obj === undefined) {
                    return false;
                }

                stone = keys.shift();

                if (!obj.hasOwnProperty(stone)) {
                    return false;
                }

                obj = obj[stone];
            } while (keys.length);

            return true;
        },

        /**
        * Gets highest z-index of the given element parents
        * @param {object} el jQuery element object
        * @returns {number}  
        */
        getHighestZindex: function getHighestZindex(el) {
            var elem = $(el),
                position,
                value;

            while (elem.length && elem[0] !== document) {
                // Ignore z-index if position is set to a value where z-index is ignored by the browser
                // This makes behavior of this function consistent across browsers
                // WebKit always returns auto if the element is positioned
                position = elem.css("position");

                if (position === "absolute" || position === "relative" || position === "fixed") {
                    // IE returns 0 when zIndex is not specified
                    // other browsers return a string
                    // we ignore the case of nested elements with an explicit value of 0
                    // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
                    value = parseInt(elem.css("zIndex"), 10);
                    if (!isNaN(value) && value !== 0) {
                        return value;
                    }
                }
                elem = elem.parent();
            }
        },

        /**
        * Checks whether the element has given classes
        * @param {object} el jQuery element object
        * @param {string} Classes string
        * @returns {boolean}  
        */
        hasClasses: function hasClasses(el, classes) {
            var classesArr = classes.split(" ");

            for (var i = 0; i < classesArr.length; i++) {
                if (el.hasClass(classesArr[i]) == false) {
                    return false;
                }
            }

            return true;
        },

        /**
        * Gets element actual/real width
        * @param {object} el jQuery element object
        * @returns {number}  
        */
        realWidth: function realWidth(el) {
            var clone = $(el).clone();
            clone.css("visibility", "hidden");
            clone.css('overflow', 'hidden');
            clone.css("height", "0");
            $('body').append(clone);
            var width = clone.outerWidth();
            clone.remove();

            return width;
        },

        /**
        * Checks whether the element has any parent with fixed position
        * @param {object} el jQuery element object
        * @returns {boolean}  
        */
        hasFixedPositionedParent: function hasFixedPositionedParent(el) {
            var result = false;

            el.parents().each(function () {
                if ($(this).css('position') == 'fixed') {
                    result = true;
                    return;
                }
            });

            return result;
        },

        /**
        * Simulates delay
        */
        sleep: function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if (new Date().getTime() - start > milliseconds) {
                    break;
                }
            }
        },

        /**
        * Gets randomly generated integer value within given min and max range
        * @param {number} min Range start value
        * @param {number} min Range end value
        * @returns {number}  
        */
        getRandomInt: function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        /**
        * Gets state color's hex code by color name
        * @param {string} name Color name
        * @returns {string}  
        */
        getColor: function getColor(name) {
            return colors[name];
        },

        /**
        * Checks whether Angular library is included
        * @returns {boolean}  
        */
        isAngularVersion: function isAngularVersion() {
            return window.Zone !== undefined ? true : false;
        }
    };
}();

//== Initialize mUtil class on document ready
// $(document).ready(function() {
//     mUtil.init();
// });
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js"), __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/components/general/dropdown.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

var _util = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/util.js");

var _app = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/app.js");

(function ($) {
    // Plugin function
    $.fn.mDropdown = function (options) {
        // Plugin scope variable
        var dropdown = {};
        var element = $(this);

        // Plugin class
        var Plugin = {
            /**
             * Run
             */
            run: function run(options) {
                if (!element.data("dropdown")) {
                    // create instance
                    Plugin.init(options);
                    Plugin.build();
                    Plugin.setup();

                    // assign instance to the element                    
                    element.data("dropdown", dropdown);
                } else {
                    // get instance from the element
                    dropdown = element.data("dropdown");
                }

                return dropdown;
            },

            /**
             * Initialize
             */
            init: function init(options) {
                dropdown.events = [];
                dropdown.eventOne = false;
                dropdown.close = element.find(".m-dropdown__close");
                dropdown.toggle = element.find(".m-dropdown__toggle");
                dropdown.arrow = element.find(".m-dropdown__arrow");
                dropdown.wrapper = element.find(".m-dropdown__wrapper");
                dropdown.scrollable = element.find(".m-dropdown__scrollable");
                dropdown.defaultDropPos = element.hasClass("m-dropdown--up") ? "up" : "down";
                dropdown.currentDropPos = dropdown.defaultDropPos;

                dropdown.options = $.extend(true, {}, $.fn.mDropdown.defaults, options);
                if (element.data("drop-auto") === true) {
                    dropdown.options.dropAuto = true;
                } else if (element.data("drop-auto") === false) {
                    dropdown.options.dropAuto = false;
                }

                if (dropdown.scrollable.length > 0) {
                    if (dropdown.scrollable.data("min-height")) {
                        dropdown.options.minHeight = dropdown.scrollable.data("min-height");
                    }

                    if (dropdown.scrollable.data("max-height")) {
                        dropdown.options.maxHeight = dropdown.scrollable.data("max-height");
                    }
                }
            },

            /**
             * Build DOM and init event handlers
             */
            build: function build() {
                if (_util.mUtil.isMobileDevice()) {
                    if (element.data("dropdown-toggle") == "hover" || element.data("dropdown-toggle") == "click") {
                        dropdown.options.toggle = "click";
                    } else {
                        dropdown.options.toggle = "click";
                        dropdown.toggle.click(Plugin.toggle);
                    }
                } else {
                    if (element.data("dropdown-toggle") == "hover") {
                        dropdown.options.toggle = "hover";
                        element.mouseleave(Plugin.hide);
                    } else if (element.data("dropdown-toggle") == "click") {
                        dropdown.options.toggle = "click";
                    } else {
                        if (dropdown.options.toggle == "hover") {
                            element.mouseenter(Plugin.show);
                            element.mouseleave(Plugin.hide);
                        } else {
                            dropdown.toggle.click(Plugin.toggle);
                        }
                    }
                }

                // handle dropdown close icon
                if (dropdown.close.length) {
                    dropdown.close.on("click", Plugin.hide);
                }

                // disable dropdown close
                Plugin.disableClose();
            },

            /**
             * Setup dropdown
             */
            setup: function setup() {
                if (dropdown.options.placement) {
                    element.addClass("m-dropdown--" + dropdown.options.placement);
                }

                if (dropdown.options.align) {
                    element.addClass("m-dropdown--align-" + dropdown.options.align);
                }

                if (dropdown.options.width) {
                    dropdown.wrapper.css("width", dropdown.options.width);
                }

                if (element.data("dropdown-persistent")) {
                    dropdown.options.persistent = true;
                }

                // handle height
                if (dropdown.options.minHeight) {
                    dropdown.scrollable.css("min-height", dropdown.options.minHeight);
                }

                if (dropdown.options.maxHeight) {
                    dropdown.scrollable.css("max-height", dropdown.options.maxHeight);
                    dropdown.scrollable.css("overflow-y", "auto");

                    if (_util.mUtil.isDesktopDevice()) {
                        _app.mApp.initScroller(dropdown.scrollable, {});
                    }
                }

                // set zindex
                Plugin.setZindex();
            },

            /**
             * sync
             */
            sync: function sync() {
                $(element).data("dropdown", dropdown);
            },

            /**
             * Sync dropdown object with jQuery element
             */
            disableClose: function disableClose() {
                element.on("click", ".m-dropdown--disable-close, .mCSB_1_scrollbar", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });
            },

            /**
             * Toggle dropdown
             */
            toggle: function toggle() {
                if (dropdown.open) {
                    return Plugin.hide();
                } else {
                    return Plugin.show();
                }
            },

            /**
             * Set content
             */
            setContent: function setContent(content) {
                element.find(".m-dropdown__content").html(content);

                return dropdown;
            },

            /**
             * Show dropdown
             */
            show: function show() {
                if (dropdown.options.toggle == "hover" && element.data("hover")) {
                    Plugin.clearHovered();
                    return dropdown;
                }

                if (dropdown.open) {
                    return dropdown;
                }

                if (dropdown.arrow.length > 0) {
                    Plugin.adjustArrowPos();
                }

                Plugin.eventTrigger("beforeShow");

                Plugin.hideOpened();

                element.addClass("m-dropdown--open");

                if (_util.mUtil.isMobileDevice() && dropdown.options.mobileOverlay) {
                    var zIndex = dropdown.wrapper.css("zIndex") - 1;
                    var dropdownoff = $("<div class=\"m-dropdown__dropoff\"></div>");

                    dropdownoff.css("zIndex", zIndex);
                    dropdownoff.data("dropdown", element);
                    element.data("dropoff", dropdownoff);
                    element.after(dropdownoff);
                    dropdownoff.click(function (e) {
                        Plugin.hide();
                        $(this).remove();
                        e.preventDefault();
                    });
                }

                element.focus();
                element.attr("aria-expanded", "true");
                dropdown.open = true;

                Plugin.handleDropPosition();

                Plugin.eventTrigger("afterShow");

                return dropdown;
            },

            /**
             * Clear dropdown hover
             */
            clearHovered: function clearHovered() {
                element.removeData("hover");
                var timeout = element.data("timeout");
                element.removeData("timeout");
                clearTimeout(timeout);
            },

            /**
             * Hide hovered dropdown
             */
            hideHovered: function hideHovered(force) {
                if (force) {
                    if (Plugin.eventTrigger("beforeHide") === false) {
                        // cancel hide
                        return;
                    }

                    Plugin.clearHovered();
                    element.removeClass("m-dropdown--open");
                    dropdown.open = false;
                    Plugin.eventTrigger("afterHide");
                } else {
                    if (Plugin.eventTrigger("beforeHide") === false) {
                        // cancel hide
                        return;
                    }
                    var timeout = setTimeout(function () {
                        if (element.data("hover")) {
                            Plugin.clearHovered();
                            element.removeClass("m-dropdown--open");
                            dropdown.open = false;
                            Plugin.eventTrigger("afterHide");
                        }
                    }, dropdown.options.hoverTimeout);

                    element.data("hover", true);
                    element.data("timeout", timeout);
                }
            },

            /**
             * Hide clicked dropdown
             */
            hideClicked: function hideClicked() {
                if (Plugin.eventTrigger("beforeHide") === false) {
                    // cancel hide
                    return;
                }
                element.removeClass("m-dropdown--open");
                if (element.data("dropoff")) {
                    element.data("dropoff").remove();
                }
                dropdown.open = false;
                Plugin.eventTrigger("afterHide");
            },

            /**
             * Hide dropdown
             */
            hide: function hide(force) {
                if (dropdown.open === false) {
                    return dropdown;
                }

                if (dropdown.options.toggle == "hover") {
                    Plugin.hideHovered(force);
                } else {
                    Plugin.hideClicked();
                }

                if (dropdown.defaultDropPos == "down" && dropdown.currentDropPos == "up") {
                    element.removeClass("m-dropdown--up");
                    dropdown.arrow.prependTo(dropdown.wrapper);
                    dropdown.currentDropPos = "down";
                }

                return dropdown;
            },

            /**
             * Hide opened dropdowns
             */
            hideOpened: function hideOpened() {
                $(".m-dropdown.m-dropdown--open").each(function () {
                    $(this).mDropdown().hide(true);
                });
            },

            /**
             * Adjust dropdown arrow positions
             */
            adjustArrowPos: function adjustArrowPos() {
                var width = element.outerWidth();
                var alignment = dropdown.arrow.hasClass("m-dropdown__arrow--right") ? "right" : "left";
                var pos = 0;

                if (dropdown.arrow.length > 0) {
                    if (_util.mUtil.isInResponsiveRange("mobile") && element.hasClass("m-dropdown--mobile-full-width")) {
                        pos = element.offset().left + width / 2 - Math.abs(dropdown.arrow.width() / 2) - parseInt(dropdown.wrapper.css("left"));
                        dropdown.arrow.css("right", "auto");
                        dropdown.arrow.css("left", pos);
                        dropdown.arrow.css("margin-left", "auto");
                        dropdown.arrow.css("margin-right", "auto");
                    } else if (dropdown.arrow.hasClass("m-dropdown__arrow--adjust")) {
                        pos = width / 2 - Math.abs(dropdown.arrow.width() / 2);
                        if (element.hasClass("m-dropdown--align-push")) {
                            pos = pos + 20;
                        }
                        if (alignment == "right") {
                            dropdown.arrow.css("left", "auto");
                            dropdown.arrow.css("right", pos);
                        } else {
                            dropdown.arrow.css("right", "auto");
                            dropdown.arrow.css("left", pos);
                        }
                    }
                }
            },

            /**
             * Change dropdown drop position
             */
            handleDropPosition: function handleDropPosition() {
                return;

                if (dropdown.options.dropAuto == true) {
                    if (Plugin.isInVerticalViewport() === false) {
                        if (dropdown.currentDropPos == "up") {
                            element.removeClass("m-dropdown--up");
                            dropdown.arrow.prependTo(dropdown.wrapper);
                            dropdown.currentDropPos = "down";
                        } else if (dropdown.currentDropPos == "down") {
                            element.addClass("m-dropdown--up");
                            dropdown.arrow.appendTo(dropdown.wrapper);
                            dropdown.currentDropPos = "up";
                        }
                    }
                }
            },

            /**
             * Get zindex
             */
            setZindex: function setZindex() {
                var oldZindex = dropdown.wrapper.css("z-index");
                var newZindex = _util.mUtil.getHighestZindex(element);
                if (newZindex > oldZindex) {
                    dropdown.wrapper.css("z-index", zindex);
                }
            },

            /**
             * Check persistent
             */
            isPersistent: function isPersistent() {
                return dropdown.options.persistent;
            },

            /**
             * Check persistent
             */
            isShown: function isShown() {
                return dropdown.open;
            },

            /**
             * Check if dropdown is in viewport
             */
            isInVerticalViewport: function isInVerticalViewport() {
                var el = dropdown.wrapper;
                var offset = el.offset();
                var height = el.outerHeight();
                var width = el.width();
                var scrollable = el.find("[data-scrollable]");

                if (scrollable.length) {
                    if (scrollable.data("max-height")) {
                        height += parseInt(scrollable.data("max-height"));
                    } else if (scrollable.data("height")) {
                        height += parseInt(scrollable.data("height"));
                    }
                }

                return offset.top + height < $(window).scrollTop() + $(window).height();
            },

            /**
             * Trigger events
             */
            eventTrigger: function eventTrigger(name) {
                for (var i = 0; i < dropdown.events.length; i++) {
                    var event = dropdown.events[i];
                    if (event.name == name) {
                        if (event.one == true) {
                            if (event.fired == false) {
                                dropdown.events[i].fired = true;
                                return event.handler.call(this, dropdown);
                            }
                        } else {
                            return event.handler.call(this, dropdown);
                        }
                    }
                }
            },

            addEvent: function addEvent(name, handler, one) {
                dropdown.events.push({
                    name: name,
                    handler: handler,
                    one: one,
                    fired: false
                });

                Plugin.sync();

                return dropdown;
            }
        };

        // Run plugin
        Plugin.run.apply(this, [options]);

        //////////////////////
        // ** Public API ** //
        //////////////////////

        /**
         * Show dropdown
         * @returns {mDropdown}
         */
        dropdown.show = function () {
            return Plugin.show();
        };

        /**
         * Hide dropdown
         * @returns {mDropdown}
         */
        dropdown.hide = function () {
            return Plugin.hide();
        };

        /**
         * Toggle dropdown
         * @returns {mDropdown}
         */
        dropdown.toggle = function () {
            return Plugin.toggle();
        };

        /**
         * Toggle dropdown
         * @returns {mDropdown}
         */
        dropdown.isPersistent = function () {
            return Plugin.isPersistent();
        };

        /**
         * Check shown state
         * @returns {mDropdown}
         */
        dropdown.isShown = function () {
            return Plugin.isShown();
        };

        /**
         * Check shown state
         * @returns {mDropdown}
         */
        dropdown.fixDropPosition = function () {
            return Plugin.handleDropPosition();
        };

        /**
         * Set dropdown content
         * @returns {mDropdown}
         */
        dropdown.setContent = function (content) {
            return Plugin.setContent(content);
        };

        /**
         * Set dropdown content
         * @returns {mDropdown}
         */
        dropdown.on = function (name, handler) {
            return Plugin.addEvent(name, handler);
        };

        /**
         * Set dropdown content
         * @returns {mDropdown}
         */
        dropdown.one = function (name, handler) {
            return Plugin.addEvent(name, handler, true);
        };

        return dropdown;
    };

    // default options
    $.fn.mDropdown.defaults = {
        toggle: "click",
        hoverTimeout: 300,
        skin: "default",
        height: "auto",
        dropAuto: true,
        maxHeight: false,
        minHeight: false,
        persistent: false,
        mobileOverlay: true
    };

    // global init
    if (_util.mUtil.isMobileDevice()) {
        $(document).on("click", "[data-dropdown-toggle=\"click\"] .m-dropdown__toggle, [data-dropdown-toggle=\"hover\"] .m-dropdown__toggle", function (e) {
            e.preventDefault();
            $(this).parent(".m-dropdown").mDropdown().toggle();
        });
    } else {
        $(document).on("click", "[data-dropdown-toggle=\"click\"] .m-dropdown__toggle", function (e) {
            e.preventDefault();
            $(this).parent(".m-dropdown").mDropdown().toggle();
        });
        $(document).on("mouseenter", "[data-dropdown-toggle=\"hover\"]", function (e) {
            $(this).mDropdown().toggle();
        });
    }

    // handle global document click
    $(document).on("click", function (e) {
        $(".m-dropdown.m-dropdown--open").each(function () {
            if (!$(this).data("dropdown")) {
                return;
            }

            var target = $(e.target);
            var dropdown = $(this).mDropdown();
            var toggle = $(this).find(".m-dropdown__toggle");

            if (toggle.length > 0 && target.is(toggle) !== true && toggle.find(target).length === 0 && target.find(toggle).length === 0 && dropdown.isPersistent() == false) {
                dropdown.hide();
            } else if ($(this).find(target).length === 0) {
                dropdown.hide();
            }
        });
    });
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/components/general/header.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

(function ($) {

    // Plugin function
    $.fn.mHeader = function (options) {
        // Plugin scope variable
        var header = this;
        var element = $(this);

        // Plugin class
        var Plugin = {
            /**
             * Run plugin
             * @returns {mHeader}
             */
            run: function run(options) {
                if (element.data('header')) {
                    header = element.data('header');
                } else {
                    // reset header
                    Plugin.init(options);

                    // reset header
                    Plugin.reset();

                    // build header
                    Plugin.build();

                    element.data('header', header);
                }

                return header;
            },

            /**
             * Handles subheader click toggle
             * @returns {mHeader}
             */
            init: function init(options) {
                header.options = $.extend(true, {}, $.fn.mHeader.defaults, options);
            },

            /**
             * Reset header
             * @returns {mHeader}
             */
            build: function build() {
                Plugin.toggle();
            },

            toggle: function toggle() {
                var lastScrollTop = 0;

                if (header.options.minimize.mobile === false && header.options.minimize.desktop === false) {
                    return;
                }

                $(window).scroll(function () {
                    var offset = 0;

                    if (mUtil.isInResponsiveRange('desktop')) {
                        offset = header.options.offset.desktop;
                        on = header.options.minimize.desktop.on;
                        off = header.options.minimize.desktop.off;
                    } else if (mUtil.isInResponsiveRange('tablet-and-mobile')) {
                        offset = header.options.offset.mobile;
                        on = header.options.minimize.mobile.on;
                        off = header.options.minimize.mobile.off;
                    }

                    var st = $(this).scrollTop();

                    if (header.options.classic) {
                        if (st > offset) {
                            // down scroll mode
                            $("body").addClass(on);
                            $("body").removeClass(off);
                        } else {
                            // back scroll mode
                            $("body").addClass(off);
                            $("body").removeClass(on);
                        }
                    } else {
                        if (st > offset && lastScrollTop < st) {
                            // down scroll mode
                            $("body").addClass(on);
                            $("body").removeClass(off);
                        } else {
                            // back scroll mode
                            $("body").addClass(off);
                            $("body").removeClass(on);
                        }

                        lastScrollTop = st;
                    }
                });
            },

            /**
             * Reset menu
             * @returns {mMenu}
             */
            reset: function reset() {}
        };

        // Run plugin
        Plugin.run.apply(header, [options]);

        //////////////////////
        // ** Public API ** //
        //////////////////////

        /**
         * Disable header for given time
         * @returns {jQuery}
         */
        header.publicMethod = function () {
            //return Plugin.publicMethod();
        };

        // Return plugin instance
        return header;
    };

    // Plugin default options
    $.fn.mHeader.defaults = {
        classic: false,
        offset: {
            mobile: 150,
            desktop: 200
        },
        minimize: {
            mobile: false,
            desktop: false
        }
    };
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/components/general/menu.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

var _util = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/util.js");

var _app = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/app.js");

(function ($) {

    // Plugin function
    $.fn.mMenu = function (options) {
        // Plugin scope variable
        var menu = this;
        var element = $(this);

        // Plugin class
        var Plugin = {
            /**
             * Run plugin
             * @returns {mMenu}
             */
            run: function run(options, reinit) {
                if (element.data("menu") && reinit !== true) {
                    menu = element.data("menu");
                } else {
                    // reset menu
                    Plugin.init(options);

                    // reset menu
                    Plugin.reset();

                    // build menu
                    Plugin.build();

                    element.data("menu", menu);
                }

                return menu;
            },

            /**
             * Handles submenu click toggle
             * @returns {mMenu}
             */
            init: function init(options) {
                menu.events = [];

                // merge default and user defined options
                menu.options = $.extend(true, {}, $.fn.mMenu.defaults, options);

                // pause menu
                menu.pauseDropdownHoverTime = 0;
            },

            /**
             * Reset menu
             * @returns {mMenu}
             */
            build: function build() {
                element.on("click", ".m-menu__toggle", Plugin.handleSubmenuAccordion);

                // dropdown mode(hoverable)
                if (Plugin.getSubmenuMode() === "dropdown" || Plugin.isConditionalSubmenuDropdown()) {
                    // dropdown submenu - hover toggle
                    element.on({ mouseenter: Plugin.handleSubmenuDrodownHoverEnter, mouseleave: Plugin.handleSubmenuDrodownHoverExit }, "[data-menu-submenu-toggle=\"hover\"]");

                    // dropdown submenu - click toggle
                    element.on("click", "[data-menu-submenu-toggle=\"click\"] .m-menu__toggle", Plugin.handleSubmenuDropdownClick);
                }

                element.find(".m-menu__item:not(.m-menu__item--submenu) > .m-menu__link:not(.m-menu__toggle)").click(Plugin.handleLinkClick);
            },

            /**
             * Reset menu
             * @returns {mMenu}
             */
            reset: function reset() {
                // remove accordion handler
                element.off("click", ".m-menu__toggle", Plugin.handleSubmenuAccordion);

                // remove dropdown handlers
                element.off({ mouseenter: Plugin.handleSubmenuDrodownHoverEnter, mouseleave: Plugin.handleSubmenuDrodownHoverExit }, "[data-menu-submenu-toggle=\"hover\"]");
                element.off("click", "[data-menu-submenu-toggle=\"click\"] .m-menu__toggle", Plugin.handleSubmenuDropdownClick);

                // reset mobile menu attributes
                menu.find(".m-menu__submenu, .m-menu__inner").css("display", "");
                menu.find(".m-menu__item--hover").removeClass("m-menu__item--hover");
                menu.find(".m-menu__item--open:not(.m-menu__item--expanded)").removeClass("m-menu__item--open");
            },

            /**
             * Get submenu mode for current breakpoint and menu state
             * @returns {mMenu}
             */
            getSubmenuMode: function getSubmenuMode() {
                if (_util.mUtil.isInResponsiveRange("desktop")) {
                    if (_util.mUtil.isset(menu.options.submenu, "desktop.state.body")) {
                        if ($("body").hasClass(menu.options.submenu.desktop.state.body)) {
                            return menu.options.submenu.desktop.state.mode;
                        } else {
                            return menu.options.submenu.desktop.default;
                        }
                    } else if (_util.mUtil.isset(menu.options.submenu, "desktop")) {
                        return menu.options.submenu.desktop;
                    }
                } else if (_util.mUtil.isInResponsiveRange("tablet") && _util.mUtil.isset(menu.options.submenu, "tablet")) {
                    return menu.options.submenu.tablet;
                } else if (_util.mUtil.isInResponsiveRange("mobile") && _util.mUtil.isset(menu.options.submenu, "mobile")) {
                    return menu.options.submenu.mobile;
                } else {
                    return false;
                }
            },

            /**
             * Get submenu mode for current breakpoint and menu state
             * @returns {mMenu}
             */
            isConditionalSubmenuDropdown: function isConditionalSubmenuDropdown() {
                if (_util.mUtil.isInResponsiveRange("desktop") && _util.mUtil.isset(menu.options.submenu, "desktop.state.body")) {
                    return true;
                } else {
                    return false;
                }
            },

            /**
             * Handles menu link click
             * @returns {mMenu}
             */
            handleLinkClick: function handleLinkClick(e) {

                if (Plugin.eventTrigger("linkClick", $(this)) === false) {
                    e.preventDefault();
                }
                ;

                if (Plugin.getSubmenuMode() === "dropdown" || Plugin.isConditionalSubmenuDropdown()) {
                    Plugin.handleSubmenuDropdownClose(e, $(this));
                }
            },

            /**
             * Handles submenu hover toggle
             * @returns {mMenu}
             */
            handleSubmenuDrodownHoverEnter: function handleSubmenuDrodownHoverEnter(e) {
                if (Plugin.getSubmenuMode() === "accordion") {
                    return;
                }

                if (menu.resumeDropdownHover() === false) {
                    return;
                }

                var item = $(this);

                Plugin.showSubmenuDropdown(item);

                if (item.data("hover") == true) {
                    Plugin.hideSubmenuDropdown(item, false);
                }
            },

            /**
             * Handles submenu hover toggle
             * @returns {mMenu}
             */
            handleSubmenuDrodownHoverExit: function handleSubmenuDrodownHoverExit(e) {
                if (menu.resumeDropdownHover() === false) {
                    return;
                }

                if (Plugin.getSubmenuMode() === "accordion") {
                    return;
                }

                var item = $(this);
                var time = menu.options.dropdown.timeout;

                var timeout = setTimeout(function () {
                    if (item.data("hover") == true) {
                        Plugin.hideSubmenuDropdown(item, true);
                    }
                }, time);

                item.data("hover", true);
                item.data("timeout", timeout);
            },

            /**
             * Handles submenu click toggle
             * @returns {mMenu}
             */
            handleSubmenuDropdownClick: function handleSubmenuDropdownClick(e) {
                if (Plugin.getSubmenuMode() === "accordion") {
                    return;
                }

                var item = $(this).closest(".m-menu__item");

                if (item.data("menu-submenu-mode") == "accordion") {
                    return;
                }

                if (item.hasClass("m-menu__item--hover") == false) {
                    item.addClass("m-menu__item--open-dropdown");
                    Plugin.showSubmenuDropdown(item);
                } else {
                    item.removeClass("m-menu__item--open-dropdown");
                    Plugin.hideSubmenuDropdown(item, true);
                }

                e.preventDefault();
            },

            /**
             * Handles submenu dropdown close on link click
             * @returns {mMenu}
             */
            handleSubmenuDropdownClose: function handleSubmenuDropdownClose(e, el) {
                // exit if its not submenu dropdown mode
                if (Plugin.getSubmenuMode() === "accordion") {
                    return;
                }

                var shown = element.find(".m-menu__item.m-menu__item--submenu.m-menu__item--hover");

                // check if currently clicked link's parent item ha
                if (shown.length > 0 && el.hasClass("m-menu__toggle") === false && el.find(".m-menu__toggle").length === 0) {
                    // close opened dropdown menus
                    shown.each(function () {
                        Plugin.hideSubmenuDropdown($(this), true);
                    });
                }
            },

            /**
             * helper functions
             * @returns {mMenu}
             */
            handleSubmenuAccordion: function handleSubmenuAccordion(e, el) {
                var item = el ? $(el) : $(this);

                if (Plugin.getSubmenuMode() === "dropdown" && item.closest(".m-menu__item").data("menu-submenu-mode") != "accordion") {
                    e.preventDefault();
                    return;
                }

                var li = item.closest("li");
                var submenu = li.children(".m-menu__submenu, .m-menu__inner");

                if (submenu.parent(".m-menu__item--expanded").length != 0) {
                    //return;
                }

                if (submenu.length > 0) {
                    e.preventDefault();
                    var speed = menu.options.accordion.slideSpeed;
                    var hasClosables = false;

                    if (li.hasClass("m-menu__item--open") === false) {
                        // hide other accordions
                        if (menu.options.accordion.expandAll === false) {
                            var closables = item.closest(".m-menu__nav, .m-menu__subnav").find("> .m-menu__item.m-menu__item--open.m-menu__item--submenu:not(.m-menu__item--expanded)");
                            closables.each(function () {
                                $(this).children(".m-menu__submenu").slideUp(speed, function () {
                                    Plugin.scrollToItem(item);
                                });
                                $(this).removeClass("m-menu__item--open");
                            });

                            if (closables.length > 0) {
                                hasClosables = true;
                            }
                        }

                        if (hasClosables) {
                            submenu.slideDown(speed, function () {
                                Plugin.scrollToItem(item);
                            });
                            li.addClass("m-menu__item--open");
                        } else {
                            submenu.slideDown(speed, function () {
                                Plugin.scrollToItem(item);
                            });
                            li.addClass("m-menu__item--open");
                        }
                    } else {
                        submenu.slideUp(speed, function () {
                            Plugin.scrollToItem(item);
                        });
                        li.removeClass("m-menu__item--open");
                    }
                }
            },

            /**
             * scroll to item function
             * @returns {mMenu}
             */
            scrollToItem: function scrollToItem(item) {
                // handle auto scroll for accordion submenus
                if (_util.mUtil.isInResponsiveRange("desktop") && menu.options.accordion.autoScroll && !element.data("menu-scrollable")) {
                    _app.mApp.scrollToViewport(item);
                }
            },

            /**
             * helper functions
             * @returns {mMenu}
             */
            hideSubmenuDropdown: function hideSubmenuDropdown(item, classAlso) {
                // remove submenu activation class
                if (classAlso) {
                    item.removeClass("m-menu__item--hover");
                }
                // clear timeout
                item.removeData("hover");
                if (item.data("menu-dropdown-toggle-class")) {
                    $("body").removeClass(item.data("menu-dropdown-toggle-class"));
                }
                var timeout = item.data("timeout");
                item.removeData("timeout");
                clearTimeout(timeout);
            },

            /**
             * helper functions
             * @returns {mMenu}
             */
            showSubmenuDropdown: function showSubmenuDropdown(item) {
                // close active submenus
                element.find(".m-menu__item--submenu.m-menu__item--hover").each(function () {
                    var el = $(this);
                    if (item.is(el) || el.find(item).length > 0 || item.find(el).length > 0) {
                        return;
                    } else {
                        Plugin.hideSubmenuDropdown(el, true);
                    }
                });

                // adjust submenu position
                Plugin.adjustSubmenuDropdownArrowPos(item);

                // add submenu activation class
                item.addClass("m-menu__item--hover");

                if (item.data("menu-dropdown-toggle-class")) {
                    $("body").addClass(item.data("menu-dropdown-toggle-class"));
                }

                // handle auto scroll for accordion submenus
                if (Plugin.getSubmenuMode() === "accordion" && menu.options.accordion.autoScroll) {
                    _app.mApp.scrollTo(item.children(".m-menu__item--submenu"));
                }
            },

            /**
             * Handles submenu click toggle
             * @returns {mMenu}
             */
            resize: function resize(e) {
                if (Plugin.getSubmenuMode() !== "dropdown") {
                    return;
                }

                var resize = element.find("> .m-menu__nav > .m-menu__item--resize");
                var submenu = resize.find("> .m-menu__submenu");
                var breakpoint;
                var currentWidth = _util.mUtil.getViewPort().width;
                var itemsNumber = element.find("> .m-menu__nav > .m-menu__item").length - 1;
                var check;

                if (Plugin.getSubmenuMode() == "dropdown" && (_util.mUtil.isInResponsiveRange("desktop") && _util.mUtil.isset(menu.options, "resize.desktop") && (check = menu.options.resize.desktop) && currentWidth <= (breakpoint = resize.data("menu-resize-desktop-breakpoint")) || _util.mUtil.isInResponsiveRange("tablet") && _util.mUtil.isset(menu.options, "resize.tablet") && (check = menu.options.resize.tablet) && currentWidth <= (breakpoint = resize.data("menu-resize-tablet-breakpoint")) || _util.mUtil.isInResponsiveRange("mobile") && _util.mUtil.isset(menu.options, "resize.mobile") && (check = menu.options.resize.mobile) && currentWidth <= (breakpoint = resize.data("menu-resize-mobile-breakpoint")))) {

                    var moved = submenu.find("> .m-menu__subnav > .m-menu__item").length; // currently move
                    var left = element.find("> .m-menu__nav > .m-menu__item:not(.m-menu__item--resize)").length; // currently left
                    var total = moved + left;

                    if (check.apply() === true) {
                        // return
                        if (moved > 0) {
                            submenu.find("> .m-menu__subnav > .m-menu__item").each(function () {
                                var item = $(this);

                                var elementsNumber = submenu.find("> .m-menu__nav > .m-menu__item:not(.m-menu__item--resize)").length;
                                element.find("> .m-menu__nav > .m-menu__item:not(.m-menu__item--resize)").eq(elementsNumber - 1).after(item);

                                if (check.apply() === false) {
                                    item.appendTo(submenu.find("> .m-menu__subnav"));
                                    return false;
                                }

                                moved--;
                                left++;
                            });
                        }
                    } else {
                        // move
                        if (left > 0) {
                            var items = element.find("> .m-menu__nav > .m-menu__item:not(.m-menu__item--resize)");
                            var index = items.length - 1;

                            for (var i = 0; i < items.length; i++) {
                                var item = $(items.get(index));
                                index--;

                                if (check.apply() === true) {
                                    break;
                                }

                                item.appendTo(submenu.find("> .m-menu__subnav"));

                                moved++;
                                left--;
                            }
                        }
                    }

                    if (moved > 0) {
                        resize.show();
                    } else {
                        resize.hide();
                    }
                } else {
                    submenu.find("> .m-menu__subnav > .m-menu__item").each(function () {
                        var elementsNumber = submenu.find("> .m-menu__subnav > .m-menu__item").length;
                        element.find("> .m-menu__nav > .m-menu__item").get(elementsNumber).after($(this));
                    });

                    resize.hide();
                }
            },

            /**
             * Handles submenu slide toggle
             * @returns {mMenu}
             */
            createSubmenuDropdownClickDropoff: function createSubmenuDropdownClickDropoff(el) {
                var zIndex = el.find("> .m-menu__submenu").css("zIndex") - 1;
                var dropoff = $("<div class=\"m-menu__dropoff\" style=\"background: transparent; position: fixed; top: 0; bottom: 0; left: 0; right: 0; z-index: " + zIndex + "\"></div>");
                $("body").after(dropoff);
                dropoff.on("click", function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).remove();

                    alert(1);
                    Plugin.hideSubmenuDropdown(el, true);
                });
            },

            /**
             * Handles submenu click toggle
             * @returns {mMenu}
             */
            adjustSubmenuDropdownArrowPos: function adjustSubmenuDropdownArrowPos(item) {
                var arrow = item.find("> .m-menu__submenu > .m-menu__arrow.m-menu__arrow--adjust");
                var submenu = item.find("> .m-menu__submenu");
                var subnav = item.find("> .m-menu__submenu > .m-menu__subnav");

                if (arrow.length > 0) {
                    var pos;
                    var link = item.children(".m-menu__link");

                    if (submenu.hasClass("m-menu__submenu--classic") || submenu.hasClass("m-menu__submenu--fixed")) {
                        if (submenu.hasClass("m-menu__submenu--right")) {
                            pos = item.outerWidth() / 2;
                            if (submenu.hasClass("m-menu__submenu--pull")) {
                                pos = pos + Math.abs(parseInt(submenu.css("margin-right")));
                            }
                            pos = submenu.width() - pos;
                        } else if (submenu.hasClass("m-menu__submenu--left")) {
                            pos = item.outerWidth() / 2;
                            if (submenu.hasClass("m-menu__submenu--pull")) {
                                pos = pos + Math.abs(parseInt(submenu.css("margin-left")));
                            }
                        }
                    } else {
                        if (submenu.hasClass("m-menu__submenu--center") || submenu.hasClass("m-menu__submenu--full")) {
                            pos = item.offset().left - (_util.mUtil.getViewPort().width - submenu.outerWidth()) / 2;
                            pos = pos + item.outerWidth() / 2;
                        } else if (submenu.hasClass("m-menu__submenu--left")) {
                            // to do
                        } else if (submenu.hasClass("m-menu__submenu--right")) {
                            // to do
                        }
                    }

                    arrow.css("left", pos);
                }
            },

            /**
             * Handles submenu hover toggle
             * @returns {mMenu}
             */
            pauseDropdownHover: function pauseDropdownHover(time) {
                var date = new Date();

                menu.pauseDropdownHoverTime = date.getTime() + time;
            },

            /**
             * Handles submenu hover toggle
             * @returns {mMenu}
             */
            resumeDropdownHover: function resumeDropdownHover() {
                var date = new Date();

                return date.getTime() > menu.pauseDropdownHoverTime ? true : false;
            },

            /**
             * Reset menu's current active item
             * @returns {mMenu}
             */
            resetActiveItem: function resetActiveItem(item) {
                element.find(".m-menu__item--active").each(function () {
                    $(this).removeClass("m-menu__item--active");
                    $(this).children(".m-menu__submenu").css("display", "");

                    $(this).parents(".m-menu__item--submenu").each(function () {
                        $(this).removeClass("m-menu__item--open");
                        $(this).children(".m-menu__submenu").css("display", "");
                    });
                });

                // close open submenus
                if (menu.options.accordion.expandAll === false) {
                    element.find(".m-menu__item--open").each(function () {
                        $(this).removeClass("m-menu__item--open");
                    });
                }
            },

            /**
             * Sets menu's active item
             * @returns {mMenu}
             */
            setActiveItem: function setActiveItem(item) {
                // reset current active item
                Plugin.resetActiveItem();

                var item = $(item);
                item.addClass("m-menu__item--active");
                item.parents(".m-menu__item--submenu").each(function () {
                    $(this).addClass("m-menu__item--open");
                });
            },

            /**
             * Returns page breadcrumbs for the menu's active item
             * @returns {mMenu}
             */
            getBreadcrumbs: function getBreadcrumbs(item) {
                var breadcrumbs = [];
                var item = $(item);
                var link = item.children(".m-menu__link");

                breadcrumbs.push({
                    text: link.find(".m-menu__link-text").html(),
                    title: link.attr("title"),
                    href: link.attr("href")
                });

                item.parents(".m-menu__item--submenu").each(function () {
                    var submenuLink = $(this).children(".m-menu__link");
                    breadcrumbs.push({
                        text: submenuLink.find(".m-menu__link-text").html(),
                        title: submenuLink.attr("title"),
                        href: submenuLink.attr("href")
                    });
                });

                breadcrumbs.reverse();

                return breadcrumbs;
            },

            /**
             * Returns page title for the menu's active item
             * @returns {mMenu}
             */
            getPageTitle: function getPageTitle(item) {
                item = $(item);

                return item.children(".m-menu__link").find(".m-menu__link-text").html();
            },

            /**
             * Sync
             */
            sync: function sync() {
                $(element).data("menu", menu);
            },

            /**
             * Trigger events
             */
            eventTrigger: function eventTrigger(name, args) {
                for (i = 0; i < menu.events.length; i++) {
                    var event = menu.events[i];
                    if (event.name == name) {
                        if (event.one == true) {
                            if (event.fired == false) {
                                menu.events[i].fired = true;
                                return event.handler.call(this, menu, args);
                            }
                        } else {
                            return event.handler.call(this, menu, args);
                        }
                    }
                }
            },

            addEvent: function addEvent(name, handler, one) {
                menu.events.push({
                    name: name,
                    handler: handler,
                    one: one,
                    fired: false
                });

                Plugin.sync();
            }
        };

        // Run plugin
        Plugin.run.apply(menu, [options]);

        // Handle plugin on window resize
        if (typeof options !== "undefined") {
            $(window).resize(function () {
                Plugin.run.apply(menu, [options, true]);
            });
        }

        //////////////////////
        // ** Public API ** //
        //////////////////////

        /**
         * Set active menu item
         */
        menu.setActiveItem = function (item) {
            return Plugin.setActiveItem(item);
        };

        /**
         * Set breadcrumb for menu item
         */
        menu.getBreadcrumbs = function (item) {
            return Plugin.getBreadcrumbs(item);
        };

        /**
         * Set page title for menu item
         */
        menu.getPageTitle = function (item) {
            return Plugin.getPageTitle(item);
        };

        /**
         * Get submenu mode
         */
        menu.getSubmenuMode = function () {
            return Plugin.getSubmenuMode();
        };

        /**
         * Hide dropdown submenu
         * @returns {jQuery}
         */
        menu.hideDropdown = function (item) {
            Plugin.hideSubmenuDropdown(item, true);
        };

        /**
         * Disable menu for given time
         * @returns {jQuery}
         */
        menu.pauseDropdownHover = function (time) {
            Plugin.pauseDropdownHover(time);
        };

        /**
         * Disable menu for given time
         * @returns {jQuery}
         */
        menu.resumeDropdownHover = function () {
            return Plugin.resumeDropdownHover();
        };

        /**
         * Register event
         */
        menu.on = function (name, handler) {
            return Plugin.addEvent(name, handler);
        };

        // Return plugin instance
        return menu;
    };

    // Plugin default options
    $.fn.mMenu.defaults = {
        // accordion submenu mode
        accordion: {
            slideSpeed: 200, // accordion toggle slide speed in milliseconds
            autoScroll: true, // enable auto scrolling(focus) to the clicked menu item
            expandAll: true // allow having multiple expanded accordions in the menu
        },

        // dropdown submenu mode
        dropdown: {
            timeout: 500 // timeout in milliseconds to show and hide the hoverable submenu dropdown
        }
    };

    // Plugin global lazy initialization
    $(document).on("click", function (e) {
        $(".m-menu__nav .m-menu__item.m-menu__item--submenu.m-menu__item--hover[data-menu-submenu-toggle=\"click\"]").each(function () {
            var element = $(this).parent(".m-menu__nav").parent();
            var menu = element.mMenu();

            if (menu.getSubmenuMode() !== "dropdown") {
                return;
            }

            if ($(e.target).is(element) == false && element.find($(e.target)).length == 0) {
                var items = element.find(".m-menu__item--submenu.m-menu__item--hover[data-menu-submenu-toggle=\"click\"]");
                items.each(function () {
                    menu.hideDropdown($(this));
                });
            }
        });
    });
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/components/general/offcanvas.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

(function ($) {
    // plugin setup
    $.fn.mOffcanvas = function (options) {
        // main object
        var offcanvas = this;
        var element = $(this);

        /********************
         ** PRIVATE METHODS
         ********************/
        var Plugin = {
            /**
             * Run
             */
            run: function run(options) {
                if (!element.data('offcanvas')) {
                    // create instance
                    Plugin.init(options);
                    Plugin.build();

                    // assign instance to the element                    
                    element.data('offcanvas', offcanvas);
                } else {
                    // get instance from the element
                    offcanvas = element.data('offcanvas');
                }

                return offcanvas;
            },

            /**
             * Handles suboffcanvas click toggle
             */
            init: function init(options) {
                offcanvas.events = [];

                // merge default and user defined options
                offcanvas.options = $.extend(true, {}, $.fn.mOffcanvas.defaults, options);

                offcanvas.overlay;

                offcanvas.classBase = offcanvas.options.class;
                offcanvas.classShown = offcanvas.classBase + '--on';
                offcanvas.classOverlay = offcanvas.classBase + '-overlay';

                offcanvas.state = element.hasClass(offcanvas.classShown) ? 'shown' : 'hidden';
                offcanvas.close = offcanvas.options.close;

                if (offcanvas.options.toggle && offcanvas.options.toggle.target) {
                    offcanvas.toggleTarget = offcanvas.options.toggle.target;
                    offcanvas.toggleState = offcanvas.options.toggle.state;
                } else {
                    offcanvas.toggleTarget = offcanvas.options.toggle;
                    offcanvas.toggleState = '';
                }
            },

            /**
             * Setup offcanvas
             */
            build: function build() {
                // offcanvas toggle
                $(offcanvas.toggleTarget).on('click', Plugin.toggle);

                if (offcanvas.close) {
                    $(offcanvas.close).on('click', Plugin.hide);
                }
            },

            /**
             * sync 
             */
            sync: function sync() {
                $(element).data('offcanvas', offcanvas);
            },

            /**
             * Handles offcanvas click toggle
             */
            toggle: function toggle() {
                if (offcanvas.state == 'shown') {
                    Plugin.hide();
                } else {
                    Plugin.show();
                }
            },

            /**
             * Handles offcanvas click toggle
             */
            show: function show() {
                if (offcanvas.state == 'shown') {
                    return;
                }

                Plugin.eventTrigger('beforeShow');

                if (offcanvas.toggleState != '') {
                    $(offcanvas.toggleTarget).addClass(offcanvas.toggleState);
                }

                $('body').addClass(offcanvas.classShown);
                element.addClass(offcanvas.classShown);

                offcanvas.state = 'shown';

                if (offcanvas.options.overlay) {
                    var overlay = $('<div class="' + offcanvas.classOverlay + '"></div>');
                    element.after(overlay);
                    offcanvas.overlay = overlay;
                    offcanvas.overlay.on('click', function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        Plugin.hide();
                    });
                }

                Plugin.eventTrigger('afterShow');

                return offcanvas;
            },

            /**
             * Handles offcanvas click toggle
             */
            hide: function hide() {
                if (offcanvas.state == 'hidden') {
                    return;
                }

                Plugin.eventTrigger('beforeHide');

                if (offcanvas.toggleState != '') {
                    $(offcanvas.toggleTarget).removeClass(offcanvas.toggleState);
                }

                $('body').removeClass(offcanvas.classShown);
                element.removeClass(offcanvas.classShown);

                offcanvas.state = 'hidden';

                if (offcanvas.options.overlay) {
                    offcanvas.overlay.remove();
                }

                Plugin.eventTrigger('afterHide');

                return offcanvas;
            },

            /**
             * Trigger events
             */
            eventTrigger: function eventTrigger(name) {
                for (i = 0; i < offcanvas.events.length; i++) {
                    var event = offcanvas.events[i];
                    if (event.name == name) {
                        if (event.one == true) {
                            if (event.fired == false) {
                                offcanvas.events[i].fired = true;
                                return event.handler.call(this, offcanvas);
                            }
                        } else {
                            return event.handler.call(this, offcanvas);
                        }
                    }
                }
            },

            addEvent: function addEvent(name, handler, one) {
                offcanvas.events.push({
                    name: name,
                    handler: handler,
                    one: one,
                    fired: false
                });

                Plugin.sync();
            }
        };

        // main variables
        var the = this;

        // init plugin
        Plugin.run.apply(this, [options]);

        /********************
         ** PUBLIC API METHODS
         ********************/

        /**
         * Hide 
         */
        offcanvas.hide = function () {
            return Plugin.hide();
        };

        /**
         * Show 
         */
        offcanvas.show = function () {
            return Plugin.show();
        };

        /**
         * Get suboffcanvas mode
         */
        offcanvas.on = function (name, handler) {
            return Plugin.addEvent(name, handler);
        };

        /**
         * Set offcanvas content
         * @returns {mOffcanvas}
         */
        offcanvas.one = function (name, handler) {
            return Plugin.addEvent(name, handler, true);
        };

        return offcanvas;
    };

    // default options
    $.fn.mOffcanvas.defaults = {};
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/components/general/quicksearch.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

(function ($) {
    // Plugin function
    $.fn.mQuicksearch = function (options) {

        // Plugin scope variables
        var qs = this;
        var element = $(this);

        // Plugin class        
        var Plugin = {
            /**
             * Run plugin 
             */
            run: function run(options) {
                if (!element.data('qs')) {
                    // init plugin
                    Plugin.init(options);
                    // build dom
                    Plugin.build();
                    // store the instance in the element's data
                    element.data('qs', qs);
                } else {
                    // retrieve the instance fro the element's data
                    qs = element.data('qs');
                }

                return qs;
            },

            /**
             * Init plugin
             */
            init: function init(options) {
                // merge default and user defined options
                qs.options = $.extend(true, {}, $.fn.mQuicksearch.defaults, options);

                // form
                qs.form = element.find('form');

                // input element
                qs.input = $(qs.options.input);

                // close icon
                qs.iconClose = $(qs.options.iconClose);

                if (qs.options.type == 'default') {
                    // search icon
                    qs.iconSearch = $(qs.options.iconSearch);

                    // cancel icon
                    qs.iconCancel = $(qs.options.iconCancel);
                }

                // dropdown
                qs.dropdown = element.mDropdown({ mobileOverlay: false });

                // cancel search timeout
                qs.cancelTimeout;

                // ajax processing state
                qs.processing = false;
            },

            /**
             * Build plugin
             */
            build: function build() {
                // attach input keyup handler
                qs.input.keyup(Plugin.handleSearch);

                if (qs.options.type == 'default') {
                    qs.input.focus(Plugin.showDropdown);

                    qs.iconCancel.click(Plugin.handleCancel);

                    qs.iconSearch.click(function () {
                        if (mUtil.isInResponsiveRange('tablet-and-mobile')) {
                            $('body').addClass('m-header-search--mobile-expanded');
                            qs.input.focus();
                        }
                    });

                    qs.iconClose.click(function () {
                        if (mUtil.isInResponsiveRange('tablet-and-mobile')) {
                            $('body').removeClass('m-header-search--mobile-expanded');
                            Plugin.closeDropdown();
                        }
                    });
                } else if (qs.options.type == 'dropdown') {
                    qs.dropdown.on('afterShow', function () {
                        qs.input.focus();
                    });
                    qs.iconClose.click(Plugin.closeDropdown);
                }
            },

            /**
             * Search handler
             */
            handleSearch: function handleSearch(e) {
                var query = qs.input.val();

                if (query.length === 0) {
                    qs.dropdown.hide();
                    Plugin.handleCancelIconVisibility('on');
                    Plugin.closeDropdown();
                    element.removeClass(qs.options.hasResultClass);
                }

                if (query.length < qs.options.minLength || qs.processing == true) {
                    return;
                }

                qs.processing = true;
                qs.form.addClass(qs.options.spinner);
                Plugin.handleCancelIconVisibility('off');

                $.ajax({
                    url: qs.options.source,
                    data: { query: query },
                    dataType: 'html',
                    success: function success(res) {
                        qs.processing = false;
                        qs.form.removeClass(qs.options.spinner);
                        Plugin.handleCancelIconVisibility('on');
                        qs.dropdown.setContent(res).show();
                        element.addClass(qs.options.hasResultClass);
                    },
                    error: function error(res) {
                        qs.processing = false;
                        qs.form.removeClass(qs.options.spinner);
                        Plugin.handleCancelIconVisibility('on');
                        qs.dropdown.setContent(qs.options.templates.error.apply(qs, res)).show();
                        element.addClass(qs.options.hasResultClass);
                    }
                });
            },

            /**
             * Handle cancel icon visibility
             */
            handleCancelIconVisibility: function handleCancelIconVisibility(status) {
                if (qs.options.type == 'dropdown') {
                    return;
                }

                if (status == 'on') {
                    if (qs.input.val().length === 0) {
                        qs.iconCancel.css('visibility', 'hidden');
                        qs.iconClose.css('visibility', 'hidden');
                    } else {
                        clearTimeout(qs.cancelTimeout);
                        qs.cancelTimeout = setTimeout(function () {
                            qs.iconCancel.css('visibility', 'visible');
                            qs.iconClose.css('visibility', 'visible');
                        }, 500);
                    }
                } else {
                    qs.iconCancel.css('visibility', 'hidden');
                    qs.iconClose.css('visibility', 'hidden');
                }
            },

            /**
             * Cancel handler
             */
            handleCancel: function handleCancel(e) {
                qs.input.val('');
                qs.iconCancel.css('visibility', 'hidden');
                element.removeClass(qs.options.hasResultClass);
                //qs.input.focus();

                Plugin.closeDropdown();
            },

            /**
             * Cancel handler
             */
            closeDropdown: function closeDropdown() {
                qs.dropdown.hide();
            },

            /**
             * Show dropdown
             */
            showDropdown: function showDropdown(e) {
                if (qs.dropdown.isShown() == false && qs.input.val().length > qs.options.minLength && qs.processing == false) {
                    qs.dropdown.show();
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        };

        // Run plugin
        Plugin.run.apply(qs, [options]);

        //////////////////////
        // ** Public API ** //
        //////////////////////

        /**
         * Public method
         * @returns {mQuicksearch}
         */
        qs.test = function (time) {
            //Plugin.method(time);
        };

        // Return plugin object
        return qs;
    };

    // Plugin default options
    $.fn.mQuicksearch.defaults = {
        minLength: 1,
        maxHeight: 300
    };
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/components/general/scroll-top.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

(function ($) {
    // plugin setup
    $.fn.mScrollTop = function (options) {
        // main object
        var scrollTop = this;
        var element = $(this);

        /********************
         ** PRIVATE METHODS
         ********************/
        var Plugin = {
            /**
             * Run
             */
            run: function run(options) {
                if (!element.data('scrollTop')) {
                    // create instance
                    Plugin.init(options);
                    Plugin.build();

                    // assign instance to the element                    
                    element.data('scrollTop', scrollTop);
                } else {
                    // get instance from the element
                    scrollTop = element.data('scrollTop');
                }

                return scrollTop;
            },

            /**
             * Handles subscrollTop click scrollTop
             */
            init: function init(options) {
                scrollTop.element = element;
                scrollTop.events = [];

                // merge default and user defined options
                scrollTop.options = $.extend(true, {}, $.fn.mScrollTop.defaults, options);
            },

            /**
             * Setup scrollTop
             */
            build: function build() {
                // handle window scroll
                if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
                    $(window).bind("touchend touchcancel touchleave", function () {
                        Plugin.handle();
                    });
                } else {
                    $(window).scroll(function () {
                        Plugin.handle();
                    });
                }

                // handle button click 
                element.on('click', Plugin.scroll);
            },

            /**
             * sync 
             */
            sync: function sync() {
                $(element).data('scrollTop', scrollTop);
            },

            /**
             * Handles offcanvas click scrollTop
             */
            handle: function handle() {
                var pos = $(window).scrollTop(); // current vertical position
                if (pos > scrollTop.options.offset) {
                    $("body").addClass('m-scroll-top--shown');
                } else {
                    $("body").removeClass('m-scroll-top--shown');
                }
            },

            /**
             * Handles offcanvas click scrollTop
             */
            scroll: function scroll(e) {
                e.preventDefault();

                $("html, body").animate({
                    scrollTop: 0
                }, scrollTop.options.speed);
            },

            /**
             * Trigger events
             */
            eventTrigger: function eventTrigger(name) {
                for (i = 0; i < scrollTop.events.length; i++) {
                    var event = scrollTop.events[i];
                    if (event.name == name) {
                        if (event.one == true) {
                            if (event.fired == false) {
                                scrollTop.events[i].fired = true;
                                return event.handler.call(this, scrollTop);
                            }
                        } else {
                            return event.handler.call(this, scrollTop);
                        }
                    }
                }
            },

            addEvent: function addEvent(name, handler, one) {
                scrollTop.events.push({
                    name: name,
                    handler: handler,
                    one: one,
                    fired: false
                });

                Plugin.sync();
            }
        };

        // main variables
        var the = this;

        // init plugin
        Plugin.run.apply(this, [options]);

        /********************
         ** PUBLIC API METHODS
         ********************/

        /**
         * Get subscrollTop mode
         */
        scrollTop.on = function (name, handler) {
            return Plugin.addEvent(name, handler);
        };

        /**
         * Set scrollTop content
         * @returns {mScrollTop}
         */
        scrollTop.one = function (name, handler) {
            return Plugin.addEvent(name, handler, true);
        };

        return scrollTop;
    };

    // default options
    $.fn.mScrollTop.defaults = {
        offset: 300,
        speed: 600
    };
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/framework/components/general/toggle.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

var _util = __webpack_require__("./resources/assets/domain/common/theme/js/framework/base/util.js");

(function ($) {
    // plugin setup
    $.fn.mToggle = function (options) {
        // main object
        var _toggle = this;
        var element = $(this);

        /********************
         ** PRIVATE METHODS
         ********************/
        var Plugin = {
            /**
             * Run
             */
            run: function run(options) {
                if (!element.data('toggle')) {
                    // create instance
                    Plugin.init(options);
                    Plugin.build();

                    // assign instance to the element                    
                    element.data('toggle', _toggle);
                } else {
                    // get instance from the element
                    _toggle = element.data('toggle');
                }

                return _toggle;
            },

            /**
             * Handles subtoggle click toggle
             */
            init: function init(options) {
                _toggle.element = element;
                _toggle.events = [];

                // merge default and user defined options
                _toggle.options = $.extend(true, {}, $.fn.mToggle.defaults, options);

                _toggle.target = $(_toggle.options.target);
                _toggle.targetState = _toggle.options.targetState;
                _toggle.togglerState = _toggle.options.togglerState;

                _toggle.state = _util.mUtil.hasClasses(_toggle.target, _toggle.targetState) ? 'on' : 'off';
            },

            /**
             * Setup toggle
             */
            build: function build() {
                element.on('click', Plugin.toggle);
            },

            /**
             * sync 
             */
            sync: function sync() {
                $(element).data('toggle', _toggle);
            },

            /**
             * Handles offcanvas click toggle
             */
            toggle: function toggle() {
                if (_toggle.state == 'off') {
                    Plugin.toggleOn();
                } else {
                    Plugin.toggleOff();
                }
                Plugin.eventTrigger('toggle');

                return _toggle;
            },

            /**
             * Handles toggle click toggle
             */
            toggleOn: function toggleOn() {
                Plugin.eventTrigger('beforeOn');

                _toggle.target.addClass(_toggle.targetState);

                if (_toggle.togglerState) {
                    element.addClass(_toggle.togglerState);
                }

                _toggle.state = 'on';

                Plugin.eventTrigger('afterOn');

                return _toggle;
            },

            /**
             * Handles toggle click toggle
             */
            toggleOff: function toggleOff() {
                Plugin.eventTrigger('beforeOff');

                _toggle.target.removeClass(_toggle.targetState);

                if (_toggle.togglerState) {
                    element.removeClass(_toggle.togglerState);
                }

                _toggle.state = 'off';

                Plugin.eventTrigger('afterOff');

                return _toggle;
            },

            /**
             * Trigger events
             */
            eventTrigger: function eventTrigger(name) {
                _toggle.trigger(name);
                for (var i = 0; i < _toggle.events.length; i++) {
                    var event = _toggle.events[i];
                    if (event.name == name) {
                        if (event.one == true) {
                            if (event.fired == false) {
                                _toggle.events[i].fired = true;
                                return event.handler.call(this, _toggle);
                            }
                        } else {
                            return event.handler.call(this, _toggle);
                        }
                    }
                }
            },

            addEvent: function addEvent(name, handler, one) {
                _toggle.events.push({
                    name: name,
                    handler: handler,
                    one: one,
                    fired: false
                });

                Plugin.sync();

                return _toggle;
            }
        };

        // main variables
        var the = this;

        // init plugin
        Plugin.run.apply(this, [options]);

        /********************
         ** PUBLIC API METHODS
         ********************/

        /**
         * Toggle 
         */
        _toggle.toggle = function () {
            return Plugin.toggle();
        };

        /**
         * Toggle on 
         */
        _toggle.toggleOn = function () {
            return Plugin.toggleOn();
        };

        /**
         * Toggle off 
         */
        _toggle.toggleOff = function () {
            return Plugin.toggleOff();
        };

        /**
         * Attach event
         * @returns {mToggle}
         */
        _toggle.on = function (name, handler) {
            return Plugin.addEvent(name, handler);
        };

        /**
         * Attach event that will be fired once
         * @returns {mToggle}
         */
        _toggle.one = function (name, handler) {
            return Plugin.addEvent(name, handler, true);
        };

        return _toggle;
    };

    // default options
    $.fn.mToggle.defaults = {
        togglerState: '',
        targetState: ''
    };
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./resources/assets/domain/common/theme/js/snippets/base/quick-sidebar.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
var mQuickSidebar = exports.mQuickSidebar = function () {
    var topbarAside = $('#m_quick_sidebar');
    var topbarAsideTabs = $('#m_quick_sidebar_tabs');
    var topbarAsideClose = $('#m_quick_sidebar_close');
    var topbarAsideToggle = $('#m_quick_sidebar_toggle');
    var topbarAsideContent = topbarAside.find('.m-quick-sidebar__content');

    var initMessages = function initMessages() {
        var init = function init() {
            var messenger = $('#m_quick_sidebar_tabs_messenger');
            var messengerMessages = messenger.find('.m-messenger__messages');

            var height = topbarAside.outerHeight(true) - topbarAsideTabs.outerHeight(true) - messenger.find('.m-messenger__form').outerHeight(true) - 120;

            // init messages scrollable content
            messengerMessages.css('height', height);
            mApp.initScroller(messengerMessages, {});
        };

        init();

        // reinit on window resize
        mUtil.addResizeHandler(init);
    };

    var initSettings = function initSettings() {
        // init dropdown tabbable content
        var init = function init() {
            var settings = $('#m_quick_sidebar_tabs_settings');
            var height = mUtil.getViewPort().height - topbarAsideTabs.outerHeight(true) - 60;

            // init settings scrollable content
            settings.css('height', height);
            mApp.initScroller(settings, {});
        };

        init();

        // reinit on window resize
        mUtil.addResizeHandler(init);
    };

    var initLogs = function initLogs() {
        // init dropdown tabbable content
        var init = function init() {
            var logs = $('#m_quick_sidebar_tabs_logs');
            var height = mUtil.getViewPort().height - topbarAsideTabs.outerHeight(true) - 60;

            // init settings scrollable content
            logs.css('height', height);
            mApp.initScroller(logs, {});
        };

        init();

        // reinit on window resize
        mUtil.addResizeHandler(init);
    };

    var initOffcanvasTabs = function initOffcanvasTabs() {
        initMessages();
        initSettings();
        initLogs();
    };

    var initOffcanvas = function initOffcanvas() {
        topbarAside.mOffcanvas({
            class: 'm-quick-sidebar',
            overlay: true,
            close: topbarAsideClose,
            toggle: topbarAsideToggle
        });

        // run once on first time dropdown shown
        topbarAside.mOffcanvas().one('afterShow', function () {
            mApp.block(topbarAside);

            setTimeout(function () {
                mApp.unblock(topbarAside);

                topbarAsideContent.removeClass('m--hide');

                initOffcanvasTabs();
            }, 1000);
        });
    };

    return {
        init: function init() {
            initOffcanvas();
        }
    };
}();

// $(document).ready(function() {
//     mQuickSidebar.init();
// });
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./resources/assets/domain/admin/js/theme.js");
__webpack_require__("./resources/assets/domain/admin/sass/vendor.scss");
module.exports = __webpack_require__("./resources/assets/domain/admin/sass/app.scss");


/***/ })

},[0]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9hZG1pbi9qcy90aGVtZS5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9hZG1pbi9zYXNzL2FwcC5zY3NzPzQ4MTciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vYWRtaW4vc2Fzcy92ZW5kb3Iuc2Nzcz8xMDE3Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvZG9tYWluL2NvbW1vbi90aGVtZS9qcy9kZW1vL2RlZmF1bHQvYmFzZS9sYXlvdXQuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9iYXNlL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2Jhc2UvdXRpbC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9kcm9wZG93bi5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9oZWFkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvbWVudS5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9vZmZjYW52YXMuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvcXVpY2tzZWFyY2guanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvc2Nyb2xsLXRvcC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC90b2dnbGUuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL3NuaXBwZXRzL2Jhc2UvcXVpY2stc2lkZWJhci5qcyJdLCJuYW1lcyI6WyJyZXF1aXJlIiwiJCIsImRvY3VtZW50IiwicmVhZHkiLCJtVXRpbCIsImluaXQiLCJtQXBwIiwibUxheW91dCIsIm1RdWlja1NpZGViYXIiLCJob3JNZW51IiwiYXNpZGVNZW51IiwiYXNpZGVNZW51T2ZmY2FudmFzIiwiaG9yTWVudU9mZmNhbnZhcyIsImluaXRTdGlja3lIZWFkZXIiLCJoZWFkZXIiLCJvcHRpb25zIiwib2Zmc2V0IiwibWluaW1pemUiLCJkYXRhIiwibW9iaWxlIiwib24iLCJvZmYiLCJkZXNrdG9wIiwibUhlYWRlciIsImluaXRIb3JNZW51IiwibU9mZmNhbnZhcyIsImNsYXNzIiwib3ZlcmxheSIsImNsb3NlIiwidG9nZ2xlIiwidGFyZ2V0Iiwic3RhdGUiLCJtTWVudSIsInN1Ym1lbnUiLCJ0YWJsZXQiLCJyZXNpemUiLCJoZWFkZXJOYXZXaWR0aCIsIndpZHRoIiwiaGVhZGVyTWVudVdpZHRoIiwiaGVhZGVyVG9wYmFyV2lkdGgiLCJzcGFyZVdpZHRoIiwiaW5pdExlZnRBc2lkZU1lbnUiLCJtZW51IiwibWVudU9wdGlvbnMiLCJkZWZhdWx0IiwiYm9keSIsIm1vZGUiLCJhY2NvcmRpb24iLCJhdXRvU2Nyb2xsIiwiZXhwYW5kQWxsIiwiaW5pdFNjcm9sbGFibGVNZW51Iiwib2JqIiwiaXNJblJlc3BvbnNpdmVSYW5nZSIsImRlc3Ryb3lTY3JvbGxlciIsImhlaWdodCIsImdldFZpZXdQb3J0Iiwib3V0ZXJIZWlnaHQiLCJsZW5ndGgiLCJpbml0U2Nyb2xsZXIiLCJhZGRSZXNpemVIYW5kbGVyIiwiaW5pdExlZnRBc2lkZSIsImFzaWRlT2ZmY2FudmFzQ2xhc3MiLCJoYXNDbGFzcyIsImluaXRMZWZ0QXNpZGVUb2dnbGUiLCJhc2lkZUxlZnRUb2dnbGUiLCJtVG9nZ2xlIiwidGFyZ2V0U3RhdGUiLCJ0b2dnbGVyU3RhdGUiLCJwYXVzZURyb3Bkb3duSG92ZXIiLCJpbml0VG9wYmFyIiwiY2xpY2siLCJ0b2dnbGVDbGFzcyIsInNldEludGVydmFsIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImluaXRRdWlja3NlYXJjaCIsInFzIiwibVF1aWNrc2VhcmNoIiwidHlwZSIsInNvdXJjZSIsInNwaW5uZXIiLCJpbnB1dCIsImljb25DbG9zZSIsImljb25DYW5jZWwiLCJpY29uU2VhcmNoIiwiaGFzUmVzdWx0Q2xhc3MiLCJtaW5MZW5ndGgiLCJ0ZW1wbGF0ZXMiLCJlcnJvciIsImluaXRTY3JvbGxUb3AiLCJtU2Nyb2xsVG9wIiwic3BlZWQiLCJpbml0SGVhZGVyIiwiaW5pdEFzaWRlIiwib25MZWZ0U2lkZWJhclRvZ2dsZSIsImUiLCJkYXRhdGFibGVzIiwiZWFjaCIsIm1EYXRhdGFibGUiLCJnZXRBc2lkZU1lbnUiLCJmdW5jIiwiY2xvc2VNb2JpbGVBc2lkZU1lbnVPZmZjYW52YXMiLCJpc01vYmlsZURldmljZSIsImhpZGUiLCJjbG9zZU1vYmlsZUhvck1lbnVPZmZjYW52YXMiLCJpbml0VG9vbHRpcCIsImVsIiwic2tpbiIsInRvb2x0aXAiLCJ0cmlnZ2VyIiwidGVtcGxhdGUiLCJpbml0VG9vbHRpcHMiLCJpbml0UG9wb3ZlciIsInRyaWdnZXJWYWx1ZSIsInBvcG92ZXIiLCJpbml0UG9wb3ZlcnMiLCJpbml0RmlsZUlucHV0IiwiZmlsZU5hbWUiLCJ2YWwiLCJuZXh0IiwiaHRtbCIsImluaXRQb3J0bGV0IiwibVBvcnRsZXQiLCJpbml0UG9ydGxldHMiLCJpbml0U2Nyb2xsYWJsZXMiLCJtYXhIZWlnaHQiLCJjc3MiLCJpbml0QWxlcnRzIiwiY2xvc2VzdCIsImluaXRBY2NvcmRpb25zIiwiaGlkZVRvdWNoV2FybmluZyIsImpRdWVyeSIsImV2ZW50Iiwic3BlY2lhbCIsInRvdWNoc3RhcnQiLCJzZXR1cCIsIl8iLCJucyIsImhhbmRsZSIsImluY2x1ZGVzIiwiYWRkRXZlbnRMaXN0ZW5lciIsInBhc3NpdmUiLCJ0b3VjaG1vdmUiLCJ3aGVlbCIsImluaXRDb21wb25lbnRzIiwic2Nyb2xsVG8iLCJwb3MiLCJ0b3AiLCJhbmltYXRlIiwic2Nyb2xsVG9wIiwic2Nyb2xsVG9WaWV3cG9ydCIsImVsT2Zmc2V0IiwiZWxIZWlnaHQiLCJ3aW5kb3dIZWlnaHQiLCJtQ3VzdG9tU2Nyb2xsYmFyIiwic2Nyb2xsSW5lcnRpYSIsImF1dG9EcmFnZ2VyTGVuZ3RoIiwiYXV0b0hpZGVTY3JvbGxiYXIiLCJhdXRvRXhwYW5kU2Nyb2xsYmFyIiwiYWx3YXlzU2hvd1Njcm9sbGJhciIsImF4aXMiLCJtb3VzZVdoZWVsIiwic2Nyb2xsQW1vdW50IiwicHJldmVudERlZmF1bHQiLCJzZXRIZWlnaHQiLCJ0aGVtZSIsImFsZXJ0IiwiZXh0ZW5kIiwiY29udGFpbmVyIiwicGxhY2UiLCJtZXNzYWdlIiwicmVzZXQiLCJmb2N1cyIsImNsb3NlSW5TZWNvbmRzIiwiaWNvbiIsImlkIiwiZ2V0VW5pcXVlSUQiLCJyZW1vdmUiLCJzaXplIiwicHJlcGVuZCIsImFmdGVyIiwiYXBwZW5kIiwic2V0VGltZW91dCIsImJsb2NrIiwib3BhY2l0eSIsIm92ZXJsYXlDb2xvciIsImNlbnRlclgiLCJjZW50ZXJZIiwic2hhZG93IiwibG9hZGluZyIsImNsYXNzZXMiLCJyZWFsV2lkdGgiLCJwYXJhbXMiLCJsZWZ0IiwiYm9yZGVyIiwicGFkZGluZyIsImJhY2tncm91bmRDb2xvciIsIm92ZXJsYXlDU1MiLCJjdXJzb3IiLCJ6SW5kZXgiLCJvblVuYmxvY2siLCJibG9ja1VJIiwidW5ibG9jayIsInVuYmxvY2tVSSIsImJsb2NrUGFnZSIsInVuYmxvY2tQYWdlIiwicHJvZ3Jlc3MiLCJhbGlnbm1lbnQiLCJ1bnByb2dyZXNzIiwicmVzaXplSGFuZGxlcnMiLCJicmVha3BvaW50cyIsInNtIiwibWQiLCJsZyIsInhsIiwiY29sb3JzIiwiYnJhbmQiLCJtZXRhbCIsImxpZ2h0IiwiYWNjZW50IiwicHJpbWFyeSIsInN1Y2Nlc3MiLCJpbmZvIiwid2FybmluZyIsImRhbmdlciIsIl93aW5kb3dSZXNpemVIYW5kbGVyIiwiX3J1blJlc2l6ZUhhbmRsZXJzIiwiaSIsImNhbGwiLCJ3aW5kb3ciLCJjbGVhclRpbWVvdXQiLCJjYWxsYmFjayIsInB1c2giLCJydW5SZXNpemVIYW5kbGVycyIsImdldFVSTFBhcmFtIiwicGFyYW1OYW1lIiwic2VhcmNoU3RyaW5nIiwibG9jYXRpb24iLCJzZWFyY2giLCJzdWJzdHJpbmciLCJzcGxpdCIsInVuZXNjYXBlIiwiZ2V0QnJlYWtwb2ludCIsImlzRGVza3RvcERldmljZSIsImEiLCJkb2N1bWVudEVsZW1lbnQiLCJicmVha3BvaW50IiwicHJlZml4IiwiTWF0aCIsImZsb29yIiwicmFuZG9tIiwiRGF0ZSIsImdldFRpbWUiLCJpbkFycmF5IiwiaXNzZXQiLCJrZXlzIiwic3RvbmUiLCJpbmRleE9mIiwiRXJyb3IiLCJ1bmRlZmluZWQiLCJzaGlmdCIsImhhc093blByb3BlcnR5IiwiZ2V0SGlnaGVzdFppbmRleCIsImVsZW0iLCJwb3NpdGlvbiIsInZhbHVlIiwicGFyc2VJbnQiLCJpc05hTiIsInBhcmVudCIsImhhc0NsYXNzZXMiLCJjbGFzc2VzQXJyIiwiY2xvbmUiLCJvdXRlcldpZHRoIiwiaGFzRml4ZWRQb3NpdGlvbmVkUGFyZW50IiwicmVzdWx0IiwicGFyZW50cyIsInNsZWVwIiwibWlsbGlzZWNvbmRzIiwic3RhcnQiLCJnZXRSYW5kb21JbnQiLCJtaW4iLCJtYXgiLCJnZXRDb2xvciIsIm5hbWUiLCJpc0FuZ3VsYXJWZXJzaW9uIiwiWm9uZSIsImZuIiwibURyb3Bkb3duIiwiZHJvcGRvd24iLCJlbGVtZW50IiwiUGx1Z2luIiwicnVuIiwiYnVpbGQiLCJldmVudHMiLCJldmVudE9uZSIsImZpbmQiLCJhcnJvdyIsIndyYXBwZXIiLCJzY3JvbGxhYmxlIiwiZGVmYXVsdERyb3BQb3MiLCJjdXJyZW50RHJvcFBvcyIsImRlZmF1bHRzIiwiZHJvcEF1dG8iLCJtaW5IZWlnaHQiLCJtb3VzZWxlYXZlIiwibW91c2VlbnRlciIsInNob3ciLCJkaXNhYmxlQ2xvc2UiLCJwbGFjZW1lbnQiLCJhbGlnbiIsInBlcnNpc3RlbnQiLCJzZXRaaW5kZXgiLCJzeW5jIiwic3RvcFByb3BhZ2F0aW9uIiwib3BlbiIsInNldENvbnRlbnQiLCJjb250ZW50IiwiY2xlYXJIb3ZlcmVkIiwiYWRqdXN0QXJyb3dQb3MiLCJldmVudFRyaWdnZXIiLCJoaWRlT3BlbmVkIiwibW9iaWxlT3ZlcmxheSIsImRyb3Bkb3dub2ZmIiwiYXR0ciIsImhhbmRsZURyb3BQb3NpdGlvbiIsInJlbW92ZURhdGEiLCJ0aW1lb3V0IiwiaGlkZUhvdmVyZWQiLCJmb3JjZSIsImhvdmVyVGltZW91dCIsImhpZGVDbGlja2VkIiwicHJlcGVuZFRvIiwiYWJzIiwiaXNJblZlcnRpY2FsVmlld3BvcnQiLCJhcHBlbmRUbyIsIm9sZFppbmRleCIsIm5ld1ppbmRleCIsInppbmRleCIsImlzUGVyc2lzdGVudCIsImlzU2hvd24iLCJvbmUiLCJmaXJlZCIsImhhbmRsZXIiLCJhZGRFdmVudCIsImFwcGx5IiwiZml4RHJvcFBvc2l0aW9uIiwiaXMiLCJsYXN0U2Nyb2xsVG9wIiwic2Nyb2xsIiwic3QiLCJjbGFzc2ljIiwicHVibGljTWV0aG9kIiwicmVpbml0IiwicGF1c2VEcm9wZG93bkhvdmVyVGltZSIsImhhbmRsZVN1Ym1lbnVBY2NvcmRpb24iLCJnZXRTdWJtZW51TW9kZSIsImlzQ29uZGl0aW9uYWxTdWJtZW51RHJvcGRvd24iLCJoYW5kbGVTdWJtZW51RHJvZG93bkhvdmVyRW50ZXIiLCJoYW5kbGVTdWJtZW51RHJvZG93bkhvdmVyRXhpdCIsImhhbmRsZVN1Ym1lbnVEcm9wZG93bkNsaWNrIiwiaGFuZGxlTGlua0NsaWNrIiwiaGFuZGxlU3VibWVudURyb3Bkb3duQ2xvc2UiLCJyZXN1bWVEcm9wZG93bkhvdmVyIiwiaXRlbSIsInNob3dTdWJtZW51RHJvcGRvd24iLCJoaWRlU3VibWVudURyb3Bkb3duIiwidGltZSIsInNob3duIiwibGkiLCJjaGlsZHJlbiIsInNsaWRlU3BlZWQiLCJoYXNDbG9zYWJsZXMiLCJjbG9zYWJsZXMiLCJzbGlkZVVwIiwic2Nyb2xsVG9JdGVtIiwic2xpZGVEb3duIiwiY2xhc3NBbHNvIiwiYWRqdXN0U3VibWVudURyb3Bkb3duQXJyb3dQb3MiLCJjdXJyZW50V2lkdGgiLCJpdGVtc051bWJlciIsImNoZWNrIiwibW92ZWQiLCJ0b3RhbCIsImVsZW1lbnRzTnVtYmVyIiwiZXEiLCJpdGVtcyIsImluZGV4IiwiZ2V0IiwiY3JlYXRlU3VibWVudURyb3Bkb3duQ2xpY2tEcm9wb2ZmIiwiZHJvcG9mZiIsInN1Ym5hdiIsImxpbmsiLCJkYXRlIiwicmVzZXRBY3RpdmVJdGVtIiwic2V0QWN0aXZlSXRlbSIsImdldEJyZWFkY3J1bWJzIiwiYnJlYWRjcnVtYnMiLCJ0ZXh0IiwidGl0bGUiLCJocmVmIiwic3VibWVudUxpbmsiLCJyZXZlcnNlIiwiZ2V0UGFnZVRpdGxlIiwiYXJncyIsImhpZGVEcm9wZG93biIsIm9mZmNhbnZhcyIsImNsYXNzQmFzZSIsImNsYXNzU2hvd24iLCJjbGFzc092ZXJsYXkiLCJ0b2dnbGVUYXJnZXQiLCJ0b2dnbGVTdGF0ZSIsInRoZSIsImZvcm0iLCJjYW5jZWxUaW1lb3V0IiwicHJvY2Vzc2luZyIsImtleXVwIiwiaGFuZGxlU2VhcmNoIiwic2hvd0Ryb3Bkb3duIiwiaGFuZGxlQ2FuY2VsIiwiY2xvc2VEcm9wZG93biIsInF1ZXJ5IiwiaGFuZGxlQ2FuY2VsSWNvblZpc2liaWxpdHkiLCJhamF4IiwidXJsIiwiZGF0YVR5cGUiLCJyZXMiLCJzdGF0dXMiLCJ0ZXN0IiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwibWF0Y2giLCJiaW5kIiwidG9nZ2xlT24iLCJ0b2dnbGVPZmYiLCJ0b3BiYXJBc2lkZSIsInRvcGJhckFzaWRlVGFicyIsInRvcGJhckFzaWRlQ2xvc2UiLCJ0b3BiYXJBc2lkZVRvZ2dsZSIsInRvcGJhckFzaWRlQ29udGVudCIsImluaXRNZXNzYWdlcyIsIm1lc3NlbmdlciIsIm1lc3Nlbmdlck1lc3NhZ2VzIiwiaW5pdFNldHRpbmdzIiwic2V0dGluZ3MiLCJpbml0TG9ncyIsImxvZ3MiLCJpbml0T2ZmY2FudmFzVGFicyIsImluaXRPZmZjYW52YXMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7QUFDQSxtQkFBQUEsQ0FBUSxvRkFBUjtBQUNBO0FBQ0EsbUJBQUFBLENBQVEsa0ZBQVI7QUFDQSxtQkFBQUEsQ0FBUSxnRkFBUjtBQUNBO0FBQ0EsbUJBQUFBLENBQVEscUZBQVI7QUFDQTtBQUNBLG1CQUFBQSxDQUFRLHVGQUFSO0FBQ0EsbUJBQUFBLENBQVEsc0ZBQVI7QUFDQSxtQkFBQUEsQ0FBUSxrRkFBUjtBQUNBOztBQUVBQyxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVztBQUN6QkMsZ0JBQU1DLElBQU47QUFDQUMsY0FBS0QsSUFBTDtBQUNBRSxvQkFBUUYsSUFBUjtBQUNBRyxnQ0FBY0gsSUFBZDtBQUNILENBTEQsRTs7Ozs7Ozs7QUNsQkEseUM7Ozs7Ozs7QUNBQSx5Qzs7Ozs7Ozs7Ozs7OztBQ0FPLElBQU1FLDRCQUFVLFlBQVc7QUFDOUIsUUFBSUUsT0FBSjtBQUNBLFFBQUlDLFNBQUo7QUFDQSxRQUFJQyxrQkFBSjtBQUNBLFFBQUlDLGdCQUFKOztBQUVBLFFBQUlDLG1CQUFtQixTQUFuQkEsZ0JBQW1CLEdBQVc7QUFDOUIsWUFBSUMsU0FBU2IsRUFBRSxXQUFGLENBQWI7QUFDQSxZQUFJYyxVQUFVO0FBQ1ZDLG9CQUFRLEVBREU7QUFFVkMsc0JBQVM7QUFGQyxTQUFkOztBQUtBLFlBQUlILE9BQU9JLElBQVAsQ0FBWSxpQkFBWixLQUFrQyxNQUF0QyxFQUE4QztBQUMxQ0gsb0JBQVFFLFFBQVIsQ0FBaUJFLE1BQWpCLEdBQTBCLEVBQTFCO0FBQ0FKLG9CQUFRRSxRQUFSLENBQWlCRSxNQUFqQixDQUF3QkMsRUFBeEIsR0FBNkIsZ0JBQTdCO0FBQ0FMLG9CQUFRRSxRQUFSLENBQWlCRSxNQUFqQixDQUF3QkUsR0FBeEIsR0FBOEIsZ0JBQTlCO0FBQ0gsU0FKRCxNQUlPO0FBQ0hOLG9CQUFRRSxRQUFSLENBQWlCRSxNQUFqQixHQUEwQixLQUExQjtBQUNIOztBQUVELFlBQUlMLE9BQU9JLElBQVAsQ0FBWSxVQUFaLEtBQTJCLE1BQS9CLEVBQXVDO0FBQ25DSCxvQkFBUUUsUUFBUixDQUFpQkssT0FBakIsR0FBMkIsRUFBM0I7QUFDQVAsb0JBQVFFLFFBQVIsQ0FBaUJLLE9BQWpCLENBQXlCRixFQUF6QixHQUE4QixnQkFBOUI7QUFDQUwsb0JBQVFFLFFBQVIsQ0FBaUJLLE9BQWpCLENBQXlCRCxHQUF6QixHQUErQixnQkFBL0I7QUFDSCxTQUpELE1BSU87QUFDSE4sb0JBQVFFLFFBQVIsQ0FBaUJLLE9BQWpCLEdBQTJCLEtBQTNCO0FBQ0g7O0FBRUQsWUFBSVIsT0FBT0ksSUFBUCxDQUFZLGlCQUFaLENBQUosRUFBb0M7QUFDaENILG9CQUFRQyxNQUFSLENBQWVNLE9BQWYsR0FBeUJSLE9BQU9JLElBQVAsQ0FBWSxpQkFBWixDQUF6QjtBQUNIOztBQUVELFlBQUlKLE9BQU9JLElBQVAsQ0FBWSx3QkFBWixDQUFKLEVBQTJDO0FBQ3ZDSCxvQkFBUUMsTUFBUixDQUFlRyxNQUFmLEdBQXdCTCxPQUFPSSxJQUFQLENBQVksd0JBQVosQ0FBeEI7QUFDSDs7QUFFREosZUFBT1MsT0FBUCxDQUFlUixPQUFmO0FBQ0gsS0FoQ0Q7O0FBa0NBO0FBQ0EsUUFBSVMsY0FBYyxTQUFkQSxXQUFjLEdBQVc7QUFDekI7QUFDQVosMkJBQW1CWCxFQUFFLGdCQUFGLEVBQW9Cd0IsVUFBcEIsQ0FBK0I7QUFDOUNDLG1CQUFPLDRCQUR1QztBQUU5Q0MscUJBQVMsSUFGcUM7QUFHOUNDLG1CQUFPLHVDQUh1QztBQUk5Q0Msb0JBQVE7QUFDSkMsd0JBQVEsb0NBREo7QUFFSkMsdUJBQU87QUFGSDtBQUpzQyxTQUEvQixDQUFuQjs7QUFVQXRCLGtCQUFVUixFQUFFLGdCQUFGLEVBQW9CK0IsS0FBcEIsQ0FBMEI7QUFDaEM7QUFDQUMscUJBQVM7QUFDTFgseUJBQVMsVUFESjtBQUVMWSx3QkFBUSxXQUZIO0FBR0xmLHdCQUFRO0FBSEgsYUFGdUI7QUFPaEM7QUFDQWdCLG9CQUFRO0FBQ0piLHlCQUFTLG1CQUFXO0FBQ2hCLHdCQUFJYyxpQkFBaUJuQyxFQUFFLGVBQUYsRUFBbUJvQyxLQUFuQixFQUFyQjtBQUNBLHdCQUFJQyxrQkFBa0JyQyxFQUFFLDBCQUFGLEVBQThCb0MsS0FBOUIsRUFBdEI7QUFDQSx3QkFBSUUsb0JBQW9CdEMsRUFBRSxrQkFBRixFQUFzQm9DLEtBQXRCLEVBQXhCO0FBQ0Esd0JBQUlHLGFBQWEsRUFBakI7O0FBRUEsd0JBQUtGLGtCQUFrQkMsaUJBQWxCLEdBQXNDQyxVQUF2QyxHQUFxREosY0FBekQsRUFBMEU7QUFDdEUsK0JBQU8sS0FBUDtBQUNILHFCQUZELE1BRU87QUFDSCwrQkFBTyxJQUFQO0FBQ0g7QUFDSjtBQVpHO0FBUndCLFNBQTFCLENBQVY7QUF1QkgsS0FuQ0Q7O0FBcUNBO0FBQ0EsUUFBSUssb0JBQW9CLFNBQXBCQSxpQkFBb0IsR0FBVztBQUMvQixZQUFJQyxPQUFPekMsRUFBRSxhQUFGLENBQVg7O0FBRUE7QUFDQSxZQUFJMEMsY0FBYztBQUNkO0FBQ0FWLHFCQUFTO0FBQ0xYLHlCQUFTO0FBQ0w7QUFDQXNCLDZCQUFVRixLQUFLeEIsSUFBTCxDQUFVLGVBQVYsS0FBOEIsSUFBOUIsR0FBcUMsVUFBckMsR0FBa0QsV0FGdkQ7QUFHTDtBQUNBYSwyQkFBTztBQUNIYyw4QkFBTSx3QkFESDtBQUVIQyw4QkFBTTtBQUZIO0FBSkYsaUJBREo7QUFVTFosd0JBQVEsV0FWSCxFQVVnQjtBQUNyQmYsd0JBQVEsV0FYSCxDQVdnQjtBQVhoQixhQUZLOztBQWdCZDtBQUNBNEIsdUJBQVc7QUFDUEMsNEJBQVksSUFETDtBQUVQQywyQkFBVztBQUZKO0FBakJHLFNBQWxCOztBQXVCQXZDLG9CQUFZZ0MsS0FBS1YsS0FBTCxDQUFXVyxXQUFYLENBQVo7O0FBRUE7QUFDQSxZQUFJRCxLQUFLeEIsSUFBTCxDQUFVLGlCQUFWLENBQUosRUFBa0M7QUFBQSxnQkFDckJnQyxrQkFEcUIsR0FDOUIsU0FBU0Esa0JBQVQsQ0FBNEJDLEdBQTVCLEVBQWlDO0FBQzdCLG9CQUFJL0MsTUFBTWdELG1CQUFOLENBQTBCLG1CQUExQixDQUFKLEVBQW9EO0FBQ2hEO0FBQ0E5Qyx5QkFBSytDLGVBQUwsQ0FBcUJGLEdBQXJCO0FBQ0E7QUFDSDs7QUFFRCxvQkFBSUcsU0FBU2xELE1BQU1tRCxXQUFOLEdBQW9CRCxNQUFwQixHQUE2QnJELEVBQUUsV0FBRixFQUFldUQsV0FBZixFQUE3QixJQUNOdkQsRUFBRSxnQ0FBRixFQUFvQ3dELE1BQXBDLElBQThDLENBQTlDLEdBQWtEeEQsRUFBRSxnQ0FBRixFQUFvQ3VELFdBQXBDLEVBQWxELEdBQXNHLENBRGhHLEtBRU52RCxFQUFFLGdDQUFGLEVBQW9Dd0QsTUFBcEMsSUFBOEMsQ0FBOUMsR0FBa0R4RCxFQUFFLGdDQUFGLEVBQW9DdUQsV0FBcEMsRUFBbEQsR0FBc0csQ0FGaEcsQ0FBYjtBQUdJOztBQUVKO0FBQ0FsRCxxQkFBS29ELFlBQUwsQ0FBa0JQLEdBQWxCLEVBQXVCLEVBQUNHLFFBQVFBLE1BQVQsRUFBdkI7QUFDSCxhQWY2Qjs7QUFpQjlCSiwrQkFBbUJ4QyxTQUFuQjs7QUFFQU4sa0JBQU11RCxnQkFBTixDQUF1QixZQUFXO0FBQzlCVCxtQ0FBbUJ4QyxTQUFuQjtBQUNILGFBRkQ7QUFHSDtBQUNKLEtBckREOztBQXVEQTtBQUNBLFFBQUlrRCxnQkFBZ0IsU0FBaEJBLGFBQWdCLEdBQVc7QUFDM0I7QUFDQSxZQUFJQyxzQkFBdUI1RCxFQUFFLGVBQUYsRUFBbUI2RCxRQUFuQixDQUE0QixpQ0FBNUIsSUFBaUUsaUNBQWpFLEdBQXFHLGNBQWhJOztBQUVBbkQsNkJBQXFCVixFQUFFLGVBQUYsRUFBbUJ3QixVQUFuQixDQUE4QjtBQUMvQ0MsbUJBQU9tQyxtQkFEd0M7QUFFL0NsQyxxQkFBUyxJQUZzQztBQUcvQ0MsbUJBQU8seUJBSHdDO0FBSS9DQyxvQkFBUTtBQUNKQyx3QkFBUSxnQ0FESjtBQUVKQyx1QkFBTztBQUZIO0FBSnVDLFNBQTlCLENBQXJCO0FBU0gsS0FiRDs7QUFlQTtBQUNBLFFBQUlnQyxzQkFBc0IsU0FBdEJBLG1CQUFzQixHQUFXO0FBQ2pDLFlBQUlDLGtCQUFrQi9ELEVBQUUsK0JBQUYsRUFBbUNnRSxPQUFuQyxDQUEyQztBQUM3RG5DLG9CQUFRLE1BRHFEO0FBRTdEb0MseUJBQWEsMENBRmdEO0FBRzdEQywwQkFBYztBQUgrQyxTQUEzQyxFQUluQi9DLEVBSm1CLENBSWhCLFFBSmdCLEVBSU4sWUFBVztBQUN2Qlgsb0JBQVEyRCxrQkFBUixDQUEyQixHQUEzQjtBQUNBMUQsc0JBQVUwRCxrQkFBVixDQUE2QixHQUE3QjtBQUNILFNBUHFCLENBQXRCOztBQVNBO0FBQ0E7O0FBRUFuRSxVQUFFLDJCQUFGLEVBQStCZ0UsT0FBL0IsQ0FBdUM7QUFDbkNuQyxvQkFBUSxNQUQyQjtBQUVuQ29DLHlCQUFhLG9CQUZzQjtBQUduQ0MsMEJBQWM7QUFIcUIsU0FBdkMsRUFJRy9DLEVBSkgsQ0FJTSxRQUpOLEVBSWdCLFlBQVc7QUFDdkJYLG9CQUFRMkQsa0JBQVIsQ0FBMkIsR0FBM0I7QUFDQTFELHNCQUFVMEQsa0JBQVYsQ0FBNkIsR0FBN0I7QUFDSCxTQVBEO0FBUUgsS0FyQkQ7O0FBdUJBLFFBQUlDLGFBQWEsU0FBYkEsVUFBYSxHQUFXO0FBQ3hCcEUsVUFBRSxzQ0FBRixFQUEwQ3FFLEtBQTFDLENBQWdELFlBQVc7QUFDdkRyRSxjQUFFLE1BQUYsRUFBVXNFLFdBQVYsQ0FBc0IsY0FBdEI7QUFDSCxTQUZEOztBQUlBO0FBQ0FDLG9CQUFZLFlBQVc7QUFDbkJ2RSxjQUFFLCtDQUFGLEVBQW1Ed0UsUUFBbkQsQ0FBNEQsaUJBQTVEO0FBQ0F4RSxjQUFFLGdEQUFGLEVBQW9Ed0UsUUFBcEQsQ0FBNkQsaUJBQTdEO0FBQ0gsU0FIRCxFQUdHLElBSEg7O0FBS0FELG9CQUFZLFlBQVc7QUFDbkJ2RSxjQUFFLCtDQUFGLEVBQW1EeUUsV0FBbkQsQ0FBK0QsaUJBQS9EO0FBQ0F6RSxjQUFFLGdEQUFGLEVBQW9EeUUsV0FBcEQsQ0FBZ0UsaUJBQWhFO0FBQ0gsU0FIRCxFQUdHLElBSEg7QUFJSCxLQWZEOztBQWlCQTtBQUNBLFFBQUlDLGtCQUFrQixTQUFsQkEsZUFBa0IsR0FBVztBQUM3QixZQUFJQyxLQUFLM0UsRUFBRSxnQkFBRixDQUFUOztBQUVBMkUsV0FBR0MsWUFBSCxDQUFnQjtBQUNaQyxrQkFBTUYsR0FBRzFELElBQUgsQ0FBUSxhQUFSLENBRE0sRUFDa0I7QUFDOUI2RCxvQkFBUSwwQkFGSTtBQUdaQyxxQkFBUywrQ0FIRzs7QUFLWkMsbUJBQU8sc0JBTEs7QUFNWkMsdUJBQVcsc0JBTkM7QUFPWkMsd0JBQVksdUJBUEE7QUFRWkMsd0JBQVksdUJBUkE7O0FBVVpDLDRCQUFnQiwyQkFWSjtBQVdaQyx1QkFBVyxDQVhDO0FBWVpDLHVCQUFXO0FBQ1BDLHVCQUFPLGVBQVNaLEVBQVQsRUFBYTtBQUNoQiwyQkFBTyxvSUFBUDtBQUNIO0FBSE07QUFaQyxTQUFoQjtBQWtCSCxLQXJCRDs7QUF1QkEsUUFBSWEsZ0JBQWdCLFNBQWhCQSxhQUFnQixHQUFXO0FBQzNCeEYsVUFBRSw4QkFBRixFQUFrQ3lGLFVBQWxDLENBQTZDO0FBQ3pDMUUsb0JBQVEsR0FEaUM7QUFFekMyRSxtQkFBTztBQUZrQyxTQUE3QztBQUlILEtBTEQ7O0FBT0EsV0FBTztBQUNIdEYsY0FBTSxnQkFBVztBQUNiLGlCQUFLdUYsVUFBTDtBQUNBLGlCQUFLQyxTQUFMO0FBQ0gsU0FKRTs7QUFNSEQsb0JBQVksc0JBQVc7QUFDbkIvRTtBQUNBVztBQUNBNkM7QUFDQU07QUFDQWM7QUFDSCxTQVpFOztBQWNISSxtQkFBVyxxQkFBVztBQUNsQmpDO0FBQ0FuQjtBQUNBc0I7O0FBRUEsaUJBQUsrQixtQkFBTCxDQUF5QixVQUFTQyxDQUFULEVBQVk7QUFDbkMsb0JBQUlDLGFBQWEvRixFQUFFLGNBQUYsQ0FBakI7QUFDQUEsa0JBQUUrRixVQUFGLEVBQWNDLElBQWQsQ0FBbUIsWUFBVztBQUM1QmhHLHNCQUFFLElBQUYsRUFBUWlHLFVBQVIsQ0FBbUIsUUFBbkI7QUFDRCxpQkFGRDtBQUdELGFBTEQ7QUFNSCxTQXpCRTs7QUEyQkhDLHNCQUFjLHdCQUFXO0FBQ3JCLG1CQUFPekYsU0FBUDtBQUNILFNBN0JFOztBQStCSG9GLDZCQUFxQiw2QkFBU00sSUFBVCxFQUFlO0FBQ2hDbkcsY0FBRSwrQkFBRixFQUFtQ2dFLE9BQW5DLEdBQTZDN0MsRUFBN0MsQ0FBZ0QsUUFBaEQsRUFBMERnRixJQUExRDtBQUNILFNBakNFOztBQW1DSEMsdUNBQStCLHlDQUFXO0FBQ3RDLGdCQUFJakcsTUFBTWtHLGNBQU4sRUFBSixFQUE0QjtBQUN4QjNGLG1DQUFtQjRGLElBQW5CO0FBQ0g7QUFDSixTQXZDRTs7QUF5Q0hDLHFDQUE2Qix1Q0FBVztBQUNwQyxnQkFBSXBHLE1BQU1rRyxjQUFOLEVBQUosRUFBNEI7QUFDeEIxRixpQ0FBaUIyRixJQUFqQjtBQUNIO0FBQ0o7QUE3Q0UsS0FBUDtBQStDSCxDQTdRc0IsRUFBaEI7O0FBK1FQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTTs7Ozs7Ozs7Ozs7Ozs7OztBQy9RQTs7QUFFTyxJQUFNakcsc0JBQU8sWUFBVzs7QUFFM0I7OztBQUdBLFFBQUltRyxlQUFjLFNBQWRBLFlBQWMsQ0FBU0MsRUFBVCxFQUFhO0FBQzNCLFlBQUlDLE9BQU9ELEdBQUd4RixJQUFILENBQVEsTUFBUixJQUFrQixxQkFBcUJ3RixHQUFHeEYsSUFBSCxDQUFRLE1BQVIsQ0FBdkMsR0FBeUQsRUFBcEU7QUFDQSxZQUFJbUIsUUFBUXFFLEdBQUd4RixJQUFILENBQVEsT0FBUixLQUFvQixNQUFwQixHQUE2Qix1QkFBN0IsR0FBdUQsRUFBbkU7O0FBRUF3RixXQUFHRSxPQUFILENBQVc7QUFDUEMscUJBQVMsT0FERjtBQUVQQyxzQkFBVSw0QkFBNEJILElBQTVCLEdBQW1DLEdBQW5DLEdBQXlDdEUsS0FBekMsR0FBaUQ7Ozs7QUFGcEQsU0FBWDtBQU9ILEtBWEQ7O0FBYUE7OztBQUdBLFFBQUkwRSxnQkFBZSxTQUFmQSxhQUFlLEdBQVc7QUFDMUI7QUFDQTlHLFVBQUUsNkJBQUYsRUFBaUNnRyxJQUFqQyxDQUFzQyxZQUFXO0FBQzdDUSx5QkFBWXhHLEVBQUUsSUFBRixDQUFaO0FBQ0gsU0FGRDtBQUdILEtBTEQ7O0FBT0E7OztBQUdBLFFBQUkrRyxlQUFjLFNBQWRBLFlBQWMsQ0FBU04sRUFBVCxFQUFhO0FBQzNCLFlBQUlDLE9BQU9ELEdBQUd4RixJQUFILENBQVEsTUFBUixJQUFrQixxQkFBcUJ3RixHQUFHeEYsSUFBSCxDQUFRLE1BQVIsQ0FBdkMsR0FBeUQsRUFBcEU7QUFDQSxZQUFJK0YsZUFBZVAsR0FBR3hGLElBQUgsQ0FBUSxTQUFSLElBQXFCd0YsR0FBR3hGLElBQUgsQ0FBUSxTQUFSLENBQXJCLEdBQTBDLE9BQTdEOztBQUVBd0YsV0FBR1EsT0FBSCxDQUFXO0FBQ1BMLHFCQUFTSSxZQURGO0FBRVBILHNCQUFVO29DQUFBLEdBQ2lCSCxJQURqQixHQUN3Qjs7Ozs7QUFIM0IsU0FBWDtBQVNILEtBYkQ7O0FBZUE7OztBQUdBLFFBQUlRLGdCQUFlLFNBQWZBLGFBQWUsR0FBVztBQUMxQjtBQUNBbEgsVUFBRSw2QkFBRixFQUFpQ2dHLElBQWpDLENBQXNDLFlBQVc7QUFDN0NlLHlCQUFZL0csRUFBRSxJQUFGLENBQVo7QUFDSCxTQUZEO0FBR0gsS0FMRDs7QUFPQTs7O0FBR0EsUUFBSW1ILGdCQUFnQixTQUFoQkEsYUFBZ0IsR0FBVztBQUMzQjtBQUNBbkgsVUFBRSxvQkFBRixFQUF3Qm1CLEVBQXhCLENBQTJCLFFBQTNCLEVBQXFDLFlBQVc7QUFDNUMsZ0JBQUlpRyxXQUFXcEgsRUFBRSxJQUFGLEVBQVFxSCxHQUFSLEVBQWY7QUFDQXJILGNBQUUsSUFBRixFQUFRc0gsSUFBUixDQUFhLHNCQUFiLEVBQXFDOUMsUUFBckMsQ0FBOEMsVUFBOUMsRUFBMEQrQyxJQUExRCxDQUErREgsUUFBL0Q7QUFDSCxTQUhEO0FBSUgsS0FORDs7QUFRQTs7O0FBR0EsUUFBSUksZUFBYyxTQUFkQSxZQUFjLENBQVNmLEVBQVQsRUFBYTNGLE9BQWIsRUFBc0I7QUFDcEM7QUFDQTJGLFdBQUdnQixRQUFILENBQVkzRyxPQUFaO0FBQ0gsS0FIRDs7QUFLQTs7O0FBR0EsUUFBSTRHLGdCQUFlLFNBQWZBLGFBQWUsR0FBVztBQUMxQjtBQUNBMUgsVUFBRSx5QkFBRixFQUE2QmdHLElBQTdCLENBQWtDLFlBQVc7QUFDekMsZ0JBQUlTLEtBQUt6RyxFQUFFLElBQUYsQ0FBVDs7QUFFQSxnQkFBSXlHLEdBQUd4RixJQUFILENBQVEscUJBQVIsTUFBbUMsSUFBdkMsRUFBNkM7QUFDekN1Ryw2QkFBWWYsRUFBWixFQUFnQixFQUFoQjtBQUNBQSxtQkFBR3hGLElBQUgsQ0FBUSxxQkFBUixFQUErQixJQUEvQjtBQUNIO0FBQ0osU0FQRDtBQVFILEtBVkQ7O0FBWUE7OztBQUdBLFFBQUkwRyxrQkFBa0IsU0FBbEJBLGVBQWtCLEdBQVc7QUFDN0IzSCxVQUFFLDRCQUFGLEVBQWdDZ0csSUFBaEMsQ0FBcUMsWUFBVztBQUM1QyxnQkFBSTRCLFNBQUo7QUFDQSxnQkFBSXZFLE1BQUo7QUFDQSxnQkFBSW9ELEtBQUt6RyxFQUFFLElBQUYsQ0FBVDs7QUFFQSxnQkFBSUcsWUFBTWdELG1CQUFOLENBQTBCLG1CQUExQixDQUFKLEVBQW9EO0FBQ2hELG9CQUFJc0QsR0FBR3hGLElBQUgsQ0FBUSxtQkFBUixDQUFKLEVBQWtDO0FBQzlCMkcsZ0NBQVluQixHQUFHeEYsSUFBSCxDQUFRLG1CQUFSLENBQVo7QUFDSCxpQkFGRCxNQUVPO0FBQ0gyRyxnQ0FBWW5CLEdBQUd4RixJQUFILENBQVEsWUFBUixDQUFaO0FBQ0g7O0FBRUQsb0JBQUl3RixHQUFHeEYsSUFBSCxDQUFRLGVBQVIsQ0FBSixFQUE4QjtBQUMxQm9DLDZCQUFTb0QsR0FBR3hGLElBQUgsQ0FBUSxlQUFSLENBQVQ7QUFDSCxpQkFGRCxNQUVPO0FBQ0hvQyw2QkFBU29ELEdBQUd4RixJQUFILENBQVEsUUFBUixDQUFUO0FBQ0g7QUFDSixhQVpELE1BWU87QUFDSDJHLDRCQUFZbkIsR0FBR3hGLElBQUgsQ0FBUSxZQUFSLENBQVo7QUFDQW9DLHlCQUFTb0QsR0FBR3hGLElBQUgsQ0FBUSxZQUFSLENBQVQ7QUFDSDs7QUFFRCxnQkFBSTJHLFNBQUosRUFBZTtBQUNYbkIsbUJBQUdvQixHQUFILENBQU8sWUFBUCxFQUFxQkQsU0FBckI7QUFDSDtBQUNELGdCQUFJdkUsTUFBSixFQUFZO0FBQ1JvRCxtQkFBR29CLEdBQUgsQ0FBTyxRQUFQLEVBQWlCeEUsTUFBakI7QUFDSDs7QUFFRGhELGlCQUFLb0QsWUFBTCxDQUFrQmdELEVBQWxCLEVBQXNCLEVBQXRCO0FBQ0gsU0E5QkQ7QUErQkgsS0FoQ0Q7O0FBa0NBOzs7QUFHQSxRQUFJcUIsYUFBYSxTQUFiQSxVQUFhLEdBQVc7QUFDeEI7QUFDQTlILFVBQUUsTUFBRixFQUFVbUIsRUFBVixDQUFhLE9BQWIsRUFBc0Isb0JBQXRCLEVBQTRDLFlBQVc7QUFDbkRuQixjQUFFLElBQUYsRUFBUStILE9BQVIsQ0FBZ0IsUUFBaEIsRUFBMEJ6QixJQUExQjtBQUNILFNBRkQ7QUFHSCxLQUxEOztBQU9BOzs7QUFHQSxRQUFJMEIsaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFTdkIsRUFBVCxFQUFhLENBRWpDLENBRkQ7O0FBSUEsUUFBSXdCLG1CQUFtQixTQUFuQkEsZ0JBQW1CLEdBQVc7QUFDOUJDLGVBQU9DLEtBQVAsQ0FBYUMsT0FBYixDQUFxQkMsVUFBckIsR0FBa0M7QUFDOUJDLG1CQUFPLGVBQVNDLENBQVQsRUFBWUMsRUFBWixFQUFnQkMsTUFBaEIsRUFBd0I7QUFDM0Isb0JBQUksT0FBTyxJQUFQLEtBQWdCLFVBQXBCLEVBQ0ksSUFBSUQsR0FBR0UsUUFBSCxDQUFZLGtCQUFaLENBQUosRUFBcUM7QUFDakMseUJBQUtDLGdCQUFMLENBQXNCLFlBQXRCLEVBQW9DRixNQUFwQyxFQUE0QyxFQUFDRyxTQUFTLEtBQVYsRUFBNUM7QUFDSCxpQkFGRCxNQUVPO0FBQ0gseUJBQUtELGdCQUFMLENBQXNCLFlBQXRCLEVBQW9DRixNQUFwQyxFQUE0QyxFQUFDRyxTQUFTLElBQVYsRUFBNUM7QUFDSDtBQUNSO0FBUjZCLFNBQWxDO0FBVUFWLGVBQU9DLEtBQVAsQ0FBYUMsT0FBYixDQUFxQlMsU0FBckIsR0FBaUM7QUFDN0JQLG1CQUFPLGVBQVNDLENBQVQsRUFBWUMsRUFBWixFQUFnQkMsTUFBaEIsRUFBd0I7QUFDM0Isb0JBQUksT0FBTyxJQUFQLEtBQWdCLFVBQXBCLEVBQ0ksSUFBSUQsR0FBR0UsUUFBSCxDQUFZLGtCQUFaLENBQUosRUFBcUM7QUFDakMseUJBQUtDLGdCQUFMLENBQXNCLFdBQXRCLEVBQW1DRixNQUFuQyxFQUEyQyxFQUFDRyxTQUFTLEtBQVYsRUFBM0M7QUFDSCxpQkFGRCxNQUVPO0FBQ0gseUJBQUtELGdCQUFMLENBQXNCLFdBQXRCLEVBQW1DRixNQUFuQyxFQUEyQyxFQUFDRyxTQUFTLElBQVYsRUFBM0M7QUFDSDtBQUNSO0FBUjRCLFNBQWpDO0FBVUFWLGVBQU9DLEtBQVAsQ0FBYUMsT0FBYixDQUFxQlUsS0FBckIsR0FBNkI7QUFDekJSLG1CQUFPLGVBQVNDLENBQVQsRUFBWUMsRUFBWixFQUFnQkMsTUFBaEIsRUFBd0I7QUFDM0Isb0JBQUksT0FBTyxJQUFQLEtBQWdCLFVBQXBCLEVBQ0ksSUFBSUQsR0FBR0UsUUFBSCxDQUFZLGtCQUFaLENBQUosRUFBcUM7QUFDakMseUJBQUtDLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCRixNQUEvQixFQUF1QyxFQUFDRyxTQUFTLEtBQVYsRUFBdkM7QUFDSCxpQkFGRCxNQUVPO0FBQ0gseUJBQUtELGdCQUFMLENBQXNCLE9BQXRCLEVBQStCRixNQUEvQixFQUF1QyxFQUFDRyxTQUFTLElBQVYsRUFBdkM7QUFDSDtBQUNSO0FBUndCLFNBQTdCO0FBVUgsS0EvQkQ7O0FBaUNBLFdBQU87QUFDSDs7O0FBR0F4SSxjQUFNLGdCQUFXO0FBQ2JDLGlCQUFLMEksY0FBTDtBQUNILFNBTkU7O0FBUUg7OztBQUdBQSx3QkFBZ0IsMEJBQVc7QUFDdkJkO0FBQ0FOO0FBQ0FiO0FBQ0FJO0FBQ0FZO0FBQ0FKO0FBQ0FQO0FBQ0FhO0FBQ0gsU0FwQkU7O0FBc0JIOzs7O0FBSUE7QUFDQWxCLHNCQUFjLHdCQUFXO0FBQ3JCQTtBQUNILFNBN0JFOztBQStCSDs7OztBQUlBO0FBQ0FOLHFCQUFhLHFCQUFTQyxFQUFULEVBQWE7QUFDdEJELHlCQUFZQyxFQUFaO0FBQ0gsU0F0Q0U7O0FBd0NIOzs7O0FBSUE7QUFDQVMsc0JBQWMsd0JBQVc7QUFDckJBO0FBQ0gsU0EvQ0U7O0FBaURIOzs7O0FBSUE7QUFDQUgscUJBQWEscUJBQVNOLEVBQVQsRUFBYTtBQUN0Qk0seUJBQVlOLEVBQVo7QUFDSCxTQXhERTs7QUEwREg7Ozs7QUFJQTtBQUNBZSxxQkFBYSxxQkFBU2YsRUFBVCxFQUFhM0YsT0FBYixFQUFzQjtBQUMvQjBHLHlCQUFZZixFQUFaLEVBQWdCM0YsT0FBaEI7QUFDSCxTQWpFRTs7QUFtRUg7Ozs7QUFJQTtBQUNBNEcsc0JBQWMsd0JBQVc7QUFDckJBO0FBQ0gsU0ExRUU7O0FBNEVIOzs7OztBQUtBc0Isa0JBQVUsa0JBQVN2QyxFQUFULEVBQWExRixNQUFiLEVBQXFCO0FBQzNCLGdCQUFJa0ksTUFBT3hDLE1BQU1BLEdBQUdqRCxNQUFILEdBQVksQ0FBbkIsR0FBd0JpRCxHQUFHMUYsTUFBSCxHQUFZbUksR0FBcEMsR0FBMEMsQ0FBcEQ7QUFDQUQsa0JBQU1BLE9BQU9sSSxTQUFTQSxNQUFULEdBQWtCLENBQXpCLENBQU47O0FBRUFtSCxtQkFBTyxXQUFQLEVBQW9CaUIsT0FBcEIsQ0FBNEI7QUFDeEJDLDJCQUFXSDtBQURhLGFBQTVCLEVBRUcsTUFGSDtBQUdILFNBeEZFOztBQTBGSDs7OztBQUlBO0FBQ0FJLDBCQUFrQiwwQkFBUzVDLEVBQVQsRUFBYTtBQUMzQixnQkFBSTZDLFdBQVc3QyxHQUFHMUYsTUFBSCxHQUFZbUksR0FBM0I7QUFDQSxnQkFBSUssV0FBVzlDLEdBQUdwRCxNQUFILEVBQWY7QUFDQSxnQkFBSW1HLGVBQWVySixZQUFNbUQsV0FBTixHQUFvQkQsTUFBdkM7QUFDQSxnQkFBSXRDLFNBQVN1SSxZQUFhRSxlQUFlLENBQWhCLEdBQXNCRCxXQUFXLENBQTdDLENBQWI7O0FBRUFyQixtQkFBTyxXQUFQLEVBQW9CaUIsT0FBcEIsQ0FBNEI7QUFDeEJDLDJCQUFXckk7QUFEYSxhQUE1QixFQUVHLE1BRkg7QUFHSCxTQXhHRTs7QUEwR0g7OztBQUdBO0FBQ0FxSSxtQkFBVyxxQkFBVztBQUNsQi9JLGlCQUFLMkksUUFBTDtBQUNILFNBaEhFOztBQWtISDs7Ozs7QUFLQXZGLHNCQUFjLHNCQUFTZ0QsRUFBVCxFQUFhM0YsT0FBYixFQUFzQjtBQUNoQyxnQkFBSVgsWUFBTWtHLGNBQU4sRUFBSixFQUE0QjtBQUN4QkksbUJBQUdvQixHQUFILENBQU8sVUFBUCxFQUFtQixNQUFuQjtBQUNILGFBRkQsTUFFTztBQUNIO0FBQ0FwQixtQkFBR2dELGdCQUFILENBQW9CO0FBQ2hCQyxtQ0FBZSxDQURDO0FBRWhCQyx1Q0FBbUIsSUFGSDtBQUdoQkMsdUNBQW1CLElBSEg7QUFJaEJDLHlDQUFxQixLQUpMO0FBS2hCQyx5Q0FBcUIsQ0FMTDtBQU1oQkMsMEJBQU10RCxHQUFHeEYsSUFBSCxDQUFRLE1BQVIsSUFBa0J3RixHQUFHeEYsSUFBSCxDQUFRLE1BQVIsQ0FBbEIsR0FBb0MsR0FOMUI7QUFPaEIrSSxnQ0FBWTtBQUNSQyxzQ0FBYyxHQUROO0FBRVJDLHdDQUFnQjtBQUZSLHFCQVBJO0FBV2hCQywrQkFBWXJKLFFBQVF1QyxNQUFSLEdBQWlCdkMsUUFBUXVDLE1BQXpCLEdBQWtDLEVBWDlCO0FBWWhCK0csMkJBQU87QUFaUyxpQkFBcEI7QUFjSDtBQUNKLFNBM0lFOztBQTZJSDs7OztBQUlBaEgseUJBQWlCLHlCQUFTcUQsRUFBVCxFQUFhO0FBQzFCQSxlQUFHZ0QsZ0JBQUgsQ0FBb0IsU0FBcEI7QUFDSCxTQW5KRTs7QUFxSkg7Ozs7O0FBS0FZLGVBQU8sZUFBU3ZKLE9BQVQsRUFBa0I7QUFDckJBLHNCQUFVZCxFQUFFc0ssTUFBRixDQUFTLElBQVQsRUFBZTtBQUNyQkMsMkJBQVcsRUFEVSxFQUNOO0FBQ2ZDLHVCQUFPLFFBRmMsRUFFSjtBQUNqQjNGLHNCQUFNLFNBSGUsRUFHSjtBQUNqQjRGLHlCQUFTLEVBSlksRUFJUjtBQUNiOUksdUJBQU8sSUFMYyxFQUtSO0FBQ2IrSSx1QkFBTyxJQU5jLEVBTVI7QUFDYkMsdUJBQU8sSUFQYyxFQU9SO0FBQ2JDLGdDQUFnQixDQVJLLEVBUUY7QUFDbkJDLHNCQUFNLEVBVGUsQ0FTWDtBQVRXLGFBQWYsRUFVUC9KLE9BVk8sQ0FBVjs7QUFZQSxnQkFBSWdLLEtBQUszSyxZQUFNNEssV0FBTixDQUFrQixXQUFsQixDQUFUOztBQUVBLGdCQUFJeEQsT0FBTyxlQUFldUQsRUFBZixHQUFvQix1Q0FBcEIsR0FBOERoSyxRQUFRK0QsSUFBdEUsR0FBNkUsYUFBN0UsSUFBOEYvRCxRQUFRYSxLQUFSLEdBQWdCLCtGQUFoQixHQUFrSCxFQUFoTixLQUF1TmIsUUFBUStKLElBQVIsS0FBaUIsRUFBakIsR0FBc0IsNEJBQTRCL0osUUFBUStKLElBQXBDLEdBQTJDLFdBQWpFLEdBQStFLEVBQXRTLElBQTRTL0osUUFBUTJKLE9BQXBULEdBQThULFFBQXpVOztBQUVBLGdCQUFJM0osUUFBUTRKLEtBQVosRUFBbUI7QUFDZjFLLGtCQUFFLGdCQUFGLEVBQW9CZ0wsTUFBcEI7QUFDSDs7QUFFRCxnQkFBSSxDQUFDbEssUUFBUXlKLFNBQWIsRUFBd0I7QUFDcEIsb0JBQUl2SyxFQUFFLDBCQUFGLEVBQThCaUwsSUFBOUIsT0FBeUMsQ0FBN0MsRUFBZ0Q7QUFDNUNqTCxzQkFBRSwwQkFBRixFQUE4QmtMLE9BQTlCLENBQXNDM0QsSUFBdEM7QUFDSCxpQkFGRCxNQUVPLElBQUksQ0FBQ3ZILEVBQUUsTUFBRixFQUFVNkQsUUFBVixDQUFtQix5QkFBbkIsS0FBaUQ3RCxFQUFFLE1BQUYsRUFBVTZELFFBQVYsQ0FBbUIsb0JBQW5CLENBQWxELEtBQStGN0QsRUFBRSxZQUFGLEVBQWdCaUwsSUFBaEIsT0FBMkIsQ0FBOUgsRUFBaUk7QUFDcElqTCxzQkFBRSxhQUFGLEVBQWlCbUwsS0FBakIsQ0FBdUI1RCxJQUF2QjtBQUNILGlCQUZNLE1BRUE7QUFDSCx3QkFBSXZILEVBQUUsV0FBRixFQUFlaUwsSUFBZixLQUF3QixDQUE1QixFQUErQjtBQUMzQmpMLDBCQUFFLFdBQUYsRUFBZW1MLEtBQWYsQ0FBcUI1RCxJQUFyQjtBQUNILHFCQUZELE1BRU87QUFDSHZILDBCQUFFLGdDQUFGLEVBQW9DbUwsS0FBcEMsQ0FBMEM1RCxJQUExQztBQUNIO0FBQ0o7QUFDSixhQVpELE1BWU87QUFDSCxvQkFBSXpHLFFBQVEwSixLQUFSLElBQWlCLFFBQXJCLEVBQStCO0FBQzNCeEssc0JBQUVjLFFBQVF5SixTQUFWLEVBQXFCYSxNQUFyQixDQUE0QjdELElBQTVCO0FBQ0gsaUJBRkQsTUFFTztBQUNIdkgsc0JBQUVjLFFBQVF5SixTQUFWLEVBQXFCVyxPQUFyQixDQUE2QjNELElBQTdCO0FBQ0g7QUFDSjs7QUFFRCxnQkFBSXpHLFFBQVE2SixLQUFaLEVBQW1CO0FBQ2Z0SyxxQkFBSzJJLFFBQUwsQ0FBY2hKLEVBQUUsTUFBTThLLEVBQVIsQ0FBZDtBQUNIOztBQUVELGdCQUFJaEssUUFBUThKLGNBQVIsR0FBeUIsQ0FBN0IsRUFBZ0M7QUFDNUJTLDJCQUFXLFlBQVc7QUFDbEJyTCxzQkFBRSxNQUFNOEssRUFBUixFQUFZRSxNQUFaO0FBQ0gsaUJBRkQsRUFFR2xLLFFBQVE4SixjQUFSLEdBQXlCLElBRjVCO0FBR0g7O0FBRUQsbUJBQU9FLEVBQVA7QUFDSCxTQTlNRTs7QUFnTkg7Ozs7O0FBS0FRLGVBQU8sZUFBU3pKLE1BQVQsRUFBaUJmLE9BQWpCLEVBQTBCO0FBQzdCLGdCQUFJMkYsS0FBS3pHLEVBQUU2QixNQUFGLENBQVQ7O0FBRUFmLHNCQUFVZCxFQUFFc0ssTUFBRixDQUFTLElBQVQsRUFBZTtBQUNyQmlCLHlCQUFTLElBRFk7QUFFckJDLDhCQUFjLFNBRk87QUFHckIxSix1QkFBTyxPQUhjO0FBSXJCK0Msc0JBQU0sUUFKZTtBQUtyQm9HLHNCQUFNLElBTGU7QUFNckJRLHlCQUFTLElBTlk7QUFPckJDLHlCQUFTLElBUFk7QUFRckJqQix5QkFBUyxFQVJZO0FBU3JCa0Isd0JBQVEsSUFUYTtBQVVyQnZKLHVCQUFPO0FBVmMsYUFBZixFQVdQdEIsT0FYTyxDQUFWOztBQWFBLGdCQUFJNEYsSUFBSjtBQUNBLGdCQUFJNUUsS0FBSjtBQUNBLGdCQUFJOEosT0FBSjs7QUFFQSxnQkFBSTlLLFFBQVErRCxJQUFSLElBQWdCLFNBQXBCLEVBQStCO0FBQzNCNkIsdUJBQU81RixRQUFRNEYsSUFBUixHQUFlLHFCQUFxQjVGLFFBQVE0RixJQUE1QyxHQUFtRCxFQUExRDtBQUNBNUUsd0JBQVFoQixRQUFRZ0IsS0FBUixHQUFnQixnQkFBZ0JoQixRQUFRZ0IsS0FBeEMsR0FBZ0QsRUFBeEQ7QUFDQThKLDBCQUFVLDRCQUE0QmxGLElBQTVCLEdBQW1DLEdBQW5DLEdBQXlDNUUsS0FBekMsR0FBaUQsVUFBM0Q7QUFDSCxhQUpELE1BSU87QUFDSDRFLHVCQUFPNUYsUUFBUTRGLElBQVIsR0FBZSxvQkFBb0I1RixRQUFRNEYsSUFBM0MsR0FBa0QsRUFBekQ7QUFDQTVFLHdCQUFRaEIsUUFBUWdCLEtBQVIsR0FBZ0IsZUFBZWhCLFFBQVFnQixLQUF2QyxHQUErQyxFQUF2RDtBQUNBbUosdUJBQU9uSyxRQUFRbUssSUFBUixHQUFlLGVBQWVuSyxRQUFRbUssSUFBdEMsR0FBNkMsRUFBcEQ7QUFDQVcsMEJBQVUsMkJBQTJCbEYsSUFBM0IsR0FBa0MsR0FBbEMsR0FBd0M1RSxLQUF4QyxHQUFnRCxHQUFoRCxHQUFzRG1KLElBQXRELEdBQTZELFVBQXZFO0FBQ0g7O0FBRUQsZ0JBQUluSyxRQUFRMkosT0FBUixJQUFtQjNKLFFBQVEySixPQUFSLENBQWdCakgsTUFBaEIsR0FBeUIsQ0FBaEQsRUFBbUQ7QUFDL0Msb0JBQUlxSSxVQUFVLGdCQUFnQi9LLFFBQVE2SyxNQUFSLEtBQW1CLEtBQW5CLEdBQTJCLHFCQUEzQixHQUFtRCxFQUFuRSxDQUFkOztBQUVBcEUsdUJBQU8sa0JBQWtCc0UsT0FBbEIsR0FBNEIsV0FBNUIsR0FBMEMvSyxRQUFRMkosT0FBbEQsR0FBNEQsZUFBNUQsR0FBOEVtQixPQUE5RSxHQUF3RixlQUEvRjtBQUNBOUssd0JBQVFzQixLQUFSLEdBQWdCakMsWUFBTTJMLFNBQU4sQ0FBZ0J2RSxJQUFoQixJQUF3QixFQUF4QztBQUNBLG9CQUFJMUYsVUFBVSxNQUFkLEVBQXNCO0FBQ2xCMEYsMkJBQU8sa0JBQWtCc0UsT0FBbEIsR0FBNEIsMEJBQTVCLEdBQTBEL0ssUUFBUXNCLEtBQVIsR0FBZ0IsQ0FBMUUsR0FBK0UsY0FBL0UsR0FBZ0d0QixRQUFRMkosT0FBeEcsR0FBa0gsZUFBbEgsR0FBb0ltQixPQUFwSSxHQUE4SSxlQUFySjtBQUNIO0FBQ0osYUFSRCxNQVFPO0FBQ0hyRSx1QkFBT3FFLE9BQVA7QUFDSDs7QUFFRCxnQkFBSUcsU0FBUztBQUNUdEIseUJBQVNsRCxJQURBO0FBRVRtRSx5QkFBUzVLLFFBQVE0SyxPQUZSO0FBR1RELHlCQUFTM0ssUUFBUTJLLE9BSFI7QUFJVDVELHFCQUFLO0FBQ0RxQix5QkFBSyxLQURKO0FBRUQ4QywwQkFBTSxLQUZMO0FBR0RDLDRCQUFRLEdBSFA7QUFJREMsNkJBQVMsR0FKUjtBQUtEQyxxQ0FBaUIsTUFMaEI7QUFNRC9KLDJCQUFPdEIsUUFBUXNCO0FBTmQsaUJBSkk7QUFZVGdLLDRCQUFZO0FBQ1JELHFDQUFpQnJMLFFBQVEwSyxZQURqQjtBQUVSRCw2QkFBU3pLLFFBQVF5SyxPQUZUO0FBR1JjLDRCQUFRLE1BSEE7QUFJUkMsNEJBQVE7QUFKQSxpQkFaSDtBQWtCVEMsMkJBQVcscUJBQVc7QUFDbEIsd0JBQUk5RixFQUFKLEVBQVE7QUFDSkEsMkJBQUdvQixHQUFILENBQU8sVUFBUCxFQUFtQixFQUFuQjtBQUNBcEIsMkJBQUdvQixHQUFILENBQU8sTUFBUCxFQUFlLEVBQWY7QUFDSDtBQUNKO0FBdkJRLGFBQWI7O0FBMEJBLGdCQUFJaEcsVUFBVSxNQUFkLEVBQXNCO0FBQ2xCa0ssdUJBQU9sRSxHQUFQLENBQVdxQixHQUFYLEdBQWlCLEtBQWpCO0FBQ0FsSixrQkFBRXdNLE9BQUYsQ0FBVVQsTUFBVjtBQUNILGFBSEQsTUFHTztBQUNILG9CQUFJdEYsS0FBS3pHLEVBQUU2QixNQUFGLENBQVQ7QUFDQTRFLG1CQUFHNkUsS0FBSCxDQUFTUyxNQUFUO0FBQ0g7QUFDSixTQWpTRTs7QUFtU0g7Ozs7QUFJQVUsaUJBQVMsaUJBQVM1SyxNQUFULEVBQWlCO0FBQ3RCLGdCQUFJQSxVQUFVQSxVQUFVLE1BQXhCLEVBQWdDO0FBQzVCN0Isa0JBQUU2QixNQUFGLEVBQVU0SyxPQUFWO0FBQ0gsYUFGRCxNQUVPO0FBQ0h6TSxrQkFBRTBNLFNBQUY7QUFDSDtBQUNKLFNBN1NFOztBQStTSDs7OztBQUlBQyxtQkFBVyxtQkFBUzdMLE9BQVQsRUFBa0I7QUFDekIsbUJBQU9ULEtBQUtpTCxLQUFMLENBQVcsTUFBWCxFQUFtQnhLLE9BQW5CLENBQVA7QUFDSCxTQXJURTs7QUF1VEg7OztBQUdBOEwscUJBQWEsdUJBQVc7QUFDcEIsbUJBQU92TSxLQUFLb00sT0FBTCxDQUFhLE1BQWIsQ0FBUDtBQUNILFNBNVRFOztBQThUSDs7Ozs7QUFLQUksa0JBQVUsa0JBQVNoTCxNQUFULEVBQWlCZixPQUFqQixFQUEwQjtBQUNoQyxnQkFBSTRGLE9BQVE1RixXQUFXQSxRQUFRNEYsSUFBcEIsR0FBNEI1RixRQUFRNEYsSUFBcEMsR0FBMkMsT0FBdEQ7QUFDQSxnQkFBSW9HLFlBQWFoTSxXQUFXQSxRQUFRZ00sU0FBcEIsR0FBaUNoTSxRQUFRZ00sU0FBekMsR0FBcUQsT0FBckU7QUFDQSxnQkFBSTdCLE9BQVFuSyxXQUFXQSxRQUFRbUssSUFBcEIsR0FBNEIsZ0JBQWdCbkssUUFBUW1LLElBQXBELEdBQTJELEVBQXRFO0FBQ0EsZ0JBQUlZLFVBQVUsY0FBYyxZQUFkLEdBQTZCbkYsSUFBN0IsR0FBb0MsYUFBcEMsR0FBb0RvRyxTQUFwRCxHQUFnRSxhQUFoRSxHQUFnRjdCLElBQTlGOztBQUVBNUssaUJBQUswTSxVQUFMLENBQWdCbEwsTUFBaEI7O0FBRUE3QixjQUFFNkIsTUFBRixFQUFVMkMsUUFBVixDQUFtQnFILE9BQW5CO0FBQ0E3TCxjQUFFNkIsTUFBRixFQUFVWixJQUFWLENBQWUsa0JBQWYsRUFBbUM0SyxPQUFuQztBQUNILFNBN1VFOztBQStVSDs7OztBQUlBa0Isb0JBQVksb0JBQVNsTCxNQUFULEVBQWlCO0FBQ3pCN0IsY0FBRTZCLE1BQUYsRUFBVTRDLFdBQVYsQ0FBc0J6RSxFQUFFNkIsTUFBRixFQUFVWixJQUFWLENBQWUsa0JBQWYsQ0FBdEI7QUFDSDtBQXJWRSxLQUFQO0FBdVZILENBeGdCbUIsRUFBYjs7QUEwZ0JQO0FBQ0E7QUFDQTtBQUNBO0FBbmhCQTs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBOzs7O0FBSU8sSUFBTWQsd0JBQVEsWUFBVztBQUM1QixRQUFJNk0saUJBQWlCLEVBQXJCOztBQUVBO0FBQ0EsUUFBSUMsY0FBYztBQUNkQyxZQUFJLEdBRFUsRUFDTDtBQUNUQyxZQUFJLEdBRlUsRUFFTDtBQUNUQyxZQUFJLEdBSFUsRUFHTDtBQUNUQyxZQUFJLElBSlUsQ0FJTDtBQUpLLEtBQWxCOztBQU9BO0FBQ0EsUUFBSUMsU0FBUztBQUNUQyxlQUFZLFNBREg7QUFFVEMsZUFBWSxTQUZIO0FBR1RDLGVBQVksU0FISDtBQUlUQyxnQkFBWSxTQUpIO0FBS1RDLGlCQUFZLFNBTEg7QUFNVEMsaUJBQVksU0FOSDtBQU9UQyxjQUFZLFNBUEg7QUFRVEMsaUJBQVksU0FSSDtBQVNUQyxnQkFBWTtBQVRILEtBQWI7O0FBWUE7Ozs7QUFJQSxRQUFJQyx1QkFBdUIsU0FBdkJBLG9CQUF1QixHQUFXO0FBQ2xDLFlBQUk5TCxNQUFKO0FBQ0EsWUFBSStMLHFCQUFxQixTQUFyQkEsa0JBQXFCLEdBQVc7QUFDaEM7QUFDQSxpQkFBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUlsQixlQUFleEosTUFBbkMsRUFBMkMwSyxHQUEzQyxFQUFnRDtBQUM1QyxvQkFBSWxJLE9BQU9nSCxlQUFla0IsQ0FBZixDQUFYO0FBQ0FsSSxxQkFBS21JLElBQUw7QUFDSDtBQUNKLFNBTkQ7O0FBUUFqRyxlQUFPa0csTUFBUCxFQUFlbE0sTUFBZixDQUFzQixZQUFXO0FBQzdCLGdCQUFJQSxNQUFKLEVBQVk7QUFDUm1NLDZCQUFhbk0sTUFBYjtBQUNIO0FBQ0RBLHFCQUFTbUosV0FBVyxZQUFXO0FBQzNCNEM7QUFDSCxhQUZRLEVBRU4sR0FGTSxDQUFULENBSjZCLENBTXBCO0FBQ1osU0FQRDtBQVFILEtBbEJEOztBQW9CQSxXQUFPO0FBQ0g7Ozs7O0FBS0E7QUFDQTdOLGNBQU0sY0FBU1UsT0FBVCxFQUFrQjtBQUNwQixnQkFBSUEsV0FBV0EsUUFBUW1NLFdBQXZCLEVBQW9DO0FBQ2hDQSw4QkFBY25NLFFBQVFtTSxXQUF0QjtBQUNIOztBQUVELGdCQUFJbk0sV0FBV0EsUUFBUXdNLE1BQXZCLEVBQStCO0FBQzNCQSx5QkFBU3hNLFFBQVF3TSxNQUFqQjtBQUNIOztBQUVEVTtBQUNILFNBakJFOztBQW1CSDs7OztBQUlBdEssMEJBQWtCLDBCQUFTNEssUUFBVCxFQUFtQjtBQUNqQ3RCLDJCQUFldUIsSUFBZixDQUFvQkQsUUFBcEI7QUFDSCxTQXpCRTs7QUEyQkg7OztBQUdBRSwyQkFBbUIsNkJBQVc7QUFDMUJQO0FBQ0gsU0FoQ0U7O0FBa0NIOzs7OztBQUtBUSxxQkFBYSxxQkFBU0MsU0FBVCxFQUFvQjtBQUM3QixnQkFBSUMsZUFBZVAsT0FBT1EsUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJDLFNBQXZCLENBQWlDLENBQWpDLENBQW5CO0FBQUEsZ0JBQ0laLENBREo7QUFBQSxnQkFDTzdHLEdBRFA7QUFBQSxnQkFDWTBFLFNBQVM0QyxhQUFhSSxLQUFiLENBQW1CLEdBQW5CLENBRHJCOztBQUdBLGlCQUFLYixJQUFJLENBQVQsRUFBWUEsSUFBSW5DLE9BQU92SSxNQUF2QixFQUErQjBLLEdBQS9CLEVBQW9DO0FBQ2hDN0csc0JBQU0wRSxPQUFPbUMsQ0FBUCxFQUFVYSxLQUFWLENBQWdCLEdBQWhCLENBQU47QUFDQSxvQkFBSTFILElBQUksQ0FBSixLQUFVcUgsU0FBZCxFQUF5QjtBQUNyQiwyQkFBT00sU0FBUzNILElBQUksQ0FBSixDQUFULENBQVA7QUFDSDtBQUNKOztBQUVELG1CQUFPLElBQVA7QUFDSCxTQW5ERTs7QUFxREg7Ozs7QUFJQWhCLHdCQUFnQiwwQkFBVztBQUN2QixtQkFBUSxLQUFLL0MsV0FBTCxHQUFtQmxCLEtBQW5CLEdBQTJCLEtBQUs2TSxhQUFMLENBQW1CLElBQW5CLENBQTNCLEdBQXNELElBQXRELEdBQTZELEtBQXJFO0FBQ0gsU0EzREU7O0FBNkRIOzs7O0FBSUFDLHlCQUFpQiwyQkFBVztBQUN4QixtQkFBTy9PLE1BQU1rRyxjQUFOLEtBQXlCLEtBQXpCLEdBQWlDLElBQXhDO0FBQ0gsU0FuRUU7O0FBcUVIOzs7O0FBSUEvQyxxQkFBYSx1QkFBVztBQUNwQixnQkFBSXdDLElBQUlzSSxNQUFSO0FBQUEsZ0JBQ0llLElBQUksT0FEUjtBQUVBLGdCQUFJLEVBQUUsZ0JBQWdCZixNQUFsQixDQUFKLEVBQStCO0FBQzNCZSxvQkFBSSxRQUFKO0FBQ0FySixvQkFBSTdGLFNBQVNtUCxlQUFULElBQTRCblAsU0FBUzJDLElBQXpDO0FBQ0g7O0FBRUQsbUJBQU87QUFDSFIsdUJBQU8wRCxFQUFFcUosSUFBSSxPQUFOLENBREo7QUFFSDlMLHdCQUFReUMsRUFBRXFKLElBQUksUUFBTjtBQUZMLGFBQVA7QUFJSCxTQXJGRTs7QUF1Rkg7Ozs7O0FBS0FoTSw2QkFBcUIsNkJBQVNOLElBQVQsRUFBZTtBQUNoQyxnQkFBSXdNLGFBQWEsS0FBSy9MLFdBQUwsR0FBbUJsQixLQUFwQzs7QUFFQSxnQkFBSVMsUUFBUSxTQUFaLEVBQXVCO0FBQ25CLHVCQUFPLElBQVA7QUFDSCxhQUZELE1BRU8sSUFBSUEsUUFBUSxTQUFSLElBQXFCd00sY0FBZSxLQUFLSixhQUFMLENBQW1CLElBQW5CLElBQTJCLENBQW5FLEVBQXVFO0FBQzFFLHVCQUFPLElBQVA7QUFDSCxhQUZNLE1BRUEsSUFBSXBNLFFBQVEsUUFBUixJQUFxQndNLGNBQWUsS0FBS0osYUFBTCxDQUFtQixJQUFuQixJQUEyQixDQUExQyxJQUFnREksYUFBYSxLQUFLSixhQUFMLENBQW1CLElBQW5CLENBQXRGLEVBQWlIO0FBQ3BILHVCQUFPLElBQVA7QUFDSCxhQUZNLE1BRUEsSUFBSXBNLFFBQVEsUUFBUixJQUFvQndNLGNBQWMsS0FBS0osYUFBTCxDQUFtQixJQUFuQixDQUF0QyxFQUFnRTtBQUNuRSx1QkFBTyxJQUFQO0FBQ0gsYUFGTSxNQUVBLElBQUlwTSxRQUFRLG9CQUFSLElBQWdDd00sY0FBZSxLQUFLSixhQUFMLENBQW1CLElBQW5CLElBQTJCLENBQTlFLEVBQWtGO0FBQ3JGLHVCQUFPLElBQVA7QUFDSCxhQUZNLE1BRUEsSUFBSXBNLFFBQVEsbUJBQVIsSUFBK0J3TSxjQUFjLEtBQUtKLGFBQUwsQ0FBbUIsSUFBbkIsQ0FBakQsRUFBMkU7QUFDOUUsdUJBQU8sSUFBUDtBQUNILGFBRk0sTUFFQSxJQUFJcE0sUUFBUSwyQkFBUixJQUF1Q3dNLGNBQWMsS0FBS0osYUFBTCxDQUFtQixJQUFuQixDQUF6RCxFQUFtRjtBQUN0Rix1QkFBTyxJQUFQO0FBQ0g7O0FBRUQsbUJBQU8sS0FBUDtBQUNILFNBaEhFOztBQWtISDs7Ozs7QUFLQWxFLHFCQUFhLHFCQUFTdUUsTUFBVCxFQUFpQjtBQUMxQixtQkFBT0EsU0FBU0MsS0FBS0MsS0FBTCxDQUFXRCxLQUFLRSxNQUFMLEtBQWlCLElBQUlDLElBQUosRUFBRCxDQUFhQyxPQUFiLEVBQTNCLENBQWhCO0FBQ0gsU0F6SEU7O0FBMkhIOzs7OztBQUtBVix1QkFBZSx1QkFBU3BNLElBQVQsRUFBZTtBQUMxQixnQkFBSTdDLEVBQUU0UCxPQUFGLENBQVUvTSxJQUFWLEVBQWdCb0ssV0FBaEIsQ0FBSixFQUFrQztBQUM5Qix1QkFBT0EsWUFBWXBLLElBQVosQ0FBUDtBQUNIO0FBQ0osU0FwSUU7O0FBc0lIOzs7Ozs7QUFNQWdOLGVBQU8sZUFBUzNNLEdBQVQsRUFBYzRNLElBQWQsRUFBb0I7QUFDdkIsZ0JBQUlDLEtBQUo7O0FBRUFELG1CQUFPQSxRQUFRLEVBQWY7O0FBRUEsZ0JBQUlBLEtBQUtFLE9BQUwsQ0FBYSxHQUFiLE1BQXNCLENBQUMsQ0FBM0IsRUFBOEI7QUFDMUIsc0JBQU0sSUFBSUMsS0FBSixDQUFVLG1DQUFWLENBQU47QUFDSDs7QUFFREgsbUJBQU9BLEtBQUtmLEtBQUwsQ0FBVyxHQUFYLENBQVA7O0FBRUEsZUFBRztBQUNDLG9CQUFJN0wsUUFBUWdOLFNBQVosRUFBdUI7QUFDbkIsMkJBQU8sS0FBUDtBQUNIOztBQUVESCx3QkFBUUQsS0FBS0ssS0FBTCxFQUFSOztBQUVBLG9CQUFJLENBQUNqTixJQUFJa04sY0FBSixDQUFtQkwsS0FBbkIsQ0FBTCxFQUFnQztBQUM1QiwyQkFBTyxLQUFQO0FBQ0g7O0FBRUQ3TSxzQkFBTUEsSUFBSTZNLEtBQUosQ0FBTjtBQUVILGFBYkQsUUFhU0QsS0FBS3RNLE1BYmQ7O0FBZUEsbUJBQU8sSUFBUDtBQUNILFNBdktFOztBQXlLSDs7Ozs7QUFLQTZNLDBCQUFrQiwwQkFBUzVKLEVBQVQsRUFBYTtBQUMzQixnQkFBSTZKLE9BQU90USxFQUFFeUcsRUFBRixDQUFYO0FBQUEsZ0JBQ0k4SixRQURKO0FBQUEsZ0JBQ2NDLEtBRGQ7O0FBR0EsbUJBQU9GLEtBQUs5TSxNQUFMLElBQWU4TSxLQUFLLENBQUwsTUFBWXJRLFFBQWxDLEVBQTRDO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBc1EsMkJBQVdELEtBQUt6SSxHQUFMLENBQVMsVUFBVCxDQUFYOztBQUVBLG9CQUFJMEksYUFBYSxVQUFiLElBQTJCQSxhQUFhLFVBQXhDLElBQXNEQSxhQUFhLE9BQXZFLEVBQWdGO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0FDLDRCQUFRQyxTQUFTSCxLQUFLekksR0FBTCxDQUFTLFFBQVQsQ0FBVCxFQUE2QixFQUE3QixDQUFSO0FBQ0Esd0JBQUksQ0FBQzZJLE1BQU1GLEtBQU4sQ0FBRCxJQUFpQkEsVUFBVSxDQUEvQixFQUFrQztBQUM5QiwrQkFBT0EsS0FBUDtBQUNIO0FBQ0o7QUFDREYsdUJBQU9BLEtBQUtLLE1BQUwsRUFBUDtBQUNIO0FBQ0osU0FwTUU7O0FBc01IOzs7Ozs7QUFNQUMsb0JBQVksb0JBQVNuSyxFQUFULEVBQWFvRixPQUFiLEVBQXNCO0FBQzlCLGdCQUFJZ0YsYUFBYWhGLFFBQVFrRCxLQUFSLENBQWMsR0FBZCxDQUFqQjs7QUFFQSxpQkFBTSxJQUFJYixJQUFJLENBQWQsRUFBaUJBLElBQUkyQyxXQUFXck4sTUFBaEMsRUFBd0MwSyxHQUF4QyxFQUE4QztBQUMxQyxvQkFBS3pILEdBQUc1QyxRQUFILENBQWFnTixXQUFXM0MsQ0FBWCxDQUFiLEtBQWdDLEtBQXJDLEVBQTZDO0FBQ3pDLDJCQUFPLEtBQVA7QUFDSDtBQUNKOztBQUVELG1CQUFPLElBQVA7QUFDSCxTQXRORTs7QUF3Tkg7Ozs7O0FBS0FwQyxtQkFBVyxtQkFBU3JGLEVBQVQsRUFBWTtBQUNuQixnQkFBSXFLLFFBQVE5USxFQUFFeUcsRUFBRixFQUFNcUssS0FBTixFQUFaO0FBQ0FBLGtCQUFNakosR0FBTixDQUFVLFlBQVYsRUFBdUIsUUFBdkI7QUFDQWlKLGtCQUFNakosR0FBTixDQUFVLFVBQVYsRUFBc0IsUUFBdEI7QUFDQWlKLGtCQUFNakosR0FBTixDQUFVLFFBQVYsRUFBbUIsR0FBbkI7QUFDQTdILGNBQUUsTUFBRixFQUFVb0wsTUFBVixDQUFpQjBGLEtBQWpCO0FBQ0EsZ0JBQUkxTyxRQUFRME8sTUFBTUMsVUFBTixFQUFaO0FBQ0FELGtCQUFNOUYsTUFBTjs7QUFFQSxtQkFBTzVJLEtBQVA7QUFDSCxTQXZPRTs7QUF5T0g7Ozs7O0FBS0E0TyxrQ0FBMEIsa0NBQVN2SyxFQUFULEVBQWE7QUFDbkMsZ0JBQUl3SyxTQUFTLEtBQWI7O0FBRUF4SyxlQUFHeUssT0FBSCxHQUFhbEwsSUFBYixDQUFrQixZQUFZO0FBQzFCLG9CQUFJaEcsRUFBRSxJQUFGLEVBQVE2SCxHQUFSLENBQVksVUFBWixLQUEyQixPQUEvQixFQUF3QztBQUNwQ29KLDZCQUFTLElBQVQ7QUFDQTtBQUNIO0FBQ0osYUFMRDs7QUFPQSxtQkFBT0EsTUFBUDtBQUNILFNBelBFOztBQTJQSDs7O0FBR0FFLGVBQU8sZUFBU0MsWUFBVCxFQUF1QjtBQUMxQixnQkFBSUMsUUFBUSxJQUFJM0IsSUFBSixHQUFXQyxPQUFYLEVBQVo7QUFDQSxpQkFBSyxJQUFJekIsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEdBQXBCLEVBQXlCQSxHQUF6QixFQUE4QjtBQUMxQixvQkFBSyxJQUFJd0IsSUFBSixHQUFXQyxPQUFYLEtBQXVCMEIsS0FBeEIsR0FBaUNELFlBQXJDLEVBQWtEO0FBQzlDO0FBQ0g7QUFDSjtBQUNKLFNBclFFOztBQXVRSDs7Ozs7O0FBTUFFLHNCQUFjLHNCQUFTQyxHQUFULEVBQWNDLEdBQWQsRUFBbUI7QUFDN0IsbUJBQU9qQyxLQUFLQyxLQUFMLENBQVdELEtBQUtFLE1BQUwsTUFBaUIrQixNQUFNRCxHQUFOLEdBQVksQ0FBN0IsQ0FBWCxJQUE4Q0EsR0FBckQ7QUFDSCxTQS9RRTs7QUFpUkg7Ozs7O0FBS0FFLGtCQUFVLGtCQUFTQyxJQUFULEVBQWU7QUFDckIsbUJBQU9wRSxPQUFPb0UsSUFBUCxDQUFQO0FBQ0gsU0F4UkU7O0FBMFJIOzs7O0FBSUFDLDBCQUFrQiw0QkFBVztBQUN6QixtQkFBT3ZELE9BQU93RCxJQUFQLEtBQWdCMUIsU0FBaEIsR0FBNkIsSUFBN0IsR0FBb0MsS0FBM0M7QUFDSDtBQWhTRSxLQUFQO0FBa1NILENBbFZvQixFQUFkOztBQW9WUDtBQUNBO0FBQ0E7QUFDQSxNOzs7Ozs7Ozs7OztBQzNWQTs7QUFDQTs7QUFFQyxXQUFTbFEsQ0FBVCxFQUFZO0FBQ1Q7QUFDQUEsTUFBRTZSLEVBQUYsQ0FBS0MsU0FBTCxHQUFpQixVQUFTaFIsT0FBVCxFQUFrQjtBQUMvQjtBQUNBLFlBQUlpUixXQUFXLEVBQWY7QUFDQSxZQUFJQyxVQUFVaFMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7QUFDQSxZQUFJaVMsU0FBUztBQUNUOzs7QUFHQUMsaUJBQUssYUFBU3BSLE9BQVQsRUFBa0I7QUFDbkIsb0JBQUksQ0FBQ2tSLFFBQVEvUSxJQUFSLENBQWEsVUFBYixDQUFMLEVBQStCO0FBQzNCO0FBQ0FnUiwyQkFBTzdSLElBQVAsQ0FBWVUsT0FBWjtBQUNBbVIsMkJBQU9FLEtBQVA7QUFDQUYsMkJBQU8zSixLQUFQOztBQUVBO0FBQ0EwSiw0QkFBUS9RLElBQVIsQ0FBYSxVQUFiLEVBQXlCOFEsUUFBekI7QUFDSCxpQkFSRCxNQVFPO0FBQ0g7QUFDQUEsK0JBQVdDLFFBQVEvUSxJQUFSLENBQWEsVUFBYixDQUFYO0FBQ0g7O0FBRUQsdUJBQU84USxRQUFQO0FBQ0gsYUFuQlE7O0FBcUJUOzs7QUFHQTNSLGtCQUFNLGNBQVNVLE9BQVQsRUFBa0I7QUFDcEJpUix5QkFBU0ssTUFBVCxHQUFrQixFQUFsQjtBQUNBTCx5QkFBU00sUUFBVCxHQUFvQixLQUFwQjtBQUNBTix5QkFBU3BRLEtBQVQsR0FBaUJxUSxRQUFRTSxJQUFSLENBQWEsb0JBQWIsQ0FBakI7QUFDQVAseUJBQVNuUSxNQUFULEdBQWtCb1EsUUFBUU0sSUFBUixDQUFhLHFCQUFiLENBQWxCO0FBQ0FQLHlCQUFTUSxLQUFULEdBQWlCUCxRQUFRTSxJQUFSLENBQWEsb0JBQWIsQ0FBakI7QUFDQVAseUJBQVNTLE9BQVQsR0FBbUJSLFFBQVFNLElBQVIsQ0FBYSxzQkFBYixDQUFuQjtBQUNBUCx5QkFBU1UsVUFBVCxHQUFzQlQsUUFBUU0sSUFBUixDQUFhLHlCQUFiLENBQXRCO0FBQ0FQLHlCQUFTVyxjQUFULEdBQTBCVixRQUFRbk8sUUFBUixDQUFpQixnQkFBakIsSUFBcUMsSUFBckMsR0FBNEMsTUFBdEU7QUFDQWtPLHlCQUFTWSxjQUFULEdBQTBCWixTQUFTVyxjQUFuQzs7QUFFQVgseUJBQVNqUixPQUFULEdBQW1CZCxFQUFFc0ssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CdEssRUFBRTZSLEVBQUYsQ0FBS0MsU0FBTCxDQUFlYyxRQUFsQyxFQUE0QzlSLE9BQTVDLENBQW5CO0FBQ0Esb0JBQUlrUixRQUFRL1EsSUFBUixDQUFhLFdBQWIsTUFBOEIsSUFBbEMsRUFBd0M7QUFDcEM4USw2QkFBU2pSLE9BQVQsQ0FBaUIrUixRQUFqQixHQUE0QixJQUE1QjtBQUNILGlCQUZELE1BRU8sSUFBSWIsUUFBUS9RLElBQVIsQ0FBYSxXQUFiLE1BQThCLEtBQWxDLEVBQXlDO0FBQzVDOFEsNkJBQVNqUixPQUFULENBQWlCK1IsUUFBakIsR0FBNEIsS0FBNUI7QUFDSDs7QUFFRCxvQkFBSWQsU0FBU1UsVUFBVCxDQUFvQmpQLE1BQXBCLEdBQTZCLENBQWpDLEVBQW9DO0FBQ2hDLHdCQUFJdU8sU0FBU1UsVUFBVCxDQUFvQnhSLElBQXBCLENBQXlCLFlBQXpCLENBQUosRUFBNEM7QUFDeEM4USxpQ0FBU2pSLE9BQVQsQ0FBaUJnUyxTQUFqQixHQUE2QmYsU0FBU1UsVUFBVCxDQUFvQnhSLElBQXBCLENBQXlCLFlBQXpCLENBQTdCO0FBQ0g7O0FBRUQsd0JBQUk4USxTQUFTVSxVQUFULENBQW9CeFIsSUFBcEIsQ0FBeUIsWUFBekIsQ0FBSixFQUE0QztBQUN4QzhRLGlDQUFTalIsT0FBVCxDQUFpQjhHLFNBQWpCLEdBQTZCbUssU0FBU1UsVUFBVCxDQUFvQnhSLElBQXBCLENBQXlCLFlBQXpCLENBQTdCO0FBQ0g7QUFDSjtBQUNKLGFBbkRROztBQXFEVDs7O0FBR0FrUixtQkFBTyxpQkFBVztBQUNkLG9CQUFJaFMsWUFBTWtHLGNBQU4sRUFBSixFQUE0QjtBQUN4Qix3QkFBSTJMLFFBQVEvUSxJQUFSLENBQWEsaUJBQWIsS0FBbUMsT0FBbkMsSUFBOEMrUSxRQUFRL1EsSUFBUixDQUFhLGlCQUFiLEtBQW1DLE9BQXJGLEVBQThGO0FBQzFGOFEsaUNBQVNqUixPQUFULENBQWlCYyxNQUFqQixHQUEwQixPQUExQjtBQUNILHFCQUZELE1BRU87QUFDSG1RLGlDQUFTalIsT0FBVCxDQUFpQmMsTUFBakIsR0FBMEIsT0FBMUI7QUFDQW1RLGlDQUFTblEsTUFBVCxDQUFnQnlDLEtBQWhCLENBQXNCNE4sT0FBT3JRLE1BQTdCO0FBQ0g7QUFDSixpQkFQRCxNQU9PO0FBQ0gsd0JBQUlvUSxRQUFRL1EsSUFBUixDQUFhLGlCQUFiLEtBQW1DLE9BQXZDLEVBQWdEO0FBQzVDOFEsaUNBQVNqUixPQUFULENBQWlCYyxNQUFqQixHQUEwQixPQUExQjtBQUNBb1EsZ0NBQVFlLFVBQVIsQ0FBbUJkLE9BQU8zTCxJQUExQjtBQUNILHFCQUhELE1BR08sSUFBSTBMLFFBQVEvUSxJQUFSLENBQWEsaUJBQWIsS0FBbUMsT0FBdkMsRUFBZ0Q7QUFDbkQ4USxpQ0FBU2pSLE9BQVQsQ0FBaUJjLE1BQWpCLEdBQTBCLE9BQTFCO0FBQ0gscUJBRk0sTUFFQTtBQUNILDRCQUFJbVEsU0FBU2pSLE9BQVQsQ0FBaUJjLE1BQWpCLElBQTJCLE9BQS9CLEVBQXdDO0FBQ3BDb1Esb0NBQVFnQixVQUFSLENBQW1CZixPQUFPZ0IsSUFBMUI7QUFDQWpCLG9DQUFRZSxVQUFSLENBQW1CZCxPQUFPM0wsSUFBMUI7QUFDSCx5QkFIRCxNQUdPO0FBQ0h5TCxxQ0FBU25RLE1BQVQsQ0FBZ0J5QyxLQUFoQixDQUFzQjROLE9BQU9yUSxNQUE3QjtBQUNIO0FBQ0o7QUFDSjs7QUFFRDtBQUNBLG9CQUFJbVEsU0FBU3BRLEtBQVQsQ0FBZTZCLE1BQW5CLEVBQTJCO0FBQ3ZCdU8sNkJBQVNwUSxLQUFULENBQWVSLEVBQWYsQ0FBa0IsT0FBbEIsRUFBMkI4USxPQUFPM0wsSUFBbEM7QUFDSDs7QUFFRDtBQUNBMkwsdUJBQU9pQixZQUFQO0FBQ0gsYUF2RlE7O0FBeUZUOzs7QUFHQTVLLG1CQUFPLGlCQUFXO0FBQ2Qsb0JBQUl5SixTQUFTalIsT0FBVCxDQUFpQnFTLFNBQXJCLEVBQWdDO0FBQzVCbkIsNEJBQVF4TixRQUFSLENBQWlCLGlCQUFpQnVOLFNBQVNqUixPQUFULENBQWlCcVMsU0FBbkQ7QUFDSDs7QUFFRCxvQkFBSXBCLFNBQVNqUixPQUFULENBQWlCc1MsS0FBckIsRUFBNEI7QUFDeEJwQiw0QkFBUXhOLFFBQVIsQ0FBaUIsdUJBQXVCdU4sU0FBU2pSLE9BQVQsQ0FBaUJzUyxLQUF6RDtBQUNIOztBQUVELG9CQUFJckIsU0FBU2pSLE9BQVQsQ0FBaUJzQixLQUFyQixFQUE0QjtBQUN4QjJQLDZCQUFTUyxPQUFULENBQWlCM0ssR0FBakIsQ0FBcUIsT0FBckIsRUFBOEJrSyxTQUFTalIsT0FBVCxDQUFpQnNCLEtBQS9DO0FBQ0g7O0FBRUQsb0JBQUk0UCxRQUFRL1EsSUFBUixDQUFhLHFCQUFiLENBQUosRUFBeUM7QUFDckM4USw2QkFBU2pSLE9BQVQsQ0FBaUJ1UyxVQUFqQixHQUE4QixJQUE5QjtBQUNIOztBQUVEO0FBQ0Esb0JBQUl0QixTQUFTalIsT0FBVCxDQUFpQmdTLFNBQXJCLEVBQWdDO0FBQzVCZiw2QkFBU1UsVUFBVCxDQUFvQjVLLEdBQXBCLENBQXdCLFlBQXhCLEVBQXNDa0ssU0FBU2pSLE9BQVQsQ0FBaUJnUyxTQUF2RDtBQUNIOztBQUVELG9CQUFJZixTQUFTalIsT0FBVCxDQUFpQjhHLFNBQXJCLEVBQWdDO0FBQzVCbUssNkJBQVNVLFVBQVQsQ0FBb0I1SyxHQUFwQixDQUF3QixZQUF4QixFQUFzQ2tLLFNBQVNqUixPQUFULENBQWlCOEcsU0FBdkQ7QUFDQW1LLDZCQUFTVSxVQUFULENBQW9CNUssR0FBcEIsQ0FBd0IsWUFBeEIsRUFBc0MsTUFBdEM7O0FBRUEsd0JBQUkxSCxZQUFNK08sZUFBTixFQUFKLEVBQTZCO0FBQ3pCN08sa0NBQUtvRCxZQUFMLENBQWtCc08sU0FBU1UsVUFBM0IsRUFBdUMsRUFBdkM7QUFDSDtBQUNKOztBQUVEO0FBQ0FSLHVCQUFPcUIsU0FBUDtBQUNILGFBN0hROztBQStIVDs7O0FBR0FDLGtCQUFNLGdCQUFXO0FBQ2J2VCxrQkFBRWdTLE9BQUYsRUFBVy9RLElBQVgsQ0FBZ0IsVUFBaEIsRUFBNEI4USxRQUE1QjtBQUNILGFBcElROztBQXNJVDs7O0FBR0FtQiwwQkFBYyx3QkFBVztBQUNyQmxCLHdCQUFRN1EsRUFBUixDQUFXLE9BQVgsRUFBb0IsK0NBQXBCLEVBQXFFLFVBQVMyRSxDQUFULEVBQVk7QUFDN0VBLHNCQUFFb0UsY0FBRjtBQUNBcEUsc0JBQUUwTixlQUFGO0FBQ0gsaUJBSEQ7QUFJSCxhQTlJUTs7QUFnSlQ7OztBQUdBNVIsb0JBQVEsa0JBQVc7QUFDZixvQkFBSW1RLFNBQVMwQixJQUFiLEVBQW1CO0FBQ2YsMkJBQU94QixPQUFPM0wsSUFBUCxFQUFQO0FBQ0gsaUJBRkQsTUFFTztBQUNILDJCQUFPMkwsT0FBT2dCLElBQVAsRUFBUDtBQUNIO0FBQ0osYUF6SlE7O0FBMkpUOzs7QUFHQVMsd0JBQVksb0JBQVNDLE9BQVQsRUFBa0I7QUFDMUIzQix3QkFBUU0sSUFBUixDQUFhLHNCQUFiLEVBQXFDL0ssSUFBckMsQ0FBMENvTSxPQUExQzs7QUFFQSx1QkFBTzVCLFFBQVA7QUFDSCxhQWxLUTs7QUFvS1Q7OztBQUdBa0Isa0JBQU0sZ0JBQVc7QUFDYixvQkFBSWxCLFNBQVNqUixPQUFULENBQWlCYyxNQUFqQixJQUEyQixPQUEzQixJQUFzQ29RLFFBQVEvUSxJQUFSLENBQWEsT0FBYixDQUExQyxFQUFpRTtBQUM3RGdSLDJCQUFPMkIsWUFBUDtBQUNBLDJCQUFPN0IsUUFBUDtBQUNIOztBQUVELG9CQUFJQSxTQUFTMEIsSUFBYixFQUFtQjtBQUNmLDJCQUFPMUIsUUFBUDtBQUNIOztBQUVELG9CQUFJQSxTQUFTUSxLQUFULENBQWUvTyxNQUFmLEdBQXdCLENBQTVCLEVBQStCO0FBQzNCeU8sMkJBQU80QixjQUFQO0FBQ0g7O0FBRUQ1Qix1QkFBTzZCLFlBQVAsQ0FBb0IsWUFBcEI7O0FBRUE3Qix1QkFBTzhCLFVBQVA7O0FBRUEvQix3QkFBUXhOLFFBQVIsQ0FBaUIsa0JBQWpCOztBQUVBLG9CQUFJckUsWUFBTWtHLGNBQU4sTUFBMEIwTCxTQUFTalIsT0FBVCxDQUFpQmtULGFBQS9DLEVBQThEO0FBQzFELHdCQUFJMUgsU0FBU3lGLFNBQVNTLE9BQVQsQ0FBaUIzSyxHQUFqQixDQUFxQixRQUFyQixJQUFpQyxDQUE5QztBQUNBLHdCQUFJb00sY0FBY2pVLEVBQUUsMkNBQUYsQ0FBbEI7O0FBRUFpVSxnQ0FBWXBNLEdBQVosQ0FBZ0IsUUFBaEIsRUFBMEJ5RSxNQUExQjtBQUNBMkgsZ0NBQVloVCxJQUFaLENBQWlCLFVBQWpCLEVBQTZCK1EsT0FBN0I7QUFDQUEsNEJBQVEvUSxJQUFSLENBQWEsU0FBYixFQUF3QmdULFdBQXhCO0FBQ0FqQyw0QkFBUTdHLEtBQVIsQ0FBYzhJLFdBQWQ7QUFDQUEsZ0NBQVk1UCxLQUFaLENBQWtCLFVBQVN5QixDQUFULEVBQVk7QUFDMUJtTSwrQkFBTzNMLElBQVA7QUFDQXRHLDBCQUFFLElBQUYsRUFBUWdMLE1BQVI7QUFDQWxGLDBCQUFFb0UsY0FBRjtBQUNILHFCQUpEO0FBS0g7O0FBRUQ4SCx3QkFBUXJILEtBQVI7QUFDQXFILHdCQUFRa0MsSUFBUixDQUFhLGVBQWIsRUFBOEIsTUFBOUI7QUFDQW5DLHlCQUFTMEIsSUFBVCxHQUFnQixJQUFoQjs7QUFFQXhCLHVCQUFPa0Msa0JBQVA7O0FBRUFsQyx1QkFBTzZCLFlBQVAsQ0FBb0IsV0FBcEI7O0FBRUEsdUJBQU8vQixRQUFQO0FBQ0gsYUFuTlE7O0FBcU5UOzs7QUFHQTZCLDBCQUFjLHdCQUFXO0FBQ3JCNUIsd0JBQVFvQyxVQUFSLENBQW1CLE9BQW5CO0FBQ0Esb0JBQUlDLFVBQVVyQyxRQUFRL1EsSUFBUixDQUFhLFNBQWIsQ0FBZDtBQUNBK1Esd0JBQVFvQyxVQUFSLENBQW1CLFNBQW5CO0FBQ0EvRiw2QkFBYWdHLE9BQWI7QUFDSCxhQTdOUTs7QUErTlQ7OztBQUdBQyx5QkFBYSxxQkFBU0MsS0FBVCxFQUFnQjtBQUN6QixvQkFBSUEsS0FBSixFQUFXO0FBQ1Asd0JBQUl0QyxPQUFPNkIsWUFBUCxDQUFvQixZQUFwQixNQUFzQyxLQUExQyxFQUFpRDtBQUM3QztBQUNBO0FBQ0g7O0FBRUQ3QiwyQkFBTzJCLFlBQVA7QUFDQTVCLDRCQUFRdk4sV0FBUixDQUFvQixrQkFBcEI7QUFDQXNOLDZCQUFTMEIsSUFBVCxHQUFnQixLQUFoQjtBQUNBeEIsMkJBQU82QixZQUFQLENBQW9CLFdBQXBCO0FBQ0gsaUJBVkQsTUFVTztBQUNILHdCQUFJN0IsT0FBTzZCLFlBQVAsQ0FBb0IsWUFBcEIsTUFBc0MsS0FBMUMsRUFBaUQ7QUFDN0M7QUFDQTtBQUNIO0FBQ0Qsd0JBQUlPLFVBQVVoSixXQUFXLFlBQVc7QUFDaEMsNEJBQUkyRyxRQUFRL1EsSUFBUixDQUFhLE9BQWIsQ0FBSixFQUEyQjtBQUN2QmdSLG1DQUFPMkIsWUFBUDtBQUNBNUIsb0NBQVF2TixXQUFSLENBQW9CLGtCQUFwQjtBQUNBc04scUNBQVMwQixJQUFULEdBQWdCLEtBQWhCO0FBQ0F4QixtQ0FBTzZCLFlBQVAsQ0FBb0IsV0FBcEI7QUFDSDtBQUNKLHFCQVBhLEVBT1gvQixTQUFTalIsT0FBVCxDQUFpQjBULFlBUE4sQ0FBZDs7QUFTQXhDLDRCQUFRL1EsSUFBUixDQUFhLE9BQWIsRUFBc0IsSUFBdEI7QUFDQStRLDRCQUFRL1EsSUFBUixDQUFhLFNBQWIsRUFBd0JvVCxPQUF4QjtBQUNIO0FBQ0osYUE5UFE7O0FBZ1FUOzs7QUFHQUkseUJBQWEsdUJBQVc7QUFDcEIsb0JBQUl4QyxPQUFPNkIsWUFBUCxDQUFvQixZQUFwQixNQUFzQyxLQUExQyxFQUFpRDtBQUM3QztBQUNBO0FBQ0g7QUFDRDlCLHdCQUFRdk4sV0FBUixDQUFvQixrQkFBcEI7QUFDQSxvQkFBSXVOLFFBQVEvUSxJQUFSLENBQWEsU0FBYixDQUFKLEVBQTZCO0FBQ3pCK1EsNEJBQVEvUSxJQUFSLENBQWEsU0FBYixFQUF3QitKLE1BQXhCO0FBQ0g7QUFDRCtHLHlCQUFTMEIsSUFBVCxHQUFnQixLQUFoQjtBQUNBeEIsdUJBQU82QixZQUFQLENBQW9CLFdBQXBCO0FBQ0gsYUE5UVE7O0FBZ1JUOzs7QUFHQXhOLGtCQUFNLGNBQVNpTyxLQUFULEVBQWdCO0FBQ2xCLG9CQUFJeEMsU0FBUzBCLElBQVQsS0FBa0IsS0FBdEIsRUFBNkI7QUFDekIsMkJBQU8xQixRQUFQO0FBQ0g7O0FBRUQsb0JBQUlBLFNBQVNqUixPQUFULENBQWlCYyxNQUFqQixJQUEyQixPQUEvQixFQUF3QztBQUNwQ3FRLDJCQUFPcUMsV0FBUCxDQUFtQkMsS0FBbkI7QUFDSCxpQkFGRCxNQUVPO0FBQ0h0QywyQkFBT3dDLFdBQVA7QUFDSDs7QUFFRCxvQkFBSTFDLFNBQVNXLGNBQVQsSUFBMkIsTUFBM0IsSUFBcUNYLFNBQVNZLGNBQVQsSUFBMkIsSUFBcEUsRUFBMEU7QUFDdEVYLDRCQUFRdk4sV0FBUixDQUFvQixnQkFBcEI7QUFDQXNOLDZCQUFTUSxLQUFULENBQWVtQyxTQUFmLENBQXlCM0MsU0FBU1MsT0FBbEM7QUFDQVQsNkJBQVNZLGNBQVQsR0FBMEIsTUFBMUI7QUFDSDs7QUFFRCx1QkFBT1osUUFBUDtBQUNILGFBclNROztBQXVTVDs7O0FBR0FnQyx3QkFBWSxzQkFBVztBQUNuQi9ULGtCQUFFLDhCQUFGLEVBQWtDZ0csSUFBbEMsQ0FBdUMsWUFBVztBQUM5Q2hHLHNCQUFFLElBQUYsRUFBUThSLFNBQVIsR0FBb0J4TCxJQUFwQixDQUF5QixJQUF6QjtBQUNILGlCQUZEO0FBR0gsYUE5U1E7O0FBZ1RUOzs7QUFHQXVOLDRCQUFnQiwwQkFBVztBQUN2QixvQkFBSXpSLFFBQVE0UCxRQUFRakIsVUFBUixFQUFaO0FBQ0Esb0JBQUlqRSxZQUFZaUYsU0FBU1EsS0FBVCxDQUFlMU8sUUFBZixDQUF3QiwwQkFBeEIsSUFBc0QsT0FBdEQsR0FBZ0UsTUFBaEY7QUFDQSxvQkFBSW9GLE1BQU0sQ0FBVjs7QUFFQSxvQkFBSThJLFNBQVNRLEtBQVQsQ0FBZS9PLE1BQWYsR0FBd0IsQ0FBNUIsRUFBK0I7QUFDM0Isd0JBQUlyRCxZQUFNZ0QsbUJBQU4sQ0FBMEIsUUFBMUIsS0FBdUM2TyxRQUFRbk8sUUFBUixDQUFpQiwrQkFBakIsQ0FBM0MsRUFBOEY7QUFDMUZvRiw4QkFBTStJLFFBQVFqUixNQUFSLEdBQWlCaUwsSUFBakIsR0FBeUI1SixRQUFRLENBQWpDLEdBQXNDbU4sS0FBS29GLEdBQUwsQ0FBUzVDLFNBQVNRLEtBQVQsQ0FBZW5RLEtBQWYsS0FBeUIsQ0FBbEMsQ0FBdEMsR0FBNkVxTyxTQUFTc0IsU0FBU1MsT0FBVCxDQUFpQjNLLEdBQWpCLENBQXFCLE1BQXJCLENBQVQsQ0FBbkY7QUFDQWtLLGlDQUFTUSxLQUFULENBQWUxSyxHQUFmLENBQW1CLE9BQW5CLEVBQTRCLE1BQTVCO0FBQ0FrSyxpQ0FBU1EsS0FBVCxDQUFlMUssR0FBZixDQUFtQixNQUFuQixFQUEyQm9CLEdBQTNCO0FBQ0E4SSxpQ0FBU1EsS0FBVCxDQUFlMUssR0FBZixDQUFtQixhQUFuQixFQUFrQyxNQUFsQztBQUNBa0ssaUNBQVNRLEtBQVQsQ0FBZTFLLEdBQWYsQ0FBbUIsY0FBbkIsRUFBbUMsTUFBbkM7QUFDSCxxQkFORCxNQU1PLElBQUlrSyxTQUFTUSxLQUFULENBQWUxTyxRQUFmLENBQXdCLDJCQUF4QixDQUFKLEVBQTBEO0FBQzdEb0YsOEJBQU03RyxRQUFRLENBQVIsR0FBWW1OLEtBQUtvRixHQUFMLENBQVM1QyxTQUFTUSxLQUFULENBQWVuUSxLQUFmLEtBQXlCLENBQWxDLENBQWxCO0FBQ0EsNEJBQUk0UCxRQUFRbk8sUUFBUixDQUFpQix3QkFBakIsQ0FBSixFQUFnRDtBQUM1Q29GLGtDQUFNQSxNQUFNLEVBQVo7QUFDSDtBQUNELDRCQUFJNkQsYUFBYSxPQUFqQixFQUEwQjtBQUN0QmlGLHFDQUFTUSxLQUFULENBQWUxSyxHQUFmLENBQW1CLE1BQW5CLEVBQTJCLE1BQTNCO0FBQ0FrSyxxQ0FBU1EsS0FBVCxDQUFlMUssR0FBZixDQUFtQixPQUFuQixFQUE0Qm9CLEdBQTVCO0FBQ0gseUJBSEQsTUFHTztBQUNIOEkscUNBQVNRLEtBQVQsQ0FBZTFLLEdBQWYsQ0FBbUIsT0FBbkIsRUFBNEIsTUFBNUI7QUFDQWtLLHFDQUFTUSxLQUFULENBQWUxSyxHQUFmLENBQW1CLE1BQW5CLEVBQTJCb0IsR0FBM0I7QUFDSDtBQUNKO0FBQ0o7QUFDSixhQTdVUTs7QUErVVQ7OztBQUdBa0wsZ0NBQW9CLDhCQUFXO0FBQzNCOztBQUVBLG9CQUFJcEMsU0FBU2pSLE9BQVQsQ0FBaUIrUixRQUFqQixJQUE2QixJQUFqQyxFQUF1QztBQUNuQyx3QkFBSVosT0FBTzJDLG9CQUFQLE9BQWtDLEtBQXRDLEVBQTZDO0FBQ3pDLDRCQUFJN0MsU0FBU1ksY0FBVCxJQUEyQixJQUEvQixFQUFxQztBQUNqQ1gsb0NBQVF2TixXQUFSLENBQW9CLGdCQUFwQjtBQUNBc04scUNBQVNRLEtBQVQsQ0FBZW1DLFNBQWYsQ0FBeUIzQyxTQUFTUyxPQUFsQztBQUNBVCxxQ0FBU1ksY0FBVCxHQUEwQixNQUExQjtBQUNILHlCQUpELE1BSU8sSUFBSVosU0FBU1ksY0FBVCxJQUEyQixNQUEvQixFQUF1QztBQUMxQ1gsb0NBQVF4TixRQUFSLENBQWlCLGdCQUFqQjtBQUNBdU4scUNBQVNRLEtBQVQsQ0FBZXNDLFFBQWYsQ0FBd0I5QyxTQUFTUyxPQUFqQztBQUNBVCxxQ0FBU1ksY0FBVCxHQUEwQixJQUExQjtBQUNIO0FBQ0o7QUFDSjtBQUNKLGFBbFdROztBQW9XVDs7O0FBR0FXLHVCQUFXLHFCQUFXO0FBQ2xCLG9CQUFJd0IsWUFBWS9DLFNBQVNTLE9BQVQsQ0FBaUIzSyxHQUFqQixDQUFxQixTQUFyQixDQUFoQjtBQUNBLG9CQUFJa04sWUFBWTVVLFlBQU1rUSxnQkFBTixDQUF1QjJCLE9BQXZCLENBQWhCO0FBQ0Esb0JBQUkrQyxZQUFZRCxTQUFoQixFQUEyQjtBQUN2Qi9DLDZCQUFTUyxPQUFULENBQWlCM0ssR0FBakIsQ0FBcUIsU0FBckIsRUFBZ0NtTixNQUFoQztBQUNIO0FBQ0osYUE3V1E7O0FBK1dUOzs7QUFHQUMsMEJBQWMsd0JBQVc7QUFDckIsdUJBQU9sRCxTQUFTalIsT0FBVCxDQUFpQnVTLFVBQXhCO0FBQ0gsYUFwWFE7O0FBc1hUOzs7QUFHQTZCLHFCQUFTLG1CQUFXO0FBQ2hCLHVCQUFPbkQsU0FBUzBCLElBQWhCO0FBQ0gsYUEzWFE7O0FBNlhUOzs7QUFHQW1CLGtDQUFzQixnQ0FBVztBQUM3QixvQkFBSW5PLEtBQUtzTCxTQUFTUyxPQUFsQjtBQUNBLG9CQUFJelIsU0FBUzBGLEdBQUcxRixNQUFILEVBQWI7QUFDQSxvQkFBSXNDLFNBQVNvRCxHQUFHbEQsV0FBSCxFQUFiO0FBQ0Esb0JBQUluQixRQUFRcUUsR0FBR3JFLEtBQUgsRUFBWjtBQUNBLG9CQUFJcVEsYUFBYWhNLEdBQUc2TCxJQUFILENBQVEsbUJBQVIsQ0FBakI7O0FBRUEsb0JBQUlHLFdBQVdqUCxNQUFmLEVBQXVCO0FBQ25CLHdCQUFJaVAsV0FBV3hSLElBQVgsQ0FBZ0IsWUFBaEIsQ0FBSixFQUFtQztBQUMvQm9DLGtDQUFVb04sU0FBU2dDLFdBQVd4UixJQUFYLENBQWdCLFlBQWhCLENBQVQsQ0FBVjtBQUNILHFCQUZELE1BRU8sSUFBSXdSLFdBQVd4UixJQUFYLENBQWdCLFFBQWhCLENBQUosRUFBK0I7QUFDbENvQyxrQ0FBVW9OLFNBQVNnQyxXQUFXeFIsSUFBWCxDQUFnQixRQUFoQixDQUFULENBQVY7QUFDSDtBQUNKOztBQUVELHVCQUFRRixPQUFPbUksR0FBUCxHQUFhN0YsTUFBYixHQUFzQnJELEVBQUVvTyxNQUFGLEVBQVVoRixTQUFWLEtBQXdCcEosRUFBRW9PLE1BQUYsRUFBVS9LLE1BQVYsRUFBdEQ7QUFDSCxhQWhaUTs7QUFrWlQ7OztBQUdBeVEsMEJBQWMsc0JBQVNwQyxJQUFULEVBQWU7QUFDekIscUJBQUssSUFBSXhELElBQUksQ0FBYixFQUFnQkEsSUFBSTZELFNBQVNLLE1BQVQsQ0FBZ0I1TyxNQUFwQyxFQUE0QzBLLEdBQTVDLEVBQWlEO0FBQzdDLHdCQUFJL0YsUUFBUTRKLFNBQVNLLE1BQVQsQ0FBZ0JsRSxDQUFoQixDQUFaO0FBQ0Esd0JBQUkvRixNQUFNdUosSUFBTixJQUFjQSxJQUFsQixFQUF3QjtBQUNwQiw0QkFBSXZKLE1BQU1nTixHQUFOLElBQWEsSUFBakIsRUFBdUI7QUFDbkIsZ0NBQUloTixNQUFNaU4sS0FBTixJQUFlLEtBQW5CLEVBQTBCO0FBQ3RCckQseUNBQVNLLE1BQVQsQ0FBZ0JsRSxDQUFoQixFQUFtQmtILEtBQW5CLEdBQTJCLElBQTNCO0FBQ0EsdUNBQU9qTixNQUFNa04sT0FBTixDQUFjbEgsSUFBZCxDQUFtQixJQUFuQixFQUF5QjRELFFBQXpCLENBQVA7QUFDSDtBQUNKLHlCQUxELE1BS087QUFDSCxtQ0FBTzVKLE1BQU1rTixPQUFOLENBQWNsSCxJQUFkLENBQW1CLElBQW5CLEVBQXlCNEQsUUFBekIsQ0FBUDtBQUNIO0FBQ0o7QUFDSjtBQUNKLGFBbmFROztBQXFhVHVELHNCQUFVLGtCQUFTNUQsSUFBVCxFQUFlMkQsT0FBZixFQUF3QkYsR0FBeEIsRUFBNkI7QUFDbkNwRCx5QkFBU0ssTUFBVCxDQUFnQjdELElBQWhCLENBQXFCO0FBQ2pCbUQsMEJBQU1BLElBRFc7QUFFakIyRCw2QkFBU0EsT0FGUTtBQUdqQkYseUJBQUtBLEdBSFk7QUFJakJDLDJCQUFPO0FBSlUsaUJBQXJCOztBQU9BbkQsdUJBQU9zQixJQUFQOztBQUVBLHVCQUFPeEIsUUFBUDtBQUNIO0FBaGJRLFNBQWI7O0FBbWJBO0FBQ0FFLGVBQU9DLEdBQVAsQ0FBV3FELEtBQVgsQ0FBaUIsSUFBakIsRUFBdUIsQ0FBQ3pVLE9BQUQsQ0FBdkI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7O0FBSUFpUixpQkFBU2tCLElBQVQsR0FBZ0IsWUFBVztBQUN2QixtQkFBT2hCLE9BQU9nQixJQUFQLEVBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUFsQixpQkFBU3pMLElBQVQsR0FBZ0IsWUFBVztBQUN2QixtQkFBTzJMLE9BQU8zTCxJQUFQLEVBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUF5TCxpQkFBU25RLE1BQVQsR0FBa0IsWUFBVztBQUN6QixtQkFBT3FRLE9BQU9yUSxNQUFQLEVBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUFtUSxpQkFBU2tELFlBQVQsR0FBd0IsWUFBVztBQUMvQixtQkFBT2hELE9BQU9nRCxZQUFQLEVBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUFsRCxpQkFBU21ELE9BQVQsR0FBbUIsWUFBVztBQUMxQixtQkFBT2pELE9BQU9pRCxPQUFQLEVBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUFuRCxpQkFBU3lELGVBQVQsR0FBMkIsWUFBVztBQUNsQyxtQkFBT3ZELE9BQU9rQyxrQkFBUCxFQUFQO0FBQ0gsU0FGRDs7QUFJQTs7OztBQUlBcEMsaUJBQVMyQixVQUFULEdBQXNCLFVBQVNDLE9BQVQsRUFBa0I7QUFDcEMsbUJBQU8xQixPQUFPeUIsVUFBUCxDQUFrQkMsT0FBbEIsQ0FBUDtBQUNILFNBRkQ7O0FBSUE7Ozs7QUFJQTVCLGlCQUFTNVEsRUFBVCxHQUFjLFVBQVN1USxJQUFULEVBQWUyRCxPQUFmLEVBQXdCO0FBQ2xDLG1CQUFPcEQsT0FBT3FELFFBQVAsQ0FBZ0I1RCxJQUFoQixFQUFzQjJELE9BQXRCLENBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUF0RCxpQkFBU29ELEdBQVQsR0FBZSxVQUFTekQsSUFBVCxFQUFlMkQsT0FBZixFQUF3QjtBQUNuQyxtQkFBT3BELE9BQU9xRCxRQUFQLENBQWdCNUQsSUFBaEIsRUFBc0IyRCxPQUF0QixFQUErQixJQUEvQixDQUFQO0FBQ0gsU0FGRDs7QUFJQSxlQUFPdEQsUUFBUDtBQUNILEtBemdCRDs7QUEyZ0JBO0FBQ0EvUixNQUFFNlIsRUFBRixDQUFLQyxTQUFMLENBQWVjLFFBQWYsR0FBMEI7QUFDdEJoUixnQkFBUSxPQURjO0FBRXRCNFMsc0JBQWMsR0FGUTtBQUd0QjlOLGNBQU0sU0FIZ0I7QUFJdEJyRCxnQkFBUSxNQUpjO0FBS3RCd1Asa0JBQVUsSUFMWTtBQU10QmpMLG1CQUFXLEtBTlc7QUFPdEJrTCxtQkFBVyxLQVBXO0FBUXRCTyxvQkFBWSxLQVJVO0FBU3RCVyx1QkFBZTtBQVRPLEtBQTFCOztBQVlBO0FBQ0EsUUFBSTdULFlBQU1rRyxjQUFOLEVBQUosRUFBNEI7QUFDeEJyRyxVQUFFQyxRQUFGLEVBQVlrQixFQUFaLENBQWUsT0FBZixFQUF3Qiw0R0FBeEIsRUFBc0ksVUFBUzJFLENBQVQsRUFBWTtBQUM5SUEsY0FBRW9FLGNBQUY7QUFDQWxLLGNBQUUsSUFBRixFQUFRMlEsTUFBUixDQUFlLGFBQWYsRUFBOEJtQixTQUE5QixHQUEwQ2xRLE1BQTFDO0FBQ0gsU0FIRDtBQUlILEtBTEQsTUFLTztBQUNINUIsVUFBRUMsUUFBRixFQUFZa0IsRUFBWixDQUFlLE9BQWYsRUFBd0Isc0RBQXhCLEVBQWdGLFVBQVMyRSxDQUFULEVBQVk7QUFDeEZBLGNBQUVvRSxjQUFGO0FBQ0FsSyxjQUFFLElBQUYsRUFBUTJRLE1BQVIsQ0FBZSxhQUFmLEVBQThCbUIsU0FBOUIsR0FBMENsUSxNQUExQztBQUNILFNBSEQ7QUFJQTVCLFVBQUVDLFFBQUYsRUFBWWtCLEVBQVosQ0FBZSxZQUFmLEVBQTZCLGtDQUE3QixFQUFpRSxVQUFTMkUsQ0FBVCxFQUFZO0FBQ3pFOUYsY0FBRSxJQUFGLEVBQVE4UixTQUFSLEdBQW9CbFEsTUFBcEI7QUFDSCxTQUZEO0FBR0g7O0FBRUQ7QUFDQTVCLE1BQUVDLFFBQUYsRUFBWWtCLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQVMyRSxDQUFULEVBQVk7QUFDaEM5RixVQUFFLDhCQUFGLEVBQWtDZ0csSUFBbEMsQ0FBdUMsWUFBVztBQUM5QyxnQkFBSSxDQUFDaEcsRUFBRSxJQUFGLEVBQVFpQixJQUFSLENBQWEsVUFBYixDQUFMLEVBQStCO0FBQzNCO0FBQ0g7O0FBRUQsZ0JBQUlZLFNBQVM3QixFQUFFOEYsRUFBRWpFLE1BQUosQ0FBYjtBQUNBLGdCQUFJa1EsV0FBVy9SLEVBQUUsSUFBRixFQUFROFIsU0FBUixFQUFmO0FBQ0EsZ0JBQUlsUSxTQUFTNUIsRUFBRSxJQUFGLEVBQVFzUyxJQUFSLENBQWEscUJBQWIsQ0FBYjs7QUFFQSxnQkFBSTFRLE9BQU80QixNQUFQLEdBQWdCLENBQWhCLElBQXFCM0IsT0FBTzRULEVBQVAsQ0FBVTdULE1BQVYsTUFBc0IsSUFBM0MsSUFBbURBLE9BQU8wUSxJQUFQLENBQVl6USxNQUFaLEVBQW9CMkIsTUFBcEIsS0FBK0IsQ0FBbEYsSUFBdUYzQixPQUFPeVEsSUFBUCxDQUFZMVEsTUFBWixFQUFvQjRCLE1BQXBCLEtBQStCLENBQXRILElBQTJIdU8sU0FBU2tELFlBQVQsTUFBMkIsS0FBMUosRUFBaUs7QUFDN0psRCx5QkFBU3pMLElBQVQ7QUFDSCxhQUZELE1BRU8sSUFBSXRHLEVBQUUsSUFBRixFQUFRc1MsSUFBUixDQUFhelEsTUFBYixFQUFxQjJCLE1BQXJCLEtBQWdDLENBQXBDLEVBQXVDO0FBQzFDdU8seUJBQVN6TCxJQUFUO0FBQ0g7QUFDSixTQWREO0FBZUgsS0FoQkQ7QUFpQkgsQ0E1akJBLEVBNGpCQzRCLE1BNWpCRCxDQUFELEM7Ozs7Ozs7Ozs7O0FDSEMsV0FBU2xJLENBQVQsRUFBWTs7QUFFVDtBQUNBQSxNQUFFNlIsRUFBRixDQUFLdlEsT0FBTCxHQUFlLFVBQVNSLE9BQVQsRUFBa0I7QUFDN0I7QUFDQSxZQUFJRCxTQUFTLElBQWI7QUFDQSxZQUFJbVIsVUFBVWhTLEVBQUUsSUFBRixDQUFkOztBQUVBO0FBQ0EsWUFBSWlTLFNBQVM7QUFDVDs7OztBQUlBQyxpQkFBSyxhQUFTcFIsT0FBVCxFQUFrQjtBQUNuQixvQkFBSWtSLFFBQVEvUSxJQUFSLENBQWEsUUFBYixDQUFKLEVBQTRCO0FBQ3hCSiw2QkFBU21SLFFBQVEvUSxJQUFSLENBQWEsUUFBYixDQUFUO0FBQ0gsaUJBRkQsTUFFTztBQUNIO0FBQ0FnUiwyQkFBTzdSLElBQVAsQ0FBWVUsT0FBWjs7QUFFQTtBQUNBbVIsMkJBQU92SCxLQUFQOztBQUVBO0FBQ0F1SCwyQkFBT0UsS0FBUDs7QUFFQUgsNEJBQVEvUSxJQUFSLENBQWEsUUFBYixFQUF1QkosTUFBdkI7QUFDSDs7QUFFRCx1QkFBT0EsTUFBUDtBQUNILGFBdEJROztBQXdCVDs7OztBQUlBVCxrQkFBTSxjQUFTVSxPQUFULEVBQWtCO0FBQ3BCRCx1QkFBT0MsT0FBUCxHQUFpQmQsRUFBRXNLLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQnRLLEVBQUU2UixFQUFGLENBQUt2USxPQUFMLENBQWFzUixRQUFoQyxFQUEwQzlSLE9BQTFDLENBQWpCO0FBQ0gsYUE5QlE7O0FBZ0NUOzs7O0FBSUFxUixtQkFBTyxpQkFBVztBQUNkRix1QkFBT3JRLE1BQVA7QUFDSCxhQXRDUTs7QUF3Q1RBLG9CQUFRLGtCQUFXO0FBQ2Ysb0JBQUk4VCxnQkFBZ0IsQ0FBcEI7O0FBRUEsb0JBQUk3VSxPQUFPQyxPQUFQLENBQWVFLFFBQWYsQ0FBd0JFLE1BQXhCLEtBQW1DLEtBQW5DLElBQTRDTCxPQUFPQyxPQUFQLENBQWVFLFFBQWYsQ0FBd0JLLE9BQXhCLEtBQW9DLEtBQXBGLEVBQTJGO0FBQ3ZGO0FBQ0g7O0FBRURyQixrQkFBRW9PLE1BQUYsRUFBVXVILE1BQVYsQ0FBaUIsWUFBVztBQUN4Qix3QkFBSTVVLFNBQVMsQ0FBYjs7QUFFQSx3QkFBSVosTUFBTWdELG1CQUFOLENBQTBCLFNBQTFCLENBQUosRUFBMEM7QUFDdENwQyxpQ0FBU0YsT0FBT0MsT0FBUCxDQUFlQyxNQUFmLENBQXNCTSxPQUEvQjtBQUNBRiw2QkFBS04sT0FBT0MsT0FBUCxDQUFlRSxRQUFmLENBQXdCSyxPQUF4QixDQUFnQ0YsRUFBckM7QUFDQUMsOEJBQU1QLE9BQU9DLE9BQVAsQ0FBZUUsUUFBZixDQUF3QkssT0FBeEIsQ0FBZ0NELEdBQXRDO0FBQ0gscUJBSkQsTUFJTyxJQUFJakIsTUFBTWdELG1CQUFOLENBQTBCLG1CQUExQixDQUFKLEVBQW9EO0FBQ3ZEcEMsaUNBQVNGLE9BQU9DLE9BQVAsQ0FBZUMsTUFBZixDQUFzQkcsTUFBL0I7QUFDQUMsNkJBQUtOLE9BQU9DLE9BQVAsQ0FBZUUsUUFBZixDQUF3QkUsTUFBeEIsQ0FBK0JDLEVBQXBDO0FBQ0FDLDhCQUFNUCxPQUFPQyxPQUFQLENBQWVFLFFBQWYsQ0FBd0JFLE1BQXhCLENBQStCRSxHQUFyQztBQUNIOztBQUVELHdCQUFJd1UsS0FBSzVWLEVBQUUsSUFBRixFQUFRb0osU0FBUixFQUFUOztBQUVBLHdCQUFJdkksT0FBT0MsT0FBUCxDQUFlK1UsT0FBbkIsRUFBNEI7QUFDeEIsNEJBQUlELEtBQUs3VSxNQUFULEVBQWdCO0FBQUU7QUFDZGYsOEJBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQnJELEVBQW5CO0FBQ0FuQiw4QkFBRSxNQUFGLEVBQVV5RSxXQUFWLENBQXNCckQsR0FBdEI7QUFDSCx5QkFIRCxNQUdPO0FBQUU7QUFDTHBCLDhCQUFFLE1BQUYsRUFBVXdFLFFBQVYsQ0FBbUJwRCxHQUFuQjtBQUNBcEIsOEJBQUUsTUFBRixFQUFVeUUsV0FBVixDQUFzQnRELEVBQXRCO0FBQ0g7QUFDSixxQkFSRCxNQVFPO0FBQ0gsNEJBQUl5VSxLQUFLN1UsTUFBTCxJQUFlMlUsZ0JBQWdCRSxFQUFuQyxFQUFzQztBQUFFO0FBQ3BDNVYsOEJBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQnJELEVBQW5CO0FBQ0FuQiw4QkFBRSxNQUFGLEVBQVV5RSxXQUFWLENBQXNCckQsR0FBdEI7QUFDSCx5QkFIRCxNQUdPO0FBQUU7QUFDTHBCLDhCQUFFLE1BQUYsRUFBVXdFLFFBQVYsQ0FBbUJwRCxHQUFuQjtBQUNBcEIsOEJBQUUsTUFBRixFQUFVeUUsV0FBVixDQUFzQnRELEVBQXRCO0FBQ0g7O0FBRUR1VSx3Q0FBZ0JFLEVBQWhCO0FBQ0g7QUFDSixpQkFsQ0Q7QUFtQ0gsYUFsRlE7O0FBb0ZUOzs7O0FBSUFsTCxtQkFBTyxpQkFBVyxDQUNqQjtBQXpGUSxTQUFiOztBQTRGQTtBQUNBdUgsZUFBT0MsR0FBUCxDQUFXcUQsS0FBWCxDQUFpQjFVLE1BQWpCLEVBQXlCLENBQUNDLE9BQUQsQ0FBekI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7O0FBSUFELGVBQU9pVixZQUFQLEdBQXNCLFlBQVc7QUFDaEM7QUFDQSxTQUZEOztBQUlBO0FBQ0EsZUFBT2pWLE1BQVA7QUFDSCxLQW5IRDs7QUFxSEE7QUFDQWIsTUFBRTZSLEVBQUYsQ0FBS3ZRLE9BQUwsQ0FBYXNSLFFBQWIsR0FBd0I7QUFDcEJpRCxpQkFBUyxLQURXO0FBRXBCOVUsZ0JBQVE7QUFDSkcsb0JBQVEsR0FESjtBQUVKRyxxQkFBUztBQUZMLFNBRlk7QUFNcEJMLGtCQUFVO0FBQ05FLG9CQUFRLEtBREY7QUFFTkcscUJBQVM7QUFGSDtBQU5VLEtBQXhCO0FBV0gsQ0FwSUEsRUFvSUM2RyxNQXBJRCxDQUFELEM7Ozs7Ozs7Ozs7O0FDQUE7O0FBQ0E7O0FBRUMsV0FBU2xJLENBQVQsRUFBWTs7QUFFVDtBQUNBQSxNQUFFNlIsRUFBRixDQUFLOVAsS0FBTCxHQUFhLFVBQVNqQixPQUFULEVBQWtCO0FBQzNCO0FBQ0EsWUFBSTJCLE9BQU8sSUFBWDtBQUNBLFlBQUl1UCxVQUFVaFMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7QUFDQSxZQUFJaVMsU0FBUztBQUNUOzs7O0FBSUFDLGlCQUFLLGFBQVNwUixPQUFULEVBQWtCaVYsTUFBbEIsRUFBMEI7QUFDM0Isb0JBQUkvRCxRQUFRL1EsSUFBUixDQUFhLE1BQWIsS0FBd0I4VSxXQUFXLElBQXZDLEVBQTZDO0FBQ3pDdFQsMkJBQU91UCxRQUFRL1EsSUFBUixDQUFhLE1BQWIsQ0FBUDtBQUNILGlCQUZELE1BRU87QUFDSDtBQUNBZ1IsMkJBQU83UixJQUFQLENBQVlVLE9BQVo7O0FBRUE7QUFDQW1SLDJCQUFPdkgsS0FBUDs7QUFFQTtBQUNBdUgsMkJBQU9FLEtBQVA7O0FBRUFILDRCQUFRL1EsSUFBUixDQUFhLE1BQWIsRUFBcUJ3QixJQUFyQjtBQUNIOztBQUVELHVCQUFPQSxJQUFQO0FBQ0gsYUF0QlE7O0FBd0JUOzs7O0FBSUFyQyxrQkFBTSxjQUFTVSxPQUFULEVBQWtCO0FBQ3BCMkIscUJBQUsyUCxNQUFMLEdBQWMsRUFBZDs7QUFFQTtBQUNBM1AscUJBQUszQixPQUFMLEdBQWVkLEVBQUVzSyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJ0SyxFQUFFNlIsRUFBRixDQUFLOVAsS0FBTCxDQUFXNlEsUUFBOUIsRUFBd0M5UixPQUF4QyxDQUFmOztBQUVBO0FBQ0EyQixxQkFBS3VULHNCQUFMLEdBQThCLENBQTlCO0FBQ0gsYUFwQ1E7O0FBc0NUOzs7O0FBSUE3RCxtQkFBTyxpQkFBVztBQUNkSCx3QkFBUTdRLEVBQVIsQ0FBVyxPQUFYLEVBQW9CLGlCQUFwQixFQUF1QzhRLE9BQU9nRSxzQkFBOUM7O0FBRUE7QUFDQSxvQkFBSWhFLE9BQU9pRSxjQUFQLE9BQTRCLFVBQTVCLElBQTBDakUsT0FBT2tFLDRCQUFQLEVBQTlDLEVBQXFGO0FBQ2pGO0FBQ0FuRSw0QkFBUTdRLEVBQVIsQ0FBVyxFQUFDNlIsWUFBWWYsT0FBT21FLDhCQUFwQixFQUFvRHJELFlBQVlkLE9BQU9vRSw2QkFBdkUsRUFBWCxFQUFrSCxzQ0FBbEg7O0FBRUE7QUFDQXJFLDRCQUFRN1EsRUFBUixDQUFXLE9BQVgsRUFBb0Isc0RBQXBCLEVBQTRFOFEsT0FBT3FFLDBCQUFuRjtBQUNIOztBQUVEdEUsd0JBQVFNLElBQVIsQ0FBYSxnRkFBYixFQUErRmpPLEtBQS9GLENBQXFHNE4sT0FBT3NFLGVBQTVHO0FBQ0gsYUF2RFE7O0FBeURUOzs7O0FBSUE3TCxtQkFBTyxpQkFBVztBQUNkO0FBQ0FzSCx3QkFBUTVRLEdBQVIsQ0FBWSxPQUFaLEVBQXFCLGlCQUFyQixFQUF3QzZRLE9BQU9nRSxzQkFBL0M7O0FBRUE7QUFDQWpFLHdCQUFRNVEsR0FBUixDQUFZLEVBQUM0UixZQUFZZixPQUFPbUUsOEJBQXBCLEVBQW9EckQsWUFBWWQsT0FBT29FLDZCQUF2RSxFQUFaLEVBQW1ILHNDQUFuSDtBQUNBckUsd0JBQVE1USxHQUFSLENBQVksT0FBWixFQUFxQixzREFBckIsRUFBNkU2USxPQUFPcUUsMEJBQXBGOztBQUVBO0FBQ0E3VCxxQkFBSzZQLElBQUwsQ0FBVSxrQ0FBVixFQUE4Q3pLLEdBQTlDLENBQWtELFNBQWxELEVBQTZELEVBQTdEO0FBQ0FwRixxQkFBSzZQLElBQUwsQ0FBVSxzQkFBVixFQUFrQzdOLFdBQWxDLENBQThDLHFCQUE5QztBQUNBaEMscUJBQUs2UCxJQUFMLENBQVUsa0RBQVYsRUFBOEQ3TixXQUE5RCxDQUEwRSxvQkFBMUU7QUFDSCxhQXpFUTs7QUEyRVQ7Ozs7QUFJQXlSLDRCQUFnQiwwQkFBVztBQUN2QixvQkFBSS9WLFlBQU1nRCxtQkFBTixDQUEwQixTQUExQixDQUFKLEVBQTBDO0FBQ3RDLHdCQUFJaEQsWUFBTTBQLEtBQU4sQ0FBWXBOLEtBQUszQixPQUFMLENBQWFrQixPQUF6QixFQUFrQyxvQkFBbEMsQ0FBSixFQUE2RDtBQUN6RCw0QkFBSWhDLEVBQUUsTUFBRixFQUFVNkQsUUFBVixDQUFtQnBCLEtBQUszQixPQUFMLENBQWFrQixPQUFiLENBQXFCWCxPQUFyQixDQUE2QlMsS0FBN0IsQ0FBbUNjLElBQXRELENBQUosRUFBaUU7QUFDN0QsbUNBQU9ILEtBQUszQixPQUFMLENBQWFrQixPQUFiLENBQXFCWCxPQUFyQixDQUE2QlMsS0FBN0IsQ0FBbUNlLElBQTFDO0FBQ0gseUJBRkQsTUFFTztBQUNILG1DQUFPSixLQUFLM0IsT0FBTCxDQUFha0IsT0FBYixDQUFxQlgsT0FBckIsQ0FBNkJzQixPQUFwQztBQUNIO0FBQ0oscUJBTkQsTUFNTyxJQUFJeEMsWUFBTTBQLEtBQU4sQ0FBWXBOLEtBQUszQixPQUFMLENBQWFrQixPQUF6QixFQUFrQyxTQUFsQyxDQUFKLEVBQWtEO0FBQ3JELCtCQUFPUyxLQUFLM0IsT0FBTCxDQUFha0IsT0FBYixDQUFxQlgsT0FBNUI7QUFDSDtBQUNKLGlCQVZELE1BVU8sSUFBSWxCLFlBQU1nRCxtQkFBTixDQUEwQixRQUExQixLQUF1Q2hELFlBQU0wUCxLQUFOLENBQVlwTixLQUFLM0IsT0FBTCxDQUFha0IsT0FBekIsRUFBa0MsUUFBbEMsQ0FBM0MsRUFBd0Y7QUFDM0YsMkJBQU9TLEtBQUszQixPQUFMLENBQWFrQixPQUFiLENBQXFCQyxNQUE1QjtBQUNILGlCQUZNLE1BRUEsSUFBSTlCLFlBQU1nRCxtQkFBTixDQUEwQixRQUExQixLQUF1Q2hELFlBQU0wUCxLQUFOLENBQVlwTixLQUFLM0IsT0FBTCxDQUFha0IsT0FBekIsRUFBa0MsUUFBbEMsQ0FBM0MsRUFBd0Y7QUFDM0YsMkJBQU9TLEtBQUszQixPQUFMLENBQWFrQixPQUFiLENBQXFCZCxNQUE1QjtBQUNILGlCQUZNLE1BRUE7QUFDSCwyQkFBTyxLQUFQO0FBQ0g7QUFDSixhQWpHUTs7QUFtR1Q7Ozs7QUFJQWlWLDBDQUE4Qix3Q0FBVztBQUNyQyxvQkFBSWhXLFlBQU1nRCxtQkFBTixDQUEwQixTQUExQixLQUF3Q2hELFlBQU0wUCxLQUFOLENBQVlwTixLQUFLM0IsT0FBTCxDQUFha0IsT0FBekIsRUFBa0Msb0JBQWxDLENBQTVDLEVBQXFHO0FBQ2pHLDJCQUFPLElBQVA7QUFDSCxpQkFGRCxNQUVPO0FBQ0gsMkJBQU8sS0FBUDtBQUNIO0FBQ0osYUE3R1E7O0FBK0dUOzs7O0FBSUF1VSw2QkFBaUIseUJBQVN6USxDQUFULEVBQVk7O0FBRXpCLG9CQUFJbU0sT0FBTzZCLFlBQVAsQ0FBb0IsV0FBcEIsRUFBaUM5VCxFQUFFLElBQUYsQ0FBakMsTUFBOEMsS0FBbEQsRUFBeUQ7QUFDckQ4RixzQkFBRW9FLGNBQUY7QUFDSDtBQUNEOztBQUVBLG9CQUFJK0gsT0FBT2lFLGNBQVAsT0FBNEIsVUFBNUIsSUFBMENqRSxPQUFPa0UsNEJBQVAsRUFBOUMsRUFBcUY7QUFDakZsRSwyQkFBT3VFLDBCQUFQLENBQWtDMVEsQ0FBbEMsRUFBcUM5RixFQUFFLElBQUYsQ0FBckM7QUFDSDtBQUNKLGFBN0hROztBQStIVDs7OztBQUlBb1csNENBQWdDLHdDQUFTdFEsQ0FBVCxFQUFZO0FBQ3hDLG9CQUFJbU0sT0FBT2lFLGNBQVAsT0FBNEIsV0FBaEMsRUFBNkM7QUFDekM7QUFDSDs7QUFFRCxvQkFBSXpULEtBQUtnVSxtQkFBTCxPQUErQixLQUFuQyxFQUEwQztBQUN0QztBQUNIOztBQUVELG9CQUFJQyxPQUFPMVcsRUFBRSxJQUFGLENBQVg7O0FBRUFpUyx1QkFBTzBFLG1CQUFQLENBQTJCRCxJQUEzQjs7QUFFQSxvQkFBSUEsS0FBS3pWLElBQUwsQ0FBVSxPQUFWLEtBQXNCLElBQTFCLEVBQWdDO0FBQzVCZ1IsMkJBQU8yRSxtQkFBUCxDQUEyQkYsSUFBM0IsRUFBaUMsS0FBakM7QUFDSDtBQUNKLGFBbkpROztBQXFKVDs7OztBQUlBTCwyQ0FBK0IsdUNBQVN2USxDQUFULEVBQVk7QUFDdkMsb0JBQUlyRCxLQUFLZ1UsbUJBQUwsT0FBK0IsS0FBbkMsRUFBMEM7QUFDdEM7QUFDSDs7QUFFRCxvQkFBSXhFLE9BQU9pRSxjQUFQLE9BQTRCLFdBQWhDLEVBQTZDO0FBQ3pDO0FBQ0g7O0FBRUQsb0JBQUlRLE9BQU8xVyxFQUFFLElBQUYsQ0FBWDtBQUNBLG9CQUFJNlcsT0FBT3BVLEtBQUszQixPQUFMLENBQWFpUixRQUFiLENBQXNCc0MsT0FBakM7O0FBRUEsb0JBQUlBLFVBQVVoSixXQUFXLFlBQVc7QUFDaEMsd0JBQUlxTCxLQUFLelYsSUFBTCxDQUFVLE9BQVYsS0FBc0IsSUFBMUIsRUFBZ0M7QUFDNUJnUiwrQkFBTzJFLG1CQUFQLENBQTJCRixJQUEzQixFQUFpQyxJQUFqQztBQUNIO0FBQ0osaUJBSmEsRUFJWEcsSUFKVyxDQUFkOztBQU1BSCxxQkFBS3pWLElBQUwsQ0FBVSxPQUFWLEVBQW1CLElBQW5CO0FBQ0F5VixxQkFBS3pWLElBQUwsQ0FBVSxTQUFWLEVBQXFCb1QsT0FBckI7QUFDSCxhQTdLUTs7QUErS1Q7Ozs7QUFJQWlDLHdDQUE0QixvQ0FBU3hRLENBQVQsRUFBWTtBQUNwQyxvQkFBSW1NLE9BQU9pRSxjQUFQLE9BQTRCLFdBQWhDLEVBQTZDO0FBQ3pDO0FBQ0g7O0FBRUQsb0JBQUlRLE9BQU8xVyxFQUFFLElBQUYsRUFBUStILE9BQVIsQ0FBZ0IsZUFBaEIsQ0FBWDs7QUFFQSxvQkFBSTJPLEtBQUt6VixJQUFMLENBQVUsbUJBQVYsS0FBa0MsV0FBdEMsRUFBbUQ7QUFDL0M7QUFDSDs7QUFFRCxvQkFBSXlWLEtBQUs3UyxRQUFMLENBQWMscUJBQWQsS0FBd0MsS0FBNUMsRUFBbUQ7QUFDL0M2Uyx5QkFBS2xTLFFBQUwsQ0FBYyw2QkFBZDtBQUNBeU4sMkJBQU8wRSxtQkFBUCxDQUEyQkQsSUFBM0I7QUFDSCxpQkFIRCxNQUdPO0FBQ0hBLHlCQUFLalMsV0FBTCxDQUFpQiw2QkFBakI7QUFDQXdOLDJCQUFPMkUsbUJBQVAsQ0FBMkJGLElBQTNCLEVBQWlDLElBQWpDO0FBQ0g7O0FBRUQ1USxrQkFBRW9FLGNBQUY7QUFDSCxhQXZNUTs7QUF5TVQ7Ozs7QUFJQXNNLHdDQUE0QixvQ0FBUzFRLENBQVQsRUFBWVcsRUFBWixFQUFnQjtBQUN4QztBQUNBLG9CQUFJd0wsT0FBT2lFLGNBQVAsT0FBNEIsV0FBaEMsRUFBNkM7QUFDekM7QUFDSDs7QUFFRCxvQkFBSVksUUFBUTlFLFFBQVFNLElBQVIsQ0FBYSx5REFBYixDQUFaOztBQUVBO0FBQ0Esb0JBQUl3RSxNQUFNdFQsTUFBTixHQUFlLENBQWYsSUFBb0JpRCxHQUFHNUMsUUFBSCxDQUFZLGdCQUFaLE1BQWtDLEtBQXRELElBQStENEMsR0FBRzZMLElBQUgsQ0FBUSxpQkFBUixFQUEyQjlPLE1BQTNCLEtBQXNDLENBQXpHLEVBQTRHO0FBQ3hHO0FBQ0FzVCwwQkFBTTlRLElBQU4sQ0FBVyxZQUFXO0FBQ2xCaU0sK0JBQU8yRSxtQkFBUCxDQUEyQjVXLEVBQUUsSUFBRixDQUEzQixFQUFvQyxJQUFwQztBQUNILHFCQUZEO0FBR0g7QUFDSixhQTVOUTs7QUE4TlQ7Ozs7QUFJQWlXLG9DQUF3QixnQ0FBU25RLENBQVQsRUFBWVcsRUFBWixFQUFnQjtBQUNwQyxvQkFBSWlRLE9BQU9qUSxLQUFLekcsRUFBRXlHLEVBQUYsQ0FBTCxHQUFhekcsRUFBRSxJQUFGLENBQXhCOztBQUVBLG9CQUFJaVMsT0FBT2lFLGNBQVAsT0FBNEIsVUFBNUIsSUFBMENRLEtBQUszTyxPQUFMLENBQWEsZUFBYixFQUE4QjlHLElBQTlCLENBQW1DLG1CQUFuQyxLQUEyRCxXQUF6RyxFQUFzSDtBQUNsSDZFLHNCQUFFb0UsY0FBRjtBQUNBO0FBQ0g7O0FBRUQsb0JBQUk2TSxLQUFLTCxLQUFLM08sT0FBTCxDQUFhLElBQWIsQ0FBVDtBQUNBLG9CQUFJL0YsVUFBVStVLEdBQUdDLFFBQUgsQ0FBWSxrQ0FBWixDQUFkOztBQUVBLG9CQUFJaFYsUUFBUTJPLE1BQVIsQ0FBZSx5QkFBZixFQUEwQ25OLE1BQTFDLElBQW9ELENBQXhELEVBQTJEO0FBQ3ZEO0FBQ0g7O0FBRUQsb0JBQUl4QixRQUFRd0IsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUNwQnNDLHNCQUFFb0UsY0FBRjtBQUNBLHdCQUFJeEUsUUFBUWpELEtBQUszQixPQUFMLENBQWFnQyxTQUFiLENBQXVCbVUsVUFBbkM7QUFDQSx3QkFBSUMsZUFBZSxLQUFuQjs7QUFFQSx3QkFBSUgsR0FBR2xULFFBQUgsQ0FBWSxvQkFBWixNQUFzQyxLQUExQyxFQUFpRDtBQUM3QztBQUNBLDRCQUFJcEIsS0FBSzNCLE9BQUwsQ0FBYWdDLFNBQWIsQ0FBdUJFLFNBQXZCLEtBQXFDLEtBQXpDLEVBQWdEO0FBQzVDLGdDQUFJbVUsWUFBWVQsS0FBSzNPLE9BQUwsQ0FBYSwrQkFBYixFQUE4Q3VLLElBQTlDLENBQW1ELHVGQUFuRCxDQUFoQjtBQUNBNkUsc0NBQVVuUixJQUFWLENBQWUsWUFBVztBQUN0QmhHLGtDQUFFLElBQUYsRUFBUWdYLFFBQVIsQ0FBaUIsa0JBQWpCLEVBQXFDSSxPQUFyQyxDQUE2QzFSLEtBQTdDLEVBQW9ELFlBQVc7QUFDM0R1TSwyQ0FBT29GLFlBQVAsQ0FBb0JYLElBQXBCO0FBQ0gsaUNBRkQ7QUFHQTFXLGtDQUFFLElBQUYsRUFBUXlFLFdBQVIsQ0FBb0Isb0JBQXBCO0FBQ0gsNkJBTEQ7O0FBT0EsZ0NBQUkwUyxVQUFVM1QsTUFBVixHQUFtQixDQUF2QixFQUEwQjtBQUN0QjBULCtDQUFlLElBQWY7QUFDSDtBQUNKOztBQUVELDRCQUFJQSxZQUFKLEVBQWtCO0FBQ2RsVixvQ0FBUXNWLFNBQVIsQ0FBa0I1UixLQUFsQixFQUF5QixZQUFXO0FBQ2hDdU0sdUNBQU9vRixZQUFQLENBQW9CWCxJQUFwQjtBQUNILDZCQUZEO0FBR0FLLCtCQUFHdlMsUUFBSCxDQUFZLG9CQUFaO0FBQ0gseUJBTEQsTUFLTztBQUNIeEMsb0NBQVFzVixTQUFSLENBQWtCNVIsS0FBbEIsRUFBeUIsWUFBVztBQUNoQ3VNLHVDQUFPb0YsWUFBUCxDQUFvQlgsSUFBcEI7QUFDSCw2QkFGRDtBQUdBSywrQkFBR3ZTLFFBQUgsQ0FBWSxvQkFBWjtBQUNIO0FBQ0oscUJBM0JELE1BMkJPO0FBQ0h4QyxnQ0FBUW9WLE9BQVIsQ0FBZ0IxUixLQUFoQixFQUF1QixZQUFXO0FBQzlCdU0sbUNBQU9vRixZQUFQLENBQW9CWCxJQUFwQjtBQUNILHlCQUZEO0FBR0FLLDJCQUFHdFMsV0FBSCxDQUFlLG9CQUFmO0FBQ0g7QUFDSjtBQUNKLGFBeFJROztBQTBSVDs7OztBQUlBNFMsMEJBQWMsc0JBQVNYLElBQVQsRUFBZTtBQUN6QjtBQUNBLG9CQUFJdlcsWUFBTWdELG1CQUFOLENBQTBCLFNBQTFCLEtBQXdDVixLQUFLM0IsT0FBTCxDQUFhZ0MsU0FBYixDQUF1QkMsVUFBL0QsSUFBNkUsQ0FBQ2lQLFFBQVEvUSxJQUFSLENBQWEsaUJBQWIsQ0FBbEYsRUFBbUg7QUFDL0daLDhCQUFLZ0osZ0JBQUwsQ0FBc0JxTixJQUF0QjtBQUNIO0FBQ0osYUFuU1E7O0FBcVNUOzs7O0FBSUFFLGlDQUFxQiw2QkFBU0YsSUFBVCxFQUFlYSxTQUFmLEVBQTBCO0FBQzNDO0FBQ0Esb0JBQUlBLFNBQUosRUFBZTtBQUNYYix5QkFBS2pTLFdBQUwsQ0FBaUIscUJBQWpCO0FBQ0g7QUFDRDtBQUNBaVMscUJBQUt0QyxVQUFMLENBQWdCLE9BQWhCO0FBQ0Esb0JBQUlzQyxLQUFLelYsSUFBTCxDQUFVLDRCQUFWLENBQUosRUFBNkM7QUFDekNqQixzQkFBRSxNQUFGLEVBQVV5RSxXQUFWLENBQXNCaVMsS0FBS3pWLElBQUwsQ0FBVSw0QkFBVixDQUF0QjtBQUNIO0FBQ0Qsb0JBQUlvVCxVQUFVcUMsS0FBS3pWLElBQUwsQ0FBVSxTQUFWLENBQWQ7QUFDQXlWLHFCQUFLdEMsVUFBTCxDQUFnQixTQUFoQjtBQUNBL0YsNkJBQWFnRyxPQUFiO0FBQ0gsYUF0VFE7O0FBd1RUOzs7O0FBSUFzQyxpQ0FBcUIsNkJBQVNELElBQVQsRUFBZTtBQUNoQztBQUNBMUUsd0JBQVFNLElBQVIsQ0FBYSw0Q0FBYixFQUEyRHRNLElBQTNELENBQWdFLFlBQVc7QUFDdkUsd0JBQUlTLEtBQUt6RyxFQUFFLElBQUYsQ0FBVDtBQUNBLHdCQUFJMFcsS0FBS2pCLEVBQUwsQ0FBUWhQLEVBQVIsS0FBZUEsR0FBRzZMLElBQUgsQ0FBUW9FLElBQVIsRUFBY2xULE1BQWQsR0FBdUIsQ0FBdEMsSUFBMkNrVCxLQUFLcEUsSUFBTCxDQUFVN0wsRUFBVixFQUFjakQsTUFBZCxHQUF1QixDQUF0RSxFQUF5RTtBQUNyRTtBQUNILHFCQUZELE1BRU87QUFDSHlPLCtCQUFPMkUsbUJBQVAsQ0FBMkJuUSxFQUEzQixFQUErQixJQUEvQjtBQUNIO0FBQ0osaUJBUEQ7O0FBU0E7QUFDQXdMLHVCQUFPdUYsNkJBQVAsQ0FBcUNkLElBQXJDOztBQUVBO0FBQ0FBLHFCQUFLbFMsUUFBTCxDQUFjLHFCQUFkOztBQUVBLG9CQUFJa1MsS0FBS3pWLElBQUwsQ0FBVSw0QkFBVixDQUFKLEVBQTZDO0FBQ3pDakIsc0JBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQmtTLEtBQUt6VixJQUFMLENBQVUsNEJBQVYsQ0FBbkI7QUFDSDs7QUFFRDtBQUNBLG9CQUFJZ1IsT0FBT2lFLGNBQVAsT0FBNEIsV0FBNUIsSUFBMkN6VCxLQUFLM0IsT0FBTCxDQUFhZ0MsU0FBYixDQUF1QkMsVUFBdEUsRUFBa0Y7QUFDOUUxQyw4QkFBSzJJLFFBQUwsQ0FBYzBOLEtBQUtNLFFBQUwsQ0FBYyx3QkFBZCxDQUFkO0FBQ0g7QUFDSixhQXJWUTs7QUF1VlQ7Ozs7QUFJQTlVLG9CQUFRLGdCQUFTNEQsQ0FBVCxFQUFZO0FBQ2hCLG9CQUFJbU0sT0FBT2lFLGNBQVAsT0FBNEIsVUFBaEMsRUFBNEM7QUFDeEM7QUFDSDs7QUFFRCxvQkFBSWhVLFNBQVM4UCxRQUFRTSxJQUFSLENBQWEsd0NBQWIsQ0FBYjtBQUNBLG9CQUFJdFEsVUFBVUUsT0FBT29RLElBQVAsQ0FBWSxvQkFBWixDQUFkO0FBQ0Esb0JBQUlqRCxVQUFKO0FBQ0Esb0JBQUlvSSxlQUFldFgsWUFBTW1ELFdBQU4sR0FBb0JsQixLQUF2QztBQUNBLG9CQUFJc1YsY0FBYzFGLFFBQVFNLElBQVIsQ0FBYSxnQ0FBYixFQUErQzlPLE1BQS9DLEdBQXdELENBQTFFO0FBQ0Esb0JBQUltVSxLQUFKOztBQUVBLG9CQUNJMUYsT0FBT2lFLGNBQVAsTUFBMkIsVUFBM0IsS0FFSy9WLFlBQU1nRCxtQkFBTixDQUEwQixTQUExQixLQUF3Q2hELFlBQU0wUCxLQUFOLENBQVlwTixLQUFLM0IsT0FBakIsRUFBMEIsZ0JBQTFCLENBQXhDLEtBQXdGNlcsUUFBUWxWLEtBQUszQixPQUFMLENBQWFvQixNQUFiLENBQW9CYixPQUFwSCxLQUFnSW9XLGlCQUFpQnBJLGFBQWFuTixPQUFPakIsSUFBUCxDQUFZLGdDQUFaLENBQTlCLENBQWpJLElBQ0NkLFlBQU1nRCxtQkFBTixDQUEwQixRQUExQixLQUF1Q2hELFlBQU0wUCxLQUFOLENBQVlwTixLQUFLM0IsT0FBakIsRUFBMEIsZUFBMUIsQ0FBdkMsS0FBc0Y2VyxRQUFRbFYsS0FBSzNCLE9BQUwsQ0FBYW9CLE1BQWIsQ0FBb0JELE1BQWxILEtBQTZId1YsaUJBQWlCcEksYUFBYW5OLE9BQU9qQixJQUFQLENBQVksK0JBQVosQ0FBOUIsQ0FEOUgsSUFFQ2QsWUFBTWdELG1CQUFOLENBQTBCLFFBQTFCLEtBQXVDaEQsWUFBTTBQLEtBQU4sQ0FBWXBOLEtBQUszQixPQUFqQixFQUEwQixlQUExQixDQUF2QyxLQUFzRjZXLFFBQVFsVixLQUFLM0IsT0FBTCxDQUFhb0IsTUFBYixDQUFvQmhCLE1BQWxILEtBQTZIdVcsaUJBQWlCcEksYUFBYW5OLE9BQU9qQixJQUFQLENBQVksK0JBQVosQ0FBOUIsQ0FKbEksQ0FESixFQU9FOztBQUVFLHdCQUFJMlcsUUFBUTVWLFFBQVFzUSxJQUFSLENBQWEsbUNBQWIsRUFBa0Q5TyxNQUE5RCxDQUZGLENBRXdFO0FBQ3RFLHdCQUFJd0ksT0FBT2dHLFFBQVFNLElBQVIsQ0FBYSwyREFBYixFQUEwRTlPLE1BQXJGLENBSEYsQ0FHK0Y7QUFDN0Ysd0JBQUlxVSxRQUFRRCxRQUFRNUwsSUFBcEI7O0FBRUEsd0JBQUkyTCxNQUFNcEMsS0FBTixPQUFrQixJQUF0QixFQUE0QjtBQUN4QjtBQUNBLDRCQUFJcUMsUUFBUSxDQUFaLEVBQWU7QUFDWDVWLG9DQUFRc1EsSUFBUixDQUFhLG1DQUFiLEVBQWtEdE0sSUFBbEQsQ0FBdUQsWUFBVztBQUM5RCxvQ0FBSTBRLE9BQU8xVyxFQUFFLElBQUYsQ0FBWDs7QUFFQSxvQ0FBSThYLGlCQUFpQjlWLFFBQVFzUSxJQUFSLENBQWEsMkRBQWIsRUFBMEU5TyxNQUEvRjtBQUNBd08sd0NBQVFNLElBQVIsQ0FBYSwyREFBYixFQUEwRXlGLEVBQTFFLENBQTZFRCxpQkFBaUIsQ0FBOUYsRUFBaUczTSxLQUFqRyxDQUF1R3VMLElBQXZHOztBQUVBLG9DQUFJaUIsTUFBTXBDLEtBQU4sT0FBa0IsS0FBdEIsRUFBNkI7QUFDekJtQix5Q0FBSzdCLFFBQUwsQ0FBYzdTLFFBQVFzUSxJQUFSLENBQWEsbUJBQWIsQ0FBZDtBQUNBLDJDQUFPLEtBQVA7QUFDSDs7QUFFRHNGO0FBQ0E1TDtBQUNILDZCQWJEO0FBY0g7QUFDSixxQkFsQkQsTUFrQk87QUFDSDtBQUNBLDRCQUFJQSxPQUFPLENBQVgsRUFBYztBQUNWLGdDQUFJZ00sUUFBUWhHLFFBQVFNLElBQVIsQ0FBYSwyREFBYixDQUFaO0FBQ0EsZ0NBQUkyRixRQUFRRCxNQUFNeFUsTUFBTixHQUFlLENBQTNCOztBQUVBLGlDQUFLLElBQUkwSyxJQUFJLENBQWIsRUFBZ0JBLElBQUk4SixNQUFNeFUsTUFBMUIsRUFBa0MwSyxHQUFsQyxFQUF1QztBQUNuQyxvQ0FBSXdJLE9BQU8xVyxFQUFFZ1ksTUFBTUUsR0FBTixDQUFVRCxLQUFWLENBQUYsQ0FBWDtBQUNBQTs7QUFFQSxvQ0FBSU4sTUFBTXBDLEtBQU4sT0FBa0IsSUFBdEIsRUFBNEI7QUFDeEI7QUFDSDs7QUFFRG1CLHFDQUFLN0IsUUFBTCxDQUFjN1MsUUFBUXNRLElBQVIsQ0FBYSxtQkFBYixDQUFkOztBQUVBc0Y7QUFDQTVMO0FBQ0g7QUFDSjtBQUNKOztBQUVELHdCQUFJNEwsUUFBUSxDQUFaLEVBQWU7QUFDWDFWLCtCQUFPK1EsSUFBUDtBQUNILHFCQUZELE1BRU87QUFDSC9RLCtCQUFPb0UsSUFBUDtBQUNIO0FBQ0osaUJBMURELE1BMERPO0FBQ0h0RSw0QkFBUXNRLElBQVIsQ0FBYSxtQ0FBYixFQUFrRHRNLElBQWxELENBQXVELFlBQVc7QUFDOUQsNEJBQUk4UixpQkFBaUI5VixRQUFRc1EsSUFBUixDQUFhLG1DQUFiLEVBQWtEOU8sTUFBdkU7QUFDQXdPLGdDQUFRTSxJQUFSLENBQWEsZ0NBQWIsRUFBK0M0RixHQUEvQyxDQUFtREosY0FBbkQsRUFBbUUzTSxLQUFuRSxDQUF5RW5MLEVBQUUsSUFBRixDQUF6RTtBQUNILHFCQUhEOztBQUtBa0MsMkJBQU9vRSxJQUFQO0FBQ0g7QUFDSixhQXphUTs7QUEyYVQ7Ozs7QUFJQTZSLCtDQUFtQywyQ0FBUzFSLEVBQVQsRUFBYTtBQUM1QyxvQkFBSTZGLFNBQVM3RixHQUFHNkwsSUFBSCxDQUFRLG9CQUFSLEVBQThCekssR0FBOUIsQ0FBa0MsUUFBbEMsSUFBOEMsQ0FBM0Q7QUFDQSxvQkFBSXVRLFVBQVVwWSxFQUFFLHFJQUFxSXNNLE1BQXJJLEdBQThJLFdBQWhKLENBQWQ7QUFDQXRNLGtCQUFFLE1BQUYsRUFBVW1MLEtBQVYsQ0FBZ0JpTixPQUFoQjtBQUNBQSx3QkFBUWpYLEVBQVIsQ0FBVyxPQUFYLEVBQW9CLFVBQVMyRSxDQUFULEVBQVk7QUFDNUJBLHNCQUFFME4sZUFBRjtBQUNBMU4sc0JBQUVvRSxjQUFGO0FBQ0FsSyxzQkFBRSxJQUFGLEVBQVFnTCxNQUFSOztBQUVBWCwwQkFBTSxDQUFOO0FBQ0E0SCwyQkFBTzJFLG1CQUFQLENBQTJCblEsRUFBM0IsRUFBK0IsSUFBL0I7QUFDSCxpQkFQRDtBQVFILGFBM2JROztBQTZiVDs7OztBQUlBK1EsMkNBQStCLHVDQUFTZCxJQUFULEVBQWU7QUFDMUMsb0JBQUluRSxRQUFRbUUsS0FBS3BFLElBQUwsQ0FBVSwyREFBVixDQUFaO0FBQ0Esb0JBQUl0USxVQUFVMFUsS0FBS3BFLElBQUwsQ0FBVSxvQkFBVixDQUFkO0FBQ0Esb0JBQUkrRixTQUFTM0IsS0FBS3BFLElBQUwsQ0FBVSxzQ0FBVixDQUFiOztBQUVBLG9CQUFJQyxNQUFNL08sTUFBTixHQUFlLENBQW5CLEVBQXNCO0FBQ2xCLHdCQUFJeUYsR0FBSjtBQUNBLHdCQUFJcVAsT0FBTzVCLEtBQUtNLFFBQUwsQ0FBYyxlQUFkLENBQVg7O0FBRUEsd0JBQUloVixRQUFRNkIsUUFBUixDQUFpQiwwQkFBakIsS0FBZ0Q3QixRQUFRNkIsUUFBUixDQUFpQix3QkFBakIsQ0FBcEQsRUFBZ0c7QUFDNUYsNEJBQUk3QixRQUFRNkIsUUFBUixDQUFpQix3QkFBakIsQ0FBSixFQUFnRDtBQUM1Q29GLGtDQUFNeU4sS0FBSzNGLFVBQUwsS0FBb0IsQ0FBMUI7QUFDQSxnQ0FBSS9PLFFBQVE2QixRQUFSLENBQWlCLHVCQUFqQixDQUFKLEVBQStDO0FBQzNDb0Ysc0NBQU1BLE1BQU1zRyxLQUFLb0YsR0FBTCxDQUFTbEUsU0FBU3pPLFFBQVE2RixHQUFSLENBQVksY0FBWixDQUFULENBQVQsQ0FBWjtBQUNIO0FBQ0RvQixrQ0FBTWpILFFBQVFJLEtBQVIsS0FBa0I2RyxHQUF4QjtBQUNILHlCQU5ELE1BTU8sSUFBSWpILFFBQVE2QixRQUFSLENBQWlCLHVCQUFqQixDQUFKLEVBQStDO0FBQ2xEb0Ysa0NBQU15TixLQUFLM0YsVUFBTCxLQUFvQixDQUExQjtBQUNBLGdDQUFJL08sUUFBUTZCLFFBQVIsQ0FBaUIsdUJBQWpCLENBQUosRUFBK0M7QUFDM0NvRixzQ0FBTUEsTUFBTXNHLEtBQUtvRixHQUFMLENBQVNsRSxTQUFTek8sUUFBUTZGLEdBQVIsQ0FBWSxhQUFaLENBQVQsQ0FBVCxDQUFaO0FBQ0g7QUFDSjtBQUNKLHFCQWJELE1BYU87QUFDSCw0QkFBSTdGLFFBQVE2QixRQUFSLENBQWlCLHlCQUFqQixLQUErQzdCLFFBQVE2QixRQUFSLENBQWlCLHVCQUFqQixDQUFuRCxFQUE4RjtBQUMxRm9GLGtDQUFNeU4sS0FBSzNWLE1BQUwsR0FBY2lMLElBQWQsR0FBc0IsQ0FBQzdMLFlBQU1tRCxXQUFOLEdBQW9CbEIsS0FBcEIsR0FBNEJKLFFBQVErTyxVQUFSLEVBQTdCLElBQXFELENBQWpGO0FBQ0E5SCxrQ0FBTUEsTUFBT3lOLEtBQUszRixVQUFMLEtBQW9CLENBQWpDO0FBQ0gseUJBSEQsTUFHTyxJQUFJL08sUUFBUTZCLFFBQVIsQ0FBaUIsdUJBQWpCLENBQUosRUFBK0M7QUFDbEQ7QUFDSCx5QkFGTSxNQUVBLElBQUk3QixRQUFRNkIsUUFBUixDQUFpQix3QkFBakIsQ0FBSixFQUFnRDtBQUNuRDtBQUNIO0FBQ0o7O0FBRUQwTywwQkFBTTFLLEdBQU4sQ0FBVSxNQUFWLEVBQWtCb0IsR0FBbEI7QUFDSDtBQUNKLGFBcGVROztBQXNlVDs7OztBQUlBOUUsZ0NBQW9CLDRCQUFTMFMsSUFBVCxFQUFlO0FBQy9CLG9CQUFJMEIsT0FBTyxJQUFJN0ksSUFBSixFQUFYOztBQUVBak4scUJBQUt1VCxzQkFBTCxHQUE4QnVDLEtBQUs1SSxPQUFMLEtBQWlCa0gsSUFBL0M7QUFDSCxhQTllUTs7QUFnZlQ7Ozs7QUFJQUosaUNBQXFCLCtCQUFXO0FBQzVCLG9CQUFJOEIsT0FBTyxJQUFJN0ksSUFBSixFQUFYOztBQUVBLHVCQUFRNkksS0FBSzVJLE9BQUwsS0FBaUJsTixLQUFLdVQsc0JBQXRCLEdBQStDLElBQS9DLEdBQXNELEtBQTlEO0FBQ0gsYUF4ZlE7O0FBMGZUOzs7O0FBSUF3Qyw2QkFBaUIseUJBQVM5QixJQUFULEVBQWU7QUFDNUIxRSx3QkFBUU0sSUFBUixDQUFhLHVCQUFiLEVBQXNDdE0sSUFBdEMsQ0FBMkMsWUFBVztBQUNsRGhHLHNCQUFFLElBQUYsRUFBUXlFLFdBQVIsQ0FBb0Isc0JBQXBCO0FBQ0F6RSxzQkFBRSxJQUFGLEVBQVFnWCxRQUFSLENBQWlCLGtCQUFqQixFQUFxQ25QLEdBQXJDLENBQXlDLFNBQXpDLEVBQW9ELEVBQXBEOztBQUVBN0gsc0JBQUUsSUFBRixFQUFRa1IsT0FBUixDQUFnQix3QkFBaEIsRUFBMENsTCxJQUExQyxDQUErQyxZQUFXO0FBQ3REaEcsMEJBQUUsSUFBRixFQUFReUUsV0FBUixDQUFvQixvQkFBcEI7QUFDQXpFLDBCQUFFLElBQUYsRUFBUWdYLFFBQVIsQ0FBaUIsa0JBQWpCLEVBQXFDblAsR0FBckMsQ0FBeUMsU0FBekMsRUFBb0QsRUFBcEQ7QUFDSCxxQkFIRDtBQUlILGlCQVJEOztBQVVBO0FBQ0Esb0JBQUlwRixLQUFLM0IsT0FBTCxDQUFhZ0MsU0FBYixDQUF1QkUsU0FBdkIsS0FBcUMsS0FBekMsRUFBZ0Q7QUFDNUNnUCw0QkFBUU0sSUFBUixDQUFhLHFCQUFiLEVBQW9DdE0sSUFBcEMsQ0FBeUMsWUFBVztBQUNoRGhHLDBCQUFFLElBQUYsRUFBUXlFLFdBQVIsQ0FBb0Isb0JBQXBCO0FBQ0gscUJBRkQ7QUFHSDtBQUNKLGFBL2dCUTs7QUFpaEJUOzs7O0FBSUFnVSwyQkFBZSx1QkFBUy9CLElBQVQsRUFBZTtBQUMxQjtBQUNBekUsdUJBQU91RyxlQUFQOztBQUVBLG9CQUFJOUIsT0FBTzFXLEVBQUUwVyxJQUFGLENBQVg7QUFDQUEscUJBQUtsUyxRQUFMLENBQWMsc0JBQWQ7QUFDQWtTLHFCQUFLeEYsT0FBTCxDQUFhLHdCQUFiLEVBQXVDbEwsSUFBdkMsQ0FBNEMsWUFBVztBQUNuRGhHLHNCQUFFLElBQUYsRUFBUXdFLFFBQVIsQ0FBaUIsb0JBQWpCO0FBQ0gsaUJBRkQ7QUFHSCxhQTloQlE7O0FBZ2lCVDs7OztBQUlBa1UsNEJBQWdCLHdCQUFTaEMsSUFBVCxFQUFlO0FBQzNCLG9CQUFJaUMsY0FBYyxFQUFsQjtBQUNBLG9CQUFJakMsT0FBTzFXLEVBQUUwVyxJQUFGLENBQVg7QUFDQSxvQkFBSTRCLE9BQU81QixLQUFLTSxRQUFMLENBQWMsZUFBZCxDQUFYOztBQUVBMkIsNEJBQVlwSyxJQUFaLENBQWlCO0FBQ2JxSywwQkFBTU4sS0FBS2hHLElBQUwsQ0FBVSxvQkFBVixFQUFnQy9LLElBQWhDLEVBRE87QUFFYnNSLDJCQUFPUCxLQUFLcEUsSUFBTCxDQUFVLE9BQVYsQ0FGTTtBQUdiNEUsMEJBQU1SLEtBQUtwRSxJQUFMLENBQVUsTUFBVjtBQUhPLGlCQUFqQjs7QUFNQXdDLHFCQUFLeEYsT0FBTCxDQUFhLHdCQUFiLEVBQXVDbEwsSUFBdkMsQ0FBNEMsWUFBVztBQUNuRCx3QkFBSStTLGNBQWMvWSxFQUFFLElBQUYsRUFBUWdYLFFBQVIsQ0FBaUIsZUFBakIsQ0FBbEI7QUFDQTJCLGdDQUFZcEssSUFBWixDQUFpQjtBQUNicUssOEJBQU1HLFlBQVl6RyxJQUFaLENBQWlCLG9CQUFqQixFQUF1Qy9LLElBQXZDLEVBRE87QUFFYnNSLCtCQUFPRSxZQUFZN0UsSUFBWixDQUFpQixPQUFqQixDQUZNO0FBR2I0RSw4QkFBTUMsWUFBWTdFLElBQVosQ0FBaUIsTUFBakI7QUFITyxxQkFBakI7QUFLSCxpQkFQRDs7QUFTQXlFLDRCQUFZSyxPQUFaOztBQUVBLHVCQUFPTCxXQUFQO0FBQ0gsYUEzakJROztBQTZqQlQ7Ozs7QUFJQU0sMEJBQWMsc0JBQVN2QyxJQUFULEVBQWU7QUFDekJBLHVCQUFPMVcsRUFBRTBXLElBQUYsQ0FBUDs7QUFFQSx1QkFBT0EsS0FBS00sUUFBTCxDQUFjLGVBQWQsRUFBK0IxRSxJQUEvQixDQUFvQyxvQkFBcEMsRUFBMEQvSyxJQUExRCxFQUFQO0FBQ0gsYUFya0JROztBQXVrQlQ7OztBQUdBZ00sa0JBQU0sZ0JBQVc7QUFDYnZULGtCQUFFZ1MsT0FBRixFQUFXL1EsSUFBWCxDQUFnQixNQUFoQixFQUF3QndCLElBQXhCO0FBQ0gsYUE1a0JROztBQThrQlQ7OztBQUdBcVIsMEJBQWMsc0JBQVNwQyxJQUFULEVBQWV3SCxJQUFmLEVBQXFCO0FBQy9CLHFCQUFLaEwsSUFBSSxDQUFULEVBQVlBLElBQUl6TCxLQUFLMlAsTUFBTCxDQUFZNU8sTUFBNUIsRUFBb0MwSyxHQUFwQyxFQUF5QztBQUNyQyx3QkFBSS9GLFFBQVExRixLQUFLMlAsTUFBTCxDQUFZbEUsQ0FBWixDQUFaO0FBQ0Esd0JBQUkvRixNQUFNdUosSUFBTixJQUFjQSxJQUFsQixFQUF3QjtBQUNwQiw0QkFBSXZKLE1BQU1nTixHQUFOLElBQWEsSUFBakIsRUFBdUI7QUFDbkIsZ0NBQUloTixNQUFNaU4sS0FBTixJQUFlLEtBQW5CLEVBQTBCO0FBQ3RCM1MscUNBQUsyUCxNQUFMLENBQVlsRSxDQUFaLEVBQWVrSCxLQUFmLEdBQXVCLElBQXZCO0FBQ0EsdUNBQU9qTixNQUFNa04sT0FBTixDQUFjbEgsSUFBZCxDQUFtQixJQUFuQixFQUF5QjFMLElBQXpCLEVBQStCeVcsSUFBL0IsQ0FBUDtBQUNIO0FBQ0oseUJBTEQsTUFLTztBQUNILG1DQUFPL1EsTUFBTWtOLE9BQU4sQ0FBY2xILElBQWQsQ0FBbUIsSUFBbkIsRUFBeUIxTCxJQUF6QixFQUErQnlXLElBQS9CLENBQVA7QUFDSDtBQUNKO0FBQ0o7QUFDSixhQS9sQlE7O0FBaW1CVDVELHNCQUFVLGtCQUFTNUQsSUFBVCxFQUFlMkQsT0FBZixFQUF3QkYsR0FBeEIsRUFBNkI7QUFDbkMxUyxxQkFBSzJQLE1BQUwsQ0FBWTdELElBQVosQ0FBaUI7QUFDYm1ELDBCQUFNQSxJQURPO0FBRWIyRCw2QkFBU0EsT0FGSTtBQUdiRix5QkFBS0EsR0FIUTtBQUliQywyQkFBTztBQUpNLGlCQUFqQjs7QUFPQW5ELHVCQUFPc0IsSUFBUDtBQUNIO0FBMW1CUSxTQUFiOztBQTZtQkE7QUFDQXRCLGVBQU9DLEdBQVAsQ0FBV3FELEtBQVgsQ0FBaUI5UyxJQUFqQixFQUF1QixDQUFDM0IsT0FBRCxDQUF2Qjs7QUFFQTtBQUNBLFlBQUksT0FBT0EsT0FBUCxLQUFvQixXQUF4QixFQUFxQztBQUNqQ2QsY0FBRW9PLE1BQUYsRUFBVWxNLE1BQVYsQ0FBaUIsWUFBVztBQUN4QitQLHVCQUFPQyxHQUFQLENBQVdxRCxLQUFYLENBQWlCOVMsSUFBakIsRUFBdUIsQ0FBQzNCLE9BQUQsRUFBVSxJQUFWLENBQXZCO0FBQ0gsYUFGRDtBQUdIOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0EyQixhQUFLZ1csYUFBTCxHQUFxQixVQUFTL0IsSUFBVCxFQUFlO0FBQ2hDLG1CQUFPekUsT0FBT3dHLGFBQVAsQ0FBcUIvQixJQUFyQixDQUFQO0FBQ0gsU0FGRDs7QUFJQTs7O0FBR0FqVSxhQUFLaVcsY0FBTCxHQUFzQixVQUFTaEMsSUFBVCxFQUFlO0FBQ2pDLG1CQUFPekUsT0FBT3lHLGNBQVAsQ0FBc0JoQyxJQUF0QixDQUFQO0FBQ0gsU0FGRDs7QUFJQTs7O0FBR0FqVSxhQUFLd1csWUFBTCxHQUFvQixVQUFTdkMsSUFBVCxFQUFlO0FBQy9CLG1CQUFPekUsT0FBT2dILFlBQVAsQ0FBb0J2QyxJQUFwQixDQUFQO0FBQ0gsU0FGRDs7QUFJQTs7O0FBR0FqVSxhQUFLeVQsY0FBTCxHQUFzQixZQUFXO0FBQzdCLG1CQUFPakUsT0FBT2lFLGNBQVAsRUFBUDtBQUNILFNBRkQ7O0FBSUE7Ozs7QUFJQXpULGFBQUswVyxZQUFMLEdBQW9CLFVBQVN6QyxJQUFULEVBQWU7QUFDL0J6RSxtQkFBTzJFLG1CQUFQLENBQTJCRixJQUEzQixFQUFpQyxJQUFqQztBQUNILFNBRkQ7O0FBSUE7Ozs7QUFJQWpVLGFBQUswQixrQkFBTCxHQUEwQixVQUFTMFMsSUFBVCxFQUFlO0FBQ3JDNUUsbUJBQU85TixrQkFBUCxDQUEwQjBTLElBQTFCO0FBQ0gsU0FGRDs7QUFJQTs7OztBQUlBcFUsYUFBS2dVLG1CQUFMLEdBQTJCLFlBQVc7QUFDbEMsbUJBQU94RSxPQUFPd0UsbUJBQVAsRUFBUDtBQUNILFNBRkQ7O0FBSUE7OztBQUdBaFUsYUFBS3RCLEVBQUwsR0FBVSxVQUFTdVEsSUFBVCxFQUFlMkQsT0FBZixFQUF3QjtBQUM5QixtQkFBT3BELE9BQU9xRCxRQUFQLENBQWdCNUQsSUFBaEIsRUFBc0IyRCxPQUF0QixDQUFQO0FBQ0gsU0FGRDs7QUFJQTtBQUNBLGVBQU81UyxJQUFQO0FBQ0gsS0E5ckJEOztBQWdzQkE7QUFDQXpDLE1BQUU2UixFQUFGLENBQUs5UCxLQUFMLENBQVc2USxRQUFYLEdBQXNCO0FBQ2xCO0FBQ0E5UCxtQkFBVztBQUNQbVUsd0JBQVksR0FETCxFQUNXO0FBQ2xCbFUsd0JBQVksSUFGTCxFQUVXO0FBQ2xCQyx1QkFBVyxJQUhKLENBR1k7QUFIWixTQUZPOztBQVFsQjtBQUNBK08sa0JBQVU7QUFDTnNDLHFCQUFTLEdBREgsQ0FDUztBQURUO0FBVFEsS0FBdEI7O0FBY0E7QUFDQXJVLE1BQUVDLFFBQUYsRUFBWWtCLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQVMyRSxDQUFULEVBQVk7QUFDaEM5RixVQUFFLDBHQUFGLEVBQThHZ0csSUFBOUcsQ0FBbUgsWUFBVztBQUMxSCxnQkFBSWdNLFVBQVVoUyxFQUFFLElBQUYsRUFBUTJRLE1BQVIsQ0FBZSxjQUFmLEVBQStCQSxNQUEvQixFQUFkO0FBQ0EsZ0JBQUlsTyxPQUFPdVAsUUFBUWpRLEtBQVIsRUFBWDs7QUFFQSxnQkFBSVUsS0FBS3lULGNBQUwsT0FBMEIsVUFBOUIsRUFBMEM7QUFDdEM7QUFDSDs7QUFFRCxnQkFBSWxXLEVBQUU4RixFQUFFakUsTUFBSixFQUFZNFQsRUFBWixDQUFlekQsT0FBZixLQUEyQixLQUEzQixJQUFvQ0EsUUFBUU0sSUFBUixDQUFhdFMsRUFBRThGLEVBQUVqRSxNQUFKLENBQWIsRUFBMEIyQixNQUExQixJQUFvQyxDQUE1RSxFQUErRTtBQUMzRSxvQkFBSXdVLFFBQVFoRyxRQUFRTSxJQUFSLENBQWEsZ0ZBQWIsQ0FBWjtBQUNBMEYsc0JBQU1oUyxJQUFOLENBQVcsWUFBVztBQUNsQnZELHlCQUFLMFcsWUFBTCxDQUFrQm5aLEVBQUUsSUFBRixDQUFsQjtBQUNILGlCQUZEO0FBR0g7QUFDSixTQWREO0FBZUgsS0FoQkQ7QUFpQkgsQ0FwdUJBLEVBb3VCQ2tJLE1BcHVCRCxDQUFELEM7Ozs7Ozs7Ozs7O0FDSEMsV0FBU2xJLENBQVQsRUFBWTtBQUNUO0FBQ0FBLE1BQUU2UixFQUFGLENBQUtyUSxVQUFMLEdBQWtCLFVBQVNWLE9BQVQsRUFBa0I7QUFDaEM7QUFDQSxZQUFJc1ksWUFBWSxJQUFoQjtBQUNBLFlBQUlwSCxVQUFVaFMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7OztBQUdBLFlBQUlpUyxTQUFTO0FBQ1Q7OztBQUdBQyxpQkFBSyxhQUFVcFIsT0FBVixFQUFtQjtBQUNwQixvQkFBSSxDQUFDa1IsUUFBUS9RLElBQVIsQ0FBYSxXQUFiLENBQUwsRUFBZ0M7QUFDNUI7QUFDQWdSLDJCQUFPN1IsSUFBUCxDQUFZVSxPQUFaO0FBQ0FtUiwyQkFBT0UsS0FBUDs7QUFFQTtBQUNBSCw0QkFBUS9RLElBQVIsQ0FBYSxXQUFiLEVBQTBCbVksU0FBMUI7QUFDSCxpQkFQRCxNQU9PO0FBQ0g7QUFDQUEsZ0NBQVlwSCxRQUFRL1EsSUFBUixDQUFhLFdBQWIsQ0FBWjtBQUNIOztBQUVELHVCQUFPbVksU0FBUDtBQUNILGFBbEJROztBQW9CVDs7O0FBR0FoWixrQkFBTSxjQUFTVSxPQUFULEVBQWtCO0FBQ3BCc1ksMEJBQVVoSCxNQUFWLEdBQW1CLEVBQW5COztBQUVBO0FBQ0FnSCwwQkFBVXRZLE9BQVYsR0FBb0JkLEVBQUVzSyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJ0SyxFQUFFNlIsRUFBRixDQUFLclEsVUFBTCxDQUFnQm9SLFFBQW5DLEVBQTZDOVIsT0FBN0MsQ0FBcEI7O0FBRUFzWSwwQkFBVTFYLE9BQVY7O0FBRUEwWCwwQkFBVUMsU0FBVixHQUFzQkQsVUFBVXRZLE9BQVYsQ0FBa0JXLEtBQXhDO0FBQ0EyWCwwQkFBVUUsVUFBVixHQUF1QkYsVUFBVUMsU0FBVixHQUFzQixNQUE3QztBQUNBRCwwQkFBVUcsWUFBVixHQUF5QkgsVUFBVUMsU0FBVixHQUFzQixVQUEvQzs7QUFFQUQsMEJBQVV0WCxLQUFWLEdBQWtCa1EsUUFBUW5PLFFBQVIsQ0FBaUJ1VixVQUFVRSxVQUEzQixJQUF5QyxPQUF6QyxHQUFtRCxRQUFyRTtBQUNBRiwwQkFBVXpYLEtBQVYsR0FBa0J5WCxVQUFVdFksT0FBVixDQUFrQmEsS0FBcEM7O0FBRUEsb0JBQUl5WCxVQUFVdFksT0FBVixDQUFrQmMsTUFBbEIsSUFBNEJ3WCxVQUFVdFksT0FBVixDQUFrQmMsTUFBbEIsQ0FBeUJDLE1BQXpELEVBQWlFO0FBQzdEdVgsOEJBQVVJLFlBQVYsR0FBeUJKLFVBQVV0WSxPQUFWLENBQWtCYyxNQUFsQixDQUF5QkMsTUFBbEQ7QUFDQXVYLDhCQUFVSyxXQUFWLEdBQXdCTCxVQUFVdFksT0FBVixDQUFrQmMsTUFBbEIsQ0FBeUJFLEtBQWpEO0FBQ0gsaUJBSEQsTUFHTztBQUNIc1gsOEJBQVVJLFlBQVYsR0FBeUJKLFVBQVV0WSxPQUFWLENBQWtCYyxNQUEzQztBQUNBd1gsOEJBQVVLLFdBQVYsR0FBd0IsRUFBeEI7QUFDSDtBQUNKLGFBN0NROztBQStDVDs7O0FBR0F0SCxtQkFBTyxpQkFBVztBQUNkO0FBQ0FuUyxrQkFBRW9aLFVBQVVJLFlBQVosRUFBMEJyWSxFQUExQixDQUE2QixPQUE3QixFQUFzQzhRLE9BQU9yUSxNQUE3Qzs7QUFFQSxvQkFBSXdYLFVBQVV6WCxLQUFkLEVBQXFCO0FBQ2pCM0Isc0JBQUVvWixVQUFVelgsS0FBWixFQUFtQlIsRUFBbkIsQ0FBc0IsT0FBdEIsRUFBK0I4USxPQUFPM0wsSUFBdEM7QUFDSDtBQUNKLGFBekRROztBQTJEVDs7O0FBR0FpTixrQkFBTSxnQkFBWTtBQUNkdlQsa0JBQUVnUyxPQUFGLEVBQVcvUSxJQUFYLENBQWdCLFdBQWhCLEVBQTZCbVksU0FBN0I7QUFDSCxhQWhFUTs7QUFrRVQ7OztBQUdBeFgsb0JBQVEsa0JBQVc7QUFDZixvQkFBSXdYLFVBQVV0WCxLQUFWLElBQW1CLE9BQXZCLEVBQWdDO0FBQzVCbVEsMkJBQU8zTCxJQUFQO0FBQ0gsaUJBRkQsTUFFTztBQUNIMkwsMkJBQU9nQixJQUFQO0FBQ0g7QUFDSixhQTNFUTs7QUE2RVQ7OztBQUdBQSxrQkFBTSxnQkFBVztBQUNiLG9CQUFJbUcsVUFBVXRYLEtBQVYsSUFBbUIsT0FBdkIsRUFBZ0M7QUFDNUI7QUFDSDs7QUFFRG1RLHVCQUFPNkIsWUFBUCxDQUFvQixZQUFwQjs7QUFFQSxvQkFBSXNGLFVBQVVLLFdBQVYsSUFBeUIsRUFBN0IsRUFBaUM7QUFDN0J6WixzQkFBRW9aLFVBQVVJLFlBQVosRUFBMEJoVixRQUExQixDQUFtQzRVLFVBQVVLLFdBQTdDO0FBQ0g7O0FBRUR6WixrQkFBRSxNQUFGLEVBQVV3RSxRQUFWLENBQW1CNFUsVUFBVUUsVUFBN0I7QUFDQXRILHdCQUFReE4sUUFBUixDQUFpQjRVLFVBQVVFLFVBQTNCOztBQUVBRiwwQkFBVXRYLEtBQVYsR0FBa0IsT0FBbEI7O0FBRUEsb0JBQUlzWCxVQUFVdFksT0FBVixDQUFrQlksT0FBdEIsRUFBK0I7QUFDM0Isd0JBQUlBLFVBQVUxQixFQUFFLGlCQUFpQm9aLFVBQVVHLFlBQTNCLEdBQTBDLFVBQTVDLENBQWQ7QUFDQXZILDRCQUFRN0csS0FBUixDQUFjekosT0FBZDtBQUNBMFgsOEJBQVUxWCxPQUFWLEdBQW9CQSxPQUFwQjtBQUNBMFgsOEJBQVUxWCxPQUFWLENBQWtCUCxFQUFsQixDQUFxQixPQUFyQixFQUE4QixVQUFTMkUsQ0FBVCxFQUFZO0FBQ3RDQSwwQkFBRTBOLGVBQUY7QUFDQTFOLDBCQUFFb0UsY0FBRjtBQUNBK0gsK0JBQU8zTCxJQUFQO0FBQ0gscUJBSkQ7QUFLSDs7QUFFRDJMLHVCQUFPNkIsWUFBUCxDQUFvQixXQUFwQjs7QUFFQSx1QkFBT3NGLFNBQVA7QUFDSCxhQTlHUTs7QUFnSFQ7OztBQUdBOVMsa0JBQU0sZ0JBQVc7QUFDYixvQkFBSThTLFVBQVV0WCxLQUFWLElBQW1CLFFBQXZCLEVBQWlDO0FBQzdCO0FBQ0g7O0FBRURtUSx1QkFBTzZCLFlBQVAsQ0FBb0IsWUFBcEI7O0FBRUEsb0JBQUlzRixVQUFVSyxXQUFWLElBQXlCLEVBQTdCLEVBQWlDO0FBQzdCelosc0JBQUVvWixVQUFVSSxZQUFaLEVBQTBCL1UsV0FBMUIsQ0FBc0MyVSxVQUFVSyxXQUFoRDtBQUNIOztBQUVEelosa0JBQUUsTUFBRixFQUFVeUUsV0FBVixDQUFzQjJVLFVBQVVFLFVBQWhDO0FBQ0F0SCx3QkFBUXZOLFdBQVIsQ0FBb0IyVSxVQUFVRSxVQUE5Qjs7QUFFQUYsMEJBQVV0WCxLQUFWLEdBQWtCLFFBQWxCOztBQUVBLG9CQUFJc1gsVUFBVXRZLE9BQVYsQ0FBa0JZLE9BQXRCLEVBQStCO0FBQzNCMFgsOEJBQVUxWCxPQUFWLENBQWtCc0osTUFBbEI7QUFDSDs7QUFFRGlILHVCQUFPNkIsWUFBUCxDQUFvQixXQUFwQjs7QUFFQSx1QkFBT3NGLFNBQVA7QUFDSCxhQTFJUTs7QUE0SVQ7OztBQUdBdEYsMEJBQWMsc0JBQVNwQyxJQUFULEVBQWU7QUFDekIscUJBQUt4RCxJQUFJLENBQVQsRUFBWUEsSUFBSWtMLFVBQVVoSCxNQUFWLENBQWlCNU8sTUFBakMsRUFBeUMwSyxHQUF6QyxFQUE4QztBQUMxQyx3QkFBSS9GLFFBQVFpUixVQUFVaEgsTUFBVixDQUFpQmxFLENBQWpCLENBQVo7QUFDQSx3QkFBSS9GLE1BQU11SixJQUFOLElBQWNBLElBQWxCLEVBQXdCO0FBQ3BCLDRCQUFJdkosTUFBTWdOLEdBQU4sSUFBYSxJQUFqQixFQUF1QjtBQUNuQixnQ0FBSWhOLE1BQU1pTixLQUFOLElBQWUsS0FBbkIsRUFBMEI7QUFDdEJnRSwwQ0FBVWhILE1BQVYsQ0FBaUJsRSxDQUFqQixFQUFvQmtILEtBQXBCLEdBQTRCLElBQTVCO0FBQ0EsdUNBQU9qTixNQUFNa04sT0FBTixDQUFjbEgsSUFBZCxDQUFtQixJQUFuQixFQUF5QmlMLFNBQXpCLENBQVA7QUFDSDtBQUNKLHlCQUxELE1BS087QUFDSCxtQ0FBUWpSLE1BQU1rTixPQUFOLENBQWNsSCxJQUFkLENBQW1CLElBQW5CLEVBQXlCaUwsU0FBekIsQ0FBUjtBQUNIO0FBQ0o7QUFDSjtBQUNKLGFBN0pROztBQStKVDlELHNCQUFVLGtCQUFTNUQsSUFBVCxFQUFlMkQsT0FBZixFQUF3QkYsR0FBeEIsRUFBNkI7QUFDbkNpRSwwQkFBVWhILE1BQVYsQ0FBaUI3RCxJQUFqQixDQUFzQjtBQUNsQm1ELDBCQUFNQSxJQURZO0FBRWxCMkQsNkJBQVNBLE9BRlM7QUFHbEJGLHlCQUFLQSxHQUhhO0FBSWxCQywyQkFBTztBQUpXLGlCQUF0Qjs7QUFPQW5ELHVCQUFPc0IsSUFBUDtBQUNIO0FBeEtRLFNBQWI7O0FBMktBO0FBQ0EsWUFBSW1HLE1BQU0sSUFBVjs7QUFFQTtBQUNBekgsZUFBT0MsR0FBUCxDQUFXcUQsS0FBWCxDQUFpQixJQUFqQixFQUF1QixDQUFDelUsT0FBRCxDQUF2Qjs7QUFFQTs7OztBQUlBOzs7QUFHQXNZLGtCQUFVOVMsSUFBVixHQUFrQixZQUFZO0FBQzFCLG1CQUFPMkwsT0FBTzNMLElBQVAsRUFBUDtBQUNILFNBRkQ7O0FBSUE7OztBQUdBOFMsa0JBQVVuRyxJQUFWLEdBQWtCLFlBQVk7QUFDMUIsbUJBQU9oQixPQUFPZ0IsSUFBUCxFQUFQO0FBQ0gsU0FGRDs7QUFJQTs7O0FBR0FtRyxrQkFBVWpZLEVBQVYsR0FBZ0IsVUFBVXVRLElBQVYsRUFBZ0IyRCxPQUFoQixFQUF5QjtBQUNyQyxtQkFBT3BELE9BQU9xRCxRQUFQLENBQWdCNUQsSUFBaEIsRUFBc0IyRCxPQUF0QixDQUFQO0FBQ0gsU0FGRDs7QUFJQTs7OztBQUlBK0Qsa0JBQVVqRSxHQUFWLEdBQWlCLFVBQVV6RCxJQUFWLEVBQWdCMkQsT0FBaEIsRUFBeUI7QUFDdEMsbUJBQU9wRCxPQUFPcUQsUUFBUCxDQUFnQjVELElBQWhCLEVBQXNCMkQsT0FBdEIsRUFBK0IsSUFBL0IsQ0FBUDtBQUNILFNBRkQ7O0FBSUEsZUFBTytELFNBQVA7QUFDSCxLQTNORDs7QUE2TkE7QUFDQXBaLE1BQUU2UixFQUFGLENBQUtyUSxVQUFMLENBQWdCb1IsUUFBaEIsR0FBMkIsRUFBM0I7QUFHSCxDQW5PQSxFQW1PQzFLLE1Bbk9ELENBQUQsQzs7Ozs7Ozs7Ozs7QUNBQyxXQUFTbEksQ0FBVCxFQUFZO0FBQ1Q7QUFDQUEsTUFBRTZSLEVBQUYsQ0FBS2pOLFlBQUwsR0FBb0IsVUFBUzlELE9BQVQsRUFBa0I7O0FBRWxDO0FBQ0EsWUFBSTZELEtBQUssSUFBVDtBQUNBLFlBQUlxTixVQUFVaFMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7QUFDQSxZQUFJaVMsU0FBUztBQUNUOzs7QUFHQUMsaUJBQUssYUFBU3BSLE9BQVQsRUFBa0I7QUFDbkIsb0JBQUksQ0FBQ2tSLFFBQVEvUSxJQUFSLENBQWEsSUFBYixDQUFMLEVBQXlCO0FBQ3JCO0FBQ0FnUiwyQkFBTzdSLElBQVAsQ0FBWVUsT0FBWjtBQUNBO0FBQ0FtUiwyQkFBT0UsS0FBUDtBQUNBO0FBQ0FILDRCQUFRL1EsSUFBUixDQUFhLElBQWIsRUFBbUIwRCxFQUFuQjtBQUNILGlCQVBELE1BT087QUFDSDtBQUNBQSx5QkFBS3FOLFFBQVEvUSxJQUFSLENBQWEsSUFBYixDQUFMO0FBQ0g7O0FBRUQsdUJBQU8wRCxFQUFQO0FBQ0gsYUFsQlE7O0FBb0JUOzs7QUFHQXZFLGtCQUFNLGNBQVNVLE9BQVQsRUFBa0I7QUFDcEI7QUFDQTZELG1CQUFHN0QsT0FBSCxHQUFhZCxFQUFFc0ssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CdEssRUFBRTZSLEVBQUYsQ0FBS2pOLFlBQUwsQ0FBa0JnTyxRQUFyQyxFQUErQzlSLE9BQS9DLENBQWI7O0FBRUE7QUFDQTZELG1CQUFHZ1YsSUFBSCxHQUFVM0gsUUFBUU0sSUFBUixDQUFhLE1BQWIsQ0FBVjs7QUFFQTtBQUNBM04sbUJBQUdLLEtBQUgsR0FBV2hGLEVBQUUyRSxHQUFHN0QsT0FBSCxDQUFXa0UsS0FBYixDQUFYOztBQUVDO0FBQ0RMLG1CQUFHTSxTQUFILEdBQWVqRixFQUFFMkUsR0FBRzdELE9BQUgsQ0FBV21FLFNBQWIsQ0FBZjs7QUFFQSxvQkFBSU4sR0FBRzdELE9BQUgsQ0FBVytELElBQVgsSUFBbUIsU0FBdkIsRUFBa0M7QUFDOUI7QUFDQUYsdUJBQUdRLFVBQUgsR0FBZ0JuRixFQUFFMkUsR0FBRzdELE9BQUgsQ0FBV3FFLFVBQWIsQ0FBaEI7O0FBRUE7QUFDQVIsdUJBQUdPLFVBQUgsR0FBZ0JsRixFQUFFMkUsR0FBRzdELE9BQUgsQ0FBV29FLFVBQWIsQ0FBaEI7QUFDSDs7QUFFRDtBQUNBUCxtQkFBR29OLFFBQUgsR0FBY0MsUUFBUUYsU0FBUixDQUFrQixFQUFDa0MsZUFBZSxLQUFoQixFQUFsQixDQUFkOztBQUVBO0FBQ0FyUCxtQkFBR2lWLGFBQUg7O0FBRUE7QUFDQWpWLG1CQUFHa1YsVUFBSCxHQUFnQixLQUFoQjtBQUNILGFBcERROztBQXNEVDs7O0FBR0ExSCxtQkFBTyxpQkFBVztBQUNkO0FBQ0F4TixtQkFBR0ssS0FBSCxDQUFTOFUsS0FBVCxDQUFlN0gsT0FBTzhILFlBQXRCOztBQUVBLG9CQUFJcFYsR0FBRzdELE9BQUgsQ0FBVytELElBQVgsSUFBbUIsU0FBdkIsRUFBa0M7QUFDOUJGLHVCQUFHSyxLQUFILENBQVMyRixLQUFULENBQWVzSCxPQUFPK0gsWUFBdEI7O0FBRUFyVix1QkFBR08sVUFBSCxDQUFjYixLQUFkLENBQW9CNE4sT0FBT2dJLFlBQTNCOztBQUVBdFYsdUJBQUdRLFVBQUgsQ0FBY2QsS0FBZCxDQUFvQixZQUFXO0FBQzNCLDRCQUFJbEUsTUFBTWdELG1CQUFOLENBQTBCLG1CQUExQixDQUFKLEVBQW9EO0FBQ2hEbkQsOEJBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQixrQ0FBbkI7QUFDQUcsK0JBQUdLLEtBQUgsQ0FBUzJGLEtBQVQ7QUFDSDtBQUNKLHFCQUxEOztBQU9BaEcsdUJBQUdNLFNBQUgsQ0FBYVosS0FBYixDQUFtQixZQUFXO0FBQzFCLDRCQUFJbEUsTUFBTWdELG1CQUFOLENBQTBCLG1CQUExQixDQUFKLEVBQW9EO0FBQ2hEbkQsOEJBQUUsTUFBRixFQUFVeUUsV0FBVixDQUFzQixrQ0FBdEI7QUFDQXdOLG1DQUFPaUksYUFBUDtBQUNIO0FBQ0oscUJBTEQ7QUFPSCxpQkFuQkQsTUFtQk8sSUFBSXZWLEdBQUc3RCxPQUFILENBQVcrRCxJQUFYLElBQW1CLFVBQXZCLEVBQW1DO0FBQ3RDRix1QkFBR29OLFFBQUgsQ0FBWTVRLEVBQVosQ0FBZSxXQUFmLEVBQTRCLFlBQVc7QUFDbkN3RCwyQkFBR0ssS0FBSCxDQUFTMkYsS0FBVDtBQUNILHFCQUZEO0FBR0FoRyx1QkFBR00sU0FBSCxDQUFhWixLQUFiLENBQW1CNE4sT0FBT2lJLGFBQTFCO0FBQ0g7QUFDSixhQXRGUTs7QUF3RlQ7OztBQUdBSCwwQkFBYyxzQkFBU2pVLENBQVQsRUFBWTtBQUN0QixvQkFBSXFVLFFBQVF4VixHQUFHSyxLQUFILENBQVNxQyxHQUFULEVBQVo7O0FBRUEsb0JBQUk4UyxNQUFNM1csTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUNwQm1CLHVCQUFHb04sUUFBSCxDQUFZekwsSUFBWjtBQUNBMkwsMkJBQU9tSSwwQkFBUCxDQUFrQyxJQUFsQztBQUNBbkksMkJBQU9pSSxhQUFQO0FBQ0FsSSw0QkFBUXZOLFdBQVIsQ0FBb0JFLEdBQUc3RCxPQUFILENBQVdzRSxjQUEvQjtBQUNIOztBQUVELG9CQUFJK1UsTUFBTTNXLE1BQU4sR0FBZW1CLEdBQUc3RCxPQUFILENBQVd1RSxTQUExQixJQUF1Q1YsR0FBR2tWLFVBQUgsSUFBaUIsSUFBNUQsRUFBa0U7QUFDOUQ7QUFDSDs7QUFFRGxWLG1CQUFHa1YsVUFBSCxHQUFnQixJQUFoQjtBQUNBbFYsbUJBQUdnVixJQUFILENBQVFuVixRQUFSLENBQWlCRyxHQUFHN0QsT0FBSCxDQUFXaUUsT0FBNUI7QUFDQWtOLHVCQUFPbUksMEJBQVAsQ0FBa0MsS0FBbEM7O0FBRUFwYSxrQkFBRXFhLElBQUYsQ0FBTztBQUNIQyx5QkFBSzNWLEdBQUc3RCxPQUFILENBQVdnRSxNQURiO0FBRUg3RCwwQkFBTSxFQUFDa1osT0FBT0EsS0FBUixFQUZIO0FBR0hJLDhCQUFVLE1BSFA7QUFJSDNNLDZCQUFTLGlCQUFTNE0sR0FBVCxFQUFjO0FBQ25CN1YsMkJBQUdrVixVQUFILEdBQWdCLEtBQWhCO0FBQ0FsViwyQkFBR2dWLElBQUgsQ0FBUWxWLFdBQVIsQ0FBb0JFLEdBQUc3RCxPQUFILENBQVdpRSxPQUEvQjtBQUNBa04sK0JBQU9tSSwwQkFBUCxDQUFrQyxJQUFsQztBQUNBelYsMkJBQUdvTixRQUFILENBQVkyQixVQUFaLENBQXVCOEcsR0FBdkIsRUFBNEJ2SCxJQUE1QjtBQUNBakIsZ0NBQVF4TixRQUFSLENBQWlCRyxHQUFHN0QsT0FBSCxDQUFXc0UsY0FBNUI7QUFDSCxxQkFWRTtBQVdIRywyQkFBTyxlQUFTaVYsR0FBVCxFQUFjO0FBQ2pCN1YsMkJBQUdrVixVQUFILEdBQWdCLEtBQWhCO0FBQ0FsViwyQkFBR2dWLElBQUgsQ0FBUWxWLFdBQVIsQ0FBb0JFLEdBQUc3RCxPQUFILENBQVdpRSxPQUEvQjtBQUNBa04sK0JBQU9tSSwwQkFBUCxDQUFrQyxJQUFsQztBQUNBelYsMkJBQUdvTixRQUFILENBQVkyQixVQUFaLENBQXVCL08sR0FBRzdELE9BQUgsQ0FBV3dFLFNBQVgsQ0FBcUJDLEtBQXJCLENBQTJCZ1EsS0FBM0IsQ0FBaUM1USxFQUFqQyxFQUFxQzZWLEdBQXJDLENBQXZCLEVBQWtFdkgsSUFBbEU7QUFDQWpCLGdDQUFReE4sUUFBUixDQUFpQkcsR0FBRzdELE9BQUgsQ0FBV3NFLGNBQTVCO0FBQ0g7QUFqQkUsaUJBQVA7QUFtQkgsYUFoSVE7O0FBa0lUOzs7QUFHQWdWLHdDQUE0QixvQ0FBU0ssTUFBVCxFQUFpQjtBQUN6QyxvQkFBSTlWLEdBQUc3RCxPQUFILENBQVcrRCxJQUFYLElBQW1CLFVBQXZCLEVBQW1DO0FBQy9CO0FBQ0g7O0FBRUQsb0JBQUk0VixVQUFVLElBQWQsRUFBb0I7QUFDaEIsd0JBQUk5VixHQUFHSyxLQUFILENBQVNxQyxHQUFULEdBQWU3RCxNQUFmLEtBQTBCLENBQTlCLEVBQWlDO0FBQzdCbUIsMkJBQUdPLFVBQUgsQ0FBYzJDLEdBQWQsQ0FBa0IsWUFBbEIsRUFBZ0MsUUFBaEM7QUFDQWxELDJCQUFHTSxTQUFILENBQWE0QyxHQUFiLENBQWlCLFlBQWpCLEVBQStCLFFBQS9CO0FBQ0gscUJBSEQsTUFHTztBQUNId0cscUNBQWExSixHQUFHaVYsYUFBaEI7QUFDQWpWLDJCQUFHaVYsYUFBSCxHQUFtQnZPLFdBQVcsWUFBVztBQUNyQzFHLCtCQUFHTyxVQUFILENBQWMyQyxHQUFkLENBQWtCLFlBQWxCLEVBQWdDLFNBQWhDO0FBQ0FsRCwrQkFBR00sU0FBSCxDQUFhNEMsR0FBYixDQUFpQixZQUFqQixFQUErQixTQUEvQjtBQUNILHlCQUhrQixFQUdoQixHQUhnQixDQUFuQjtBQUlIO0FBQ0osaUJBWEQsTUFXTztBQUNIbEQsdUJBQUdPLFVBQUgsQ0FBYzJDLEdBQWQsQ0FBa0IsWUFBbEIsRUFBZ0MsUUFBaEM7QUFDQWxELHVCQUFHTSxTQUFILENBQWE0QyxHQUFiLENBQWlCLFlBQWpCLEVBQStCLFFBQS9CO0FBQ0g7QUFDSixhQXpKUTs7QUEySlQ7OztBQUdBb1MsMEJBQWMsc0JBQVNuVSxDQUFULEVBQVk7QUFDdEJuQixtQkFBR0ssS0FBSCxDQUFTcUMsR0FBVCxDQUFhLEVBQWI7QUFDQTFDLG1CQUFHTyxVQUFILENBQWMyQyxHQUFkLENBQWtCLFlBQWxCLEVBQWdDLFFBQWhDO0FBQ0FtSyx3QkFBUXZOLFdBQVIsQ0FBb0JFLEdBQUc3RCxPQUFILENBQVdzRSxjQUEvQjtBQUNBOztBQUVBNk0sdUJBQU9pSSxhQUFQO0FBQ0gsYUFyS1E7O0FBdUtUOzs7QUFHQUEsMkJBQWUseUJBQVc7QUFDdEJ2VixtQkFBR29OLFFBQUgsQ0FBWXpMLElBQVo7QUFDSCxhQTVLUTs7QUE4S1Q7OztBQUdBMFQsMEJBQWMsc0JBQVNsVSxDQUFULEVBQVk7QUFDdEIsb0JBQUluQixHQUFHb04sUUFBSCxDQUFZbUQsT0FBWixNQUF5QixLQUF6QixJQUFrQ3ZRLEdBQUdLLEtBQUgsQ0FBU3FDLEdBQVQsR0FBZTdELE1BQWYsR0FBd0JtQixHQUFHN0QsT0FBSCxDQUFXdUUsU0FBckUsSUFBa0ZWLEdBQUdrVixVQUFILElBQWlCLEtBQXZHLEVBQThHO0FBQzFHbFYsdUJBQUdvTixRQUFILENBQVlrQixJQUFaO0FBQ0FuTixzQkFBRW9FLGNBQUY7QUFDQXBFLHNCQUFFME4sZUFBRjtBQUNIO0FBQ0o7QUF2TFEsU0FBYjs7QUEwTEE7QUFDQXZCLGVBQU9DLEdBQVAsQ0FBV3FELEtBQVgsQ0FBaUI1USxFQUFqQixFQUFxQixDQUFDN0QsT0FBRCxDQUFyQjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7QUFJQTZELFdBQUcrVixJQUFILEdBQVUsVUFBUzdELElBQVQsRUFBZTtBQUN4QjtBQUNBLFNBRkQ7O0FBSUE7QUFDQSxlQUFPbFMsRUFBUDtBQUNILEtBbE5EOztBQW9OQTtBQUNBM0UsTUFBRTZSLEVBQUYsQ0FBS2pOLFlBQUwsQ0FBa0JnTyxRQUFsQixHQUE2QjtBQUM1QnZOLG1CQUFXLENBRGlCO0FBRXpCdUMsbUJBQVc7QUFGYyxLQUE3QjtBQUtILENBNU5BLEVBNE5DTSxNQTVORCxDQUFELEM7Ozs7Ozs7Ozs7O0FDQUMsV0FBU2xJLENBQVQsRUFBWTtBQUNUO0FBQ0FBLE1BQUU2UixFQUFGLENBQUtwTSxVQUFMLEdBQWtCLFVBQVMzRSxPQUFULEVBQWtCO0FBQ2hDO0FBQ0EsWUFBSXNJLFlBQVksSUFBaEI7QUFDQSxZQUFJNEksVUFBVWhTLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7QUFHQSxZQUFJaVMsU0FBUztBQUNUOzs7QUFHQUMsaUJBQUssYUFBVXBSLE9BQVYsRUFBbUI7QUFDcEIsb0JBQUksQ0FBQ2tSLFFBQVEvUSxJQUFSLENBQWEsV0FBYixDQUFMLEVBQWdDO0FBQzVCO0FBQ0FnUiwyQkFBTzdSLElBQVAsQ0FBWVUsT0FBWjtBQUNBbVIsMkJBQU9FLEtBQVA7O0FBRUE7QUFDQUgsNEJBQVEvUSxJQUFSLENBQWEsV0FBYixFQUEwQm1JLFNBQTFCO0FBQ0gsaUJBUEQsTUFPTztBQUNIO0FBQ0FBLGdDQUFZNEksUUFBUS9RLElBQVIsQ0FBYSxXQUFiLENBQVo7QUFDSDs7QUFFRCx1QkFBT21JLFNBQVA7QUFDSCxhQWxCUTs7QUFvQlQ7OztBQUdBaEosa0JBQU0sY0FBU1UsT0FBVCxFQUFrQjtBQUNwQnNJLDBCQUFVNEksT0FBVixHQUFvQkEsT0FBcEI7QUFDQTVJLDBCQUFVZ0osTUFBVixHQUFtQixFQUFuQjs7QUFFQTtBQUNBaEosMEJBQVV0SSxPQUFWLEdBQW9CZCxFQUFFc0ssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CdEssRUFBRTZSLEVBQUYsQ0FBS3BNLFVBQUwsQ0FBZ0JtTixRQUFuQyxFQUE2QzlSLE9BQTdDLENBQXBCO0FBQ0gsYUE3QlE7O0FBK0JUOzs7QUFHQXFSLG1CQUFPLGlCQUFXO0FBQ2Q7QUFDQSxvQkFBSXdJLFVBQVVDLFNBQVYsQ0FBb0JDLEtBQXBCLENBQTBCLG1CQUExQixDQUFKLEVBQW9EO0FBQ2hEN2Esc0JBQUVvTyxNQUFGLEVBQVUwTSxJQUFWLENBQWUsaUNBQWYsRUFBa0QsWUFBVztBQUN6RDdJLCtCQUFPeEosTUFBUDtBQUNILHFCQUZEO0FBR0gsaUJBSkQsTUFJTztBQUNIekksc0JBQUVvTyxNQUFGLEVBQVV1SCxNQUFWLENBQWlCLFlBQVc7QUFDeEIxRCwrQkFBT3hKLE1BQVA7QUFDSCxxQkFGRDtBQUdIOztBQUVEO0FBQ0F1Six3QkFBUTdRLEVBQVIsQ0FBVyxPQUFYLEVBQW9COFEsT0FBTzBELE1BQTNCO0FBQ0gsYUFoRFE7O0FBa0RUOzs7QUFHQXBDLGtCQUFNLGdCQUFZO0FBQ2R2VCxrQkFBRWdTLE9BQUYsRUFBVy9RLElBQVgsQ0FBZ0IsV0FBaEIsRUFBNkJtSSxTQUE3QjtBQUNILGFBdkRROztBQXlEVDs7O0FBR0FYLG9CQUFRLGtCQUFXO0FBQ2Ysb0JBQUlRLE1BQU1qSixFQUFFb08sTUFBRixFQUFVaEYsU0FBVixFQUFWLENBRGUsQ0FDa0I7QUFDakMsb0JBQUlILE1BQU1HLFVBQVV0SSxPQUFWLENBQWtCQyxNQUE1QixFQUFvQztBQUNoQ2Ysc0JBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQixxQkFBbkI7QUFDSCxpQkFGRCxNQUVPO0FBQ0h4RSxzQkFBRSxNQUFGLEVBQVV5RSxXQUFWLENBQXNCLHFCQUF0QjtBQUNIO0FBQ0osYUFuRVE7O0FBcUVUOzs7QUFHQWtSLG9CQUFRLGdCQUFTN1AsQ0FBVCxFQUFZO0FBQ2hCQSxrQkFBRW9FLGNBQUY7O0FBRUFsSyxrQkFBRSxZQUFGLEVBQWdCbUosT0FBaEIsQ0FBd0I7QUFDcEJDLCtCQUFXO0FBRFMsaUJBQXhCLEVBRUdBLFVBQVV0SSxPQUFWLENBQWtCNEUsS0FGckI7QUFHSCxhQTlFUTs7QUFnRlQ7OztBQUdBb08sMEJBQWMsc0JBQVNwQyxJQUFULEVBQWU7QUFDekIscUJBQUt4RCxJQUFJLENBQVQsRUFBWUEsSUFBSTlFLFVBQVVnSixNQUFWLENBQWlCNU8sTUFBakMsRUFBeUMwSyxHQUF6QyxFQUE4QztBQUMxQyx3QkFBSS9GLFFBQVFpQixVQUFVZ0osTUFBVixDQUFpQmxFLENBQWpCLENBQVo7QUFDQSx3QkFBSS9GLE1BQU11SixJQUFOLElBQWNBLElBQWxCLEVBQXdCO0FBQ3BCLDRCQUFJdkosTUFBTWdOLEdBQU4sSUFBYSxJQUFqQixFQUF1QjtBQUNuQixnQ0FBSWhOLE1BQU1pTixLQUFOLElBQWUsS0FBbkIsRUFBMEI7QUFDdEJoTSwwQ0FBVWdKLE1BQVYsQ0FBaUJsRSxDQUFqQixFQUFvQmtILEtBQXBCLEdBQTRCLElBQTVCO0FBQ0EsdUNBQU9qTixNQUFNa04sT0FBTixDQUFjbEgsSUFBZCxDQUFtQixJQUFuQixFQUF5Qi9FLFNBQXpCLENBQVA7QUFDSDtBQUNKLHlCQUxELE1BS087QUFDSCxtQ0FBUWpCLE1BQU1rTixPQUFOLENBQWNsSCxJQUFkLENBQW1CLElBQW5CLEVBQXlCL0UsU0FBekIsQ0FBUjtBQUNIO0FBQ0o7QUFDSjtBQUNKLGFBakdROztBQW1HVGtNLHNCQUFVLGtCQUFTNUQsSUFBVCxFQUFlMkQsT0FBZixFQUF3QkYsR0FBeEIsRUFBNkI7QUFDbkMvTCwwQkFBVWdKLE1BQVYsQ0FBaUI3RCxJQUFqQixDQUFzQjtBQUNsQm1ELDBCQUFNQSxJQURZO0FBRWxCMkQsNkJBQVNBLE9BRlM7QUFHbEJGLHlCQUFLQSxHQUhhO0FBSWxCQywyQkFBTztBQUpXLGlCQUF0Qjs7QUFPQW5ELHVCQUFPc0IsSUFBUDtBQUNIO0FBNUdRLFNBQWI7O0FBK0dBO0FBQ0EsWUFBSW1HLE1BQU0sSUFBVjs7QUFFQTtBQUNBekgsZUFBT0MsR0FBUCxDQUFXcUQsS0FBWCxDQUFpQixJQUFqQixFQUF1QixDQUFDelUsT0FBRCxDQUF2Qjs7QUFFQTs7OztBQUlBOzs7QUFHQXNJLGtCQUFVakksRUFBVixHQUFnQixVQUFVdVEsSUFBVixFQUFnQjJELE9BQWhCLEVBQXlCO0FBQ3JDLG1CQUFPcEQsT0FBT3FELFFBQVAsQ0FBZ0I1RCxJQUFoQixFQUFzQjJELE9BQXRCLENBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUFqTSxrQkFBVStMLEdBQVYsR0FBaUIsVUFBVXpELElBQVYsRUFBZ0IyRCxPQUFoQixFQUF5QjtBQUN0QyxtQkFBT3BELE9BQU9xRCxRQUFQLENBQWdCNUQsSUFBaEIsRUFBc0IyRCxPQUF0QixFQUErQixJQUEvQixDQUFQO0FBQ0gsU0FGRDs7QUFJQSxlQUFPak0sU0FBUDtBQUNILEtBakpEOztBQW1KQTtBQUNBcEosTUFBRTZSLEVBQUYsQ0FBS3BNLFVBQUwsQ0FBZ0JtTixRQUFoQixHQUEyQjtBQUN2QjdSLGdCQUFRLEdBRGU7QUFFdkIyRSxlQUFPO0FBRmdCLEtBQTNCO0FBSUgsQ0ExSkEsRUEwSkN3QyxNQTFKRCxDQUFELEM7Ozs7Ozs7Ozs7O0FDQUE7O0FBRUMsV0FBU2xJLENBQVQsRUFBWTtBQUNUO0FBQ0FBLE1BQUU2UixFQUFGLENBQUs3TixPQUFMLEdBQWUsVUFBU2xELE9BQVQsRUFBa0I7QUFDN0I7QUFDQSxZQUFJYyxVQUFTLElBQWI7QUFDQSxZQUFJb1EsVUFBVWhTLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7QUFHQSxZQUFJaVMsU0FBUztBQUNUOzs7QUFHQUMsaUJBQUssYUFBVXBSLE9BQVYsRUFBbUI7QUFDcEIsb0JBQUksQ0FBQ2tSLFFBQVEvUSxJQUFSLENBQWEsUUFBYixDQUFMLEVBQTZCO0FBQ3pCO0FBQ0FnUiwyQkFBTzdSLElBQVAsQ0FBWVUsT0FBWjtBQUNBbVIsMkJBQU9FLEtBQVA7O0FBRUE7QUFDQUgsNEJBQVEvUSxJQUFSLENBQWEsUUFBYixFQUF1QlcsT0FBdkI7QUFDSCxpQkFQRCxNQU9PO0FBQ0g7QUFDQUEsOEJBQVNvUSxRQUFRL1EsSUFBUixDQUFhLFFBQWIsQ0FBVDtBQUNIOztBQUVELHVCQUFPVyxPQUFQO0FBQ0gsYUFsQlE7O0FBb0JUOzs7QUFHQXhCLGtCQUFNLGNBQVNVLE9BQVQsRUFBa0I7QUFDcEJjLHdCQUFPb1EsT0FBUCxHQUFpQkEsT0FBakI7QUFDQXBRLHdCQUFPd1EsTUFBUCxHQUFnQixFQUFoQjs7QUFFQTtBQUNBeFEsd0JBQU9kLE9BQVAsR0FBaUJkLEVBQUVzSyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJ0SyxFQUFFNlIsRUFBRixDQUFLN04sT0FBTCxDQUFhNE8sUUFBaEMsRUFBMEM5UixPQUExQyxDQUFqQjs7QUFFQWMsd0JBQU9DLE1BQVAsR0FBZ0I3QixFQUFFNEIsUUFBT2QsT0FBUCxDQUFlZSxNQUFqQixDQUFoQjtBQUNBRCx3QkFBT3FDLFdBQVAsR0FBcUJyQyxRQUFPZCxPQUFQLENBQWVtRCxXQUFwQztBQUNBckMsd0JBQU9zQyxZQUFQLEdBQXNCdEMsUUFBT2QsT0FBUCxDQUFlb0QsWUFBckM7O0FBRUF0Qyx3QkFBT0UsS0FBUCxHQUFlM0IsWUFBTXlRLFVBQU4sQ0FBaUJoUCxRQUFPQyxNQUF4QixFQUFnQ0QsUUFBT3FDLFdBQXZDLElBQXNELElBQXRELEdBQTZELEtBQTVFO0FBQ0gsYUFuQ1E7O0FBcUNUOzs7QUFHQWtPLG1CQUFPLGlCQUFXO0FBQ2RILHdCQUFRN1EsRUFBUixDQUFXLE9BQVgsRUFBb0I4USxPQUFPclEsTUFBM0I7QUFDSCxhQTFDUTs7QUE0Q1Q7OztBQUdBMlIsa0JBQU0sZ0JBQVk7QUFDZHZULGtCQUFFZ1MsT0FBRixFQUFXL1EsSUFBWCxDQUFnQixRQUFoQixFQUEwQlcsT0FBMUI7QUFDSCxhQWpEUTs7QUFtRFQ7OztBQUdBQSxvQkFBUSxrQkFBVztBQUNmLG9CQUFJQSxRQUFPRSxLQUFQLElBQWdCLEtBQXBCLEVBQTJCO0FBQ3ZCbVEsMkJBQU84SSxRQUFQO0FBQ0gsaUJBRkQsTUFFTztBQUNIOUksMkJBQU8rSSxTQUFQO0FBQ0g7QUFDRC9JLHVCQUFPNkIsWUFBUCxDQUFvQixRQUFwQjs7QUFFQSx1QkFBT2xTLE9BQVA7QUFDSCxhQS9EUTs7QUFpRVQ7OztBQUdBbVosc0JBQVUsb0JBQVc7QUFDakI5SSx1QkFBTzZCLFlBQVAsQ0FBb0IsVUFBcEI7O0FBRUFsUyx3QkFBT0MsTUFBUCxDQUFjMkMsUUFBZCxDQUF1QjVDLFFBQU9xQyxXQUE5Qjs7QUFFQSxvQkFBSXJDLFFBQU9zQyxZQUFYLEVBQXlCO0FBQ3JCOE4sNEJBQVF4TixRQUFSLENBQWlCNUMsUUFBT3NDLFlBQXhCO0FBQ0g7O0FBRUR0Qyx3QkFBT0UsS0FBUCxHQUFlLElBQWY7O0FBRUFtUSx1QkFBTzZCLFlBQVAsQ0FBb0IsU0FBcEI7O0FBRUEsdUJBQU9sUyxPQUFQO0FBQ0gsYUFsRlE7O0FBb0ZUOzs7QUFHQW9aLHVCQUFXLHFCQUFXO0FBQ2xCL0ksdUJBQU82QixZQUFQLENBQW9CLFdBQXBCOztBQUVBbFMsd0JBQU9DLE1BQVAsQ0FBYzRDLFdBQWQsQ0FBMEI3QyxRQUFPcUMsV0FBakM7O0FBRUEsb0JBQUlyQyxRQUFPc0MsWUFBWCxFQUF5QjtBQUNyQjhOLDRCQUFRdk4sV0FBUixDQUFvQjdDLFFBQU9zQyxZQUEzQjtBQUNIOztBQUVEdEMsd0JBQU9FLEtBQVAsR0FBZSxLQUFmOztBQUVBbVEsdUJBQU82QixZQUFQLENBQW9CLFVBQXBCOztBQUVBLHVCQUFPbFMsT0FBUDtBQUNILGFBckdROztBQXVHVDs7O0FBR0FrUywwQkFBYyxzQkFBU3BDLElBQVQsRUFBZTtBQUN6QjlQLHdCQUFPZ0YsT0FBUCxDQUFlOEssSUFBZjtBQUNBLHFCQUFLLElBQUl4RCxJQUFJLENBQWIsRUFBZ0JBLElBQUl0TSxRQUFPd1EsTUFBUCxDQUFjNU8sTUFBbEMsRUFBMEMwSyxHQUExQyxFQUErQztBQUMzQyx3QkFBSS9GLFFBQVF2RyxRQUFPd1EsTUFBUCxDQUFjbEUsQ0FBZCxDQUFaO0FBQ0Esd0JBQUkvRixNQUFNdUosSUFBTixJQUFjQSxJQUFsQixFQUF3QjtBQUNwQiw0QkFBSXZKLE1BQU1nTixHQUFOLElBQWEsSUFBakIsRUFBdUI7QUFDbkIsZ0NBQUloTixNQUFNaU4sS0FBTixJQUFlLEtBQW5CLEVBQTBCO0FBQ3RCeFQsd0NBQU93USxNQUFQLENBQWNsRSxDQUFkLEVBQWlCa0gsS0FBakIsR0FBeUIsSUFBekI7QUFDQSx1Q0FBT2pOLE1BQU1rTixPQUFOLENBQWNsSCxJQUFkLENBQW1CLElBQW5CLEVBQXlCdk0sT0FBekIsQ0FBUDtBQUNIO0FBQ0oseUJBTEQsTUFLTztBQUNILG1DQUFRdUcsTUFBTWtOLE9BQU4sQ0FBY2xILElBQWQsQ0FBbUIsSUFBbkIsRUFBeUJ2TSxPQUF6QixDQUFSO0FBQ0g7QUFDSjtBQUNKO0FBQ0osYUF6SFE7O0FBMkhUMFQsc0JBQVUsa0JBQVM1RCxJQUFULEVBQWUyRCxPQUFmLEVBQXdCRixHQUF4QixFQUE2QjtBQUNuQ3ZULHdCQUFPd1EsTUFBUCxDQUFjN0QsSUFBZCxDQUFtQjtBQUNmbUQsMEJBQU1BLElBRFM7QUFFZjJELDZCQUFTQSxPQUZNO0FBR2ZGLHlCQUFLQSxHQUhVO0FBSWZDLDJCQUFPO0FBSlEsaUJBQW5COztBQU9BbkQsdUJBQU9zQixJQUFQOztBQUVBLHVCQUFPM1IsT0FBUDtBQUNIO0FBdElRLFNBQWI7O0FBeUlBO0FBQ0EsWUFBSThYLE1BQU0sSUFBVjs7QUFFQTtBQUNBekgsZUFBT0MsR0FBUCxDQUFXcUQsS0FBWCxDQUFpQixJQUFqQixFQUF1QixDQUFDelUsT0FBRCxDQUF2Qjs7QUFFQTs7OztBQUlBOzs7QUFHQWMsZ0JBQU9BLE1BQVAsR0FBaUIsWUFBWTtBQUN6QixtQkFBT3FRLE9BQU9yUSxNQUFQLEVBQVA7QUFDSCxTQUZEOztBQUlBOzs7QUFHQUEsZ0JBQU9tWixRQUFQLEdBQW1CLFlBQVk7QUFDM0IsbUJBQU85SSxPQUFPOEksUUFBUCxFQUFQO0FBQ0gsU0FGRDs7QUFJQTs7O0FBR0FuWixnQkFBT29aLFNBQVAsR0FBb0IsWUFBWTtBQUM1QixtQkFBTy9JLE9BQU8rSSxTQUFQLEVBQVA7QUFDSCxTQUZEOztBQUlBOzs7O0FBSUFwWixnQkFBT1QsRUFBUCxHQUFhLFVBQVV1USxJQUFWLEVBQWdCMkQsT0FBaEIsRUFBeUI7QUFDbEMsbUJBQU9wRCxPQUFPcUQsUUFBUCxDQUFnQjVELElBQWhCLEVBQXNCMkQsT0FBdEIsQ0FBUDtBQUNILFNBRkQ7O0FBSUE7Ozs7QUFJQXpULGdCQUFPdVQsR0FBUCxHQUFjLFVBQVV6RCxJQUFWLEVBQWdCMkQsT0FBaEIsRUFBeUI7QUFDbkMsbUJBQU9wRCxPQUFPcUQsUUFBUCxDQUFnQjVELElBQWhCLEVBQXNCMkQsT0FBdEIsRUFBK0IsSUFBL0IsQ0FBUDtBQUNILFNBRkQ7O0FBSUEsZUFBT3pULE9BQVA7QUFDSCxLQWpNRDs7QUFtTUE7QUFDQTVCLE1BQUU2UixFQUFGLENBQUs3TixPQUFMLENBQWE0TyxRQUFiLEdBQXdCO0FBQ3BCMU8sc0JBQWMsRUFETTtBQUVwQkQscUJBQWE7QUFGTyxLQUF4QjtBQUlILENBMU1BLEVBME1DaUUsTUExTUQsQ0FBRCxDOzs7Ozs7Ozs7Ozs7OztBQ0ZPLElBQU0zSCx3Q0FBZ0IsWUFBVztBQUNwQyxRQUFJMGEsY0FBY2piLEVBQUUsa0JBQUYsQ0FBbEI7QUFDQSxRQUFJa2Isa0JBQWtCbGIsRUFBRSx1QkFBRixDQUF0QjtBQUNBLFFBQUltYixtQkFBbUJuYixFQUFFLHdCQUFGLENBQXZCO0FBQ0EsUUFBSW9iLG9CQUFvQnBiLEVBQUUseUJBQUYsQ0FBeEI7QUFDQSxRQUFJcWIscUJBQXFCSixZQUFZM0ksSUFBWixDQUFpQiwyQkFBakIsQ0FBekI7O0FBRUEsUUFBSWdKLGVBQWUsU0FBZkEsWUFBZSxHQUFXO0FBQzFCLFlBQUlsYixPQUFPLFNBQVBBLElBQU8sR0FBVztBQUNsQixnQkFBSW1iLFlBQVl2YixFQUFFLGlDQUFGLENBQWhCO0FBQ0EsZ0JBQUl3YixvQkFBb0JELFVBQVVqSixJQUFWLENBQWUsd0JBQWYsQ0FBeEI7O0FBRUEsZ0JBQUlqUCxTQUFTNFgsWUFBWTFYLFdBQVosQ0FBd0IsSUFBeEIsSUFDVDJYLGdCQUFnQjNYLFdBQWhCLENBQTRCLElBQTVCLENBRFMsR0FFVGdZLFVBQVVqSixJQUFWLENBQWUsb0JBQWYsRUFBcUMvTyxXQUFyQyxDQUFpRCxJQUFqRCxDQUZTLEdBRWdELEdBRjdEOztBQUlBO0FBQ0FpWSw4QkFBa0IzVCxHQUFsQixDQUFzQixRQUF0QixFQUFnQ3hFLE1BQWhDO0FBQ0FoRCxpQkFBS29ELFlBQUwsQ0FBa0IrWCxpQkFBbEIsRUFBcUMsRUFBckM7QUFDSCxTQVhEOztBQWFBcGI7O0FBRUE7QUFDQUQsY0FBTXVELGdCQUFOLENBQXVCdEQsSUFBdkI7QUFDSCxLQWxCRDs7QUFvQkEsUUFBSXFiLGVBQWUsU0FBZkEsWUFBZSxHQUFXO0FBQzFCO0FBQ0EsWUFBSXJiLE9BQU8sU0FBUEEsSUFBTyxHQUFXO0FBQ2xCLGdCQUFJc2IsV0FBVzFiLEVBQUUsZ0NBQUYsQ0FBZjtBQUNBLGdCQUFJcUQsU0FBU2xELE1BQU1tRCxXQUFOLEdBQW9CRCxNQUFwQixHQUE2QjZYLGdCQUFnQjNYLFdBQWhCLENBQTRCLElBQTVCLENBQTdCLEdBQWlFLEVBQTlFOztBQUVBO0FBQ0FtWSxxQkFBUzdULEdBQVQsQ0FBYSxRQUFiLEVBQXVCeEUsTUFBdkI7QUFDQWhELGlCQUFLb0QsWUFBTCxDQUFrQmlZLFFBQWxCLEVBQTRCLEVBQTVCO0FBQ0gsU0FQRDs7QUFTQXRiOztBQUVBO0FBQ0FELGNBQU11RCxnQkFBTixDQUF1QnRELElBQXZCO0FBQ0gsS0FmRDs7QUFpQkEsUUFBSXViLFdBQVcsU0FBWEEsUUFBVyxHQUFXO0FBQ3RCO0FBQ0EsWUFBSXZiLE9BQU8sU0FBUEEsSUFBTyxHQUFXO0FBQ2xCLGdCQUFJd2IsT0FBTzViLEVBQUUsNEJBQUYsQ0FBWDtBQUNBLGdCQUFJcUQsU0FBU2xELE1BQU1tRCxXQUFOLEdBQW9CRCxNQUFwQixHQUE2QjZYLGdCQUFnQjNYLFdBQWhCLENBQTRCLElBQTVCLENBQTdCLEdBQWlFLEVBQTlFOztBQUVBO0FBQ0FxWSxpQkFBSy9ULEdBQUwsQ0FBUyxRQUFULEVBQW1CeEUsTUFBbkI7QUFDQWhELGlCQUFLb0QsWUFBTCxDQUFrQm1ZLElBQWxCLEVBQXdCLEVBQXhCO0FBQ0gsU0FQRDs7QUFTQXhiOztBQUVBO0FBQ0FELGNBQU11RCxnQkFBTixDQUF1QnRELElBQXZCO0FBQ0gsS0FmRDs7QUFpQkEsUUFBSXliLG9CQUFvQixTQUFwQkEsaUJBQW9CLEdBQVc7QUFDL0JQO0FBQ0FHO0FBQ0FFO0FBQ0gsS0FKRDs7QUFNQSxRQUFJRyxnQkFBZ0IsU0FBaEJBLGFBQWdCLEdBQVc7QUFDM0JiLG9CQUFZelosVUFBWixDQUF1QjtBQUNuQkMsbUJBQU8saUJBRFk7QUFFbkJDLHFCQUFTLElBRlU7QUFHbkJDLG1CQUFPd1osZ0JBSFk7QUFJbkJ2WixvQkFBUXdaO0FBSlcsU0FBdkI7O0FBT0E7QUFDQUgsb0JBQVl6WixVQUFaLEdBQXlCMlQsR0FBekIsQ0FBNkIsV0FBN0IsRUFBMEMsWUFBVztBQUNqRDlVLGlCQUFLaUwsS0FBTCxDQUFXMlAsV0FBWDs7QUFFQTVQLHVCQUFXLFlBQVc7QUFDbEJoTCxxQkFBS29NLE9BQUwsQ0FBYXdPLFdBQWI7O0FBRUFJLG1DQUFtQjVXLFdBQW5CLENBQStCLFNBQS9COztBQUVBb1g7QUFDSCxhQU5ELEVBTUcsSUFOSDtBQU9ILFNBVkQ7QUFXSCxLQXBCRDs7QUFzQkEsV0FBTztBQUNIemIsY0FBTSxnQkFBVztBQUNiMGI7QUFDSDtBQUhFLEtBQVA7QUFLSCxDQTlGNEIsRUFBdEI7O0FBZ0dQO0FBQ0E7QUFDQSxNIiwiZmlsZSI6Ii9qcy90aGVtZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7bVV0aWx9ICAgICAgICAgZnJvbSBcIi4vLi4vLi4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9iYXNlL3V0aWxcIjtcbmltcG9ydCB7bUFwcH0gICAgICAgICAgZnJvbSBcIi4vLi4vLi4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9iYXNlL2FwcFwiO1xuaW1wb3J0IHttTGF5b3V0fSAgICAgICBmcm9tIFwiLi8uLi8uLi9jb21tb24vdGhlbWUvanMvZGVtby9kZWZhdWx0L2Jhc2UvbGF5b3V0XCI7XG5pbXBvcnQge21RdWlja1NpZGViYXJ9IGZyb20gXCIuLy4uLy4uL2NvbW1vbi90aGVtZS9qcy9zbmlwcGV0cy9iYXNlL3F1aWNrLXNpZGViYXJcIjtcblxuLy8gcmVxdWlyZShcIi4vLi4vLi4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvYW5pbWF0ZVwiKTtcbnJlcXVpcmUoXCIuLy4uLy4uL2NvbW1vbi90aGVtZS9qcy9mcmFtZXdvcmsvY29tcG9uZW50cy9nZW5lcmFsL2Ryb3Bkb3duXCIpO1xuLy8gcmVxdWlyZShcIi4vLi4vLi4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvZXhhbXBsZVwiKTtcbnJlcXVpcmUoXCIuLy4uLy4uL2NvbW1vbi90aGVtZS9qcy9mcmFtZXdvcmsvY29tcG9uZW50cy9nZW5lcmFsL2hlYWRlclwiKTtcbnJlcXVpcmUoXCIuLy4uLy4uL2NvbW1vbi90aGVtZS9qcy9mcmFtZXdvcmsvY29tcG9uZW50cy9nZW5lcmFsL21lbnVcIik7XG4vLyByZXF1aXJlKFwiLi8uLi8uLi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9tZXNzZW5nZXJcIik7XG5yZXF1aXJlKFwiLi8uLi8uLi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9vZmZjYW52YXNcIik7XG4vLyByZXF1aXJlKFwiLi8uLi8uLi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9wb3J0bGV0XCIpO1xucmVxdWlyZShcIi4vLi4vLi4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvcXVpY2tzZWFyY2hcIik7XG5yZXF1aXJlKFwiLi8uLi8uLi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9zY3JvbGwtdG9wXCIpO1xucmVxdWlyZShcIi4vLi4vLi4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvdG9nZ2xlXCIpO1xuLy8gcmVxdWlyZShcIi4vLi4vLi4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvd2l6YXJkXCIpO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICBtVXRpbC5pbml0KCk7XG4gICAgbUFwcC5pbml0KCk7XG4gICAgbUxheW91dC5pbml0KCk7XG4gICAgbVF1aWNrU2lkZWJhci5pbml0KCk7XG59KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvZG9tYWluL2FkbWluL2pzL3RoZW1lLmpzIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvZG9tYWluL2FkbWluL3Nhc3MvYXBwLnNjc3Ncbi8vIG1vZHVsZSBpZCA9IC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vYWRtaW4vc2Fzcy9hcHAuc2Nzc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vYWRtaW4vc2Fzcy92ZW5kb3Iuc2Nzc1xuLy8gbW9kdWxlIGlkID0gLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9hZG1pbi9zYXNzL3ZlbmRvci5zY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsImV4cG9ydCBjb25zdCBtTGF5b3V0ID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGhvck1lbnU7XG4gICAgdmFyIGFzaWRlTWVudTtcbiAgICB2YXIgYXNpZGVNZW51T2ZmY2FudmFzO1xuICAgIHZhciBob3JNZW51T2ZmY2FudmFzO1xuXG4gICAgdmFyIGluaXRTdGlja3lIZWFkZXIgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIGhlYWRlciA9ICQoJy5tLWhlYWRlcicpO1xuICAgICAgICB2YXIgb3B0aW9ucyA9IHtcbiAgICAgICAgICAgIG9mZnNldDoge30sXG4gICAgICAgICAgICBtaW5pbWl6ZTp7fSAgICAgICBcbiAgICAgICAgfTtcblxuICAgICAgICBpZiAoaGVhZGVyLmRhdGEoJ21pbmltaXplLW1vYmlsZScpID09ICdoaWRlJykge1xuICAgICAgICAgICAgb3B0aW9ucy5taW5pbWl6ZS5tb2JpbGUgPSB7fTtcbiAgICAgICAgICAgIG9wdGlvbnMubWluaW1pemUubW9iaWxlLm9uID0gJ20taGVhZGVyLS1oaWRlJztcbiAgICAgICAgICAgIG9wdGlvbnMubWluaW1pemUubW9iaWxlLm9mZiA9ICdtLWhlYWRlci0tc2hvdyc7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBvcHRpb25zLm1pbmltaXplLm1vYmlsZSA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGhlYWRlci5kYXRhKCdtaW5pbWl6ZScpID09ICdoaWRlJykge1xuICAgICAgICAgICAgb3B0aW9ucy5taW5pbWl6ZS5kZXNrdG9wID0ge307XG4gICAgICAgICAgICBvcHRpb25zLm1pbmltaXplLmRlc2t0b3Aub24gPSAnbS1oZWFkZXItLWhpZGUnO1xuICAgICAgICAgICAgb3B0aW9ucy5taW5pbWl6ZS5kZXNrdG9wLm9mZiA9ICdtLWhlYWRlci0tc2hvdyc7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBvcHRpb25zLm1pbmltaXplLmRlc2t0b3AgPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChoZWFkZXIuZGF0YSgnbWluaW1pemUtb2Zmc2V0JykpIHtcbiAgICAgICAgICAgIG9wdGlvbnMub2Zmc2V0LmRlc2t0b3AgPSBoZWFkZXIuZGF0YSgnbWluaW1pemUtb2Zmc2V0Jyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaGVhZGVyLmRhdGEoJ21pbmltaXplLW1vYmlsZS1vZmZzZXQnKSkge1xuICAgICAgICAgICAgb3B0aW9ucy5vZmZzZXQubW9iaWxlID0gaGVhZGVyLmRhdGEoJ21pbmltaXplLW1vYmlsZS1vZmZzZXQnKTtcbiAgICAgICAgfSAgICAgICAgXG5cbiAgICAgICAgaGVhZGVyLm1IZWFkZXIob3B0aW9ucyk7XG4gICAgfVxuXG4gICAgLy8gaGFuZGxlIGhvcml6b250YWwgbWVudVxuICAgIHZhciBpbml0SG9yTWVudSA9IGZ1bmN0aW9uKCkgeyBcbiAgICAgICAgLy8gaW5pdCBhc2lkZSBsZWZ0IG9mZmNhbnZhc1xuICAgICAgICBob3JNZW51T2ZmY2FudmFzID0gJCgnI21faGVhZGVyX21lbnUnKS5tT2ZmY2FudmFzKHtcbiAgICAgICAgICAgIGNsYXNzOiAnbS1hc2lkZS1oZWFkZXItbWVudS1tb2JpbGUnLFxuICAgICAgICAgICAgb3ZlcmxheTogdHJ1ZSxcbiAgICAgICAgICAgIGNsb3NlOiAnI21fYXNpZGVfaGVhZGVyX21lbnVfbW9iaWxlX2Nsb3NlX2J0bicsXG4gICAgICAgICAgICB0b2dnbGU6IHtcbiAgICAgICAgICAgICAgICB0YXJnZXQ6ICcjbV9hc2lkZV9oZWFkZXJfbWVudV9tb2JpbGVfdG9nZ2xlJyxcbiAgICAgICAgICAgICAgICBzdGF0ZTogJ20tYnJhbmRfX3RvZ2dsZXItLWFjdGl2ZSdcbiAgICAgICAgICAgIH0gICAgICAgICAgICBcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgICAgICBob3JNZW51ID0gJCgnI21faGVhZGVyX21lbnUnKS5tTWVudSh7XG4gICAgICAgICAgICAvLyBzdWJtZW51IG1vZGVzXG4gICAgICAgICAgICBzdWJtZW51OiB7XG4gICAgICAgICAgICAgICAgZGVza3RvcDogJ2Ryb3Bkb3duJyxcbiAgICAgICAgICAgICAgICB0YWJsZXQ6ICdhY2NvcmRpb24nLFxuICAgICAgICAgICAgICAgIG1vYmlsZTogJ2FjY29yZGlvbidcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAvLyByZXNpemUgbWVudSBvbiB3aW5kb3cgcmVzaXplXG4gICAgICAgICAgICByZXNpemU6IHtcbiAgICAgICAgICAgICAgICBkZXNrdG9wOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGhlYWRlck5hdldpZHRoID0gJCgnI21faGVhZGVyX25hdicpLndpZHRoKCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBoZWFkZXJNZW51V2lkdGggPSAkKCcjbV9oZWFkZXJfbWVudV9jb250YWluZXInKS53aWR0aCgpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgaGVhZGVyVG9wYmFyV2lkdGggPSAkKCcjbV9oZWFkZXJfdG9wYmFyJykud2lkdGgoKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHNwYXJlV2lkdGggPSAyMDtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoKGhlYWRlck1lbnVXaWR0aCArIGhlYWRlclRvcGJhcldpZHRoICsgc3BhcmVXaWR0aCkgPiBoZWFkZXJOYXZXaWR0aCApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSAgICBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gaGFuZGxlIHZlcnRpY2FsIG1lbnVcbiAgICB2YXIgaW5pdExlZnRBc2lkZU1lbnUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIG1lbnUgPSAkKCcjbV92ZXJfbWVudScpO1xuXG4gICAgICAgIC8vIGluaXQgYXNpZGUgbWVudVxuICAgICAgICB2YXIgbWVudU9wdGlvbnMgPSB7ICBcbiAgICAgICAgICAgIC8vIHN1Ym1lbnUgc2V0dXBcbiAgICAgICAgICAgIHN1Ym1lbnU6IHtcbiAgICAgICAgICAgICAgICBkZXNrdG9wOiB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGJ5IGRlZmF1bHQgdGhlIG1lbnUgbW9kZSBzZXQgdG8gYWNjb3JkaW9uIGluIGRlc2t0b3AgbW9kZVxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OiAobWVudS5kYXRhKCdtZW51LWRyb3Bkb3duJykgPT0gdHJ1ZSA/ICdkcm9wZG93bicgOiAnYWNjb3JkaW9uJyksXG4gICAgICAgICAgICAgICAgICAgIC8vIHdoZW5ldmVyIGJvZHkgaGFzIHRoaXMgY2xhc3Mgc3dpdGNoIHRoZSBtZW51IG1vZGUgdG8gZHJvcGRvd25cbiAgICAgICAgICAgICAgICAgICAgc3RhdGU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvZHk6ICdtLWFzaWRlLWxlZnQtLW1pbmltaXplJywgIFxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kZTogJ2Ryb3Bkb3duJ1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB0YWJsZXQ6ICdhY2NvcmRpb24nLCAvLyBtZW51IHNldCB0byBhY2NvcmRpb24gaW4gdGFibGV0IG1vZGVcbiAgICAgICAgICAgICAgICBtb2JpbGU6ICdhY2NvcmRpb24nICAvLyBtZW51IHNldCB0byBhY2NvcmRpb24gaW4gbW9iaWxlIG1vZGVcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8vYWNjb3JkaW9uIHNldHVwXG4gICAgICAgICAgICBhY2NvcmRpb246IHtcbiAgICAgICAgICAgICAgICBhdXRvU2Nyb2xsOiB0cnVlLFxuICAgICAgICAgICAgICAgIGV4cGFuZEFsbDogZmFsc2VcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBhc2lkZU1lbnUgPSBtZW51Lm1NZW51KG1lbnVPcHRpb25zKTtcblxuICAgICAgICAvLyBoYW5kbGUgZml4ZWQgYXNpZGUgbWVudVxuICAgICAgICBpZiAobWVudS5kYXRhKCdtZW51LXNjcm9sbGFibGUnKSkge1xuICAgICAgICAgICAgZnVuY3Rpb24gaW5pdFNjcm9sbGFibGVNZW51KG9iaikgeyAgICBcbiAgICAgICAgICAgICAgICBpZiAobVV0aWwuaXNJblJlc3BvbnNpdmVSYW5nZSgndGFibGV0LWFuZC1tb2JpbGUnKSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBkZXN0cm95IGlmIHRoZSBpbnN0YW5jZSB3YXMgcHJldmlvdXNseSBjcmVhdGVkXG4gICAgICAgICAgICAgICAgICAgIG1BcHAuZGVzdHJveVNjcm9sbGVyKG9iaik7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgaGVpZ2h0ID0gbVV0aWwuZ2V0Vmlld1BvcnQoKS5oZWlnaHQgLSAkKCcubS1oZWFkZXInKS5vdXRlckhlaWdodCgpXG4gICAgICAgICAgICAgICAgICAgIC0gKCQoJy5tLWFzaWRlLWxlZnQgLm0tYXNpZGVfX2hlYWRlcicpLmxlbmd0aCAhPSAwID8gJCgnLm0tYXNpZGUtbGVmdCAubS1hc2lkZV9faGVhZGVyJykub3V0ZXJIZWlnaHQoKSA6IDApXG4gICAgICAgICAgICAgICAgICAgIC0gKCQoJy5tLWFzaWRlLWxlZnQgLm0tYXNpZGVfX2Zvb3RlcicpLmxlbmd0aCAhPSAwID8gJCgnLm0tYXNpZGUtbGVmdCAubS1hc2lkZV9fZm9vdGVyJykub3V0ZXJIZWlnaHQoKSA6IDApO1xuICAgICAgICAgICAgICAgICAgICAvLy0gJCgnLm0tZm9vdGVyJykub3V0ZXJIZWlnaHQoKTsgXG5cbiAgICAgICAgICAgICAgICAvLyBjcmVhdGUvcmUtY3JlYXRlIGEgbmV3IGluc3RhbmNlXG4gICAgICAgICAgICAgICAgbUFwcC5pbml0U2Nyb2xsZXIob2JqLCB7aGVpZ2h0OiBoZWlnaHR9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW5pdFNjcm9sbGFibGVNZW51KGFzaWRlTWVudSk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIG1VdGlsLmFkZFJlc2l6ZUhhbmRsZXIoZnVuY3Rpb24oKSB7ICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaW5pdFNjcm9sbGFibGVNZW51KGFzaWRlTWVudSk7XG4gICAgICAgICAgICB9KTsgICBcbiAgICAgICAgfSAgICAgIFxuICAgIH1cblxuICAgIC8vIGhhbmRsZSB2ZXJ0aWNhbCBtZW51XG4gICAgdmFyIGluaXRMZWZ0QXNpZGUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgLy8gaW5pdCBhc2lkZSBsZWZ0IG9mZmNhbnZhc1xuICAgICAgICB2YXIgYXNpZGVPZmZjYW52YXNDbGFzcyA9ICgkKCcjbV9hc2lkZV9sZWZ0JykuaGFzQ2xhc3MoJ20tYXNpZGUtbGVmdC0tb2ZmY2FudmFzLWRlZmF1bHQnKSA/ICdtLWFzaWRlLWxlZnQtLW9mZmNhbnZhcy1kZWZhdWx0JyA6ICdtLWFzaWRlLWxlZnQnKTtcblxuICAgICAgICBhc2lkZU1lbnVPZmZjYW52YXMgPSAkKCcjbV9hc2lkZV9sZWZ0JykubU9mZmNhbnZhcyh7XG4gICAgICAgICAgICBjbGFzczogYXNpZGVPZmZjYW52YXNDbGFzcyxcbiAgICAgICAgICAgIG92ZXJsYXk6IHRydWUsXG4gICAgICAgICAgICBjbG9zZTogJyNtX2FzaWRlX2xlZnRfY2xvc2VfYnRuJyxcbiAgICAgICAgICAgIHRvZ2dsZToge1xuICAgICAgICAgICAgICAgIHRhcmdldDogJyNtX2FzaWRlX2xlZnRfb2ZmY2FudmFzX3RvZ2dsZScsXG4gICAgICAgICAgICAgICAgc3RhdGU6ICdtLWJyYW5kX190b2dnbGVyLS1hY3RpdmUnICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSAgICAgICAgICAgIFxuICAgICAgICB9KTsgICAgICAgIFxuICAgIH1cblxuICAgIC8vIGhhbmRsZSBzaWRlYmFyIHRvZ2dsZVxuICAgIHZhciBpbml0TGVmdEFzaWRlVG9nZ2xlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBhc2lkZUxlZnRUb2dnbGUgPSAkKCcjbV9hc2lkZV9sZWZ0X21pbmltaXplX3RvZ2dsZScpLm1Ub2dnbGUoe1xuICAgICAgICAgICAgdGFyZ2V0OiAnYm9keScsXG4gICAgICAgICAgICB0YXJnZXRTdGF0ZTogJ20tYnJhbmQtLW1pbmltaXplIG0tYXNpZGUtbGVmdC0tbWluaW1pemUnLFxuICAgICAgICAgICAgdG9nZ2xlclN0YXRlOiAnbS1icmFuZF9fdG9nZ2xlci0tYWN0aXZlJ1xuICAgICAgICB9KS5vbigndG9nZ2xlJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBob3JNZW51LnBhdXNlRHJvcGRvd25Ib3Zlcig4MDApO1xuICAgICAgICAgICAgYXNpZGVNZW51LnBhdXNlRHJvcGRvd25Ib3Zlcig4MDApO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLz09IEV4YW1wbGU6IG1pbmltaXplIHRoZSBsZWZ0IGFzaWRlIG9uIHBhZ2UgbG9hZFxuICAgICAgICAvLz09IGFzaWRlTGVmdFRvZ2dsZS50b2dnbGVPbigpO1xuXG4gICAgICAgICQoJyNtX2FzaWRlX2xlZnRfaGlkZV90b2dnbGUnKS5tVG9nZ2xlKHtcbiAgICAgICAgICAgIHRhcmdldDogJ2JvZHknLFxuICAgICAgICAgICAgdGFyZ2V0U3RhdGU6ICdtLWFzaWRlLWxlZnQtLWhpZGUnLFxuICAgICAgICAgICAgdG9nZ2xlclN0YXRlOiAnbS1icmFuZF9fdG9nZ2xlci0tYWN0aXZlJ1xuICAgICAgICB9KS5vbigndG9nZ2xlJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBob3JNZW51LnBhdXNlRHJvcGRvd25Ib3Zlcig4MDApO1xuICAgICAgICAgICAgYXNpZGVNZW51LnBhdXNlRHJvcGRvd25Ib3Zlcig4MDApO1xuICAgICAgICB9KVxuICAgIH1cblxuICAgIHZhciBpbml0VG9wYmFyID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJyNtX2FzaWRlX2hlYWRlcl90b3BiYXJfbW9iaWxlX3RvZ2dsZScpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJCgnYm9keScpLnRvZ2dsZUNsYXNzKCdtLXRvcGJhci0tb24nKTtcbiAgICAgICAgfSk7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuXG4gICAgICAgIC8vIEFuaW1hdGVkIE5vdGlmaWNhdGlvbiBJY29uIFxuICAgICAgICBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICQoJyNtX3RvcGJhcl9ub3RpZmljYXRpb25faWNvbiAubS1uYXZfX2xpbmstaWNvbicpLmFkZENsYXNzKCdtLWFuaW1hdGUtc2hha2UnKTtcbiAgICAgICAgICAgICQoJyNtX3RvcGJhcl9ub3RpZmljYXRpb25faWNvbiAubS1uYXZfX2xpbmstYmFkZ2UnKS5hZGRDbGFzcygnbS1hbmltYXRlLWJsaW5rJyk7XG4gICAgICAgIH0sIDMwMDApO1xuXG4gICAgICAgIHNldEludGVydmFsKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJCgnI21fdG9wYmFyX25vdGlmaWNhdGlvbl9pY29uIC5tLW5hdl9fbGluay1pY29uJykucmVtb3ZlQ2xhc3MoJ20tYW5pbWF0ZS1zaGFrZScpO1xuICAgICAgICAgICAgJCgnI21fdG9wYmFyX25vdGlmaWNhdGlvbl9pY29uIC5tLW5hdl9fbGluay1iYWRnZScpLnJlbW92ZUNsYXNzKCdtLWFuaW1hdGUtYmxpbmsnKTtcbiAgICAgICAgfSwgNjAwMCk7XG4gICAgfVxuXG4gICAgLy8gaGFuZGxlIHF1aWNrIHNlYXJjaFxuICAgIHZhciBpbml0UXVpY2tzZWFyY2ggPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHFzID0gJCgnI21fcXVpY2tzZWFyY2gnKTtcblxuICAgICAgICBxcy5tUXVpY2tzZWFyY2goe1xuICAgICAgICAgICAgdHlwZTogcXMuZGF0YSgnc2VhcmNoLXR5cGUnKSwgLy8gcXVpY2sgc2VhcmNoIHR5cGVcbiAgICAgICAgICAgIHNvdXJjZTogJ2luYy9hcGkvcXVpY2tfc2VhcmNoLnBocCcsICAgICAgICAgICAgXG4gICAgICAgICAgICBzcGlubmVyOiAnbS1sb2FkZXIgbS1sb2FkZXItLXNraW4tbGlnaHQgbS1sb2FkZXItLXJpZ2h0JyxcblxuICAgICAgICAgICAgaW5wdXQ6ICcjbV9xdWlja3NlYXJjaF9pbnB1dCcsXG4gICAgICAgICAgICBpY29uQ2xvc2U6ICcjbV9xdWlja3NlYXJjaF9jbG9zZScsXG4gICAgICAgICAgICBpY29uQ2FuY2VsOiAnI21fcXVpY2tzZWFyY2hfY2FuY2VsJyxcbiAgICAgICAgICAgIGljb25TZWFyY2g6ICcjbV9xdWlja3NlYXJjaF9zZWFyY2gnLFxuXG4gICAgICAgICAgICBoYXNSZXN1bHRDbGFzczogJ20tbGlzdC1zZWFyY2gtLWhhcy1yZXN1bHQnLFxuICAgICAgICAgICAgbWluTGVuZ3RoOiAxLCAgICAgICAgICAgIFxuICAgICAgICAgICAgdGVtcGxhdGVzOiB7XG4gICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKHFzKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnPGRpdiBjbGFzcz1cIm0tc2VhcmNoLXJlc3VsdHMgbS1zZWFyY2gtcmVzdWx0cy0tc2tpbi1saWdodFwiPjxzcGFuIGNsYXNzPVwibS1zZWFyY2gtcmVzdWx0X19tZXNzYWdlXCI+U29tZXRoaW5nIHdlbnQgd3Jvbmc8L2Rpdj48L2Rpdj4nO1xuICAgICAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pOyAgICAgIFxuICAgIH1cblxuICAgIHZhciBpbml0U2Nyb2xsVG9wID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cIm0tc2Nyb2xsLXRvcFwiXScpLm1TY3JvbGxUb3Aoe1xuICAgICAgICAgICAgb2Zmc2V0OiAzMDAsXG4gICAgICAgICAgICBzcGVlZDogNjAwXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICAgIGluaXQ6IGZ1bmN0aW9uKCkgeyAgXG4gICAgICAgICAgICB0aGlzLmluaXRIZWFkZXIoKTtcbiAgICAgICAgICAgIHRoaXMuaW5pdEFzaWRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5pdEhlYWRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpbml0U3RpY2t5SGVhZGVyKCk7XG4gICAgICAgICAgICBpbml0SG9yTWVudSgpO1xuICAgICAgICAgICAgaW5pdFRvcGJhcigpO1xuICAgICAgICAgICAgaW5pdFF1aWNrc2VhcmNoKCk7XG4gICAgICAgICAgICBpbml0U2Nyb2xsVG9wKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5pdEFzaWRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGluaXRMZWZ0QXNpZGUoKTtcbiAgICAgICAgICAgIGluaXRMZWZ0QXNpZGVNZW51KCk7ICAgICAgICAgICAgXG4gICAgICAgICAgICBpbml0TGVmdEFzaWRlVG9nZ2xlKCk7XG5cbiAgICAgICAgICAgIHRoaXMub25MZWZ0U2lkZWJhclRvZ2dsZShmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgIHZhciBkYXRhdGFibGVzID0gJCgnLm0tZGF0YXRhYmxlJyk7XG4gICAgICAgICAgICAgICQoZGF0YXRhYmxlcykuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLm1EYXRhdGFibGUoJ3JlZHJhdycpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldEFzaWRlTWVudTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gYXNpZGVNZW51O1xuICAgICAgICB9LFxuXG4gICAgICAgIG9uTGVmdFNpZGViYXJUb2dnbGU6IGZ1bmN0aW9uKGZ1bmMpIHtcbiAgICAgICAgICAgICQoJyNtX2FzaWRlX2xlZnRfbWluaW1pemVfdG9nZ2xlJykubVRvZ2dsZSgpLm9uKCd0b2dnbGUnLCBmdW5jKTtcbiAgICAgICAgfSxcblxuICAgICAgICBjbG9zZU1vYmlsZUFzaWRlTWVudU9mZmNhbnZhczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiAobVV0aWwuaXNNb2JpbGVEZXZpY2UoKSkge1xuICAgICAgICAgICAgICAgIGFzaWRlTWVudU9mZmNhbnZhcy5oaWRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2xvc2VNb2JpbGVIb3JNZW51T2ZmY2FudmFzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmIChtVXRpbC5pc01vYmlsZURldmljZSgpKSB7XG4gICAgICAgICAgICAgICAgaG9yTWVudU9mZmNhbnZhcy5oaWRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xufSgpO1xuXG4vLyAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbi8vICAgICBpZiAobVV0aWwuaXNBbmd1bGFyVmVyc2lvbigpID09PSBmYWxzZSkge1xuLy8gICAgICAgICBtTGF5b3V0LmluaXQoKTtcbi8vICAgICB9XG4vLyB9KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvZG9tYWluL2NvbW1vbi90aGVtZS9qcy9kZW1vL2RlZmF1bHQvYmFzZS9sYXlvdXQuanMiLCIvKipcbiAqIEBjbGFzcyBtQXBwICBNZXRyb25pYyBBcHAgY2xhc3NcbiAqL1xuXG5pbXBvcnQge21VdGlsfSBmcm9tIFwiLi91dGlsXCI7XG5cbmV4cG9ydCBjb25zdCBtQXBwID0gZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyBib290c3RyYXAgdG9vbHRpcFxuICAgICAqL1xuICAgIHZhciBpbml0VG9vbHRpcCA9IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgIHZhciBza2luID0gZWwuZGF0YShcInNraW5cIikgPyBcIm0tdG9vbHRpcC0tc2tpbi1cIiArIGVsLmRhdGEoXCJza2luXCIpIDogXCJcIjtcbiAgICAgICAgdmFyIHdpZHRoID0gZWwuZGF0YShcIndpZHRoXCIpID09IFwiYXV0b1wiID8gXCJtLXRvb2x0b3AtLWF1dG8td2lkdGhcIiA6IFwiXCI7XG5cbiAgICAgICAgZWwudG9vbHRpcCh7XG4gICAgICAgICAgICB0cmlnZ2VyOiBcImhvdmVyXCIsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogXCI8ZGl2IGNsYXNzPVxcXCJtLXRvb2x0aXAgXCIgKyBza2luICsgXCIgXCIgKyB3aWR0aCArIFwiIHRvb2x0aXBcXFwiIHJvbGU9XFxcInRvb2x0aXBcXFwiPlxcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiYXJyb3dcXFwiPjwvZGl2PlxcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwidG9vbHRpcC1pbm5lclxcXCI+PC9kaXY+XFxcbiAgICAgICAgICAgIDwvZGl2PlwiLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogSW5pdGlhbGl6ZXMgYm9vdHN0cmFwIHRvb2x0aXBzXG4gICAgICovXG4gICAgdmFyIGluaXRUb29sdGlwcyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyBpbml0IGJvb3RzdHJhcCB0b29sdGlwc1xuICAgICAgICAkKFwiW2RhdGEtdG9nZ2xlPVxcXCJtLXRvb2x0aXBcXFwiXVwiKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaW5pdFRvb2x0aXAoJCh0aGlzKSk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyBib290c3RyYXAgcG9wb3ZlclxuICAgICAqL1xuICAgIHZhciBpbml0UG9wb3ZlciA9IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgIHZhciBza2luID0gZWwuZGF0YShcInNraW5cIikgPyBcIm0tcG9wb3Zlci0tc2tpbi1cIiArIGVsLmRhdGEoXCJza2luXCIpIDogXCJcIjtcbiAgICAgICAgdmFyIHRyaWdnZXJWYWx1ZSA9IGVsLmRhdGEoXCJ0cmlnZ2VyXCIpID8gZWwuZGF0YShcInRyaWdnZXJcIikgOiBcImhvdmVyXCI7XG5cbiAgICAgICAgZWwucG9wb3Zlcih7XG4gICAgICAgICAgICB0cmlnZ2VyOiB0cmlnZ2VyVmFsdWUsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogXCJcXFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwibS1wb3BvdmVyIFwiICsgc2tpbiArIFwiIHBvcG92ZXJcXFwiIHJvbGU9XFxcInRvb2x0aXBcXFwiPlxcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiYXJyb3dcXFwiPjwvZGl2PlxcXG4gICAgICAgICAgICAgICAgPGgzIGNsYXNzPVxcXCJwb3BvdmVyLWhlYWRlclxcXCI+PC9oMz5cXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInBvcG92ZXItYm9keVxcXCI+PC9kaXY+XFxcbiAgICAgICAgICAgIDwvZGl2PlwiLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogSW5pdGlhbGl6ZXMgYm9vdHN0cmFwIHBvcG92ZXJzXG4gICAgICovXG4gICAgdmFyIGluaXRQb3BvdmVycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyBpbml0IGJvb3RzdHJhcCBwb3BvdmVyXG4gICAgICAgICQoXCJbZGF0YS10b2dnbGU9XFxcIm0tcG9wb3ZlclxcXCJdXCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpbml0UG9wb3ZlcigkKHRoaXMpKTtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemVzIGJvb3RzdHJhcCBmaWxlIGlucHV0XG4gICAgICovXG4gICAgdmFyIGluaXRGaWxlSW5wdXQgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgLy8gaW5pdCBib290c3RyYXAgcG9wb3ZlclxuICAgICAgICAkKFwiLmN1c3RvbS1maWxlLWlucHV0XCIpLm9uKFwiY2hhbmdlXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIGZpbGVOYW1lID0gJCh0aGlzKS52YWwoKTtcbiAgICAgICAgICAgICQodGhpcykubmV4dChcIi5jdXN0b20tZmlsZS1jb250cm9sXCIpLmFkZENsYXNzKFwic2VsZWN0ZWRcIikuaHRtbChmaWxlTmFtZSk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyBtZXRyb25pYyBwb3J0bGV0XG4gICAgICovXG4gICAgdmFyIGluaXRQb3J0bGV0ID0gZnVuY3Rpb24oZWwsIG9wdGlvbnMpIHtcbiAgICAgICAgLy8gaW5pdCBwb3J0bGV0IHRvb2xzXG4gICAgICAgIGVsLm1Qb3J0bGV0KG9wdGlvbnMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyBtZXRyb25pYyBwb3J0bGV0c1xuICAgICAqL1xuICAgIHZhciBpbml0UG9ydGxldHMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgLy8gaW5pdCBwb3J0bGV0IHRvb2xzXG4gICAgICAgICQoXCJbZGF0YS1wb3J0bGV0PVxcXCJ0cnVlXFxcIl1cIikuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBlbCA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgIGlmIChlbC5kYXRhKFwicG9ydGxldC1pbml0aWFsaXplZFwiKSAhPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIGluaXRQb3J0bGV0KGVsLCB7fSk7XG4gICAgICAgICAgICAgICAgZWwuZGF0YShcInBvcnRsZXQtaW5pdGlhbGl6ZWRcIiwgdHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyBzY3JvbGxhYmxlIGNvbnRlbnRzXG4gICAgICovXG4gICAgdmFyIGluaXRTY3JvbGxhYmxlcyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAkKFwiW2RhdGEtc2Nyb2xsYWJsZT1cXFwidHJ1ZVxcXCJdXCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgbWF4SGVpZ2h0O1xuICAgICAgICAgICAgdmFyIGhlaWdodDtcbiAgICAgICAgICAgIHZhciBlbCA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgIGlmIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKFwidGFibGV0LWFuZC1tb2JpbGVcIikpIHtcbiAgICAgICAgICAgICAgICBpZiAoZWwuZGF0YShcIm1vYmlsZS1tYXgtaGVpZ2h0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIG1heEhlaWdodCA9IGVsLmRhdGEoXCJtb2JpbGUtbWF4LWhlaWdodFwiKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBtYXhIZWlnaHQgPSBlbC5kYXRhKFwibWF4LWhlaWdodFwiKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoZWwuZGF0YShcIm1vYmlsZS1oZWlnaHRcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0ID0gZWwuZGF0YShcIm1vYmlsZS1oZWlnaHRcIik7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0ID0gZWwuZGF0YShcImhlaWdodFwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIG1heEhlaWdodCA9IGVsLmRhdGEoXCJtYXgtaGVpZ2h0XCIpO1xuICAgICAgICAgICAgICAgIGhlaWdodCA9IGVsLmRhdGEoXCJtYXgtaGVpZ2h0XCIpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAobWF4SGVpZ2h0KSB7XG4gICAgICAgICAgICAgICAgZWwuY3NzKFwibWF4LWhlaWdodFwiLCBtYXhIZWlnaHQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGhlaWdodCkge1xuICAgICAgICAgICAgICAgIGVsLmNzcyhcImhlaWdodFwiLCBoZWlnaHQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBtQXBwLmluaXRTY3JvbGxlcihlbCwge30pO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogSW5pdGlhbGl6ZXMgYm9vdHN0cmFwIGFsZXJ0c1xuICAgICAqL1xuICAgIHZhciBpbml0QWxlcnRzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIC8vIGluaXQgYm9vdHN0cmFwIHBvcG92ZXJcbiAgICAgICAgJChcImJvZHlcIikub24oXCJjbGlja1wiLCBcIltkYXRhLWNsb3NlPWFsZXJ0XVwiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICQodGhpcykuY2xvc2VzdChcIi5hbGVydFwiKS5oaWRlKCk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyBib290c3RyYXAgY29sbGFwc2UgZm9yIE1ldHJvbmljJ3MgYWNjb3JkaW9uIGZlYXR1cmVcbiAgICAgKi9cbiAgICB2YXIgaW5pdEFjY29yZGlvbnMgPSBmdW5jdGlvbihlbCkge1xuXG4gICAgfTtcblxuICAgIHZhciBoaWRlVG91Y2hXYXJuaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIGpRdWVyeS5ldmVudC5zcGVjaWFsLnRvdWNoc3RhcnQgPSB7XG4gICAgICAgICAgICBzZXR1cDogZnVuY3Rpb24oXywgbnMsIGhhbmRsZSkge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgdGhpcyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICAgICAgICAgICAgICBpZiAobnMuaW5jbHVkZXMoXCJub1ByZXZlbnREZWZhdWx0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaHN0YXJ0XCIsIGhhbmRsZSwge3Bhc3NpdmU6IGZhbHNlfSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaHN0YXJ0XCIsIGhhbmRsZSwge3Bhc3NpdmU6IHRydWV9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICAgICAgalF1ZXJ5LmV2ZW50LnNwZWNpYWwudG91Y2htb3ZlID0ge1xuICAgICAgICAgICAgc2V0dXA6IGZ1bmN0aW9uKF8sIG5zLCBoYW5kbGUpIHtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgICAgICAgICAgICAgaWYgKG5zLmluY2x1ZGVzKFwibm9QcmV2ZW50RGVmYXVsdFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFdmVudExpc3RlbmVyKFwidG91Y2htb3ZlXCIsIGhhbmRsZSwge3Bhc3NpdmU6IGZhbHNlfSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaG1vdmVcIiwgaGFuZGxlLCB7cGFzc2l2ZTogdHJ1ZX0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICB9O1xuICAgICAgICBqUXVlcnkuZXZlbnQuc3BlY2lhbC53aGVlbCA9IHtcbiAgICAgICAgICAgIHNldHVwOiBmdW5jdGlvbihfLCBucywgaGFuZGxlKSB7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0aGlzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgICAgICAgICAgICAgIGlmIChucy5pbmNsdWRlcyhcIm5vUHJldmVudERlZmF1bHRcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXZlbnRMaXN0ZW5lcihcIndoZWVsXCIsIGhhbmRsZSwge3Bhc3NpdmU6IGZhbHNlfSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEV2ZW50TGlzdGVuZXIoXCJ3aGVlbFwiLCBoYW5kbGUsIHtwYXNzaXZlOiB0cnVlfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgIH07XG4gICAgfTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBNYWluIGNsYXNzIGluaXRpYWxpemVyXG4gICAgICAgICAqL1xuICAgICAgICBpbml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1BcHAuaW5pdENvbXBvbmVudHMoKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogSW5pdGlhbGl6ZXMgY29tcG9uZW50c1xuICAgICAgICAgKi9cbiAgICAgICAgaW5pdENvbXBvbmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaGlkZVRvdWNoV2FybmluZygpO1xuICAgICAgICAgICAgaW5pdFNjcm9sbGFibGVzKCk7XG4gICAgICAgICAgICBpbml0VG9vbHRpcHMoKTtcbiAgICAgICAgICAgIGluaXRQb3BvdmVycygpO1xuICAgICAgICAgICAgaW5pdEFsZXJ0cygpO1xuICAgICAgICAgICAgaW5pdFBvcnRsZXRzKCk7XG4gICAgICAgICAgICBpbml0RmlsZUlucHV0KCk7XG4gICAgICAgICAgICBpbml0QWNjb3JkaW9ucygpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gZWwgalF1ZXJ5IGVsZW1lbnQgb2JqZWN0XG4gICAgICAgICAqL1xuICAgICAgICAvLyB3ckphbmdvZXIgZnVuY3Rpb24gdG8gc2Nyb2xsKGZvY3VzKSB0byBhbiBlbGVtZW50XG4gICAgICAgIGluaXRUb29sdGlwczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpbml0VG9vbHRpcHMoKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IGVsIGpRdWVyeSBlbGVtZW50IG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgLy8gd3JKYW5nb2VyIGZ1bmN0aW9uIHRvIHNjcm9sbChmb2N1cykgdG8gYW4gZWxlbWVudFxuICAgICAgICBpbml0VG9vbHRpcDogZnVuY3Rpb24oZWwpIHtcbiAgICAgICAgICAgIGluaXRUb29sdGlwKGVsKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IGVsIGpRdWVyeSBlbGVtZW50IG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgLy8gd3JKYW5nb2VyIGZ1bmN0aW9uIHRvIHNjcm9sbChmb2N1cykgdG8gYW4gZWxlbWVudFxuICAgICAgICBpbml0UG9wb3ZlcnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaW5pdFBvcG92ZXJzKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBlbCBqUXVlcnkgZWxlbWVudCBvYmplY3RcbiAgICAgICAgICovXG4gICAgICAgIC8vIHdySmFuZ29lciBmdW5jdGlvbiB0byBzY3JvbGwoZm9jdXMpIHRvIGFuIGVsZW1lbnRcbiAgICAgICAgaW5pdFBvcG92ZXI6IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgICAgICBpbml0UG9wb3ZlcihlbCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBlbCBqUXVlcnkgZWxlbWVudCBvYmplY3RcbiAgICAgICAgICovXG4gICAgICAgIC8vIGZ1bmN0aW9uIHRvIGluaXQgcG9ydGxldFxuICAgICAgICBpbml0UG9ydGxldDogZnVuY3Rpb24oZWwsIG9wdGlvbnMpIHtcbiAgICAgICAgICAgIGluaXRQb3J0bGV0KGVsLCBvcHRpb25zKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IGVsIGpRdWVyeSBlbGVtZW50IG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgLy8gZnVuY3Rpb24gdG8gaW5pdCBwb3J0bGV0c1xuICAgICAgICBpbml0UG9ydGxldHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaW5pdFBvcnRsZXRzKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNjcm9sbHMgdG8gYW4gZWxlbWVudCB3aXRoIGFuaW1hdGlvblxuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gZWwgalF1ZXJ5IGVsZW1lbnQgb2JqZWN0XG4gICAgICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvZmZzZXQgT2Zmc2V0IHRvIGVsZW1lbnQgc2Nyb2xsIHBvc2l0aW9uXG4gICAgICAgICAqL1xuICAgICAgICBzY3JvbGxUbzogZnVuY3Rpb24oZWwsIG9mZnNldCkge1xuICAgICAgICAgICAgdmFyIHBvcyA9IChlbCAmJiBlbC5sZW5ndGggPiAwKSA/IGVsLm9mZnNldCgpLnRvcCA6IDA7XG4gICAgICAgICAgICBwb3MgPSBwb3MgKyAob2Zmc2V0ID8gb2Zmc2V0IDogMCk7XG5cbiAgICAgICAgICAgIGpRdWVyeShcImh0bWwsYm9keVwiKS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IHBvcyxcbiAgICAgICAgICAgIH0sIFwic2xvd1wiKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2Nyb2xscyB1bnRpbCBlbGVtZW50IGlzIGNlbnRlcmVkIGluIHRoZSB2aWV3cG9ydFxuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gZWwgalF1ZXJ5IGVsZW1lbnQgb2JqZWN0XG4gICAgICAgICAqL1xuICAgICAgICAvLyB3ckphbmdvZXIgZnVuY3Rpb24gdG8gc2Nyb2xsKGZvY3VzKSB0byBhbiBlbGVtZW50XG4gICAgICAgIHNjcm9sbFRvVmlld3BvcnQ6IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgICAgICB2YXIgZWxPZmZzZXQgPSBlbC5vZmZzZXQoKS50b3A7XG4gICAgICAgICAgICB2YXIgZWxIZWlnaHQgPSBlbC5oZWlnaHQoKTtcbiAgICAgICAgICAgIHZhciB3aW5kb3dIZWlnaHQgPSBtVXRpbC5nZXRWaWV3UG9ydCgpLmhlaWdodDtcbiAgICAgICAgICAgIHZhciBvZmZzZXQgPSBlbE9mZnNldCAtICgod2luZG93SGVpZ2h0IC8gMikgLSAoZWxIZWlnaHQgLyAyKSk7XG5cbiAgICAgICAgICAgIGpRdWVyeShcImh0bWwsYm9keVwiKS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IG9mZnNldCxcbiAgICAgICAgICAgIH0sIFwic2xvd1wiKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2Nyb2xscyB0byB0aGUgdG9wIG9mIHRoZSBwYWdlXG4gICAgICAgICAqL1xuICAgICAgICAvLyBmdW5jdGlvbiB0byBzY3JvbGwgdG8gdGhlIHRvcFxuICAgICAgICBzY3JvbGxUb3A6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbUFwcC5zY3JvbGxUbygpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBJbml0aWFsaXplcyBzY3JvbGxhYmxlIGNvbnRlbnQgdXNpbmcgbUN1c3RvbVNjcm9sbGJhciBwbHVnaW5cbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IGVsIGpRdWVyeSBlbGVtZW50IG9iamVjdFxuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gb3B0aW9ucyBtQ3VzdG9tU2Nyb2xsYmFyIHBsdWdpbiBvcHRpb25zKHJlZmVyOiBodHRwOi8vbWFub3MubWFsaWh1LmdyL2pxdWVyeS1jdXN0b20tY29udGVudC1zY3JvbGxlci8pXG4gICAgICAgICAqL1xuICAgICAgICBpbml0U2Nyb2xsZXI6IGZ1bmN0aW9uKGVsLCBvcHRpb25zKSB7XG4gICAgICAgICAgICBpZiAobVV0aWwuaXNNb2JpbGVEZXZpY2UoKSkge1xuICAgICAgICAgICAgICAgIGVsLmNzcyhcIm92ZXJmbG93XCIsIFwiYXV0b1wiKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgLy9lbC5tQ3VzdG9tU2Nyb2xsYmFyKFwiZGVzdHJveVwiKTtcbiAgICAgICAgICAgICAgICBlbC5tQ3VzdG9tU2Nyb2xsYmFyKHtcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsSW5lcnRpYTogMCxcbiAgICAgICAgICAgICAgICAgICAgYXV0b0RyYWdnZXJMZW5ndGg6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIGF1dG9IaWRlU2Nyb2xsYmFyOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBhdXRvRXhwYW5kU2Nyb2xsYmFyOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgYWx3YXlzU2hvd1Njcm9sbGJhcjogMCxcbiAgICAgICAgICAgICAgICAgICAgYXhpczogZWwuZGF0YShcImF4aXNcIikgPyBlbC5kYXRhKFwiYXhpc1wiKSA6IFwieVwiLFxuICAgICAgICAgICAgICAgICAgICBtb3VzZVdoZWVsOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY3JvbGxBbW91bnQ6IDEyMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZXZlbnREZWZhdWx0OiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBzZXRIZWlnaHQ6IChvcHRpb25zLmhlaWdodCA/IG9wdGlvbnMuaGVpZ2h0IDogXCJcIiksXG4gICAgICAgICAgICAgICAgICAgIHRoZW1lOiBcIm1pbmltYWwtZGFya1wiLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBEZXN0cm95cyBzY3JvbGxhYmxlIGNvbnRlbnQncyBtQ3VzdG9tU2Nyb2xsYmFyIHBsdWdpbiBpbnN0YW5jZVxuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gZWwgalF1ZXJ5IGVsZW1lbnQgb2JqZWN0XG4gICAgICAgICAqL1xuICAgICAgICBkZXN0cm95U2Nyb2xsZXI6IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgICAgICBlbC5tQ3VzdG9tU2Nyb2xsYmFyKFwiZGVzdHJveVwiKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2hvd3MgYm9vdHN0cmFwIGFsZXJ0XG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zXG4gICAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd9IElEIGF0dHJpYnV0ZSBvZiB0aGUgY3JlYXRlZCBhbGVydFxuICAgICAgICAgKi9cbiAgICAgICAgYWxlcnQ6IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgICAgIG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7XG4gICAgICAgICAgICAgICAgY29udGFpbmVyOiBcIlwiLCAvLyBhbGVydHMgcGFyZW50IGNvbnRhaW5lcihieSBkZWZhdWx0IHBsYWNlZCBhZnRlciB0aGUgcGFnZSBicmVhZGNydW1icylcbiAgICAgICAgICAgICAgICBwbGFjZTogXCJhcHBlbmRcIiwgLy8gXCJhcHBlbmRcIiBvciBcInByZXBlbmRcIiBpbiBjb250YWluZXIgXG4gICAgICAgICAgICAgICAgdHlwZTogXCJzdWNjZXNzXCIsIC8vIGFsZXJ0J3MgdHlwZVxuICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwiXCIsIC8vIGFsZXJ0J3MgbWVzc2FnZVxuICAgICAgICAgICAgICAgIGNsb3NlOiB0cnVlLCAvLyBtYWtlIGFsZXJ0IGNsb3NhYmxlXG4gICAgICAgICAgICAgICAgcmVzZXQ6IHRydWUsIC8vIGNsb3NlIGFsbCBwcmV2aW91c2UgYWxlcnRzIGZpcnN0XG4gICAgICAgICAgICAgICAgZm9jdXM6IHRydWUsIC8vIGF1dG8gc2Nyb2xsIHRvIHRoZSBhbGVydCBhZnRlciBzaG93blxuICAgICAgICAgICAgICAgIGNsb3NlSW5TZWNvbmRzOiAwLCAvLyBhdXRvIGNsb3NlIGFmdGVyIGRlZmluZWQgc2Vjb25kc1xuICAgICAgICAgICAgICAgIGljb246IFwiXCIsIC8vIHB1dCBpY29uIGJlZm9yZSB0aGUgbWVzc2FnZVxuICAgICAgICAgICAgfSwgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgIHZhciBpZCA9IG1VdGlsLmdldFVuaXF1ZUlEKFwiQXBwX2FsZXJ0XCIpO1xuXG4gICAgICAgICAgICB2YXIgaHRtbCA9IFwiPGRpdiBpZD1cXFwiXCIgKyBpZCArIFwiXFxcIiBjbGFzcz1cXFwiY3VzdG9tLWFsZXJ0cyBhbGVydCBhbGVydC1cIiArIG9wdGlvbnMudHlwZSArIFwiIGZhZGUgaW5cXFwiPlwiICsgKG9wdGlvbnMuY2xvc2UgPyBcIjxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiY2xvc2VcXFwiIGRhdGEtZGlzbWlzcz1cXFwiYWxlcnRcXFwiIGFyaWEtaGlkZGVuPVxcXCJ0cnVlXFxcIj48L2J1dHRvbj5cIiA6IFwiXCIpICsgKG9wdGlvbnMuaWNvbiAhPT0gXCJcIiA/IFwiPGkgY2xhc3M9XFxcImZhLWxnIGZhIGZhLVwiICsgb3B0aW9ucy5pY29uICsgXCJcXFwiPjwvaT4gIFwiIDogXCJcIikgKyBvcHRpb25zLm1lc3NhZ2UgKyBcIjwvZGl2PlwiO1xuXG4gICAgICAgICAgICBpZiAob3B0aW9ucy5yZXNldCkge1xuICAgICAgICAgICAgICAgICQoXCIuY3VzdG9tLWFsZXJ0c1wiKS5yZW1vdmUoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFvcHRpb25zLmNvbnRhaW5lcikge1xuICAgICAgICAgICAgICAgIGlmICgkKFwiLnBhZ2UtZml4ZWQtbWFpbi1jb250ZW50XCIpLnNpemUoKSA9PT0gMSkge1xuICAgICAgICAgICAgICAgICAgICAkKFwiLnBhZ2UtZml4ZWQtbWFpbi1jb250ZW50XCIpLnByZXBlbmQoaHRtbCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICgoJChcImJvZHlcIikuaGFzQ2xhc3MoXCJwYWdlLWNvbnRhaW5lci1iZy1zb2xpZFwiKSB8fCAkKFwiYm9keVwiKS5oYXNDbGFzcyhcInBhZ2UtY29udGVudC13aGl0ZVwiKSkgJiYgJChcIi5wYWdlLWhlYWRcIikuc2l6ZSgpID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICQoXCIucGFnZS10aXRsZVwiKS5hZnRlcihodG1sKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoJChcIi5wYWdlLWJhclwiKS5zaXplKCkgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiLnBhZ2UtYmFyXCIpLmFmdGVyKGh0bWwpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgJChcIi5wYWdlLWJyZWFkY3J1bWIsIC5icmVhZGNydW1ic1wiKS5hZnRlcihodG1sKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMucGxhY2UgPT0gXCJhcHBlbmRcIikge1xuICAgICAgICAgICAgICAgICAgICAkKG9wdGlvbnMuY29udGFpbmVyKS5hcHBlbmQoaHRtbCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJChvcHRpb25zLmNvbnRhaW5lcikucHJlcGVuZChodG1sKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChvcHRpb25zLmZvY3VzKSB7XG4gICAgICAgICAgICAgICAgbUFwcC5zY3JvbGxUbygkKFwiI1wiICsgaWQpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKG9wdGlvbnMuY2xvc2VJblNlY29uZHMgPiAwKSB7XG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgJChcIiNcIiArIGlkKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICB9LCBvcHRpb25zLmNsb3NlSW5TZWNvbmRzICogMTAwMCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBpZDtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQmxvY2tzIGVsZW1lbnQgd2l0aCBsb2FkaW5nIGluZGljaWF0b3IgdXNpbmcgaHR0cDovL21hbHN1cC5jb20vanF1ZXJ5L2Jsb2NrL1xuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gdGFyZ2V0IGpRdWVyeSBlbGVtZW50IG9iamVjdFxuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gb3B0aW9uc1xuICAgICAgICAgKi9cbiAgICAgICAgYmxvY2s6IGZ1bmN0aW9uKHRhcmdldCwgb3B0aW9ucykge1xuICAgICAgICAgICAgdmFyIGVsID0gJCh0YXJnZXQpO1xuXG4gICAgICAgICAgICBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge1xuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAuMDMsXG4gICAgICAgICAgICAgICAgb3ZlcmxheUNvbG9yOiBcIiMwMDAwMDBcIixcbiAgICAgICAgICAgICAgICBzdGF0ZTogXCJicmFuZFwiLFxuICAgICAgICAgICAgICAgIHR5cGU6IFwibG9hZGVyXCIsXG4gICAgICAgICAgICAgICAgc2l6ZTogXCJsZ1wiLFxuICAgICAgICAgICAgICAgIGNlbnRlclg6IHRydWUsXG4gICAgICAgICAgICAgICAgY2VudGVyWTogdHJ1ZSxcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIlwiLFxuICAgICAgICAgICAgICAgIHNoYWRvdzogdHJ1ZSxcbiAgICAgICAgICAgICAgICB3aWR0aDogXCJhdXRvXCIsXG4gICAgICAgICAgICB9LCBvcHRpb25zKTtcblxuICAgICAgICAgICAgdmFyIHNraW47XG4gICAgICAgICAgICB2YXIgc3RhdGU7XG4gICAgICAgICAgICB2YXIgbG9hZGluZztcblxuICAgICAgICAgICAgaWYgKG9wdGlvbnMudHlwZSA9PSBcInNwaW5uZXJcIikge1xuICAgICAgICAgICAgICAgIHNraW4gPSBvcHRpb25zLnNraW4gPyBcIm0tc3Bpbm5lci0tc2tpbi1cIiArIG9wdGlvbnMuc2tpbiA6IFwiXCI7XG4gICAgICAgICAgICAgICAgc3RhdGUgPSBvcHRpb25zLnN0YXRlID8gXCJtLXNwaW5uZXItLVwiICsgb3B0aW9ucy5zdGF0ZSA6IFwiXCI7XG4gICAgICAgICAgICAgICAgbG9hZGluZyA9IFwiPGRpdiBjbGFzcz1cXFwibS1zcGlubmVyIFwiICsgc2tpbiArIFwiIFwiICsgc3RhdGUgKyBcIlxcXCI+PC9kaXZcIjtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgc2tpbiA9IG9wdGlvbnMuc2tpbiA/IFwibS1sb2FkZXItLXNraW4tXCIgKyBvcHRpb25zLnNraW4gOiBcIlwiO1xuICAgICAgICAgICAgICAgIHN0YXRlID0gb3B0aW9ucy5zdGF0ZSA/IFwibS1sb2FkZXItLVwiICsgb3B0aW9ucy5zdGF0ZSA6IFwiXCI7XG4gICAgICAgICAgICAgICAgc2l6ZSA9IG9wdGlvbnMuc2l6ZSA/IFwibS1sb2FkZXItLVwiICsgb3B0aW9ucy5zaXplIDogXCJcIjtcbiAgICAgICAgICAgICAgICBsb2FkaW5nID0gXCI8ZGl2IGNsYXNzPVxcXCJtLWxvYWRlciBcIiArIHNraW4gKyBcIiBcIiArIHN0YXRlICsgXCIgXCIgKyBzaXplICsgXCJcXFwiPjwvZGl2XCI7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChvcHRpb25zLm1lc3NhZ2UgJiYgb3B0aW9ucy5tZXNzYWdlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICB2YXIgY2xhc3NlcyA9IFwibS1ibG9ja3VpIFwiICsgKG9wdGlvbnMuc2hhZG93ID09PSBmYWxzZSA/IFwibS1ibG9ja3VpLW5vLXNoYWRvd1wiIDogXCJcIik7XG5cbiAgICAgICAgICAgICAgICBodG1sID0gXCI8ZGl2IGNsYXNzPVxcXCJcIiArIGNsYXNzZXMgKyBcIlxcXCI+PHNwYW4+XCIgKyBvcHRpb25zLm1lc3NhZ2UgKyBcIjwvc3Bhbj48c3Bhbj5cIiArIGxvYWRpbmcgKyBcIjwvc3Bhbj48L2Rpdj5cIjtcbiAgICAgICAgICAgICAgICBvcHRpb25zLndpZHRoID0gbVV0aWwucmVhbFdpZHRoKGh0bWwpICsgMTA7XG4gICAgICAgICAgICAgICAgaWYgKHRhcmdldCA9PSBcImJvZHlcIikge1xuICAgICAgICAgICAgICAgICAgICBodG1sID0gXCI8ZGl2IGNsYXNzPVxcXCJcIiArIGNsYXNzZXMgKyBcIlxcXCIgc3R5bGU9XFxcIm1hcmdpbi1sZWZ0Oi1cIiArIChvcHRpb25zLndpZHRoIC8gMikgKyBcInB4O1xcXCI+PHNwYW4+XCIgKyBvcHRpb25zLm1lc3NhZ2UgKyBcIjwvc3Bhbj48c3Bhbj5cIiArIGxvYWRpbmcgKyBcIjwvc3Bhbj48L2Rpdj5cIjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGh0bWwgPSBsb2FkaW5nO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGh0bWwsXG4gICAgICAgICAgICAgICAgY2VudGVyWTogb3B0aW9ucy5jZW50ZXJZLFxuICAgICAgICAgICAgICAgIGNlbnRlclg6IG9wdGlvbnMuY2VudGVyWCxcbiAgICAgICAgICAgICAgICBjc3M6IHtcbiAgICAgICAgICAgICAgICAgICAgdG9wOiBcIjMwJVwiLFxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiBcIjUwJVwiLFxuICAgICAgICAgICAgICAgICAgICBib3JkZXI6IFwiMFwiLFxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiBcIjBcIixcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcIm5vbmVcIixcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IG9wdGlvbnMud2lkdGgsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvdmVybGF5Q1NTOiB7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogb3B0aW9ucy5vdmVybGF5Q29sb3IsXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IG9wdGlvbnMub3BhY2l0eSxcbiAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBcIndhaXRcIixcbiAgICAgICAgICAgICAgICAgICAgekluZGV4OiBcIjEwXCIsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvblVuYmxvY2s6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsLmNzcyhcInBvc2l0aW9uXCIsIFwiXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZWwuY3NzKFwiem9vbVwiLCBcIlwiKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpZiAodGFyZ2V0ID09IFwiYm9keVwiKSB7XG4gICAgICAgICAgICAgICAgcGFyYW1zLmNzcy50b3AgPSBcIjUwJVwiO1xuICAgICAgICAgICAgICAgICQuYmxvY2tVSShwYXJhbXMpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB2YXIgZWwgPSAkKHRhcmdldCk7XG4gICAgICAgICAgICAgICAgZWwuYmxvY2socGFyYW1zKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogVW4tYmxvY2tzIHRoZSBibG9ja2VkIGVsZW1lbnRcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IHRhcmdldCBqUXVlcnkgZWxlbWVudCBvYmplY3RcbiAgICAgICAgICovXG4gICAgICAgIHVuYmxvY2s6IGZ1bmN0aW9uKHRhcmdldCkge1xuICAgICAgICAgICAgaWYgKHRhcmdldCAmJiB0YXJnZXQgIT0gXCJib2R5XCIpIHtcbiAgICAgICAgICAgICAgICAkKHRhcmdldCkudW5ibG9jaygpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkLnVuYmxvY2tVSSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBCbG9ja3MgdGhlIHBhZ2UgYm9keSBlbGVtZW50IHdpdGggbG9hZGluZyBpbmRpY2F0b3JcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IG9wdGlvbnNcbiAgICAgICAgICovXG4gICAgICAgIGJsb2NrUGFnZTogZnVuY3Rpb24ob3B0aW9ucykge1xuICAgICAgICAgICAgcmV0dXJuIG1BcHAuYmxvY2soXCJib2R5XCIsIG9wdGlvbnMpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBVbi1ibG9ja3MgdGhlIGJsb2NrZWQgcGFnZSBib2R5IGVsZW1lbnRcbiAgICAgICAgICovXG4gICAgICAgIHVuYmxvY2tQYWdlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBtQXBwLnVuYmxvY2soXCJib2R5XCIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBFbmFibGUgbG9hZGVyIHByb2dyZXNzIGZvciBidXR0b24gYW5kIG90aGVyIGVsZW1lbnRzXG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSB0YXJnZXQgalF1ZXJ5IGVsZW1lbnQgb2JqZWN0XG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zXG4gICAgICAgICAqL1xuICAgICAgICBwcm9ncmVzczogZnVuY3Rpb24odGFyZ2V0LCBvcHRpb25zKSB7XG4gICAgICAgICAgICB2YXIgc2tpbiA9IChvcHRpb25zICYmIG9wdGlvbnMuc2tpbikgPyBvcHRpb25zLnNraW4gOiBcImxpZ2h0XCI7XG4gICAgICAgICAgICB2YXIgYWxpZ25tZW50ID0gKG9wdGlvbnMgJiYgb3B0aW9ucy5hbGlnbm1lbnQpID8gb3B0aW9ucy5hbGlnbm1lbnQgOiBcInJpZ2h0XCI7XG4gICAgICAgICAgICB2YXIgc2l6ZSA9IChvcHRpb25zICYmIG9wdGlvbnMuc2l6ZSkgPyBcIm0tc3Bpbm5lci0tXCIgKyBvcHRpb25zLnNpemUgOiBcIlwiO1xuICAgICAgICAgICAgdmFyIGNsYXNzZXMgPSBcIm0tbG9hZGVyIFwiICsgXCJtLWxvYWRlci0tXCIgKyBza2luICsgXCIgbS1sb2FkZXItLVwiICsgYWxpZ25tZW50ICsgXCIgbS1sb2FkZXItLVwiICsgc2l6ZTtcblxuICAgICAgICAgICAgbUFwcC51bnByb2dyZXNzKHRhcmdldCk7XG5cbiAgICAgICAgICAgICQodGFyZ2V0KS5hZGRDbGFzcyhjbGFzc2VzKTtcbiAgICAgICAgICAgICQodGFyZ2V0KS5kYXRhKFwicHJvZ3Jlc3MtY2xhc3Nlc1wiLCBjbGFzc2VzKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogRGlzYWJsZSBsb2FkZXIgcHJvZ3Jlc3MgZm9yIGJ1dHRvbiBhbmQgb3RoZXIgZWxlbWVudHNcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IHRhcmdldCBqUXVlcnkgZWxlbWVudCBvYmplY3RcbiAgICAgICAgICovXG4gICAgICAgIHVucHJvZ3Jlc3M6IGZ1bmN0aW9uKHRhcmdldCkge1xuICAgICAgICAgICAgJCh0YXJnZXQpLnJlbW92ZUNsYXNzKCQodGFyZ2V0KS5kYXRhKFwicHJvZ3Jlc3MtY2xhc3Nlc1wiKSk7XG4gICAgICAgIH0sXG4gICAgfTtcbn0oKTtcblxuLy89PSBJbml0aWFsaXplIG1BcHAgY2xhc3Mgb24gZG9jdW1lbnQgcmVhZHlcbi8vICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuLy8gICAgIG1BcHAuaW5pdCgpO1xuLy8gfSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2Jhc2UvYXBwLmpzIiwiLyoqXG4gKiBAY2xhc3MgbVV0aWwgIE1ldHJvbmljIGJhc2UgdXRpbGl6ZSBjbGFzcyB0aGF0IHByaXZpZGVzIGhlbHBlciBmdW5jdGlvbnNcbiAqL1xuXG5leHBvcnQgY29uc3QgbVV0aWwgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgcmVzaXplSGFuZGxlcnMgPSBbXTtcblxuICAgIC8qKiBAdHlwZSB7b2JqZWN0fSBicmVha3BvaW50cyBUaGUgZGV2aWNlIHdpZHRoIGJyZWFrcG9pbnRzICoqL1xuICAgIHZhciBicmVha3BvaW50cyA9IHsgICAgICAgIFxuICAgICAgICBzbTogNTQ0LCAvLyBTbWFsbCBzY3JlZW4gLyBwaG9uZSAgICAgICAgICAgXG4gICAgICAgIG1kOiA3NjgsIC8vIE1lZGl1bSBzY3JlZW4gLyB0YWJsZXQgICAgICAgICAgICBcbiAgICAgICAgbGc6IDk5MiwgLy8gTGFyZ2Ugc2NyZWVuIC8gZGVza3RvcCAgICAgICAgXG4gICAgICAgIHhsOiAxMjAwIC8vIEV4dHJhIGxhcmdlIHNjcmVlbiAvIHdpZGUgZGVza3RvcFxuICAgIH07XG5cbiAgICAvKiogQHR5cGUge29iamVjdH0gY29sb3JzIFN0YXRlIGNvbG9ycyAqKi9cbiAgICB2YXIgY29sb3JzID0ge1xuICAgICAgICBicmFuZDogICAgICAnIzcxNmFjYScsXG4gICAgICAgIG1ldGFsOiAgICAgICcjYzRjNWQ2JyxcbiAgICAgICAgbGlnaHQ6ICAgICAgJyNmZmZmZmYnLFxuICAgICAgICBhY2NlbnQ6ICAgICAnIzAwYzVkYycsXG4gICAgICAgIHByaW1hcnk6ICAgICcjNTg2N2RkJyxcbiAgICAgICAgc3VjY2VzczogICAgJyMzNGJmYTMnLFxuICAgICAgICBpbmZvOiAgICAgICAnIzM2YTNmNycsXG4gICAgICAgIHdhcm5pbmc6ICAgICcjZmZiODIyJyxcbiAgICAgICAgZGFuZ2VyOiAgICAgJyNmNDUxNmMnXG4gICAgfTtcblxuICAgIC8qKlxuICAgICogSGFuZGxlIHdpbmRvdyByZXNpemUgZXZlbnQgd2l0aCBzb21lIFxuICAgICogZGVsYXkgdG8gYXR0YWNoIGV2ZW50IGhhbmRsZXJzIHVwb24gcmVzaXplIGNvbXBsZXRlIFxuICAgICovXG4gICAgdmFyIF93aW5kb3dSZXNpemVIYW5kbGVyID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciByZXNpemU7XG4gICAgICAgIHZhciBfcnVuUmVzaXplSGFuZGxlcnMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIC8vIHJlaW5pdGlhbGl6ZSBvdGhlciBzdWJzY3JpYmVkIGVsZW1lbnRzXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHJlc2l6ZUhhbmRsZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdmFyIGVhY2ggPSByZXNpemVIYW5kbGVyc1tpXTtcbiAgICAgICAgICAgICAgICBlYWNoLmNhbGwoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBqUXVlcnkod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiAocmVzaXplKSB7XG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHJlc2l6ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXNpemUgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIF9ydW5SZXNpemVIYW5kbGVycygpO1xuICAgICAgICAgICAgfSwgMjUwKTsgLy8gd2FpdCA1MG1zIHVudGlsIHdpbmRvdyByZXNpemUgZmluaXNoZXMuXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICByZXR1cm4ge1xuICAgICAgICAvKipcbiAgICAgICAgKiBDbGFzcyBtYWluIGluaXRpYWxpemVyLlxuICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zLlxuICAgICAgICAqIEByZXR1cm5zIG51bGxcbiAgICAgICAgKi9cbiAgICAgICAgLy9tYWluIGZ1bmN0aW9uIHRvIGluaXRpYXRlIHRoZSB0aGVtZVxuICAgICAgICBpbml0OiBmdW5jdGlvbihvcHRpb25zKSB7XG4gICAgICAgICAgICBpZiAob3B0aW9ucyAmJiBvcHRpb25zLmJyZWFrcG9pbnRzKSB7XG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludHMgPSBvcHRpb25zLmJyZWFrcG9pbnRzO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAob3B0aW9ucyAmJiBvcHRpb25zLmNvbG9ycykge1xuICAgICAgICAgICAgICAgIGNvbG9ycyA9IG9wdGlvbnMuY29sb3JzO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBfd2luZG93UmVzaXplSGFuZGxlcigpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAqIEFkZHMgd2luZG93IHJlc2l6ZSBldmVudCBoYW5kbGVyLlxuICAgICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IGNhbGxiYWNrIGZ1bmN0aW9uLlxuICAgICAgICAqL1xuICAgICAgICBhZGRSZXNpemVIYW5kbGVyOiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgICAgcmVzaXplSGFuZGxlcnMucHVzaChjYWxsYmFjayk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogVHJpZ2dlciB3aW5kb3cgcmVzaXplIGhhbmRsZXJzLlxuICAgICAgICAqL1xuICAgICAgICBydW5SZXNpemVIYW5kbGVyczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBfcnVuUmVzaXplSGFuZGxlcnMoKTtcbiAgICAgICAgfSwgICAgICAgIFxuXG4gICAgICAgIC8qKlxuICAgICAgICAqIEdldCBHRVQgcGFyYW1ldGVyIHZhbHVlIGZyb20gVVJMLlxuICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwYXJhbU5hbWUgUGFyYW1ldGVyIG5hbWUuXG4gICAgICAgICogQHJldHVybnMge3N0cmluZ30gIFxuICAgICAgICAqL1xuICAgICAgICBnZXRVUkxQYXJhbTogZnVuY3Rpb24ocGFyYW1OYW1lKSB7XG4gICAgICAgICAgICB2YXIgc2VhcmNoU3RyaW5nID0gd2luZG93LmxvY2F0aW9uLnNlYXJjaC5zdWJzdHJpbmcoMSksXG4gICAgICAgICAgICAgICAgaSwgdmFsLCBwYXJhbXMgPSBzZWFyY2hTdHJpbmcuc3BsaXQoXCImXCIpO1xuXG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgcGFyYW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdmFsID0gcGFyYW1zW2ldLnNwbGl0KFwiPVwiKTtcbiAgICAgICAgICAgICAgICBpZiAodmFsWzBdID09IHBhcmFtTmFtZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdW5lc2NhcGUodmFsWzFdKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAqIENoZWNrcyB3aGV0aGVyIGN1cnJlbnQgZGV2aWNlIGlzIG1vYmlsZSB0b3VjaC5cbiAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gIFxuICAgICAgICAqL1xuICAgICAgICBpc01vYmlsZURldmljZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKHRoaXMuZ2V0Vmlld1BvcnQoKS53aWR0aCA8IHRoaXMuZ2V0QnJlYWtwb2ludCgnbGcnKSA/IHRydWUgOiBmYWxzZSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogQ2hlY2tzIHdoZXRoZXIgY3VycmVudCBkZXZpY2UgaXMgZGVza3RvcC5cbiAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gIFxuICAgICAgICAqL1xuICAgICAgICBpc0Rlc2t0b3BEZXZpY2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIG1VdGlsLmlzTW9iaWxlRGV2aWNlKCkgPyBmYWxzZSA6IHRydWU7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogR2V0cyBicm93c2VyIHdpbmRvdyB2aWV3cG9ydCBzaXplLiBSZWY6IGh0dHA6Ly9hbmR5bGFuZ3Rvbi5jby51ay9hcnRpY2xlcy9qYXZhc2NyaXB0L2dldC12aWV3cG9ydC1zaXplLWphdmFzY3JpcHQvXG4gICAgICAgICogQHJldHVybnMge29iamVjdH0gIFxuICAgICAgICAqL1xuICAgICAgICBnZXRWaWV3UG9ydDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgZSA9IHdpbmRvdyxcbiAgICAgICAgICAgICAgICBhID0gJ2lubmVyJztcbiAgICAgICAgICAgIGlmICghKCdpbm5lcldpZHRoJyBpbiB3aW5kb3cpKSB7XG4gICAgICAgICAgICAgICAgYSA9ICdjbGllbnQnO1xuICAgICAgICAgICAgICAgIGUgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQgfHwgZG9jdW1lbnQuYm9keTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogZVthICsgJ1dpZHRoJ10sXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBlW2EgKyAnSGVpZ2h0J11cbiAgICAgICAgICAgIH07XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogQ2hlY2tzIHdoZXRoZXIgZ2l2ZW4gZGV2aWNlIG1vZGUgaXMgY3VycmVudGx5IGFjdGl2YXRlZC5cbiAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gbW9kZSBSZXNwb25zaXZlIG1vZGUgbmFtZShlLmc6IGRlc2t0b3AsIGRlc2t0b3AtYW5kLXRhYmxldCwgdGFibGV0LCB0YWJsZXQtYW5kLW1vYmlsZSwgbW9iaWxlKVxuICAgICAgICAqIEByZXR1cm5zIHtib29sZWFufSAgXG4gICAgICAgICovXG4gICAgICAgIGlzSW5SZXNwb25zaXZlUmFuZ2U6IGZ1bmN0aW9uKG1vZGUpIHtcbiAgICAgICAgICAgIHZhciBicmVha3BvaW50ID0gdGhpcy5nZXRWaWV3UG9ydCgpLndpZHRoO1xuXG4gICAgICAgICAgICBpZiAobW9kZSA9PSAnZ2VuZXJhbCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobW9kZSA9PSAnZGVza3RvcCcgJiYgYnJlYWtwb2ludCA+PSAodGhpcy5nZXRCcmVha3BvaW50KCdsZycpICsgMSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobW9kZSA9PSAndGFibGV0JyAmJiAoYnJlYWtwb2ludCA+PSAodGhpcy5nZXRCcmVha3BvaW50KCdtZCcpICsgMSkgJiYgYnJlYWtwb2ludCA8IHRoaXMuZ2V0QnJlYWtwb2ludCgnbGcnKSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobW9kZSA9PSAnbW9iaWxlJyAmJiBicmVha3BvaW50IDw9IHRoaXMuZ2V0QnJlYWtwb2ludCgnbWQnKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChtb2RlID09ICdkZXNrdG9wLWFuZC10YWJsZXQnICYmIGJyZWFrcG9pbnQgPj0gKHRoaXMuZ2V0QnJlYWtwb2ludCgnbWQnKSArIDEpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKG1vZGUgPT0gJ3RhYmxldC1hbmQtbW9iaWxlJyAmJiBicmVha3BvaW50IDw9IHRoaXMuZ2V0QnJlYWtwb2ludCgnbGcnKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChtb2RlID09ICdtaW5pbWFsLWRlc2t0b3AtYW5kLWJlbG93JyAmJiBicmVha3BvaW50IDw9IHRoaXMuZ2V0QnJlYWtwb2ludCgneGwnKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogR2VuZXJhdGVzIHVuaXF1ZSBJRCBmb3IgZ2l2ZSBwcmVmaXguXG4gICAgICAgICogQHBhcmFtIHtzdHJpbmd9IHByZWZpeCBQcmVmaXggZm9yIGdlbmVyYXRlZCBJRFxuICAgICAgICAqIEByZXR1cm5zIHtib29sZWFufSAgXG4gICAgICAgICovXG4gICAgICAgIGdldFVuaXF1ZUlEOiBmdW5jdGlvbihwcmVmaXgpIHtcbiAgICAgICAgICAgIHJldHVybiBwcmVmaXggKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobmV3IERhdGUoKSkuZ2V0VGltZSgpKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgKiBHZXRzIHdpbmRvdyB3aWR0aCBmb3IgZ2l2ZSBicmVha3BvaW50IG1vZGUuXG4gICAgICAgICogQHBhcmFtIHtzdHJpbmd9IG1vZGUgUmVzcG9uc2l2ZSBtb2RlIG5hbWUoZS5nOiB4bCwgbGcsIG1kLCBzbSlcbiAgICAgICAgKiBAcmV0dXJucyB7bnVtYmVyfSAgXG4gICAgICAgICovXG4gICAgICAgIGdldEJyZWFrcG9pbnQ6IGZ1bmN0aW9uKG1vZGUpIHtcbiAgICAgICAgICAgIGlmICgkLmluQXJyYXkobW9kZSwgYnJlYWtwb2ludHMpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGJyZWFrcG9pbnRzW21vZGVdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAqIENoZWNrcyB3aGV0aGVyIG9iamVjdCBoYXMgcHJvcGVydHkgbWF0Y2hzIGdpdmVuIGtleSBwYXRoLlxuICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBvYmogT2JqZWN0IGNvbnRhaW5zIHZhbHVlcyBwYWlyZWQgd2l0aCBnaXZlbiBrZXkgcGF0aFxuICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBrZXlzIEtleXMgcGF0aCBzZXBlcmF0ZWQgd2l0aCBkb3RzXG4gICAgICAgICogQHJldHVybnMge29iamVjdH0gIFxuICAgICAgICAqL1xuICAgICAgICBpc3NldDogZnVuY3Rpb24ob2JqLCBrZXlzKSB7XG4gICAgICAgICAgICB2YXIgc3RvbmU7XG5cbiAgICAgICAgICAgIGtleXMgPSBrZXlzIHx8ICcnO1xuXG4gICAgICAgICAgICBpZiAoa2V5cy5pbmRleE9mKCdbJykgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdVbnN1cHBvcnRlZCBvYmplY3QgcGF0aCBub3RhdGlvbi4nKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAga2V5cyA9IGtleXMuc3BsaXQoJy4nKTtcblxuICAgICAgICAgICAgZG8ge1xuICAgICAgICAgICAgICAgIGlmIChvYmogPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgc3RvbmUgPSBrZXlzLnNoaWZ0KCk7XG5cbiAgICAgICAgICAgICAgICBpZiAoIW9iai5oYXNPd25Qcm9wZXJ0eShzdG9uZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIG9iaiA9IG9ialtzdG9uZV07XG5cbiAgICAgICAgICAgIH0gd2hpbGUgKGtleXMubGVuZ3RoKTtcblxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogR2V0cyBoaWdoZXN0IHotaW5kZXggb2YgdGhlIGdpdmVuIGVsZW1lbnQgcGFyZW50c1xuICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBlbCBqUXVlcnkgZWxlbWVudCBvYmplY3RcbiAgICAgICAgKiBAcmV0dXJucyB7bnVtYmVyfSAgXG4gICAgICAgICovXG4gICAgICAgIGdldEhpZ2hlc3RaaW5kZXg6IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgICAgICB2YXIgZWxlbSA9ICQoZWwpLFxuICAgICAgICAgICAgICAgIHBvc2l0aW9uLCB2YWx1ZTtcblxuICAgICAgICAgICAgd2hpbGUgKGVsZW0ubGVuZ3RoICYmIGVsZW1bMF0gIT09IGRvY3VtZW50KSB7XG4gICAgICAgICAgICAgICAgLy8gSWdub3JlIHotaW5kZXggaWYgcG9zaXRpb24gaXMgc2V0IHRvIGEgdmFsdWUgd2hlcmUgei1pbmRleCBpcyBpZ25vcmVkIGJ5IHRoZSBicm93c2VyXG4gICAgICAgICAgICAgICAgLy8gVGhpcyBtYWtlcyBiZWhhdmlvciBvZiB0aGlzIGZ1bmN0aW9uIGNvbnNpc3RlbnQgYWNyb3NzIGJyb3dzZXJzXG4gICAgICAgICAgICAgICAgLy8gV2ViS2l0IGFsd2F5cyByZXR1cm5zIGF1dG8gaWYgdGhlIGVsZW1lbnQgaXMgcG9zaXRpb25lZFxuICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gZWxlbS5jc3MoXCJwb3NpdGlvblwiKTtcblxuICAgICAgICAgICAgICAgIGlmIChwb3NpdGlvbiA9PT0gXCJhYnNvbHV0ZVwiIHx8IHBvc2l0aW9uID09PSBcInJlbGF0aXZlXCIgfHwgcG9zaXRpb24gPT09IFwiZml4ZWRcIikge1xuICAgICAgICAgICAgICAgICAgICAvLyBJRSByZXR1cm5zIDAgd2hlbiB6SW5kZXggaXMgbm90IHNwZWNpZmllZFxuICAgICAgICAgICAgICAgICAgICAvLyBvdGhlciBicm93c2VycyByZXR1cm4gYSBzdHJpbmdcbiAgICAgICAgICAgICAgICAgICAgLy8gd2UgaWdub3JlIHRoZSBjYXNlIG9mIG5lc3RlZCBlbGVtZW50cyB3aXRoIGFuIGV4cGxpY2l0IHZhbHVlIG9mIDBcbiAgICAgICAgICAgICAgICAgICAgLy8gPGRpdiBzdHlsZT1cInotaW5kZXg6IC0xMDtcIj48ZGl2IHN0eWxlPVwiei1pbmRleDogMDtcIj48L2Rpdj48L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUludChlbGVtLmNzcyhcInpJbmRleFwiKSwgMTApO1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWlzTmFOKHZhbHVlKSAmJiB2YWx1ZSAhPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsZW0gPSBlbGVtLnBhcmVudCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAqIENoZWNrcyB3aGV0aGVyIHRoZSBlbGVtZW50IGhhcyBnaXZlbiBjbGFzc2VzXG4gICAgICAgICogQHBhcmFtIHtvYmplY3R9IGVsIGpRdWVyeSBlbGVtZW50IG9iamVjdFxuICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBDbGFzc2VzIHN0cmluZ1xuICAgICAgICAqIEByZXR1cm5zIHtib29sZWFufSAgXG4gICAgICAgICovXG4gICAgICAgIGhhc0NsYXNzZXM6IGZ1bmN0aW9uKGVsLCBjbGFzc2VzKSB7XG4gICAgICAgICAgICB2YXIgY2xhc3Nlc0FyciA9IGNsYXNzZXMuc3BsaXQoXCIgXCIpO1xuXG4gICAgICAgICAgICBmb3IgKCB2YXIgaSA9IDA7IGkgPCBjbGFzc2VzQXJyLmxlbmd0aDsgaSsrICkge1xuICAgICAgICAgICAgICAgIGlmICggZWwuaGFzQ2xhc3MoIGNsYXNzZXNBcnJbaV0gKSA9PSBmYWxzZSApIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgXG5cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAqIEdldHMgZWxlbWVudCBhY3R1YWwvcmVhbCB3aWR0aFxuICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBlbCBqUXVlcnkgZWxlbWVudCBvYmplY3RcbiAgICAgICAgKiBAcmV0dXJucyB7bnVtYmVyfSAgXG4gICAgICAgICovXG4gICAgICAgIHJlYWxXaWR0aDogZnVuY3Rpb24oZWwpe1xuICAgICAgICAgICAgdmFyIGNsb25lID0gJChlbCkuY2xvbmUoKTtcbiAgICAgICAgICAgIGNsb25lLmNzcyhcInZpc2liaWxpdHlcIixcImhpZGRlblwiKTtcbiAgICAgICAgICAgIGNsb25lLmNzcygnb3ZlcmZsb3cnLCAnaGlkZGVuJyk7XG4gICAgICAgICAgICBjbG9uZS5jc3MoXCJoZWlnaHRcIixcIjBcIik7XG4gICAgICAgICAgICAkKCdib2R5JykuYXBwZW5kKGNsb25lKTtcbiAgICAgICAgICAgIHZhciB3aWR0aCA9IGNsb25lLm91dGVyV2lkdGgoKTtcbiAgICAgICAgICAgIGNsb25lLnJlbW92ZSgpO1xuXG4gICAgICAgICAgICByZXR1cm4gd2lkdGg7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogQ2hlY2tzIHdoZXRoZXIgdGhlIGVsZW1lbnQgaGFzIGFueSBwYXJlbnQgd2l0aCBmaXhlZCBwb3NpdGlvblxuICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBlbCBqUXVlcnkgZWxlbWVudCBvYmplY3RcbiAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gIFxuICAgICAgICAqL1xuICAgICAgICBoYXNGaXhlZFBvc2l0aW9uZWRQYXJlbnQ6IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gZmFsc2U7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGVsLnBhcmVudHMoKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5jc3MoJ3Bvc2l0aW9uJykgPT0gJ2ZpeGVkJykge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogU2ltdWxhdGVzIGRlbGF5XG4gICAgICAgICovXG4gICAgICAgIHNsZWVwOiBmdW5jdGlvbihtaWxsaXNlY29uZHMpIHtcbiAgICAgICAgICAgIHZhciBzdGFydCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAxZTc7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmICgobmV3IERhdGUoKS5nZXRUaW1lKCkgLSBzdGFydCkgPiBtaWxsaXNlY29uZHMpe1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogR2V0cyByYW5kb21seSBnZW5lcmF0ZWQgaW50ZWdlciB2YWx1ZSB3aXRoaW4gZ2l2ZW4gbWluIGFuZCBtYXggcmFuZ2VcbiAgICAgICAgKiBAcGFyYW0ge251bWJlcn0gbWluIFJhbmdlIHN0YXJ0IHZhbHVlXG4gICAgICAgICogQHBhcmFtIHtudW1iZXJ9IG1pbiBSYW5nZSBlbmQgdmFsdWVcbiAgICAgICAgKiBAcmV0dXJucyB7bnVtYmVyfSAgXG4gICAgICAgICovXG4gICAgICAgIGdldFJhbmRvbUludDogZnVuY3Rpb24obWluLCBtYXgpIHtcbiAgICAgICAgICAgIHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluICsgMSkpICsgbWluO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAqIEdldHMgc3RhdGUgY29sb3IncyBoZXggY29kZSBieSBjb2xvciBuYW1lXG4gICAgICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWUgQ29sb3IgbmFtZVxuICAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd9ICBcbiAgICAgICAgKi9cbiAgICAgICAgZ2V0Q29sb3I6IGZ1bmN0aW9uKG5hbWUpIHtcbiAgICAgICAgICAgIHJldHVybiBjb2xvcnNbbmFtZV07XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICogQ2hlY2tzIHdoZXRoZXIgQW5ndWxhciBsaWJyYXJ5IGlzIGluY2x1ZGVkXG4gICAgICAgICogQHJldHVybnMge2Jvb2xlYW59ICBcbiAgICAgICAgKi9cbiAgICAgICAgaXNBbmd1bGFyVmVyc2lvbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gd2luZG93LlpvbmUgIT09IHVuZGVmaW5lZCAgPyB0cnVlIDogZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG59KCk7XG5cbi8vPT0gSW5pdGlhbGl6ZSBtVXRpbCBjbGFzcyBvbiBkb2N1bWVudCByZWFkeVxuLy8gJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4vLyAgICAgbVV0aWwuaW5pdCgpO1xuLy8gfSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2Jhc2UvdXRpbC5qcyIsImltcG9ydCB7bVV0aWx9IGZyb20gXCIuLy4uLy4uL2Jhc2UvdXRpbFwiO1xuaW1wb3J0IHttQXBwfSAgZnJvbSBcIi4vLi4vLi4vYmFzZS9hcHBcIjtcblxuKGZ1bmN0aW9uKCQpIHtcbiAgICAvLyBQbHVnaW4gZnVuY3Rpb25cbiAgICAkLmZuLm1Ecm9wZG93biA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgLy8gUGx1Z2luIHNjb3BlIHZhcmlhYmxlXG4gICAgICAgIHZhciBkcm9wZG93biA9IHt9O1xuICAgICAgICB2YXIgZWxlbWVudCA9ICQodGhpcyk7XG5cbiAgICAgICAgLy8gUGx1Z2luIGNsYXNzXG4gICAgICAgIHZhciBQbHVnaW4gPSB7XG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJ1blxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBydW46IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICBpZiAoIWVsZW1lbnQuZGF0YShcImRyb3Bkb3duXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSBpbnN0YW5jZVxuICAgICAgICAgICAgICAgICAgICBQbHVnaW4uaW5pdChvcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmJ1aWxkKCk7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5zZXR1cCgpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGFzc2lnbiBpbnN0YW5jZSB0byB0aGUgZWxlbWVudCAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZGF0YShcImRyb3Bkb3duXCIsIGRyb3Bkb3duKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAvLyBnZXQgaW5zdGFuY2UgZnJvbSB0aGUgZWxlbWVudFxuICAgICAgICAgICAgICAgICAgICBkcm9wZG93biA9IGVsZW1lbnQuZGF0YShcImRyb3Bkb3duXCIpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiBkcm9wZG93bjtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSW5pdGlhbGl6ZVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpbml0OiBmdW5jdGlvbihvcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgZHJvcGRvd24uZXZlbnRzID0gW107XG4gICAgICAgICAgICAgICAgZHJvcGRvd24uZXZlbnRPbmUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBkcm9wZG93bi5jbG9zZSA9IGVsZW1lbnQuZmluZChcIi5tLWRyb3Bkb3duX19jbG9zZVwiKTtcbiAgICAgICAgICAgICAgICBkcm9wZG93bi50b2dnbGUgPSBlbGVtZW50LmZpbmQoXCIubS1kcm9wZG93bl9fdG9nZ2xlXCIpO1xuICAgICAgICAgICAgICAgIGRyb3Bkb3duLmFycm93ID0gZWxlbWVudC5maW5kKFwiLm0tZHJvcGRvd25fX2Fycm93XCIpO1xuICAgICAgICAgICAgICAgIGRyb3Bkb3duLndyYXBwZXIgPSBlbGVtZW50LmZpbmQoXCIubS1kcm9wZG93bl9fd3JhcHBlclwiKTtcbiAgICAgICAgICAgICAgICBkcm9wZG93bi5zY3JvbGxhYmxlID0gZWxlbWVudC5maW5kKFwiLm0tZHJvcGRvd25fX3Njcm9sbGFibGVcIik7XG4gICAgICAgICAgICAgICAgZHJvcGRvd24uZGVmYXVsdERyb3BQb3MgPSBlbGVtZW50Lmhhc0NsYXNzKFwibS1kcm9wZG93bi0tdXBcIikgPyBcInVwXCIgOiBcImRvd25cIjtcbiAgICAgICAgICAgICAgICBkcm9wZG93bi5jdXJyZW50RHJvcFBvcyA9IGRyb3Bkb3duLmRlZmF1bHREcm9wUG9zO1xuXG4gICAgICAgICAgICAgICAgZHJvcGRvd24ub3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLm1Ecm9wZG93bi5kZWZhdWx0cywgb3B0aW9ucyk7XG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuZGF0YShcImRyb3AtYXV0b1wiKSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi5vcHRpb25zLmRyb3BBdXRvID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGVsZW1lbnQuZGF0YShcImRyb3AtYXV0b1wiKSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24ub3B0aW9ucy5kcm9wQXV0byA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5zY3JvbGxhYmxlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRyb3Bkb3duLnNjcm9sbGFibGUuZGF0YShcIm1pbi1oZWlnaHRcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLm9wdGlvbnMubWluSGVpZ2h0ID0gZHJvcGRvd24uc2Nyb2xsYWJsZS5kYXRhKFwibWluLWhlaWdodFwiKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5zY3JvbGxhYmxlLmRhdGEoXCJtYXgtaGVpZ2h0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi5vcHRpb25zLm1heEhlaWdodCA9IGRyb3Bkb3duLnNjcm9sbGFibGUuZGF0YShcIm1heC1oZWlnaHRcIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEJ1aWxkIERPTSBhbmQgaW5pdCBldmVudCBoYW5kbGVyc1xuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBidWlsZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYgKG1VdGlsLmlzTW9iaWxlRGV2aWNlKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuZGF0YShcImRyb3Bkb3duLXRvZ2dsZVwiKSA9PSBcImhvdmVyXCIgfHwgZWxlbWVudC5kYXRhKFwiZHJvcGRvd24tdG9nZ2xlXCIpID09IFwiY2xpY2tcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24ub3B0aW9ucy50b2dnbGUgPSBcImNsaWNrXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi5vcHRpb25zLnRvZ2dsZSA9IFwiY2xpY2tcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLnRvZ2dsZS5jbGljayhQbHVnaW4udG9nZ2xlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LmRhdGEoXCJkcm9wZG93bi10b2dnbGVcIikgPT0gXCJob3ZlclwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi5vcHRpb25zLnRvZ2dsZSA9IFwiaG92ZXJcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQubW91c2VsZWF2ZShQbHVnaW4uaGlkZSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZWxlbWVudC5kYXRhKFwiZHJvcGRvd24tdG9nZ2xlXCIpID09IFwiY2xpY2tcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24ub3B0aW9ucy50b2dnbGUgPSBcImNsaWNrXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24ub3B0aW9ucy50b2dnbGUgPT0gXCJob3ZlclwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5tb3VzZWVudGVyKFBsdWdpbi5zaG93KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50Lm1vdXNlbGVhdmUoUGx1Z2luLmhpZGUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi50b2dnbGUuY2xpY2soUGx1Z2luLnRvZ2dsZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZHJvcGRvd24gY2xvc2UgaWNvblxuICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5jbG9zZS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uY2xvc2Uub24oXCJjbGlja1wiLCBQbHVnaW4uaGlkZSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8gZGlzYWJsZSBkcm9wZG93biBjbG9zZVxuICAgICAgICAgICAgICAgIFBsdWdpbi5kaXNhYmxlQ2xvc2UoKTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogU2V0dXAgZHJvcGRvd25cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgc2V0dXA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5vcHRpb25zLnBsYWNlbWVudCkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmFkZENsYXNzKFwibS1kcm9wZG93bi0tXCIgKyBkcm9wZG93bi5vcHRpb25zLnBsYWNlbWVudCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGRyb3Bkb3duLm9wdGlvbnMuYWxpZ24pIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5hZGRDbGFzcyhcIm0tZHJvcGRvd24tLWFsaWduLVwiICsgZHJvcGRvd24ub3B0aW9ucy5hbGlnbik7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGRyb3Bkb3duLm9wdGlvbnMud2lkdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24ud3JhcHBlci5jc3MoXCJ3aWR0aFwiLCBkcm9wZG93bi5vcHRpb25zLndpZHRoKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudC5kYXRhKFwiZHJvcGRvd24tcGVyc2lzdGVudFwiKSkge1xuICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi5vcHRpb25zLnBlcnNpc3RlbnQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBoZWlnaHRcbiAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24ub3B0aW9ucy5taW5IZWlnaHQpIHtcbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uc2Nyb2xsYWJsZS5jc3MoXCJtaW4taGVpZ2h0XCIsIGRyb3Bkb3duLm9wdGlvbnMubWluSGVpZ2h0KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24ub3B0aW9ucy5tYXhIZWlnaHQpIHtcbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uc2Nyb2xsYWJsZS5jc3MoXCJtYXgtaGVpZ2h0XCIsIGRyb3Bkb3duLm9wdGlvbnMubWF4SGVpZ2h0KTtcbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uc2Nyb2xsYWJsZS5jc3MoXCJvdmVyZmxvdy15XCIsIFwiYXV0b1wiKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAobVV0aWwuaXNEZXNrdG9wRGV2aWNlKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1BcHAuaW5pdFNjcm9sbGVyKGRyb3Bkb3duLnNjcm9sbGFibGUsIHt9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIHNldCB6aW5kZXhcbiAgICAgICAgICAgICAgICBQbHVnaW4uc2V0WmluZGV4KCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIHN5bmNcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgc3luYzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJChlbGVtZW50KS5kYXRhKFwiZHJvcGRvd25cIiwgZHJvcGRvd24pO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBTeW5jIGRyb3Bkb3duIG9iamVjdCB3aXRoIGpRdWVyeSBlbGVtZW50XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGRpc2FibGVDbG9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5vbihcImNsaWNrXCIsIFwiLm0tZHJvcGRvd24tLWRpc2FibGUtY2xvc2UsIC5tQ1NCXzFfc2Nyb2xsYmFyXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBUb2dnbGUgZHJvcGRvd25cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgdG9nZ2xlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24ub3Blbikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gUGx1Z2luLmhpZGUoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gUGx1Z2luLnNob3coKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNldCBjb250ZW50XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldENvbnRlbnQ6IGZ1bmN0aW9uKGNvbnRlbnQpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50LmZpbmQoXCIubS1kcm9wZG93bl9fY29udGVudFwiKS5odG1sKGNvbnRlbnQpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRyb3Bkb3duO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBTaG93IGRyb3Bkb3duXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNob3c6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5vcHRpb25zLnRvZ2dsZSA9PSBcImhvdmVyXCIgJiYgZWxlbWVudC5kYXRhKFwiaG92ZXJcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmNsZWFySG92ZXJlZCgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZHJvcGRvd247XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGRyb3Bkb3duLm9wZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGRyb3Bkb3duO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5hcnJvdy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5hZGp1c3RBcnJvd1BvcygpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIFBsdWdpbi5ldmVudFRyaWdnZXIoXCJiZWZvcmVTaG93XCIpO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLmhpZGVPcGVuZWQoKTtcblxuICAgICAgICAgICAgICAgIGVsZW1lbnQuYWRkQ2xhc3MoXCJtLWRyb3Bkb3duLS1vcGVuXCIpO1xuXG4gICAgICAgICAgICAgICAgaWYgKG1VdGlsLmlzTW9iaWxlRGV2aWNlKCkgJiYgZHJvcGRvd24ub3B0aW9ucy5tb2JpbGVPdmVybGF5KSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciB6SW5kZXggPSBkcm9wZG93bi53cmFwcGVyLmNzcyhcInpJbmRleFwiKSAtIDE7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkcm9wZG93bm9mZiA9ICQoXCI8ZGl2IGNsYXNzPVxcXCJtLWRyb3Bkb3duX19kcm9wb2ZmXFxcIj48L2Rpdj5cIik7XG5cbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd25vZmYuY3NzKFwiekluZGV4XCIsIHpJbmRleCk7XG4gICAgICAgICAgICAgICAgICAgIGRyb3Bkb3dub2ZmLmRhdGEoXCJkcm9wZG93blwiLCBlbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5kYXRhKFwiZHJvcG9mZlwiLCBkcm9wZG93bm9mZik7XG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuYWZ0ZXIoZHJvcGRvd25vZmYpO1xuICAgICAgICAgICAgICAgICAgICBkcm9wZG93bm9mZi5jbGljayhmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBQbHVnaW4uaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZWxlbWVudC5mb2N1cygpO1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuYXR0cihcImFyaWEtZXhwYW5kZWRcIiwgXCJ0cnVlXCIpO1xuICAgICAgICAgICAgICAgIGRyb3Bkb3duLm9wZW4gPSB0cnVlO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLmhhbmRsZURyb3BQb3NpdGlvbigpO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLmV2ZW50VHJpZ2dlcihcImFmdGVyU2hvd1wiKTtcblxuICAgICAgICAgICAgICAgIHJldHVybiBkcm9wZG93bjtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQ2xlYXIgZHJvcGRvd24gaG92ZXJcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgY2xlYXJIb3ZlcmVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZURhdGEoXCJob3ZlclwiKTtcbiAgICAgICAgICAgICAgICB2YXIgdGltZW91dCA9IGVsZW1lbnQuZGF0YShcInRpbWVvdXRcIik7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVEYXRhKFwidGltZW91dFwiKTtcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEhpZGUgaG92ZXJlZCBkcm9wZG93blxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBoaWRlSG92ZXJlZDogZnVuY3Rpb24oZm9yY2UpIHtcbiAgICAgICAgICAgICAgICBpZiAoZm9yY2UpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKFBsdWdpbi5ldmVudFRyaWdnZXIoXCJiZWZvcmVIaWRlXCIpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2FuY2VsIGhpZGVcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5jbGVhckhvdmVyZWQoKTtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVDbGFzcyhcIm0tZHJvcGRvd24tLW9wZW5cIik7XG4gICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLm9wZW4gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmV2ZW50VHJpZ2dlcihcImFmdGVySGlkZVwiKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoUGx1Z2luLmV2ZW50VHJpZ2dlcihcImJlZm9yZUhpZGVcIikgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjYW5jZWwgaGlkZVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHZhciB0aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LmRhdGEoXCJob3ZlclwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBsdWdpbi5jbGVhckhvdmVyZWQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUNsYXNzKFwibS1kcm9wZG93bi0tb3BlblwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi5vcGVuID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmV2ZW50VHJpZ2dlcihcImFmdGVySGlkZVwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSwgZHJvcGRvd24ub3B0aW9ucy5ob3ZlclRpbWVvdXQpO1xuXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZGF0YShcImhvdmVyXCIsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmRhdGEoXCJ0aW1lb3V0XCIsIHRpbWVvdXQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGlkZSBjbGlja2VkIGRyb3Bkb3duXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhpZGVDbGlja2VkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiAoUGx1Z2luLmV2ZW50VHJpZ2dlcihcImJlZm9yZUhpZGVcIikgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNhbmNlbCBoaWRlXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVDbGFzcyhcIm0tZHJvcGRvd24tLW9wZW5cIik7XG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuZGF0YShcImRyb3BvZmZcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5kYXRhKFwiZHJvcG9mZlwiKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZHJvcGRvd24ub3BlbiA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIFBsdWdpbi5ldmVudFRyaWdnZXIoXCJhZnRlckhpZGVcIik7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEhpZGUgZHJvcGRvd25cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaGlkZTogZnVuY3Rpb24oZm9yY2UpIHtcbiAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24ub3BlbiA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGRyb3Bkb3duO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5vcHRpb25zLnRvZ2dsZSA9PSBcImhvdmVyXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmhpZGVIb3ZlcmVkKGZvcmNlKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBQbHVnaW4uaGlkZUNsaWNrZWQoKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24uZGVmYXVsdERyb3BQb3MgPT0gXCJkb3duXCIgJiYgZHJvcGRvd24uY3VycmVudERyb3BQb3MgPT0gXCJ1cFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlQ2xhc3MoXCJtLWRyb3Bkb3duLS11cFwiKTtcbiAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uYXJyb3cucHJlcGVuZFRvKGRyb3Bkb3duLndyYXBwZXIpO1xuICAgICAgICAgICAgICAgICAgICBkcm9wZG93bi5jdXJyZW50RHJvcFBvcyA9IFwiZG93blwiO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiBkcm9wZG93bjtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGlkZSBvcGVuZWQgZHJvcGRvd25zXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhpZGVPcGVuZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICQoXCIubS1kcm9wZG93bi5tLWRyb3Bkb3duLS1vcGVuXCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICQodGhpcykubURyb3Bkb3duKCkuaGlkZSh0cnVlKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQWRqdXN0IGRyb3Bkb3duIGFycm93IHBvc2l0aW9uc1xuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBhZGp1c3RBcnJvd1BvczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdmFyIHdpZHRoID0gZWxlbWVudC5vdXRlcldpZHRoKCk7XG4gICAgICAgICAgICAgICAgdmFyIGFsaWdubWVudCA9IGRyb3Bkb3duLmFycm93Lmhhc0NsYXNzKFwibS1kcm9wZG93bl9fYXJyb3ctLXJpZ2h0XCIpID8gXCJyaWdodFwiIDogXCJsZWZ0XCI7XG4gICAgICAgICAgICAgICAgdmFyIHBvcyA9IDA7XG5cbiAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24uYXJyb3cubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAobVV0aWwuaXNJblJlc3BvbnNpdmVSYW5nZShcIm1vYmlsZVwiKSAmJiBlbGVtZW50Lmhhc0NsYXNzKFwibS1kcm9wZG93bi0tbW9iaWxlLWZ1bGwtd2lkdGhcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvcyA9IGVsZW1lbnQub2Zmc2V0KCkubGVmdCArICh3aWR0aCAvIDIpIC0gTWF0aC5hYnMoZHJvcGRvd24uYXJyb3cud2lkdGgoKSAvIDIpIC0gcGFyc2VJbnQoZHJvcGRvd24ud3JhcHBlci5jc3MoXCJsZWZ0XCIpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmFycm93LmNzcyhcInJpZ2h0XCIsIFwiYXV0b1wiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmFycm93LmNzcyhcImxlZnRcIiwgcG9zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmFycm93LmNzcyhcIm1hcmdpbi1sZWZ0XCIsIFwiYXV0b1wiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmFycm93LmNzcyhcIm1hcmdpbi1yaWdodFwiLCBcImF1dG9cIik7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZHJvcGRvd24uYXJyb3cuaGFzQ2xhc3MoXCJtLWRyb3Bkb3duX19hcnJvdy0tYWRqdXN0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3MgPSB3aWR0aCAvIDIgLSBNYXRoLmFicyhkcm9wZG93bi5hcnJvdy53aWR0aCgpIC8gMik7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZWxlbWVudC5oYXNDbGFzcyhcIm0tZHJvcGRvd24tLWFsaWduLXB1c2hcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3MgPSBwb3MgKyAyMDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhbGlnbm1lbnQgPT0gXCJyaWdodFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uYXJyb3cuY3NzKFwibGVmdFwiLCBcImF1dG9cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uYXJyb3cuY3NzKFwicmlnaHRcIiwgcG9zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uYXJyb3cuY3NzKFwicmlnaHRcIiwgXCJhdXRvXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmFycm93LmNzcyhcImxlZnRcIiwgcG9zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQ2hhbmdlIGRyb3Bkb3duIGRyb3AgcG9zaXRpb25cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaGFuZGxlRHJvcFBvc2l0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG5cbiAgICAgICAgICAgICAgICBpZiAoZHJvcGRvd24ub3B0aW9ucy5kcm9wQXV0byA9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChQbHVnaW4uaXNJblZlcnRpY2FsVmlld3BvcnQoKSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkcm9wZG93bi5jdXJyZW50RHJvcFBvcyA9PSBcInVwXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUNsYXNzKFwibS1kcm9wZG93bi0tdXBcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJvcGRvd24uYXJyb3cucHJlcGVuZFRvKGRyb3Bkb3duLndyYXBwZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmN1cnJlbnREcm9wUG9zID0gXCJkb3duXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGRyb3Bkb3duLmN1cnJlbnREcm9wUG9zID09IFwiZG93blwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5hZGRDbGFzcyhcIm0tZHJvcGRvd24tLXVwXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmFycm93LmFwcGVuZFRvKGRyb3Bkb3duLndyYXBwZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmN1cnJlbnREcm9wUG9zID0gXCJ1cFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBHZXQgemluZGV4XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldFppbmRleDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdmFyIG9sZFppbmRleCA9IGRyb3Bkb3duLndyYXBwZXIuY3NzKFwiei1pbmRleFwiKTtcbiAgICAgICAgICAgICAgICB2YXIgbmV3WmluZGV4ID0gbVV0aWwuZ2V0SGlnaGVzdFppbmRleChlbGVtZW50KTtcbiAgICAgICAgICAgICAgICBpZiAobmV3WmluZGV4ID4gb2xkWmluZGV4KSB7XG4gICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLndyYXBwZXIuY3NzKFwiei1pbmRleFwiLCB6aW5kZXgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQ2hlY2sgcGVyc2lzdGVudFxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpc1BlcnNpc3RlbnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBkcm9wZG93bi5vcHRpb25zLnBlcnNpc3RlbnQ7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIENoZWNrIHBlcnNpc3RlbnRcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaXNTaG93bjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRyb3Bkb3duLm9wZW47XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIENoZWNrIGlmIGRyb3Bkb3duIGlzIGluIHZpZXdwb3J0XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGlzSW5WZXJ0aWNhbFZpZXdwb3J0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgZWwgPSBkcm9wZG93bi53cmFwcGVyO1xuICAgICAgICAgICAgICAgIHZhciBvZmZzZXQgPSBlbC5vZmZzZXQoKTtcbiAgICAgICAgICAgICAgICB2YXIgaGVpZ2h0ID0gZWwub3V0ZXJIZWlnaHQoKTtcbiAgICAgICAgICAgICAgICB2YXIgd2lkdGggPSBlbC53aWR0aCgpO1xuICAgICAgICAgICAgICAgIHZhciBzY3JvbGxhYmxlID0gZWwuZmluZChcIltkYXRhLXNjcm9sbGFibGVdXCIpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHNjcm9sbGFibGUubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzY3JvbGxhYmxlLmRhdGEoXCJtYXgtaGVpZ2h0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQgKz0gcGFyc2VJbnQoc2Nyb2xsYWJsZS5kYXRhKFwibWF4LWhlaWdodFwiKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc2Nyb2xsYWJsZS5kYXRhKFwiaGVpZ2h0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQgKz0gcGFyc2VJbnQoc2Nyb2xsYWJsZS5kYXRhKFwiaGVpZ2h0XCIpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiAob2Zmc2V0LnRvcCArIGhlaWdodCA8ICQod2luZG93KS5zY3JvbGxUb3AoKSArICQod2luZG93KS5oZWlnaHQoKSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFRyaWdnZXIgZXZlbnRzXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGV2ZW50VHJpZ2dlcjogZnVuY3Rpb24obmFtZSkge1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZHJvcGRvd24uZXZlbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBldmVudCA9IGRyb3Bkb3duLmV2ZW50c1tpXTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50Lm5hbWUgPT0gbmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50Lm9uZSA9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50LmZpcmVkID09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duLmV2ZW50c1tpXS5maXJlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudC5oYW5kbGVyLmNhbGwodGhpcywgZHJvcGRvd24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50LmhhbmRsZXIuY2FsbCh0aGlzLCBkcm9wZG93bik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBhZGRFdmVudDogZnVuY3Rpb24obmFtZSwgaGFuZGxlciwgb25lKSB7XG4gICAgICAgICAgICAgICAgZHJvcGRvd24uZXZlbnRzLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiBoYW5kbGVyLFxuICAgICAgICAgICAgICAgICAgICBvbmU6IG9uZSxcbiAgICAgICAgICAgICAgICAgICAgZmlyZWQ6IGZhbHNlLFxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLnN5bmMoKTtcblxuICAgICAgICAgICAgICAgIHJldHVybiBkcm9wZG93bjtcbiAgICAgICAgICAgIH0sXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gUnVuIHBsdWdpblxuICAgICAgICBQbHVnaW4ucnVuLmFwcGx5KHRoaXMsIFtvcHRpb25zXSk7XG5cbiAgICAgICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgICAgICAvLyAqKiBQdWJsaWMgQVBJICoqIC8vXG4gICAgICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgICAgICAvKipcbiAgICAgICAgICogU2hvdyBkcm9wZG93blxuICAgICAgICAgKiBAcmV0dXJucyB7bURyb3Bkb3dufVxuICAgICAgICAgKi9cbiAgICAgICAgZHJvcGRvd24uc2hvdyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIFBsdWdpbi5zaG93KCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEhpZGUgZHJvcGRvd25cbiAgICAgICAgICogQHJldHVybnMge21Ecm9wZG93bn1cbiAgICAgICAgICovXG4gICAgICAgIGRyb3Bkb3duLmhpZGUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uaGlkZSgpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBUb2dnbGUgZHJvcGRvd25cbiAgICAgICAgICogQHJldHVybnMge21Ecm9wZG93bn1cbiAgICAgICAgICovXG4gICAgICAgIGRyb3Bkb3duLnRvZ2dsZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIFBsdWdpbi50b2dnbGUoKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogVG9nZ2xlIGRyb3Bkb3duXG4gICAgICAgICAqIEByZXR1cm5zIHttRHJvcGRvd259XG4gICAgICAgICAqL1xuICAgICAgICBkcm9wZG93bi5pc1BlcnNpc3RlbnQgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uaXNQZXJzaXN0ZW50KCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENoZWNrIHNob3duIHN0YXRlXG4gICAgICAgICAqIEByZXR1cm5zIHttRHJvcGRvd259XG4gICAgICAgICAqL1xuICAgICAgICBkcm9wZG93bi5pc1Nob3duID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gUGx1Z2luLmlzU2hvd24oKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ2hlY2sgc2hvd24gc3RhdGVcbiAgICAgICAgICogQHJldHVybnMge21Ecm9wZG93bn1cbiAgICAgICAgICovXG4gICAgICAgIGRyb3Bkb3duLmZpeERyb3BQb3NpdGlvbiA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIFBsdWdpbi5oYW5kbGVEcm9wUG9zaXRpb24oKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2V0IGRyb3Bkb3duIGNvbnRlbnRcbiAgICAgICAgICogQHJldHVybnMge21Ecm9wZG93bn1cbiAgICAgICAgICovXG4gICAgICAgIGRyb3Bkb3duLnNldENvbnRlbnQgPSBmdW5jdGlvbihjb250ZW50KSB7XG4gICAgICAgICAgICByZXR1cm4gUGx1Z2luLnNldENvbnRlbnQoY29udGVudCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldCBkcm9wZG93biBjb250ZW50XG4gICAgICAgICAqIEByZXR1cm5zIHttRHJvcGRvd259XG4gICAgICAgICAqL1xuICAgICAgICBkcm9wZG93bi5vbiA9IGZ1bmN0aW9uKG5hbWUsIGhhbmRsZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uYWRkRXZlbnQobmFtZSwgaGFuZGxlcik7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldCBkcm9wZG93biBjb250ZW50XG4gICAgICAgICAqIEByZXR1cm5zIHttRHJvcGRvd259XG4gICAgICAgICAqL1xuICAgICAgICBkcm9wZG93bi5vbmUgPSBmdW5jdGlvbihuYW1lLCBoYW5kbGVyKSB7XG4gICAgICAgICAgICByZXR1cm4gUGx1Z2luLmFkZEV2ZW50KG5hbWUsIGhhbmRsZXIsIHRydWUpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBkcm9wZG93bjtcbiAgICB9O1xuXG4gICAgLy8gZGVmYXVsdCBvcHRpb25zXG4gICAgJC5mbi5tRHJvcGRvd24uZGVmYXVsdHMgPSB7XG4gICAgICAgIHRvZ2dsZTogXCJjbGlja1wiLFxuICAgICAgICBob3ZlclRpbWVvdXQ6IDMwMCxcbiAgICAgICAgc2tpbjogXCJkZWZhdWx0XCIsXG4gICAgICAgIGhlaWdodDogXCJhdXRvXCIsXG4gICAgICAgIGRyb3BBdXRvOiB0cnVlLFxuICAgICAgICBtYXhIZWlnaHQ6IGZhbHNlLFxuICAgICAgICBtaW5IZWlnaHQ6IGZhbHNlLFxuICAgICAgICBwZXJzaXN0ZW50OiBmYWxzZSxcbiAgICAgICAgbW9iaWxlT3ZlcmxheTogdHJ1ZSxcbiAgICB9O1xuXG4gICAgLy8gZ2xvYmFsIGluaXRcbiAgICBpZiAobVV0aWwuaXNNb2JpbGVEZXZpY2UoKSkge1xuICAgICAgICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiW2RhdGEtZHJvcGRvd24tdG9nZ2xlPVxcXCJjbGlja1xcXCJdIC5tLWRyb3Bkb3duX190b2dnbGUsIFtkYXRhLWRyb3Bkb3duLXRvZ2dsZT1cXFwiaG92ZXJcXFwiXSAubS1kcm9wZG93bl9fdG9nZ2xlXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICQodGhpcykucGFyZW50KFwiLm0tZHJvcGRvd25cIikubURyb3Bkb3duKCkudG9nZ2xlKCk7XG4gICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAgICQoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIiwgXCJbZGF0YS1kcm9wZG93bi10b2dnbGU9XFxcImNsaWNrXFxcIl0gLm0tZHJvcGRvd25fX3RvZ2dsZVwiLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAkKHRoaXMpLnBhcmVudChcIi5tLWRyb3Bkb3duXCIpLm1Ecm9wZG93bigpLnRvZ2dsZSgpO1xuICAgICAgICB9KTtcbiAgICAgICAgJChkb2N1bWVudCkub24oXCJtb3VzZWVudGVyXCIsIFwiW2RhdGEtZHJvcGRvd24tdG9nZ2xlPVxcXCJob3ZlclxcXCJdXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgICQodGhpcykubURyb3Bkb3duKCkudG9nZ2xlKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIGhhbmRsZSBnbG9iYWwgZG9jdW1lbnQgY2xpY2tcbiAgICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgJChcIi5tLWRyb3Bkb3duLm0tZHJvcGRvd24tLW9wZW5cIikuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmICghJCh0aGlzKS5kYXRhKFwiZHJvcGRvd25cIikpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciB0YXJnZXQgPSAkKGUudGFyZ2V0KTtcbiAgICAgICAgICAgIHZhciBkcm9wZG93biA9ICQodGhpcykubURyb3Bkb3duKCk7XG4gICAgICAgICAgICB2YXIgdG9nZ2xlID0gJCh0aGlzKS5maW5kKFwiLm0tZHJvcGRvd25fX3RvZ2dsZVwiKTtcblxuICAgICAgICAgICAgaWYgKHRvZ2dsZS5sZW5ndGggPiAwICYmIHRhcmdldC5pcyh0b2dnbGUpICE9PSB0cnVlICYmIHRvZ2dsZS5maW5kKHRhcmdldCkubGVuZ3RoID09PSAwICYmIHRhcmdldC5maW5kKHRvZ2dsZSkubGVuZ3RoID09PSAwICYmIGRyb3Bkb3duLmlzUGVyc2lzdGVudCgpID09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgZHJvcGRvd24uaGlkZSgpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICgkKHRoaXMpLmZpbmQodGFyZ2V0KS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgICBkcm9wZG93bi5oaWRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufShqUXVlcnkpKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvZG9tYWluL2NvbW1vbi90aGVtZS9qcy9mcmFtZXdvcmsvY29tcG9uZW50cy9nZW5lcmFsL2Ryb3Bkb3duLmpzIiwiKGZ1bmN0aW9uKCQpIHtcblxuICAgIC8vIFBsdWdpbiBmdW5jdGlvblxuICAgICQuZm4ubUhlYWRlciA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgLy8gUGx1Z2luIHNjb3BlIHZhcmlhYmxlXG4gICAgICAgIHZhciBoZWFkZXIgPSB0aGlzO1xuICAgICAgICB2YXIgZWxlbWVudCA9ICQodGhpcyk7XG5cbiAgICAgICAgLy8gUGx1Z2luIGNsYXNzXG4gICAgICAgIHZhciBQbHVnaW4gPSB7XG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJ1biBwbHVnaW5cbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttSGVhZGVyfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBydW46IGZ1bmN0aW9uKG9wdGlvbnMpIHsgXG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuZGF0YSgnaGVhZGVyJykpIHtcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyID0gZWxlbWVudC5kYXRhKCdoZWFkZXInKTsgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVzZXQgaGVhZGVyXG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5pbml0KG9wdGlvbnMpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIHJlc2V0IGhlYWRlclxuICAgICAgICAgICAgICAgICAgICBQbHVnaW4ucmVzZXQoKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBidWlsZCBoZWFkZXJcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmJ1aWxkKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5kYXRhKCdoZWFkZXInLCBoZWFkZXIpO1xuICAgICAgICAgICAgICAgIH0gXG5cbiAgICAgICAgICAgICAgICByZXR1cm4gaGVhZGVyO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIHN1YmhlYWRlciBjbGljayB0b2dnbGVcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttSGVhZGVyfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpbml0OiBmdW5jdGlvbihvcHRpb25zKSB7ICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGhlYWRlci5vcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sICQuZm4ubUhlYWRlci5kZWZhdWx0cywgb3B0aW9ucyk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJlc2V0IGhlYWRlclxuICAgICAgICAgICAgICogQHJldHVybnMge21IZWFkZXJ9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGJ1aWxkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBQbHVnaW4udG9nZ2xlKCk7ICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgdG9nZ2xlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgbGFzdFNjcm9sbFRvcCA9IDA7XG5cbiAgICAgICAgICAgICAgICBpZiAoaGVhZGVyLm9wdGlvbnMubWluaW1pemUubW9iaWxlID09PSBmYWxzZSAmJiBoZWFkZXIub3B0aW9ucy5taW5pbWl6ZS5kZXNrdG9wID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfSAgICAgICAgICBcblxuICAgICAgICAgICAgICAgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBvZmZzZXQgPSAwO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKCdkZXNrdG9wJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9mZnNldCA9IGhlYWRlci5vcHRpb25zLm9mZnNldC5kZXNrdG9wO1xuICAgICAgICAgICAgICAgICAgICAgICAgb24gPSBoZWFkZXIub3B0aW9ucy5taW5pbWl6ZS5kZXNrdG9wLm9uO1xuICAgICAgICAgICAgICAgICAgICAgICAgb2ZmID0gaGVhZGVyLm9wdGlvbnMubWluaW1pemUuZGVza3RvcC5vZmY7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobVV0aWwuaXNJblJlc3BvbnNpdmVSYW5nZSgndGFibGV0LWFuZC1tb2JpbGUnKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgb2Zmc2V0ID0gaGVhZGVyLm9wdGlvbnMub2Zmc2V0Lm1vYmlsZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uID0gaGVhZGVyLm9wdGlvbnMubWluaW1pemUubW9iaWxlLm9uO1xuICAgICAgICAgICAgICAgICAgICAgICAgb2ZmID0gaGVhZGVyLm9wdGlvbnMubWluaW1pemUubW9iaWxlLm9mZjtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIHZhciBzdCA9ICQodGhpcykuc2Nyb2xsVG9wKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKGhlYWRlci5vcHRpb25zLmNsYXNzaWMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdCA+IG9mZnNldCl7IC8vIGRvd24gc2Nyb2xsIG1vZGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiYm9keVwiKS5hZGRDbGFzcyhvbik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcImJvZHlcIikucmVtb3ZlQ2xhc3Mob2ZmKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7IC8vIGJhY2sgc2Nyb2xsIG1vZGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiYm9keVwiKS5hZGRDbGFzcyhvZmYpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCJib2R5XCIpLnJlbW92ZUNsYXNzKG9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdCA+IG9mZnNldCAmJiBsYXN0U2Nyb2xsVG9wIDwgc3QpeyAvLyBkb3duIHNjcm9sbCBtb2RlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcImJvZHlcIikuYWRkQ2xhc3Mob24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCJib2R5XCIpLnJlbW92ZUNsYXNzKG9mZik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgeyAvLyBiYWNrIHNjcm9sbCBtb2RlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcImJvZHlcIikuYWRkQ2xhc3Mob2ZmKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiYm9keVwiKS5yZW1vdmVDbGFzcyhvbik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhc3RTY3JvbGxUb3AgPSBzdDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBSZXNldCBtZW51XG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHJlc2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAvLyBSdW4gcGx1Z2luXG4gICAgICAgIFBsdWdpbi5ydW4uYXBwbHkoaGVhZGVyLCBbb3B0aW9uc10pO1xuXG4gICAgICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAgICAgLy8gKiogUHVibGljIEFQSSAqKiAvL1xuICAgICAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERpc2FibGUgaGVhZGVyIGZvciBnaXZlbiB0aW1lXG4gICAgICAgICAqIEByZXR1cm5zIHtqUXVlcnl9XG4gICAgICAgICAqL1xuICAgICAgICBoZWFkZXIucHVibGljTWV0aG9kID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIFx0Ly9yZXR1cm4gUGx1Z2luLnB1YmxpY01ldGhvZCgpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIFJldHVybiBwbHVnaW4gaW5zdGFuY2VcbiAgICAgICAgcmV0dXJuIGhlYWRlcjtcbiAgICB9O1xuXG4gICAgLy8gUGx1Z2luIGRlZmF1bHQgb3B0aW9uc1xuICAgICQuZm4ubUhlYWRlci5kZWZhdWx0cyA9IHtcbiAgICAgICAgY2xhc3NpYzogZmFsc2UsXG4gICAgICAgIG9mZnNldDoge1xuICAgICAgICAgICAgbW9iaWxlOiAxNTAsXG4gICAgICAgICAgICBkZXNrdG9wOiAyMDAgICAgICAgIFxuICAgICAgICB9LFxuICAgICAgICBtaW5pbWl6ZToge1xuICAgICAgICAgICAgbW9iaWxlOiBmYWxzZSxcbiAgICAgICAgICAgIGRlc2t0b3A6IGZhbHNlXG4gICAgICAgIH1cbiAgICB9OyBcbn0oalF1ZXJ5KSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvaGVhZGVyLmpzIiwiaW1wb3J0IHttVXRpbH0gZnJvbSBcIi4vLi4vLi4vYmFzZS91dGlsXCI7XG5pbXBvcnQge21BcHB9ICBmcm9tIFwiLi8uLi8uLi9iYXNlL2FwcFwiO1xuXG4oZnVuY3Rpb24oJCkge1xuXG4gICAgLy8gUGx1Z2luIGZ1bmN0aW9uXG4gICAgJC5mbi5tTWVudSA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgLy8gUGx1Z2luIHNjb3BlIHZhcmlhYmxlXG4gICAgICAgIHZhciBtZW51ID0gdGhpcztcbiAgICAgICAgdmFyIGVsZW1lbnQgPSAkKHRoaXMpO1xuXG4gICAgICAgIC8vIFBsdWdpbiBjbGFzc1xuICAgICAgICB2YXIgUGx1Z2luID0ge1xuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBSdW4gcGx1Z2luXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHJ1bjogZnVuY3Rpb24ob3B0aW9ucywgcmVpbml0KSB7XG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuZGF0YShcIm1lbnVcIikgJiYgcmVpbml0ICE9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIG1lbnUgPSBlbGVtZW50LmRhdGEoXCJtZW51XCIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIHJlc2V0IG1lbnVcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmluaXQob3B0aW9ucyk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gcmVzZXQgbWVudVxuICAgICAgICAgICAgICAgICAgICBQbHVnaW4ucmVzZXQoKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBidWlsZCBtZW51XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5idWlsZCgpO1xuXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZGF0YShcIm1lbnVcIiwgbWVudSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcmV0dXJuIG1lbnU7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEhhbmRsZXMgc3VibWVudSBjbGljayB0b2dnbGVcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttTWVudX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaW5pdDogZnVuY3Rpb24ob3B0aW9ucykge1xuICAgICAgICAgICAgICAgIG1lbnUuZXZlbnRzID0gW107XG5cbiAgICAgICAgICAgICAgICAvLyBtZXJnZSBkZWZhdWx0IGFuZCB1c2VyIGRlZmluZWQgb3B0aW9uc1xuICAgICAgICAgICAgICAgIG1lbnUub3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLm1NZW51LmRlZmF1bHRzLCBvcHRpb25zKTtcblxuICAgICAgICAgICAgICAgIC8vIHBhdXNlIG1lbnVcbiAgICAgICAgICAgICAgICBtZW51LnBhdXNlRHJvcGRvd25Ib3ZlclRpbWUgPSAwO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBSZXNldCBtZW51XG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGJ1aWxkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50Lm9uKFwiY2xpY2tcIiwgXCIubS1tZW51X190b2dnbGVcIiwgUGx1Z2luLmhhbmRsZVN1Ym1lbnVBY2NvcmRpb24pO1xuXG4gICAgICAgICAgICAgICAgLy8gZHJvcGRvd24gbW9kZShob3ZlcmFibGUpXG4gICAgICAgICAgICAgICAgaWYgKFBsdWdpbi5nZXRTdWJtZW51TW9kZSgpID09PSBcImRyb3Bkb3duXCIgfHwgUGx1Z2luLmlzQ29uZGl0aW9uYWxTdWJtZW51RHJvcGRvd24oKSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBkcm9wZG93biBzdWJtZW51IC0gaG92ZXIgdG9nZ2xlXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQub24oe21vdXNlZW50ZXI6IFBsdWdpbi5oYW5kbGVTdWJtZW51RHJvZG93bkhvdmVyRW50ZXIsIG1vdXNlbGVhdmU6IFBsdWdpbi5oYW5kbGVTdWJtZW51RHJvZG93bkhvdmVyRXhpdH0sIFwiW2RhdGEtbWVudS1zdWJtZW51LXRvZ2dsZT1cXFwiaG92ZXJcXFwiXVwiKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBkcm9wZG93biBzdWJtZW51IC0gY2xpY2sgdG9nZ2xlXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQub24oXCJjbGlja1wiLCBcIltkYXRhLW1lbnUtc3VibWVudS10b2dnbGU9XFxcImNsaWNrXFxcIl0gLm0tbWVudV9fdG9nZ2xlXCIsIFBsdWdpbi5oYW5kbGVTdWJtZW51RHJvcGRvd25DbGljayk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZWxlbWVudC5maW5kKFwiLm0tbWVudV9faXRlbTpub3QoLm0tbWVudV9faXRlbS0tc3VibWVudSkgPiAubS1tZW51X19saW5rOm5vdCgubS1tZW51X190b2dnbGUpXCIpLmNsaWNrKFBsdWdpbi5oYW5kbGVMaW5rQ2xpY2spO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBSZXNldCBtZW51XG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHJlc2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAvLyByZW1vdmUgYWNjb3JkaW9uIGhhbmRsZXJcbiAgICAgICAgICAgICAgICBlbGVtZW50Lm9mZihcImNsaWNrXCIsIFwiLm0tbWVudV9fdG9nZ2xlXCIsIFBsdWdpbi5oYW5kbGVTdWJtZW51QWNjb3JkaW9uKTtcblxuICAgICAgICAgICAgICAgIC8vIHJlbW92ZSBkcm9wZG93biBoYW5kbGVyc1xuICAgICAgICAgICAgICAgIGVsZW1lbnQub2ZmKHttb3VzZWVudGVyOiBQbHVnaW4uaGFuZGxlU3VibWVudURyb2Rvd25Ib3ZlckVudGVyLCBtb3VzZWxlYXZlOiBQbHVnaW4uaGFuZGxlU3VibWVudURyb2Rvd25Ib3ZlckV4aXR9LCBcIltkYXRhLW1lbnUtc3VibWVudS10b2dnbGU9XFxcImhvdmVyXFxcIl1cIik7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5vZmYoXCJjbGlja1wiLCBcIltkYXRhLW1lbnUtc3VibWVudS10b2dnbGU9XFxcImNsaWNrXFxcIl0gLm0tbWVudV9fdG9nZ2xlXCIsIFBsdWdpbi5oYW5kbGVTdWJtZW51RHJvcGRvd25DbGljayk7XG5cbiAgICAgICAgICAgICAgICAvLyByZXNldCBtb2JpbGUgbWVudSBhdHRyaWJ1dGVzXG4gICAgICAgICAgICAgICAgbWVudS5maW5kKFwiLm0tbWVudV9fc3VibWVudSwgLm0tbWVudV9faW5uZXJcIikuY3NzKFwiZGlzcGxheVwiLCBcIlwiKTtcbiAgICAgICAgICAgICAgICBtZW51LmZpbmQoXCIubS1tZW51X19pdGVtLS1ob3ZlclwiKS5yZW1vdmVDbGFzcyhcIm0tbWVudV9faXRlbS0taG92ZXJcIik7XG4gICAgICAgICAgICAgICAgbWVudS5maW5kKFwiLm0tbWVudV9faXRlbS0tb3Blbjpub3QoLm0tbWVudV9faXRlbS0tZXhwYW5kZWQpXCIpLnJlbW92ZUNsYXNzKFwibS1tZW51X19pdGVtLS1vcGVuXCIpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBHZXQgc3VibWVudSBtb2RlIGZvciBjdXJyZW50IGJyZWFrcG9pbnQgYW5kIG1lbnUgc3RhdGVcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttTWVudX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZ2V0U3VibWVudU1vZGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKFwiZGVza3RvcFwiKSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAobVV0aWwuaXNzZXQobWVudS5vcHRpb25zLnN1Ym1lbnUsIFwiZGVza3RvcC5zdGF0ZS5ib2R5XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoJChcImJvZHlcIikuaGFzQ2xhc3MobWVudS5vcHRpb25zLnN1Ym1lbnUuZGVza3RvcC5zdGF0ZS5ib2R5KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBtZW51Lm9wdGlvbnMuc3VibWVudS5kZXNrdG9wLnN0YXRlLm1vZGU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBtZW51Lm9wdGlvbnMuc3VibWVudS5kZXNrdG9wLmRlZmF1bHQ7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobVV0aWwuaXNzZXQobWVudS5vcHRpb25zLnN1Ym1lbnUsIFwiZGVza3RvcFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG1lbnUub3B0aW9ucy5zdWJtZW51LmRlc2t0b3A7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG1VdGlsLmlzSW5SZXNwb25zaXZlUmFuZ2UoXCJ0YWJsZXRcIikgJiYgbVV0aWwuaXNzZXQobWVudS5vcHRpb25zLnN1Ym1lbnUsIFwidGFibGV0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBtZW51Lm9wdGlvbnMuc3VibWVudS50YWJsZXQ7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKFwibW9iaWxlXCIpICYmIG1VdGlsLmlzc2V0KG1lbnUub3B0aW9ucy5zdWJtZW51LCBcIm1vYmlsZVwiKSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbWVudS5vcHRpb25zLnN1Ym1lbnUubW9iaWxlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEdldCBzdWJtZW51IG1vZGUgZm9yIGN1cnJlbnQgYnJlYWtwb2ludCBhbmQgbWVudSBzdGF0ZVxuICAgICAgICAgICAgICogQHJldHVybnMge21NZW51fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpc0NvbmRpdGlvbmFsU3VibWVudURyb3Bkb3duOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiAobVV0aWwuaXNJblJlc3BvbnNpdmVSYW5nZShcImRlc2t0b3BcIikgJiYgbVV0aWwuaXNzZXQobWVudS5vcHRpb25zLnN1Ym1lbnUsIFwiZGVza3RvcC5zdGF0ZS5ib2R5XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEhhbmRsZXMgbWVudSBsaW5rIGNsaWNrXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhhbmRsZUxpbmtDbGljazogZnVuY3Rpb24oZSkge1xuXG4gICAgICAgICAgICAgICAgaWYgKFBsdWdpbi5ldmVudFRyaWdnZXIoXCJsaW5rQ2xpY2tcIiwgJCh0aGlzKSkgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgO1xuXG4gICAgICAgICAgICAgICAgaWYgKFBsdWdpbi5nZXRTdWJtZW51TW9kZSgpID09PSBcImRyb3Bkb3duXCIgfHwgUGx1Z2luLmlzQ29uZGl0aW9uYWxTdWJtZW51RHJvcGRvd24oKSkge1xuICAgICAgICAgICAgICAgICAgICBQbHVnaW4uaGFuZGxlU3VibWVudURyb3Bkb3duQ2xvc2UoZSwgJCh0aGlzKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIHN1Ym1lbnUgaG92ZXIgdG9nZ2xlXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhhbmRsZVN1Ym1lbnVEcm9kb3duSG92ZXJFbnRlcjogZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAgIGlmIChQbHVnaW4uZ2V0U3VibWVudU1vZGUoKSA9PT0gXCJhY2NvcmRpb25cIikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKG1lbnUucmVzdW1lRHJvcGRvd25Ib3ZlcigpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdmFyIGl0ZW0gPSAkKHRoaXMpO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLnNob3dTdWJtZW51RHJvcGRvd24oaXRlbSk7XG5cbiAgICAgICAgICAgICAgICBpZiAoaXRlbS5kYXRhKFwiaG92ZXJcIikgPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBQbHVnaW4uaGlkZVN1Ym1lbnVEcm9wZG93bihpdGVtLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIHN1Ym1lbnUgaG92ZXIgdG9nZ2xlXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhhbmRsZVN1Ym1lbnVEcm9kb3duSG92ZXJFeGl0OiBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgICAgaWYgKG1lbnUucmVzdW1lRHJvcGRvd25Ib3ZlcigpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKFBsdWdpbi5nZXRTdWJtZW51TW9kZSgpID09PSBcImFjY29yZGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgaXRlbSA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgdmFyIHRpbWUgPSBtZW51Lm9wdGlvbnMuZHJvcGRvd24udGltZW91dDtcblxuICAgICAgICAgICAgICAgIHZhciB0aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGl0ZW0uZGF0YShcImhvdmVyXCIpID09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFBsdWdpbi5oaWRlU3VibWVudURyb3Bkb3duKGl0ZW0sIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSwgdGltZSk7XG5cbiAgICAgICAgICAgICAgICBpdGVtLmRhdGEoXCJob3ZlclwiLCB0cnVlKTtcbiAgICAgICAgICAgICAgICBpdGVtLmRhdGEoXCJ0aW1lb3V0XCIsIHRpbWVvdXQpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIHN1Ym1lbnUgY2xpY2sgdG9nZ2xlXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhhbmRsZVN1Ym1lbnVEcm9wZG93bkNsaWNrOiBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgICAgaWYgKFBsdWdpbi5nZXRTdWJtZW51TW9kZSgpID09PSBcImFjY29yZGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgaXRlbSA9ICQodGhpcykuY2xvc2VzdChcIi5tLW1lbnVfX2l0ZW1cIik7XG5cbiAgICAgICAgICAgICAgICBpZiAoaXRlbS5kYXRhKFwibWVudS1zdWJtZW51LW1vZGVcIikgPT0gXCJhY2NvcmRpb25cIikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGl0ZW0uaGFzQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLWhvdmVyXCIpID09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0uYWRkQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLW9wZW4tZHJvcGRvd25cIik7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5zaG93U3VibWVudURyb3Bkb3duKGl0ZW0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0ucmVtb3ZlQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLW9wZW4tZHJvcGRvd25cIik7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5oaWRlU3VibWVudURyb3Bkb3duKGl0ZW0sIHRydWUpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBzdWJtZW51IGRyb3Bkb3duIGNsb3NlIG9uIGxpbmsgY2xpY2tcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttTWVudX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaGFuZGxlU3VibWVudURyb3Bkb3duQ2xvc2U6IGZ1bmN0aW9uKGUsIGVsKSB7XG4gICAgICAgICAgICAgICAgLy8gZXhpdCBpZiBpdHMgbm90IHN1Ym1lbnUgZHJvcGRvd24gbW9kZVxuICAgICAgICAgICAgICAgIGlmIChQbHVnaW4uZ2V0U3VibWVudU1vZGUoKSA9PT0gXCJhY2NvcmRpb25cIikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdmFyIHNob3duID0gZWxlbWVudC5maW5kKFwiLm0tbWVudV9faXRlbS5tLW1lbnVfX2l0ZW0tLXN1Ym1lbnUubS1tZW51X19pdGVtLS1ob3ZlclwiKTtcblxuICAgICAgICAgICAgICAgIC8vIGNoZWNrIGlmIGN1cnJlbnRseSBjbGlja2VkIGxpbmsncyBwYXJlbnQgaXRlbSBoYVxuICAgICAgICAgICAgICAgIGlmIChzaG93bi5sZW5ndGggPiAwICYmIGVsLmhhc0NsYXNzKFwibS1tZW51X190b2dnbGVcIikgPT09IGZhbHNlICYmIGVsLmZpbmQoXCIubS1tZW51X190b2dnbGVcIikubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNsb3NlIG9wZW5lZCBkcm9wZG93biBtZW51c1xuICAgICAgICAgICAgICAgICAgICBzaG93bi5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmhpZGVTdWJtZW51RHJvcGRvd24oJCh0aGlzKSwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogaGVscGVyIGZ1bmN0aW9uc1xuICAgICAgICAgICAgICogQHJldHVybnMge21NZW51fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBoYW5kbGVTdWJtZW51QWNjb3JkaW9uOiBmdW5jdGlvbihlLCBlbCkge1xuICAgICAgICAgICAgICAgIHZhciBpdGVtID0gZWwgPyAkKGVsKSA6ICQodGhpcyk7XG5cbiAgICAgICAgICAgICAgICBpZiAoUGx1Z2luLmdldFN1Ym1lbnVNb2RlKCkgPT09IFwiZHJvcGRvd25cIiAmJiBpdGVtLmNsb3Nlc3QoXCIubS1tZW51X19pdGVtXCIpLmRhdGEoXCJtZW51LXN1Ym1lbnUtbW9kZVwiKSAhPSBcImFjY29yZGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHZhciBsaSA9IGl0ZW0uY2xvc2VzdChcImxpXCIpO1xuICAgICAgICAgICAgICAgIHZhciBzdWJtZW51ID0gbGkuY2hpbGRyZW4oXCIubS1tZW51X19zdWJtZW51LCAubS1tZW51X19pbm5lclwiKTtcblxuICAgICAgICAgICAgICAgIGlmIChzdWJtZW51LnBhcmVudChcIi5tLW1lbnVfX2l0ZW0tLWV4cGFuZGVkXCIpLmxlbmd0aCAhPSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChzdWJtZW51Lmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgc3BlZWQgPSBtZW51Lm9wdGlvbnMuYWNjb3JkaW9uLnNsaWRlU3BlZWQ7XG4gICAgICAgICAgICAgICAgICAgIHZhciBoYXNDbG9zYWJsZXMgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAobGkuaGFzQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLW9wZW5cIikgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBoaWRlIG90aGVyIGFjY29yZGlvbnNcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZW51Lm9wdGlvbnMuYWNjb3JkaW9uLmV4cGFuZEFsbCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY2xvc2FibGVzID0gaXRlbS5jbG9zZXN0KFwiLm0tbWVudV9fbmF2LCAubS1tZW51X19zdWJuYXZcIikuZmluZChcIj4gLm0tbWVudV9faXRlbS5tLW1lbnVfX2l0ZW0tLW9wZW4ubS1tZW51X19pdGVtLS1zdWJtZW51Om5vdCgubS1tZW51X19pdGVtLS1leHBhbmRlZClcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xvc2FibGVzLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykuY2hpbGRyZW4oXCIubS1tZW51X19zdWJtZW51XCIpLnNsaWRlVXAoc3BlZWQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLnNjcm9sbFRvSXRlbShpdGVtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLW9wZW5cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2xvc2FibGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzQ2xvc2FibGVzID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChoYXNDbG9zYWJsZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWJtZW51LnNsaWRlRG93bihzcGVlZCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBsdWdpbi5zY3JvbGxUb0l0ZW0oaXRlbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGkuYWRkQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLW9wZW5cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1Ym1lbnUuc2xpZGVEb3duKHNwZWVkLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLnNjcm9sbFRvSXRlbShpdGVtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaS5hZGRDbGFzcyhcIm0tbWVudV9faXRlbS0tb3BlblwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Ym1lbnUuc2xpZGVVcChzcGVlZCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLnNjcm9sbFRvSXRlbShpdGVtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgbGkucmVtb3ZlQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLW9wZW5cIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIHNjcm9sbCB0byBpdGVtIGZ1bmN0aW9uXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNjcm9sbFRvSXRlbTogZnVuY3Rpb24oaXRlbSkge1xuICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBhdXRvIHNjcm9sbCBmb3IgYWNjb3JkaW9uIHN1Ym1lbnVzXG4gICAgICAgICAgICAgICAgaWYgKG1VdGlsLmlzSW5SZXNwb25zaXZlUmFuZ2UoXCJkZXNrdG9wXCIpICYmIG1lbnUub3B0aW9ucy5hY2NvcmRpb24uYXV0b1Njcm9sbCAmJiAhZWxlbWVudC5kYXRhKFwibWVudS1zY3JvbGxhYmxlXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIG1BcHAuc2Nyb2xsVG9WaWV3cG9ydChpdGVtKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIGhlbHBlciBmdW5jdGlvbnNcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttTWVudX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaGlkZVN1Ym1lbnVEcm9wZG93bjogZnVuY3Rpb24oaXRlbSwgY2xhc3NBbHNvKSB7XG4gICAgICAgICAgICAgICAgLy8gcmVtb3ZlIHN1Ym1lbnUgYWN0aXZhdGlvbiBjbGFzc1xuICAgICAgICAgICAgICAgIGlmIChjbGFzc0Fsc28pIHtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5yZW1vdmVDbGFzcyhcIm0tbWVudV9faXRlbS0taG92ZXJcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIGNsZWFyIHRpbWVvdXRcbiAgICAgICAgICAgICAgICBpdGVtLnJlbW92ZURhdGEoXCJob3ZlclwiKTtcbiAgICAgICAgICAgICAgICBpZiAoaXRlbS5kYXRhKFwibWVudS1kcm9wZG93bi10b2dnbGUtY2xhc3NcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgJChcImJvZHlcIikucmVtb3ZlQ2xhc3MoaXRlbS5kYXRhKFwibWVudS1kcm9wZG93bi10b2dnbGUtY2xhc3NcIikpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YXIgdGltZW91dCA9IGl0ZW0uZGF0YShcInRpbWVvdXRcIik7XG4gICAgICAgICAgICAgICAgaXRlbS5yZW1vdmVEYXRhKFwidGltZW91dFwiKTtcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIGhlbHBlciBmdW5jdGlvbnNcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttTWVudX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgc2hvd1N1Ym1lbnVEcm9wZG93bjogZnVuY3Rpb24oaXRlbSkge1xuICAgICAgICAgICAgICAgIC8vIGNsb3NlIGFjdGl2ZSBzdWJtZW51c1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuZmluZChcIi5tLW1lbnVfX2l0ZW0tLXN1Ym1lbnUubS1tZW51X19pdGVtLS1ob3ZlclwiKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgZWwgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoaXRlbS5pcyhlbCkgfHwgZWwuZmluZChpdGVtKS5sZW5ndGggPiAwIHx8IGl0ZW0uZmluZChlbCkubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmhpZGVTdWJtZW51RHJvcGRvd24oZWwsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAvLyBhZGp1c3Qgc3VibWVudSBwb3NpdGlvblxuICAgICAgICAgICAgICAgIFBsdWdpbi5hZGp1c3RTdWJtZW51RHJvcGRvd25BcnJvd1BvcyhpdGVtKTtcblxuICAgICAgICAgICAgICAgIC8vIGFkZCBzdWJtZW51IGFjdGl2YXRpb24gY2xhc3NcbiAgICAgICAgICAgICAgICBpdGVtLmFkZENsYXNzKFwibS1tZW51X19pdGVtLS1ob3ZlclwiKTtcblxuICAgICAgICAgICAgICAgIGlmIChpdGVtLmRhdGEoXCJtZW51LWRyb3Bkb3duLXRvZ2dsZS1jbGFzc1wiKSkge1xuICAgICAgICAgICAgICAgICAgICAkKFwiYm9keVwiKS5hZGRDbGFzcyhpdGVtLmRhdGEoXCJtZW51LWRyb3Bkb3duLXRvZ2dsZS1jbGFzc1wiKSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8gaGFuZGxlIGF1dG8gc2Nyb2xsIGZvciBhY2NvcmRpb24gc3VibWVudXNcbiAgICAgICAgICAgICAgICBpZiAoUGx1Z2luLmdldFN1Ym1lbnVNb2RlKCkgPT09IFwiYWNjb3JkaW9uXCIgJiYgbWVudS5vcHRpb25zLmFjY29yZGlvbi5hdXRvU2Nyb2xsKSB7XG4gICAgICAgICAgICAgICAgICAgIG1BcHAuc2Nyb2xsVG8oaXRlbS5jaGlsZHJlbihcIi5tLW1lbnVfX2l0ZW0tLXN1Ym1lbnVcIikpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBzdWJtZW51IGNsaWNrIHRvZ2dsZVxuICAgICAgICAgICAgICogQHJldHVybnMge21NZW51fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICByZXNpemU6IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgICAgICBpZiAoUGx1Z2luLmdldFN1Ym1lbnVNb2RlKCkgIT09IFwiZHJvcGRvd25cIikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdmFyIHJlc2l6ZSA9IGVsZW1lbnQuZmluZChcIj4gLm0tbWVudV9fbmF2ID4gLm0tbWVudV9faXRlbS0tcmVzaXplXCIpO1xuICAgICAgICAgICAgICAgIHZhciBzdWJtZW51ID0gcmVzaXplLmZpbmQoXCI+IC5tLW1lbnVfX3N1Ym1lbnVcIik7XG4gICAgICAgICAgICAgICAgdmFyIGJyZWFrcG9pbnQ7XG4gICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRXaWR0aCA9IG1VdGlsLmdldFZpZXdQb3J0KCkud2lkdGg7XG4gICAgICAgICAgICAgICAgdmFyIGl0ZW1zTnVtYmVyID0gZWxlbWVudC5maW5kKFwiPiAubS1tZW51X19uYXYgPiAubS1tZW51X19pdGVtXCIpLmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAgICAgdmFyIGNoZWNrO1xuXG4gICAgICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgICAgICBQbHVnaW4uZ2V0U3VibWVudU1vZGUoKSA9PSBcImRyb3Bkb3duXCIgJiZcbiAgICAgICAgICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgICAgICAgICAgKG1VdGlsLmlzSW5SZXNwb25zaXZlUmFuZ2UoXCJkZXNrdG9wXCIpICYmIG1VdGlsLmlzc2V0KG1lbnUub3B0aW9ucywgXCJyZXNpemUuZGVza3RvcFwiKSAmJiAoY2hlY2sgPSBtZW51Lm9wdGlvbnMucmVzaXplLmRlc2t0b3ApICYmIGN1cnJlbnRXaWR0aCA8PSAoYnJlYWtwb2ludCA9IHJlc2l6ZS5kYXRhKFwibWVudS1yZXNpemUtZGVza3RvcC1icmVha3BvaW50XCIpKSkgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKFwidGFibGV0XCIpICYmIG1VdGlsLmlzc2V0KG1lbnUub3B0aW9ucywgXCJyZXNpemUudGFibGV0XCIpICYmIChjaGVjayA9IG1lbnUub3B0aW9ucy5yZXNpemUudGFibGV0KSAmJiBjdXJyZW50V2lkdGggPD0gKGJyZWFrcG9pbnQgPSByZXNpemUuZGF0YShcIm1lbnUtcmVzaXplLXRhYmxldC1icmVha3BvaW50XCIpKSkgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKFwibW9iaWxlXCIpICYmIG1VdGlsLmlzc2V0KG1lbnUub3B0aW9ucywgXCJyZXNpemUubW9iaWxlXCIpICYmIChjaGVjayA9IG1lbnUub3B0aW9ucy5yZXNpemUubW9iaWxlKSAmJiBjdXJyZW50V2lkdGggPD0gKGJyZWFrcG9pbnQgPSByZXNpemUuZGF0YShcIm1lbnUtcmVzaXplLW1vYmlsZS1icmVha3BvaW50XCIpKSlcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICkge1xuXG4gICAgICAgICAgICAgICAgICAgIHZhciBtb3ZlZCA9IHN1Ym1lbnUuZmluZChcIj4gLm0tbWVudV9fc3VibmF2ID4gLm0tbWVudV9faXRlbVwiKS5sZW5ndGg7IC8vIGN1cnJlbnRseSBtb3ZlXG4gICAgICAgICAgICAgICAgICAgIHZhciBsZWZ0ID0gZWxlbWVudC5maW5kKFwiPiAubS1tZW51X19uYXYgPiAubS1tZW51X19pdGVtOm5vdCgubS1tZW51X19pdGVtLS1yZXNpemUpXCIpLmxlbmd0aDsgLy8gY3VycmVudGx5IGxlZnRcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRvdGFsID0gbW92ZWQgKyBsZWZ0O1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChjaGVjay5hcHBseSgpID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtb3ZlZCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWJtZW51LmZpbmQoXCI+IC5tLW1lbnVfX3N1Ym5hdiA+IC5tLW1lbnVfX2l0ZW1cIikuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGl0ZW0gPSAkKHRoaXMpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50c051bWJlciA9IHN1Ym1lbnUuZmluZChcIj4gLm0tbWVudV9fbmF2ID4gLm0tbWVudV9faXRlbTpub3QoLm0tbWVudV9faXRlbS0tcmVzaXplKVwiKS5sZW5ndGg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZmluZChcIj4gLm0tbWVudV9fbmF2ID4gLm0tbWVudV9faXRlbTpub3QoLm0tbWVudV9faXRlbS0tcmVzaXplKVwiKS5lcShlbGVtZW50c051bWJlciAtIDEpLmFmdGVyKGl0ZW0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjaGVjay5hcHBseSgpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbS5hcHBlbmRUbyhzdWJtZW51LmZpbmQoXCI+IC5tLW1lbnVfX3N1Ym5hdlwiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb3ZlZC0tO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0Kys7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBtb3ZlXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobGVmdCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaXRlbXMgPSBlbGVtZW50LmZpbmQoXCI+IC5tLW1lbnVfX25hdiA+IC5tLW1lbnVfX2l0ZW06bm90KC5tLW1lbnVfX2l0ZW0tLXJlc2l6ZSlcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGluZGV4ID0gaXRlbXMubGVuZ3RoIC0gMTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaXRlbXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGl0ZW0gPSAkKGl0ZW1zLmdldChpbmRleCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleC0tO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjaGVjay5hcHBseSgpID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0uYXBwZW5kVG8oc3VibWVudS5maW5kKFwiPiAubS1tZW51X19zdWJuYXZcIikpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVkKys7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQtLTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAobW92ZWQgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNpemUuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzaXplLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHN1Ym1lbnUuZmluZChcIj4gLm0tbWVudV9fc3VibmF2ID4gLm0tbWVudV9faXRlbVwiKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnRzTnVtYmVyID0gc3VibWVudS5maW5kKFwiPiAubS1tZW51X19zdWJuYXYgPiAubS1tZW51X19pdGVtXCIpLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZmluZChcIj4gLm0tbWVudV9fbmF2ID4gLm0tbWVudV9faXRlbVwiKS5nZXQoZWxlbWVudHNOdW1iZXIpLmFmdGVyKCQodGhpcykpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICByZXNpemUuaGlkZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBzdWJtZW51IHNsaWRlIHRvZ2dsZVxuICAgICAgICAgICAgICogQHJldHVybnMge21NZW51fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBjcmVhdGVTdWJtZW51RHJvcGRvd25DbGlja0Ryb3BvZmY6IGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgICAgICAgICAgdmFyIHpJbmRleCA9IGVsLmZpbmQoXCI+IC5tLW1lbnVfX3N1Ym1lbnVcIikuY3NzKFwiekluZGV4XCIpIC0gMTtcbiAgICAgICAgICAgICAgICB2YXIgZHJvcG9mZiA9ICQoXCI8ZGl2IGNsYXNzPVxcXCJtLW1lbnVfX2Ryb3BvZmZcXFwiIHN0eWxlPVxcXCJiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDsgcG9zaXRpb246IGZpeGVkOyB0b3A6IDA7IGJvdHRvbTogMDsgbGVmdDogMDsgcmlnaHQ6IDA7IHotaW5kZXg6IFwiICsgekluZGV4ICsgXCJcXFwiPjwvZGl2PlwiKTtcbiAgICAgICAgICAgICAgICAkKFwiYm9keVwiKS5hZnRlcihkcm9wb2ZmKTtcbiAgICAgICAgICAgICAgICBkcm9wb2ZmLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICAgICQodGhpcykucmVtb3ZlKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoMSk7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5oaWRlU3VibWVudURyb3Bkb3duKGVsLCB0cnVlKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBzdWJtZW51IGNsaWNrIHRvZ2dsZVxuICAgICAgICAgICAgICogQHJldHVybnMge21NZW51fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBhZGp1c3RTdWJtZW51RHJvcGRvd25BcnJvd1BvczogZnVuY3Rpb24oaXRlbSkge1xuICAgICAgICAgICAgICAgIHZhciBhcnJvdyA9IGl0ZW0uZmluZChcIj4gLm0tbWVudV9fc3VibWVudSA+IC5tLW1lbnVfX2Fycm93Lm0tbWVudV9fYXJyb3ctLWFkanVzdFwiKTtcbiAgICAgICAgICAgICAgICB2YXIgc3VibWVudSA9IGl0ZW0uZmluZChcIj4gLm0tbWVudV9fc3VibWVudVwiKTtcbiAgICAgICAgICAgICAgICB2YXIgc3VibmF2ID0gaXRlbS5maW5kKFwiPiAubS1tZW51X19zdWJtZW51ID4gLm0tbWVudV9fc3VibmF2XCIpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGFycm93Lmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHBvcztcbiAgICAgICAgICAgICAgICAgICAgdmFyIGxpbmsgPSBpdGVtLmNoaWxkcmVuKFwiLm0tbWVudV9fbGlua1wiKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoc3VibWVudS5oYXNDbGFzcyhcIm0tbWVudV9fc3VibWVudS0tY2xhc3NpY1wiKSB8fCBzdWJtZW51Lmhhc0NsYXNzKFwibS1tZW51X19zdWJtZW51LS1maXhlZFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN1Ym1lbnUuaGFzQ2xhc3MoXCJtLW1lbnVfX3N1Ym1lbnUtLXJpZ2h0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zID0gaXRlbS5vdXRlcldpZHRoKCkgLyAyO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdWJtZW51Lmhhc0NsYXNzKFwibS1tZW51X19zdWJtZW51LS1wdWxsXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvcyA9IHBvcyArIE1hdGguYWJzKHBhcnNlSW50KHN1Ym1lbnUuY3NzKFwibWFyZ2luLXJpZ2h0XCIpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvcyA9IHN1Ym1lbnUud2lkdGgoKSAtIHBvcztcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3VibWVudS5oYXNDbGFzcyhcIm0tbWVudV9fc3VibWVudS0tbGVmdFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvcyA9IGl0ZW0ub3V0ZXJXaWR0aCgpIC8gMjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3VibWVudS5oYXNDbGFzcyhcIm0tbWVudV9fc3VibWVudS0tcHVsbFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3MgPSBwb3MgKyBNYXRoLmFicyhwYXJzZUludChzdWJtZW51LmNzcyhcIm1hcmdpbi1sZWZ0XCIpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN1Ym1lbnUuaGFzQ2xhc3MoXCJtLW1lbnVfX3N1Ym1lbnUtLWNlbnRlclwiKSB8fCBzdWJtZW51Lmhhc0NsYXNzKFwibS1tZW51X19zdWJtZW51LS1mdWxsXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zID0gaXRlbS5vZmZzZXQoKS5sZWZ0IC0gKChtVXRpbC5nZXRWaWV3UG9ydCgpLndpZHRoIC0gc3VibWVudS5vdXRlcldpZHRoKCkpIC8gMik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zID0gcG9zICsgKGl0ZW0ub3V0ZXJXaWR0aCgpIC8gMik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN1Ym1lbnUuaGFzQ2xhc3MoXCJtLW1lbnVfX3N1Ym1lbnUtLWxlZnRcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0byBkb1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdWJtZW51Lmhhc0NsYXNzKFwibS1tZW51X19zdWJtZW51LS1yaWdodFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRvIGRvXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBhcnJvdy5jc3MoXCJsZWZ0XCIsIHBvcyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIHN1Ym1lbnUgaG92ZXIgdG9nZ2xlXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHBhdXNlRHJvcGRvd25Ib3ZlcjogZnVuY3Rpb24odGltZSkge1xuICAgICAgICAgICAgICAgIHZhciBkYXRlID0gbmV3IERhdGUoKTtcblxuICAgICAgICAgICAgICAgIG1lbnUucGF1c2VEcm9wZG93bkhvdmVyVGltZSA9IGRhdGUuZ2V0VGltZSgpICsgdGltZTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBzdWJtZW51IGhvdmVyIHRvZ2dsZVxuICAgICAgICAgICAgICogQHJldHVybnMge21NZW51fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICByZXN1bWVEcm9wZG93bkhvdmVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgZGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gKGRhdGUuZ2V0VGltZSgpID4gbWVudS5wYXVzZURyb3Bkb3duSG92ZXJUaW1lID8gdHJ1ZSA6IGZhbHNlKTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogUmVzZXQgbWVudSdzIGN1cnJlbnQgYWN0aXZlIGl0ZW1cbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttTWVudX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgcmVzZXRBY3RpdmVJdGVtOiBmdW5jdGlvbihpdGVtKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5maW5kKFwiLm0tbWVudV9faXRlbS0tYWN0aXZlXCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLWFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5jaGlsZHJlbihcIi5tLW1lbnVfX3N1Ym1lbnVcIikuY3NzKFwiZGlzcGxheVwiLCBcIlwiKTtcblxuICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnBhcmVudHMoXCIubS1tZW51X19pdGVtLS1zdWJtZW51XCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwibS1tZW51X19pdGVtLS1vcGVuXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5jaGlsZHJlbihcIi5tLW1lbnVfX3N1Ym1lbnVcIikuY3NzKFwiZGlzcGxheVwiLCBcIlwiKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAvLyBjbG9zZSBvcGVuIHN1Ym1lbnVzXG4gICAgICAgICAgICAgICAgaWYgKG1lbnUub3B0aW9ucy5hY2NvcmRpb24uZXhwYW5kQWxsID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmZpbmQoXCIubS1tZW51X19pdGVtLS1vcGVuXCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwibS1tZW51X19pdGVtLS1vcGVuXCIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNldHMgbWVudSdzIGFjdGl2ZSBpdGVtXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7bU1lbnV9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldEFjdGl2ZUl0ZW06IGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAvLyByZXNldCBjdXJyZW50IGFjdGl2ZSBpdGVtXG4gICAgICAgICAgICAgICAgUGx1Z2luLnJlc2V0QWN0aXZlSXRlbSgpO1xuXG4gICAgICAgICAgICAgICAgdmFyIGl0ZW0gPSAkKGl0ZW0pO1xuICAgICAgICAgICAgICAgIGl0ZW0uYWRkQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLWFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICBpdGVtLnBhcmVudHMoXCIubS1tZW51X19pdGVtLS1zdWJtZW51XCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJtLW1lbnVfX2l0ZW0tLW9wZW5cIik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJldHVybnMgcGFnZSBicmVhZGNydW1icyBmb3IgdGhlIG1lbnUncyBhY3RpdmUgaXRlbVxuICAgICAgICAgICAgICogQHJldHVybnMge21NZW51fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBnZXRCcmVhZGNydW1iczogZnVuY3Rpb24oaXRlbSkge1xuICAgICAgICAgICAgICAgIHZhciBicmVhZGNydW1icyA9IFtdO1xuICAgICAgICAgICAgICAgIHZhciBpdGVtID0gJChpdGVtKTtcbiAgICAgICAgICAgICAgICB2YXIgbGluayA9IGl0ZW0uY2hpbGRyZW4oXCIubS1tZW51X19saW5rXCIpO1xuXG4gICAgICAgICAgICAgICAgYnJlYWRjcnVtYnMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IGxpbmsuZmluZChcIi5tLW1lbnVfX2xpbmstdGV4dFwiKS5odG1sKCksXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBsaW5rLmF0dHIoXCJ0aXRsZVwiKSxcbiAgICAgICAgICAgICAgICAgICAgaHJlZjogbGluay5hdHRyKFwiaHJlZlwiKSxcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIGl0ZW0ucGFyZW50cyhcIi5tLW1lbnVfX2l0ZW0tLXN1Ym1lbnVcIikuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHN1Ym1lbnVMaW5rID0gJCh0aGlzKS5jaGlsZHJlbihcIi5tLW1lbnVfX2xpbmtcIik7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFkY3J1bWJzLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogc3VibWVudUxpbmsuZmluZChcIi5tLW1lbnVfX2xpbmstdGV4dFwiKS5odG1sKCksXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogc3VibWVudUxpbmsuYXR0cihcInRpdGxlXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgaHJlZjogc3VibWVudUxpbmsuYXR0cihcImhyZWZcIiksXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgYnJlYWRjcnVtYnMucmV2ZXJzZSgpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGJyZWFkY3J1bWJzO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBSZXR1cm5zIHBhZ2UgdGl0bGUgZm9yIHRoZSBtZW51J3MgYWN0aXZlIGl0ZW1cbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttTWVudX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZ2V0UGFnZVRpdGxlOiBmdW5jdGlvbihpdGVtKSB7XG4gICAgICAgICAgICAgICAgaXRlbSA9ICQoaXRlbSk7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gaXRlbS5jaGlsZHJlbihcIi5tLW1lbnVfX2xpbmtcIikuZmluZChcIi5tLW1lbnVfX2xpbmstdGV4dFwiKS5odG1sKCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFN5bmNcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgc3luYzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJChlbGVtZW50KS5kYXRhKFwibWVudVwiLCBtZW51KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogVHJpZ2dlciBldmVudHNcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZXZlbnRUcmlnZ2VyOiBmdW5jdGlvbihuYW1lLCBhcmdzKSB7XG4gICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IG1lbnUuZXZlbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBldmVudCA9IG1lbnUuZXZlbnRzW2ldO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQubmFtZSA9PSBuYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQub25lID09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQuZmlyZWQgPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVudS5ldmVudHNbaV0uZmlyZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZXZlbnQuaGFuZGxlci5jYWxsKHRoaXMsIG1lbnUsIGFyZ3MpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50LmhhbmRsZXIuY2FsbCh0aGlzLCBtZW51LCBhcmdzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGFkZEV2ZW50OiBmdW5jdGlvbihuYW1lLCBoYW5kbGVyLCBvbmUpIHtcbiAgICAgICAgICAgICAgICBtZW51LmV2ZW50cy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcjogaGFuZGxlcixcbiAgICAgICAgICAgICAgICAgICAgb25lOiBvbmUsXG4gICAgICAgICAgICAgICAgICAgIGZpcmVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIFBsdWdpbi5zeW5jKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIFJ1biBwbHVnaW5cbiAgICAgICAgUGx1Z2luLnJ1bi5hcHBseShtZW51LCBbb3B0aW9uc10pO1xuXG4gICAgICAgIC8vIEhhbmRsZSBwbHVnaW4gb24gd2luZG93IHJlc2l6ZVxuICAgICAgICBpZiAodHlwZW9mKG9wdGlvbnMpICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIFBsdWdpbi5ydW4uYXBwbHkobWVudSwgW29wdGlvbnMsIHRydWVdKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgICAgICAvLyAqKiBQdWJsaWMgQVBJICoqIC8vXG4gICAgICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgICAgICAvKipcbiAgICAgICAgICogU2V0IGFjdGl2ZSBtZW51IGl0ZW1cbiAgICAgICAgICovXG4gICAgICAgIG1lbnUuc2V0QWN0aXZlSXRlbSA9IGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uc2V0QWN0aXZlSXRlbShpdGVtKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2V0IGJyZWFkY3J1bWIgZm9yIG1lbnUgaXRlbVxuICAgICAgICAgKi9cbiAgICAgICAgbWVudS5nZXRCcmVhZGNydW1icyA9IGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uZ2V0QnJlYWRjcnVtYnMoaXRlbSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldCBwYWdlIHRpdGxlIGZvciBtZW51IGl0ZW1cbiAgICAgICAgICovXG4gICAgICAgIG1lbnUuZ2V0UGFnZVRpdGxlID0gZnVuY3Rpb24oaXRlbSkge1xuICAgICAgICAgICAgcmV0dXJuIFBsdWdpbi5nZXRQYWdlVGl0bGUoaXRlbSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEdldCBzdWJtZW51IG1vZGVcbiAgICAgICAgICovXG4gICAgICAgIG1lbnUuZ2V0U3VibWVudU1vZGUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uZ2V0U3VibWVudU1vZGUoKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogSGlkZSBkcm9wZG93biBzdWJtZW51XG4gICAgICAgICAqIEByZXR1cm5zIHtqUXVlcnl9XG4gICAgICAgICAqL1xuICAgICAgICBtZW51LmhpZGVEcm9wZG93biA9IGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgICAgIFBsdWdpbi5oaWRlU3VibWVudURyb3Bkb3duKGl0ZW0sIHRydWUpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBEaXNhYmxlIG1lbnUgZm9yIGdpdmVuIHRpbWVcbiAgICAgICAgICogQHJldHVybnMge2pRdWVyeX1cbiAgICAgICAgICovXG4gICAgICAgIG1lbnUucGF1c2VEcm9wZG93bkhvdmVyID0gZnVuY3Rpb24odGltZSkge1xuICAgICAgICAgICAgUGx1Z2luLnBhdXNlRHJvcGRvd25Ib3Zlcih0aW1lKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogRGlzYWJsZSBtZW51IGZvciBnaXZlbiB0aW1lXG4gICAgICAgICAqIEByZXR1cm5zIHtqUXVlcnl9XG4gICAgICAgICAqL1xuICAgICAgICBtZW51LnJlc3VtZURyb3Bkb3duSG92ZXIgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4ucmVzdW1lRHJvcGRvd25Ib3ZlcigpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZWdpc3RlciBldmVudFxuICAgICAgICAgKi9cbiAgICAgICAgbWVudS5vbiA9IGZ1bmN0aW9uKG5hbWUsIGhhbmRsZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uYWRkRXZlbnQobmFtZSwgaGFuZGxlcik7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gUmV0dXJuIHBsdWdpbiBpbnN0YW5jZVxuICAgICAgICByZXR1cm4gbWVudTtcbiAgICB9O1xuXG4gICAgLy8gUGx1Z2luIGRlZmF1bHQgb3B0aW9uc1xuICAgICQuZm4ubU1lbnUuZGVmYXVsdHMgPSB7XG4gICAgICAgIC8vIGFjY29yZGlvbiBzdWJtZW51IG1vZGVcbiAgICAgICAgYWNjb3JkaW9uOiB7XG4gICAgICAgICAgICBzbGlkZVNwZWVkOiAyMDAsICAvLyBhY2NvcmRpb24gdG9nZ2xlIHNsaWRlIHNwZWVkIGluIG1pbGxpc2Vjb25kc1xuICAgICAgICAgICAgYXV0b1Njcm9sbDogdHJ1ZSwgLy8gZW5hYmxlIGF1dG8gc2Nyb2xsaW5nKGZvY3VzKSB0byB0aGUgY2xpY2tlZCBtZW51IGl0ZW1cbiAgICAgICAgICAgIGV4cGFuZEFsbDogdHJ1ZSwgICAvLyBhbGxvdyBoYXZpbmcgbXVsdGlwbGUgZXhwYW5kZWQgYWNjb3JkaW9ucyBpbiB0aGUgbWVudVxuICAgICAgICB9LFxuXG4gICAgICAgIC8vIGRyb3Bkb3duIHN1Ym1lbnUgbW9kZVxuICAgICAgICBkcm9wZG93bjoge1xuICAgICAgICAgICAgdGltZW91dDogNTAwLCAgLy8gdGltZW91dCBpbiBtaWxsaXNlY29uZHMgdG8gc2hvdyBhbmQgaGlkZSB0aGUgaG92ZXJhYmxlIHN1Ym1lbnUgZHJvcGRvd25cbiAgICAgICAgfSxcbiAgICB9O1xuXG4gICAgLy8gUGx1Z2luIGdsb2JhbCBsYXp5IGluaXRpYWxpemF0aW9uXG4gICAgJChkb2N1bWVudCkub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICQoXCIubS1tZW51X19uYXYgLm0tbWVudV9faXRlbS5tLW1lbnVfX2l0ZW0tLXN1Ym1lbnUubS1tZW51X19pdGVtLS1ob3ZlcltkYXRhLW1lbnUtc3VibWVudS10b2dnbGU9XFxcImNsaWNrXFxcIl1cIikuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gJCh0aGlzKS5wYXJlbnQoXCIubS1tZW51X19uYXZcIikucGFyZW50KCk7XG4gICAgICAgICAgICBsZXQgbWVudSA9IGVsZW1lbnQubU1lbnUoKTtcblxuICAgICAgICAgICAgaWYgKG1lbnUuZ2V0U3VibWVudU1vZGUoKSAhPT0gXCJkcm9wZG93blwiKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoJChlLnRhcmdldCkuaXMoZWxlbWVudCkgPT0gZmFsc2UgJiYgZWxlbWVudC5maW5kKCQoZS50YXJnZXQpKS5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgICAgIHZhciBpdGVtcyA9IGVsZW1lbnQuZmluZChcIi5tLW1lbnVfX2l0ZW0tLXN1Ym1lbnUubS1tZW51X19pdGVtLS1ob3ZlcltkYXRhLW1lbnUtc3VibWVudS10b2dnbGU9XFxcImNsaWNrXFxcIl1cIik7XG4gICAgICAgICAgICAgICAgaXRlbXMuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgbWVudS5oaWRlRHJvcGRvd24oJCh0aGlzKSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufShqUXVlcnkpKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvZG9tYWluL2NvbW1vbi90aGVtZS9qcy9mcmFtZXdvcmsvY29tcG9uZW50cy9nZW5lcmFsL21lbnUuanMiLCIoZnVuY3Rpb24oJCkge1xuICAgIC8vIHBsdWdpbiBzZXR1cFxuICAgICQuZm4ubU9mZmNhbnZhcyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgLy8gbWFpbiBvYmplY3RcbiAgICAgICAgdmFyIG9mZmNhbnZhcyA9IHRoaXM7XG4gICAgICAgIHZhciBlbGVtZW50ID0gJCh0aGlzKTtcblxuICAgICAgICAvKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICoqIFBSSVZBVEUgTUVUSE9EU1xuICAgICAgICAgKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAgIHZhciBQbHVnaW4gPSB7XG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJ1blxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBydW46IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFlbGVtZW50LmRhdGEoJ29mZmNhbnZhcycpKSB7ICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAvLyBjcmVhdGUgaW5zdGFuY2VcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmluaXQob3B0aW9ucyk7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5idWlsZCgpO1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgLy8gYXNzaWduIGluc3RhbmNlIHRvIHRoZSBlbGVtZW50ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5kYXRhKCdvZmZjYW52YXMnLCBvZmZjYW52YXMpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGdldCBpbnN0YW5jZSBmcm9tIHRoZSBlbGVtZW50XG4gICAgICAgICAgICAgICAgICAgIG9mZmNhbnZhcyA9IGVsZW1lbnQuZGF0YSgnb2ZmY2FudmFzJyk7XG4gICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgIFxuXG4gICAgICAgICAgICAgICAgcmV0dXJuIG9mZmNhbnZhcztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBzdWJvZmZjYW52YXMgY2xpY2sgdG9nZ2xlXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICBvZmZjYW52YXMuZXZlbnRzID0gW107XG5cbiAgICAgICAgICAgICAgICAvLyBtZXJnZSBkZWZhdWx0IGFuZCB1c2VyIGRlZmluZWQgb3B0aW9uc1xuICAgICAgICAgICAgICAgIG9mZmNhbnZhcy5vcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sICQuZm4ubU9mZmNhbnZhcy5kZWZhdWx0cywgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgICAgICBvZmZjYW52YXMub3ZlcmxheTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBvZmZjYW52YXMuY2xhc3NCYXNlID0gb2ZmY2FudmFzLm9wdGlvbnMuY2xhc3M7XG4gICAgICAgICAgICAgICAgb2ZmY2FudmFzLmNsYXNzU2hvd24gPSBvZmZjYW52YXMuY2xhc3NCYXNlICsgJy0tb24nO1xuICAgICAgICAgICAgICAgIG9mZmNhbnZhcy5jbGFzc092ZXJsYXkgPSBvZmZjYW52YXMuY2xhc3NCYXNlICsgJy1vdmVybGF5JztcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBvZmZjYW52YXMuc3RhdGUgPSBlbGVtZW50Lmhhc0NsYXNzKG9mZmNhbnZhcy5jbGFzc1Nob3duKSA/ICdzaG93bicgOiAnaGlkZGVuJztcbiAgICAgICAgICAgICAgICBvZmZjYW52YXMuY2xvc2UgPSBvZmZjYW52YXMub3B0aW9ucy5jbG9zZTtcblxuICAgICAgICAgICAgICAgIGlmIChvZmZjYW52YXMub3B0aW9ucy50b2dnbGUgJiYgb2ZmY2FudmFzLm9wdGlvbnMudG9nZ2xlLnRhcmdldCkge1xuICAgICAgICAgICAgICAgICAgICBvZmZjYW52YXMudG9nZ2xlVGFyZ2V0ID0gb2ZmY2FudmFzLm9wdGlvbnMudG9nZ2xlLnRhcmdldDtcbiAgICAgICAgICAgICAgICAgICAgb2ZmY2FudmFzLnRvZ2dsZVN0YXRlID0gb2ZmY2FudmFzLm9wdGlvbnMudG9nZ2xlLnN0YXRlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG9mZmNhbnZhcy50b2dnbGVUYXJnZXQgPSBvZmZjYW52YXMub3B0aW9ucy50b2dnbGU7IFxuICAgICAgICAgICAgICAgICAgICBvZmZjYW52YXMudG9nZ2xlU3RhdGUgPSAnJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNldHVwIG9mZmNhbnZhc1xuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBidWlsZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgLy8gb2ZmY2FudmFzIHRvZ2dsZVxuICAgICAgICAgICAgICAgICQob2ZmY2FudmFzLnRvZ2dsZVRhcmdldCkub24oJ2NsaWNrJywgUGx1Z2luLnRvZ2dsZSk7XG5cbiAgICAgICAgICAgICAgICBpZiAob2ZmY2FudmFzLmNsb3NlKSB7XG4gICAgICAgICAgICAgICAgICAgICQob2ZmY2FudmFzLmNsb3NlKS5vbignY2xpY2snLCBQbHVnaW4uaGlkZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBzeW5jIFxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBzeW5jOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJChlbGVtZW50KS5kYXRhKCdvZmZjYW52YXMnLCBvZmZjYW52YXMpO1xuICAgICAgICAgICAgfSwgXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBvZmZjYW52YXMgY2xpY2sgdG9nZ2xlXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHRvZ2dsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYgKG9mZmNhbnZhcy5zdGF0ZSA9PSAnc2hvd24nKSB7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5oaWRlKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLnNob3coKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEhhbmRsZXMgb2ZmY2FudmFzIGNsaWNrIHRvZ2dsZVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBzaG93OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiAob2ZmY2FudmFzLnN0YXRlID09ICdzaG93bicpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIFBsdWdpbi5ldmVudFRyaWdnZXIoJ2JlZm9yZVNob3cnKTtcblxuICAgICAgICAgICAgICAgIGlmIChvZmZjYW52YXMudG9nZ2xlU3RhdGUgIT0gJycpIHtcbiAgICAgICAgICAgICAgICAgICAgJChvZmZjYW52YXMudG9nZ2xlVGFyZ2V0KS5hZGRDbGFzcyhvZmZjYW52YXMudG9nZ2xlU3RhdGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAkKCdib2R5JykuYWRkQ2xhc3Mob2ZmY2FudmFzLmNsYXNzU2hvd24pO1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuYWRkQ2xhc3Mob2ZmY2FudmFzLmNsYXNzU2hvd24pO1xuXG4gICAgICAgICAgICAgICAgb2ZmY2FudmFzLnN0YXRlID0gJ3Nob3duJztcblxuICAgICAgICAgICAgICAgIGlmIChvZmZjYW52YXMub3B0aW9ucy5vdmVybGF5KSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBvdmVybGF5ID0gJCgnPGRpdiBjbGFzcz1cIicgKyBvZmZjYW52YXMuY2xhc3NPdmVybGF5ICsgJ1wiPjwvZGl2PicpOyAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5hZnRlcihvdmVybGF5KTtcbiAgICAgICAgICAgICAgICAgICAgb2ZmY2FudmFzLm92ZXJsYXkgPSBvdmVybGF5O1xuICAgICAgICAgICAgICAgICAgICBvZmZjYW52YXMub3ZlcmxheS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBcblxuICAgICAgICAgICAgICAgIFBsdWdpbi5ldmVudFRyaWdnZXIoJ2FmdGVyU2hvdycpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIG9mZmNhbnZhcztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBvZmZjYW52YXMgY2xpY2sgdG9nZ2xlXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhpZGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmIChvZmZjYW52YXMuc3RhdGUgPT0gJ2hpZGRlbicpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIFBsdWdpbi5ldmVudFRyaWdnZXIoJ2JlZm9yZUhpZGUnKTtcblxuICAgICAgICAgICAgICAgIGlmIChvZmZjYW52YXMudG9nZ2xlU3RhdGUgIT0gJycpIHtcbiAgICAgICAgICAgICAgICAgICAgJChvZmZjYW52YXMudG9nZ2xlVGFyZ2V0KS5yZW1vdmVDbGFzcyhvZmZjYW52YXMudG9nZ2xlU3RhdGUpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICQoJ2JvZHknKS5yZW1vdmVDbGFzcyhvZmZjYW52YXMuY2xhc3NTaG93bilcbiAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUNsYXNzKG9mZmNhbnZhcy5jbGFzc1Nob3duKTtcblxuICAgICAgICAgICAgICAgIG9mZmNhbnZhcy5zdGF0ZSA9ICdoaWRkZW4nO1xuXG4gICAgICAgICAgICAgICAgaWYgKG9mZmNhbnZhcy5vcHRpb25zLm92ZXJsYXkpIHtcbiAgICAgICAgICAgICAgICAgICAgb2ZmY2FudmFzLm92ZXJsYXkucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgfSBcblxuICAgICAgICAgICAgICAgIFBsdWdpbi5ldmVudFRyaWdnZXIoJ2FmdGVySGlkZScpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIG9mZmNhbnZhcztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogVHJpZ2dlciBldmVudHNcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZXZlbnRUcmlnZ2VyOiBmdW5jdGlvbihuYW1lKSB7XG4gICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IG9mZmNhbnZhcy5ldmVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGV2ZW50ID0gb2ZmY2FudmFzLmV2ZW50c1tpXTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50Lm5hbWUgPT0gbmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50Lm9uZSA9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50LmZpcmVkID09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9mZmNhbnZhcy5ldmVudHNbaV0uZmlyZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZXZlbnQuaGFuZGxlci5jYWxsKHRoaXMsIG9mZmNhbnZhcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gIGV2ZW50LmhhbmRsZXIuY2FsbCh0aGlzLCBvZmZjYW52YXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgYWRkRXZlbnQ6IGZ1bmN0aW9uKG5hbWUsIGhhbmRsZXIsIG9uZSkge1xuICAgICAgICAgICAgICAgIG9mZmNhbnZhcy5ldmVudHMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IG5hbWUsXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZXI6IGhhbmRsZXIsXG4gICAgICAgICAgICAgICAgICAgIG9uZTogb25lLFxuICAgICAgICAgICAgICAgICAgICBmaXJlZDogZmFsc2VcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIFBsdWdpbi5zeW5jKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gbWFpbiB2YXJpYWJsZXNcbiAgICAgICAgdmFyIHRoZSA9IHRoaXM7XG4gICAgICAgIFxuICAgICAgICAvLyBpbml0IHBsdWdpblxuICAgICAgICBQbHVnaW4ucnVuLmFwcGx5KHRoaXMsIFtvcHRpb25zXSk7XG5cbiAgICAgICAgLyoqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAqKiBQVUJMSUMgQVBJIE1FVEhPRFNcbiAgICAgICAgICoqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBIaWRlIFxuICAgICAgICAgKi9cbiAgICAgICAgb2ZmY2FudmFzLmhpZGUgPSAgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIFBsdWdpbi5oaWRlKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNob3cgXG4gICAgICAgICAqL1xuICAgICAgICBvZmZjYW52YXMuc2hvdyA9ICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gUGx1Z2luLnNob3coKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogR2V0IHN1Ym9mZmNhbnZhcyBtb2RlXG4gICAgICAgICAqL1xuICAgICAgICBvZmZjYW52YXMub24gPSAgZnVuY3Rpb24gKG5hbWUsIGhhbmRsZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uYWRkRXZlbnQobmFtZSwgaGFuZGxlcik7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldCBvZmZjYW52YXMgY29udGVudFxuICAgICAgICAgKiBAcmV0dXJucyB7bU9mZmNhbnZhc31cbiAgICAgICAgICovXG4gICAgICAgIG9mZmNhbnZhcy5vbmUgPSAgZnVuY3Rpb24gKG5hbWUsIGhhbmRsZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uYWRkRXZlbnQobmFtZSwgaGFuZGxlciwgdHJ1ZSk7XG4gICAgICAgIH07ICAgXG5cbiAgICAgICAgcmV0dXJuIG9mZmNhbnZhcztcbiAgICB9O1xuXG4gICAgLy8gZGVmYXVsdCBvcHRpb25zXG4gICAgJC5mbi5tT2ZmY2FudmFzLmRlZmF1bHRzID0ge1xuICAgICAgICBcbiAgICB9OyBcbn0oalF1ZXJ5KSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvb2ZmY2FudmFzLmpzIiwiKGZ1bmN0aW9uKCQpIHtcbiAgICAvLyBQbHVnaW4gZnVuY3Rpb25cbiAgICAkLmZuLm1RdWlja3NlYXJjaCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcblxuICAgICAgICAvLyBQbHVnaW4gc2NvcGUgdmFyaWFibGVzXG4gICAgICAgIHZhciBxcyA9IHRoaXM7XG4gICAgICAgIHZhciBlbGVtZW50ID0gJCh0aGlzKTtcbiAgICAgICAgXG4gICAgICAgIC8vIFBsdWdpbiBjbGFzcyAgICAgICAgXG4gICAgICAgIHZhciBQbHVnaW4gPSB7XG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJ1biBwbHVnaW4gXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHJ1bjogZnVuY3Rpb24ob3B0aW9ucykge1xuICAgICAgICAgICAgICAgIGlmICghZWxlbWVudC5kYXRhKCdxcycpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGluaXQgcGx1Z2luXG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5pbml0KG9wdGlvbnMpO1xuICAgICAgICAgICAgICAgICAgICAvLyBidWlsZCBkb21cbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmJ1aWxkKCk7ICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAvLyBzdG9yZSB0aGUgaW5zdGFuY2UgaW4gdGhlIGVsZW1lbnQncyBkYXRhXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZGF0YSgncXMnLCBxcyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gcmV0cmlldmUgdGhlIGluc3RhbmNlIGZybyB0aGUgZWxlbWVudCdzIGRhdGFcbiAgICAgICAgICAgICAgICAgICAgcXMgPSBlbGVtZW50LmRhdGEoJ3FzJyk7IFxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiBxcztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSW5pdCBwbHVnaW5cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaW5pdDogZnVuY3Rpb24ob3B0aW9ucykge1xuICAgICAgICAgICAgICAgIC8vIG1lcmdlIGRlZmF1bHQgYW5kIHVzZXIgZGVmaW5lZCBvcHRpb25zXG4gICAgICAgICAgICAgICAgcXMub3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLm1RdWlja3NlYXJjaC5kZWZhdWx0cywgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgICAgICAvLyBmb3JtXG4gICAgICAgICAgICAgICAgcXMuZm9ybSA9IGVsZW1lbnQuZmluZCgnZm9ybScpO1xuXG4gICAgICAgICAgICAgICAgLy8gaW5wdXQgZWxlbWVudFxuICAgICAgICAgICAgICAgIHFzLmlucHV0ID0gJChxcy5vcHRpb25zLmlucHV0KTtcblxuICAgICAgICAgICAgICAgICAvLyBjbG9zZSBpY29uXG4gICAgICAgICAgICAgICAgcXMuaWNvbkNsb3NlID0gJChxcy5vcHRpb25zLmljb25DbG9zZSk7XG5cbiAgICAgICAgICAgICAgICBpZiAocXMub3B0aW9ucy50eXBlID09ICdkZWZhdWx0Jykge1xuICAgICAgICAgICAgICAgICAgICAvLyBzZWFyY2ggaWNvblxuICAgICAgICAgICAgICAgICAgICBxcy5pY29uU2VhcmNoID0gJChxcy5vcHRpb25zLmljb25TZWFyY2gpO1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIC8vIGNhbmNlbCBpY29uXG4gICAgICAgICAgICAgICAgICAgIHFzLmljb25DYW5jZWwgPSAkKHFzLm9wdGlvbnMuaWNvbkNhbmNlbCk7XG4gICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgIFxuXG4gICAgICAgICAgICAgICAgLy8gZHJvcGRvd25cbiAgICAgICAgICAgICAgICBxcy5kcm9wZG93biA9IGVsZW1lbnQubURyb3Bkb3duKHttb2JpbGVPdmVybGF5OiBmYWxzZX0pO1xuXG4gICAgICAgICAgICAgICAgLy8gY2FuY2VsIHNlYXJjaCB0aW1lb3V0XG4gICAgICAgICAgICAgICAgcXMuY2FuY2VsVGltZW91dDtcblxuICAgICAgICAgICAgICAgIC8vIGFqYXggcHJvY2Vzc2luZyBzdGF0ZVxuICAgICAgICAgICAgICAgIHFzLnByb2Nlc3NpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0sIFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEJ1aWxkIHBsdWdpblxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBidWlsZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgLy8gYXR0YWNoIGlucHV0IGtleXVwIGhhbmRsZXJcbiAgICAgICAgICAgICAgICBxcy5pbnB1dC5rZXl1cChQbHVnaW4uaGFuZGxlU2VhcmNoKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAocXMub3B0aW9ucy50eXBlID09ICdkZWZhdWx0Jykge1xuICAgICAgICAgICAgICAgICAgICBxcy5pbnB1dC5mb2N1cyhQbHVnaW4uc2hvd0Ryb3Bkb3duKTtcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHFzLmljb25DYW5jZWwuY2xpY2soUGx1Z2luLmhhbmRsZUNhbmNlbCk7XG5cbiAgICAgICAgICAgICAgICAgICAgcXMuaWNvblNlYXJjaC5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKCd0YWJsZXQtYW5kLW1vYmlsZScpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnYm9keScpLmFkZENsYXNzKCdtLWhlYWRlci1zZWFyY2gtLW1vYmlsZS1leHBhbmRlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHFzLmlucHV0LmZvY3VzKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHFzLmljb25DbG9zZS5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtVXRpbC5pc0luUmVzcG9uc2l2ZVJhbmdlKCd0YWJsZXQtYW5kLW1vYmlsZScpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnYm9keScpLnJlbW92ZUNsYXNzKCdtLWhlYWRlci1zZWFyY2gtLW1vYmlsZS1leHBhbmRlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBsdWdpbi5jbG9zZURyb3Bkb3duKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChxcy5vcHRpb25zLnR5cGUgPT0gJ2Ryb3Bkb3duJykge1xuICAgICAgICAgICAgICAgICAgICBxcy5kcm9wZG93bi5vbignYWZ0ZXJTaG93JywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBxcy5pbnB1dC5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcXMuaWNvbkNsb3NlLmNsaWNrKFBsdWdpbi5jbG9zZURyb3Bkb3duKTtcbiAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNlYXJjaCBoYW5kbGVyXG4gICAgICAgICAgICAgKi8gXG4gICAgICAgICAgICBoYW5kbGVTZWFyY2g6IGZ1bmN0aW9uKGUpIHsgXG4gICAgICAgICAgICAgICAgdmFyIHF1ZXJ5ID0gcXMuaW5wdXQudmFsKCk7XG5cbiAgICAgICAgICAgICAgICBpZiAocXVlcnkubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHFzLmRyb3Bkb3duLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmhhbmRsZUNhbmNlbEljb25WaXNpYmlsaXR5KCdvbicpO1xuICAgICAgICAgICAgICAgICAgICBQbHVnaW4uY2xvc2VEcm9wZG93bigpO1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUNsYXNzKHFzLm9wdGlvbnMuaGFzUmVzdWx0Q2xhc3MpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChxdWVyeS5sZW5ndGggPCBxcy5vcHRpb25zLm1pbkxlbmd0aCB8fCBxcy5wcm9jZXNzaW5nID09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHFzLnByb2Nlc3NpbmcgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHFzLmZvcm0uYWRkQ2xhc3MocXMub3B0aW9ucy5zcGlubmVyKTtcbiAgICAgICAgICAgICAgICBQbHVnaW4uaGFuZGxlQ2FuY2VsSWNvblZpc2liaWxpdHkoJ29mZicpO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgIHVybDogcXMub3B0aW9ucy5zb3VyY2UsXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtxdWVyeTogcXVlcnl9LFxuICAgICAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ2h0bWwnLFxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbihyZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHFzLnByb2Nlc3NpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHFzLmZvcm0ucmVtb3ZlQ2xhc3MocXMub3B0aW9ucy5zcGlubmVyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIFBsdWdpbi5oYW5kbGVDYW5jZWxJY29uVmlzaWJpbGl0eSgnb24nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHFzLmRyb3Bkb3duLnNldENvbnRlbnQocmVzKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmFkZENsYXNzKHFzLm9wdGlvbnMuaGFzUmVzdWx0Q2xhc3MpOyAgICBcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKHJlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcXMucHJvY2Vzc2luZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgcXMuZm9ybS5yZW1vdmVDbGFzcyhxcy5vcHRpb25zLnNwaW5uZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmhhbmRsZUNhbmNlbEljb25WaXNpYmlsaXR5KCdvbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcXMuZHJvcGRvd24uc2V0Q29udGVudChxcy5vcHRpb25zLnRlbXBsYXRlcy5lcnJvci5hcHBseShxcywgcmVzKSkuc2hvdygpOyAgXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmFkZENsYXNzKHFzLm9wdGlvbnMuaGFzUmVzdWx0Q2xhc3MpOyAgIFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LCBcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGUgY2FuY2VsIGljb24gdmlzaWJpbGl0eVxuICAgICAgICAgICAgICovIFxuICAgICAgICAgICAgaGFuZGxlQ2FuY2VsSWNvblZpc2liaWxpdHk6IGZ1bmN0aW9uKHN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChxcy5vcHRpb25zLnR5cGUgPT0gJ2Ryb3Bkb3duJykge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHN0YXR1cyA9PSAnb24nKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChxcy5pbnB1dC52YWwoKS5sZW5ndGggPT09IDApIHsgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgcXMuaWNvbkNhbmNlbC5jc3MoJ3Zpc2liaWxpdHknLCAnaGlkZGVuJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBxcy5pY29uQ2xvc2UuY3NzKCd2aXNpYmlsaXR5JywgJ2hpZGRlbicpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHFzLmNhbmNlbFRpbWVvdXQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcXMuY2FuY2VsVGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcXMuaWNvbkNhbmNlbC5jc3MoJ3Zpc2liaWxpdHknLCAndmlzaWJsZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHFzLmljb25DbG9zZS5jc3MoJ3Zpc2liaWxpdHknLCAndmlzaWJsZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSwgNTAwKTsgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHFzLmljb25DYW5jZWwuY3NzKCd2aXNpYmlsaXR5JywgJ2hpZGRlbicpO1xuICAgICAgICAgICAgICAgICAgICBxcy5pY29uQ2xvc2UuY3NzKCd2aXNpYmlsaXR5JywgJ2hpZGRlbicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQ2FuY2VsIGhhbmRsZXJcbiAgICAgICAgICAgICAqLyBcbiAgICAgICAgICAgIGhhbmRsZUNhbmNlbDogZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAgIHFzLmlucHV0LnZhbCgnJyk7XG4gICAgICAgICAgICAgICAgcXMuaWNvbkNhbmNlbC5jc3MoJ3Zpc2liaWxpdHknLCAnaGlkZGVuJyk7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVDbGFzcyhxcy5vcHRpb25zLmhhc1Jlc3VsdENsYXNzKTsgICBcbiAgICAgICAgICAgICAgICAvL3FzLmlucHV0LmZvY3VzKCk7XG5cbiAgICAgICAgICAgICAgICBQbHVnaW4uY2xvc2VEcm9wZG93bigpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBDYW5jZWwgaGFuZGxlclxuICAgICAgICAgICAgICovIFxuICAgICAgICAgICAgY2xvc2VEcm9wZG93bjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgcXMuZHJvcGRvd24uaGlkZSgpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBTaG93IGRyb3Bkb3duXG4gICAgICAgICAgICAgKi8gXG4gICAgICAgICAgICBzaG93RHJvcGRvd246IGZ1bmN0aW9uKGUpIHsgXG4gICAgICAgICAgICAgICAgaWYgKHFzLmRyb3Bkb3duLmlzU2hvd24oKSA9PSBmYWxzZSAmJiBxcy5pbnB1dC52YWwoKS5sZW5ndGggPiBxcy5vcHRpb25zLm1pbkxlbmd0aCAmJiBxcy5wcm9jZXNzaW5nID09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHFzLmRyb3Bkb3duLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAvLyBSdW4gcGx1Z2luXG4gICAgICAgIFBsdWdpbi5ydW4uYXBwbHkocXMsIFtvcHRpb25zXSk7XG5cbiAgICAgICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgICAgICAvLyAqKiBQdWJsaWMgQVBJICoqIC8vXG4gICAgICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgICAgICAvKipcbiAgICAgICAgICogUHVibGljIG1ldGhvZFxuICAgICAgICAgKiBAcmV0dXJucyB7bVF1aWNrc2VhcmNofVxuICAgICAgICAgKi9cbiAgICAgICAgcXMudGVzdCA9IGZ1bmN0aW9uKHRpbWUpIHtcbiAgICAgICAgXHQvL1BsdWdpbi5tZXRob2QodGltZSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gUmV0dXJuIHBsdWdpbiBvYmplY3RcbiAgICAgICAgcmV0dXJuIHFzO1xuICAgIH07XG5cbiAgICAvLyBQbHVnaW4gZGVmYXVsdCBvcHRpb25zXG4gICAgJC5mbi5tUXVpY2tzZWFyY2guZGVmYXVsdHMgPSB7XG4gICAgXHRtaW5MZW5ndGg6IDEsXG4gICAgICAgIG1heEhlaWdodDogMzAwLFxuICAgIH07XG5cbn0oalF1ZXJ5KSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvcXVpY2tzZWFyY2guanMiLCIoZnVuY3Rpb24oJCkge1xuICAgIC8vIHBsdWdpbiBzZXR1cFxuICAgICQuZm4ubVNjcm9sbFRvcCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgLy8gbWFpbiBvYmplY3RcbiAgICAgICAgdmFyIHNjcm9sbFRvcCA9IHRoaXM7XG4gICAgICAgIHZhciBlbGVtZW50ID0gJCh0aGlzKTtcblxuICAgICAgICAvKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICoqIFBSSVZBVEUgTUVUSE9EU1xuICAgICAgICAgKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAgIHZhciBQbHVnaW4gPSB7XG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJ1blxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBydW46IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFlbGVtZW50LmRhdGEoJ3Njcm9sbFRvcCcpKSB7ICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAvLyBjcmVhdGUgaW5zdGFuY2VcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmluaXQob3B0aW9ucyk7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi5idWlsZCgpO1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgLy8gYXNzaWduIGluc3RhbmNlIHRvIHRoZSBlbGVtZW50ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5kYXRhKCdzY3JvbGxUb3AnLCBzY3JvbGxUb3ApO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGdldCBpbnN0YW5jZSBmcm9tIHRoZSBlbGVtZW50XG4gICAgICAgICAgICAgICAgICAgIHNjcm9sbFRvcCA9IGVsZW1lbnQuZGF0YSgnc2Nyb2xsVG9wJyk7XG4gICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgIFxuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHNjcm9sbFRvcDtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBzdWJzY3JvbGxUb3AgY2xpY2sgc2Nyb2xsVG9wXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3AuZWxlbWVudCA9IGVsZW1lbnQ7ICAgIFxuICAgICAgICAgICAgICAgIHNjcm9sbFRvcC5ldmVudHMgPSBbXTtcblxuICAgICAgICAgICAgICAgIC8vIG1lcmdlIGRlZmF1bHQgYW5kIHVzZXIgZGVmaW5lZCBvcHRpb25zXG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wLm9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5tU2Nyb2xsVG9wLmRlZmF1bHRzLCBvcHRpb25zKTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogU2V0dXAgc2Nyb2xsVG9wXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGJ1aWxkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAvLyBoYW5kbGUgd2luZG93IHNjcm9sbFxuICAgICAgICAgICAgICAgIGlmIChuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9pUGhvbmV8aVBhZHxpUG9kL2kpKSB7XG4gICAgICAgICAgICAgICAgICAgICQod2luZG93KS5iaW5kKFwidG91Y2hlbmQgdG91Y2hjYW5jZWwgdG91Y2hsZWF2ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFBsdWdpbi5oYW5kbGUoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFBsdWdpbi5oYW5kbGUoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8gaGFuZGxlIGJ1dHRvbiBjbGljayBcbiAgICAgICAgICAgICAgICBlbGVtZW50Lm9uKCdjbGljaycsIFBsdWdpbi5zY3JvbGwpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBzeW5jIFxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBzeW5jOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJChlbGVtZW50KS5kYXRhKCdzY3JvbGxUb3AnLCBzY3JvbGxUb3ApO1xuICAgICAgICAgICAgfSwgXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBvZmZjYW52YXMgY2xpY2sgc2Nyb2xsVG9wXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGhhbmRsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBvcyA9ICQod2luZG93KS5zY3JvbGxUb3AoKTsgLy8gY3VycmVudCB2ZXJ0aWNhbCBwb3NpdGlvblxuICAgICAgICAgICAgICAgIGlmIChwb3MgPiBzY3JvbGxUb3Aub3B0aW9ucy5vZmZzZXQpIHtcbiAgICAgICAgICAgICAgICAgICAgJChcImJvZHlcIikuYWRkQ2xhc3MoJ20tc2Nyb2xsLXRvcC0tc2hvd24nKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkKFwiYm9keVwiKS5yZW1vdmVDbGFzcygnbS1zY3JvbGwtdG9wLS1zaG93bicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyBvZmZjYW52YXMgY2xpY2sgc2Nyb2xsVG9wXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNjcm9sbDogZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IDBcbiAgICAgICAgICAgICAgICB9LCBzY3JvbGxUb3Aub3B0aW9ucy5zcGVlZCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFRyaWdnZXIgZXZlbnRzXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGV2ZW50VHJpZ2dlcjogZnVuY3Rpb24obmFtZSkge1xuICAgICAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBzY3JvbGxUb3AuZXZlbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBldmVudCA9IHNjcm9sbFRvcC5ldmVudHNbaV07XG4gICAgICAgICAgICAgICAgICAgIGlmIChldmVudC5uYW1lID09IG5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudC5vbmUgPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudC5maXJlZCA9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY3JvbGxUb3AuZXZlbnRzW2ldLmZpcmVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50LmhhbmRsZXIuY2FsbCh0aGlzLCBzY3JvbGxUb3ApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICBldmVudC5oYW5kbGVyLmNhbGwodGhpcywgc2Nyb2xsVG9wKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGFkZEV2ZW50OiBmdW5jdGlvbihuYW1lLCBoYW5kbGVyLCBvbmUpIHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3AuZXZlbnRzLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiBoYW5kbGVyLFxuICAgICAgICAgICAgICAgICAgICBvbmU6IG9uZSxcbiAgICAgICAgICAgICAgICAgICAgZmlyZWQ6IGZhbHNlXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICBQbHVnaW4uc3luYygpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIG1haW4gdmFyaWFibGVzXG4gICAgICAgIHZhciB0aGUgPSB0aGlzO1xuICAgICAgICBcbiAgICAgICAgLy8gaW5pdCBwbHVnaW5cbiAgICAgICAgUGx1Z2luLnJ1bi5hcHBseSh0aGlzLCBbb3B0aW9uc10pO1xuXG4gICAgICAgIC8qKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgKiogUFVCTElDIEFQSSBNRVRIT0RTXG4gICAgICAgICAqKioqKioqKioqKioqKioqKioqKi9cblxuICAgICAgICAvKipcbiAgICAgICAgICogR2V0IHN1YnNjcm9sbFRvcCBtb2RlXG4gICAgICAgICAqL1xuICAgICAgICBzY3JvbGxUb3Aub24gPSAgZnVuY3Rpb24gKG5hbWUsIGhhbmRsZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uYWRkRXZlbnQobmFtZSwgaGFuZGxlcik7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldCBzY3JvbGxUb3AgY29udGVudFxuICAgICAgICAgKiBAcmV0dXJucyB7bVNjcm9sbFRvcH1cbiAgICAgICAgICovXG4gICAgICAgIHNjcm9sbFRvcC5vbmUgPSAgZnVuY3Rpb24gKG5hbWUsIGhhbmRsZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4uYWRkRXZlbnQobmFtZSwgaGFuZGxlciwgdHJ1ZSk7XG4gICAgICAgIH07ICAgXG5cbiAgICAgICAgcmV0dXJuIHNjcm9sbFRvcDtcbiAgICB9O1xuXG4gICAgLy8gZGVmYXVsdCBvcHRpb25zXG4gICAgJC5mbi5tU2Nyb2xsVG9wLmRlZmF1bHRzID0ge1xuICAgICAgICBvZmZzZXQ6IDMwMCxcbiAgICAgICAgc3BlZWQ6IDYwMFxuICAgIH07IFxufShqUXVlcnkpKTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2RvbWFpbi9jb21tb24vdGhlbWUvanMvZnJhbWV3b3JrL2NvbXBvbmVudHMvZ2VuZXJhbC9zY3JvbGwtdG9wLmpzIiwiaW1wb3J0IHttVXRpbH0gZnJvbSBcIi4vLi4vLi4vYmFzZS91dGlsXCI7XG5cbihmdW5jdGlvbigkKSB7XG4gICAgLy8gcGx1Z2luIHNldHVwXG4gICAgJC5mbi5tVG9nZ2xlID0gZnVuY3Rpb24ob3B0aW9ucykge1xuICAgICAgICAvLyBtYWluIG9iamVjdFxuICAgICAgICB2YXIgdG9nZ2xlID0gdGhpcztcbiAgICAgICAgdmFyIGVsZW1lbnQgPSAkKHRoaXMpO1xuXG4gICAgICAgIC8qKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgKiogUFJJVkFURSBNRVRIT0RTXG4gICAgICAgICAqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgdmFyIFBsdWdpbiA9IHtcbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogUnVuXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHJ1bjogZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICBpZiAoIWVsZW1lbnQuZGF0YSgndG9nZ2xlJykpIHsgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSBpbnN0YW5jZVxuICAgICAgICAgICAgICAgICAgICBQbHVnaW4uaW5pdChvcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICAgUGx1Z2luLmJ1aWxkKCk7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAvLyBhc3NpZ24gaW5zdGFuY2UgdG8gdGhlIGVsZW1lbnQgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmRhdGEoJ3RvZ2dsZScsIHRvZ2dsZSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gZ2V0IGluc3RhbmNlIGZyb20gdGhlIGVsZW1lbnRcbiAgICAgICAgICAgICAgICAgICAgdG9nZ2xlID0gZWxlbWVudC5kYXRhKCd0b2dnbGUnKTtcbiAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgXG5cbiAgICAgICAgICAgICAgICByZXR1cm4gdG9nZ2xlO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIHN1YnRvZ2dsZSBjbGljayB0b2dnbGVcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaW5pdDogZnVuY3Rpb24ob3B0aW9ucykge1xuICAgICAgICAgICAgICAgIHRvZ2dsZS5lbGVtZW50ID0gZWxlbWVudDsgICAgXG4gICAgICAgICAgICAgICAgdG9nZ2xlLmV2ZW50cyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgLy8gbWVyZ2UgZGVmYXVsdCBhbmQgdXNlciBkZWZpbmVkIG9wdGlvbnNcbiAgICAgICAgICAgICAgICB0b2dnbGUub3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLm1Ub2dnbGUuZGVmYXVsdHMsIG9wdGlvbnMpO1xuXG4gICAgICAgICAgICAgICAgdG9nZ2xlLnRhcmdldCA9ICQodG9nZ2xlLm9wdGlvbnMudGFyZ2V0KTtcbiAgICAgICAgICAgICAgICB0b2dnbGUudGFyZ2V0U3RhdGUgPSB0b2dnbGUub3B0aW9ucy50YXJnZXRTdGF0ZTtcbiAgICAgICAgICAgICAgICB0b2dnbGUudG9nZ2xlclN0YXRlID0gdG9nZ2xlLm9wdGlvbnMudG9nZ2xlclN0YXRlO1xuXG4gICAgICAgICAgICAgICAgdG9nZ2xlLnN0YXRlID0gbVV0aWwuaGFzQ2xhc3Nlcyh0b2dnbGUudGFyZ2V0LCB0b2dnbGUudGFyZ2V0U3RhdGUpID8gJ29uJyA6ICdvZmYnO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBTZXR1cCB0b2dnbGVcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgYnVpbGQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQub24oJ2NsaWNrJywgUGx1Z2luLnRvZ2dsZSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIHN5bmMgXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHN5bmM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkKGVsZW1lbnQpLmRhdGEoJ3RvZ2dsZScsIHRvZ2dsZSk7XG4gICAgICAgICAgICB9LCBcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIG9mZmNhbnZhcyBjbGljayB0b2dnbGVcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgdG9nZ2xlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiAodG9nZ2xlLnN0YXRlID09ICdvZmYnKSB7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi50b2dnbGVPbigpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIFBsdWdpbi50b2dnbGVPZmYoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgUGx1Z2luLmV2ZW50VHJpZ2dlcigndG9nZ2xlJyk7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gdG9nZ2xlO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIYW5kbGVzIHRvZ2dsZSBjbGljayB0b2dnbGVcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgdG9nZ2xlT246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIFBsdWdpbi5ldmVudFRyaWdnZXIoJ2JlZm9yZU9uJyk7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdG9nZ2xlLnRhcmdldC5hZGRDbGFzcyh0b2dnbGUudGFyZ2V0U3RhdGUpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRvZ2dsZS50b2dnbGVyU3RhdGUpIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5hZGRDbGFzcyh0b2dnbGUudG9nZ2xlclN0YXRlKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0b2dnbGUuc3RhdGUgPSAnb24nO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLmV2ZW50VHJpZ2dlcignYWZ0ZXJPbicpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRvZ2dsZTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogSGFuZGxlcyB0b2dnbGUgY2xpY2sgdG9nZ2xlXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHRvZ2dsZU9mZjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgUGx1Z2luLmV2ZW50VHJpZ2dlcignYmVmb3JlT2ZmJyk7XG5cbiAgICAgICAgICAgICAgICB0b2dnbGUudGFyZ2V0LnJlbW92ZUNsYXNzKHRvZ2dsZS50YXJnZXRTdGF0ZSk7XG5cbiAgICAgICAgICAgICAgICBpZiAodG9nZ2xlLnRvZ2dsZXJTdGF0ZSkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUNsYXNzKHRvZ2dsZS50b2dnbGVyU3RhdGUpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRvZ2dsZS5zdGF0ZSA9ICdvZmYnO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLmV2ZW50VHJpZ2dlcignYWZ0ZXJPZmYnKTtcblxuICAgICAgICAgICAgICAgIHJldHVybiB0b2dnbGU7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFRyaWdnZXIgZXZlbnRzXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGV2ZW50VHJpZ2dlcjogZnVuY3Rpb24obmFtZSkge1xuICAgICAgICAgICAgICAgIHRvZ2dsZS50cmlnZ2VyKG5hbWUpO1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdG9nZ2xlLmV2ZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgZXZlbnQgPSB0b2dnbGUuZXZlbnRzW2ldO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQubmFtZSA9PSBuYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQub25lID09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQuZmlyZWQgPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9nZ2xlLmV2ZW50c1tpXS5maXJlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudC5oYW5kbGVyLmNhbGwodGhpcywgdG9nZ2xlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAgZXZlbnQuaGFuZGxlci5jYWxsKHRoaXMsIHRvZ2dsZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBhZGRFdmVudDogZnVuY3Rpb24obmFtZSwgaGFuZGxlciwgb25lKSB7XG4gICAgICAgICAgICAgICAgdG9nZ2xlLmV2ZW50cy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcjogaGFuZGxlcixcbiAgICAgICAgICAgICAgICAgICAgb25lOiBvbmUsXG4gICAgICAgICAgICAgICAgICAgIGZpcmVkOiBmYWxzZVxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgUGx1Z2luLnN5bmMoKTtcblxuICAgICAgICAgICAgICAgIHJldHVybiB0b2dnbGU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gbWFpbiB2YXJpYWJsZXNcbiAgICAgICAgdmFyIHRoZSA9IHRoaXM7XG4gICAgICAgIFxuICAgICAgICAvLyBpbml0IHBsdWdpblxuICAgICAgICBQbHVnaW4ucnVuLmFwcGx5KHRoaXMsIFtvcHRpb25zXSk7XG5cbiAgICAgICAgLyoqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAqKiBQVUJMSUMgQVBJIE1FVEhPRFNcbiAgICAgICAgICoqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBUb2dnbGUgXG4gICAgICAgICAqL1xuICAgICAgICB0b2dnbGUudG9nZ2xlID0gIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4udG9nZ2xlKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFRvZ2dsZSBvbiBcbiAgICAgICAgICovXG4gICAgICAgIHRvZ2dsZS50b2dnbGVPbiA9ICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gUGx1Z2luLnRvZ2dsZU9uKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFRvZ2dsZSBvZmYgXG4gICAgICAgICAqL1xuICAgICAgICB0b2dnbGUudG9nZ2xlT2ZmID0gIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBQbHVnaW4udG9nZ2xlT2ZmKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEF0dGFjaCBldmVudFxuICAgICAgICAgKiBAcmV0dXJucyB7bVRvZ2dsZX1cbiAgICAgICAgICovXG4gICAgICAgIHRvZ2dsZS5vbiA9ICBmdW5jdGlvbiAobmFtZSwgaGFuZGxlcikge1xuICAgICAgICAgICAgcmV0dXJuIFBsdWdpbi5hZGRFdmVudChuYW1lLCBoYW5kbGVyKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogQXR0YWNoIGV2ZW50IHRoYXQgd2lsbCBiZSBmaXJlZCBvbmNlXG4gICAgICAgICAqIEByZXR1cm5zIHttVG9nZ2xlfVxuICAgICAgICAgKi9cbiAgICAgICAgdG9nZ2xlLm9uZSA9ICBmdW5jdGlvbiAobmFtZSwgaGFuZGxlcikge1xuICAgICAgICAgICAgcmV0dXJuIFBsdWdpbi5hZGRFdmVudChuYW1lLCBoYW5kbGVyLCB0cnVlKTtcbiAgICAgICAgfTsgICAgIFxuXG4gICAgICAgIHJldHVybiB0b2dnbGU7XG4gICAgfTtcblxuICAgIC8vIGRlZmF1bHQgb3B0aW9uc1xuICAgICQuZm4ubVRvZ2dsZS5kZWZhdWx0cyA9IHtcbiAgICAgICAgdG9nZ2xlclN0YXRlOiAnJyxcbiAgICAgICAgdGFyZ2V0U3RhdGU6ICcnXG4gICAgfTsgXG59KGpRdWVyeSkpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL2ZyYW1ld29yay9jb21wb25lbnRzL2dlbmVyYWwvdG9nZ2xlLmpzIiwiZXhwb3J0IGNvbnN0IG1RdWlja1NpZGViYXIgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgdG9wYmFyQXNpZGUgPSAkKCcjbV9xdWlja19zaWRlYmFyJyk7XG4gICAgdmFyIHRvcGJhckFzaWRlVGFicyA9ICQoJyNtX3F1aWNrX3NpZGViYXJfdGFicycpOyAgICBcbiAgICB2YXIgdG9wYmFyQXNpZGVDbG9zZSA9ICQoJyNtX3F1aWNrX3NpZGViYXJfY2xvc2UnKTtcbiAgICB2YXIgdG9wYmFyQXNpZGVUb2dnbGUgPSAkKCcjbV9xdWlja19zaWRlYmFyX3RvZ2dsZScpO1xuICAgIHZhciB0b3BiYXJBc2lkZUNvbnRlbnQgPSB0b3BiYXJBc2lkZS5maW5kKCcubS1xdWljay1zaWRlYmFyX19jb250ZW50Jyk7XG5cbiAgICB2YXIgaW5pdE1lc3NhZ2VzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBpbml0ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgbWVzc2VuZ2VyID0gJCgnI21fcXVpY2tfc2lkZWJhcl90YWJzX21lc3NlbmdlcicpOyAgXG4gICAgICAgICAgICB2YXIgbWVzc2VuZ2VyTWVzc2FnZXMgPSBtZXNzZW5nZXIuZmluZCgnLm0tbWVzc2VuZ2VyX19tZXNzYWdlcycpO1xuXG4gICAgICAgICAgICB2YXIgaGVpZ2h0ID0gdG9wYmFyQXNpZGUub3V0ZXJIZWlnaHQodHJ1ZSkgLSBcbiAgICAgICAgICAgICAgICB0b3BiYXJBc2lkZVRhYnMub3V0ZXJIZWlnaHQodHJ1ZSkgLSBcbiAgICAgICAgICAgICAgICBtZXNzZW5nZXIuZmluZCgnLm0tbWVzc2VuZ2VyX19mb3JtJykub3V0ZXJIZWlnaHQodHJ1ZSkgLSAxMjA7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIGluaXQgbWVzc2FnZXMgc2Nyb2xsYWJsZSBjb250ZW50XG4gICAgICAgICAgICBtZXNzZW5nZXJNZXNzYWdlcy5jc3MoJ2hlaWdodCcsIGhlaWdodCk7XG4gICAgICAgICAgICBtQXBwLmluaXRTY3JvbGxlcihtZXNzZW5nZXJNZXNzYWdlcywge30pO1xuICAgICAgICB9XG5cbiAgICAgICAgaW5pdCgpOyAgICAgICAgXG4gICAgICAgIFxuICAgICAgICAvLyByZWluaXQgb24gd2luZG93IHJlc2l6ZVxuICAgICAgICBtVXRpbC5hZGRSZXNpemVIYW5kbGVyKGluaXQpO1xuICAgIH1cblxuICAgIHZhciBpbml0U2V0dGluZ3MgPSBmdW5jdGlvbigpIHsgXG4gICAgICAgIC8vIGluaXQgZHJvcGRvd24gdGFiYmFibGUgY29udGVudFxuICAgICAgICB2YXIgaW5pdCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHNldHRpbmdzID0gJCgnI21fcXVpY2tfc2lkZWJhcl90YWJzX3NldHRpbmdzJyk7XG4gICAgICAgICAgICB2YXIgaGVpZ2h0ID0gbVV0aWwuZ2V0Vmlld1BvcnQoKS5oZWlnaHQgLSB0b3BiYXJBc2lkZVRhYnMub3V0ZXJIZWlnaHQodHJ1ZSkgLSA2MDtcblxuICAgICAgICAgICAgLy8gaW5pdCBzZXR0aW5ncyBzY3JvbGxhYmxlIGNvbnRlbnRcbiAgICAgICAgICAgIHNldHRpbmdzLmNzcygnaGVpZ2h0JywgaGVpZ2h0KTtcbiAgICAgICAgICAgIG1BcHAuaW5pdFNjcm9sbGVyKHNldHRpbmdzLCB7fSk7XG4gICAgICAgIH1cblxuICAgICAgICBpbml0KCk7XG5cbiAgICAgICAgLy8gcmVpbml0IG9uIHdpbmRvdyByZXNpemVcbiAgICAgICAgbVV0aWwuYWRkUmVzaXplSGFuZGxlcihpbml0KTtcbiAgICB9XG5cbiAgICB2YXIgaW5pdExvZ3MgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgLy8gaW5pdCBkcm9wZG93biB0YWJiYWJsZSBjb250ZW50XG4gICAgICAgIHZhciBpbml0ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgbG9ncyA9ICQoJyNtX3F1aWNrX3NpZGViYXJfdGFic19sb2dzJyk7XG4gICAgICAgICAgICB2YXIgaGVpZ2h0ID0gbVV0aWwuZ2V0Vmlld1BvcnQoKS5oZWlnaHQgLSB0b3BiYXJBc2lkZVRhYnMub3V0ZXJIZWlnaHQodHJ1ZSkgLSA2MDtcblxuICAgICAgICAgICAgLy8gaW5pdCBzZXR0aW5ncyBzY3JvbGxhYmxlIGNvbnRlbnRcbiAgICAgICAgICAgIGxvZ3MuY3NzKCdoZWlnaHQnLCBoZWlnaHQpO1xuICAgICAgICAgICAgbUFwcC5pbml0U2Nyb2xsZXIobG9ncywge30pO1xuICAgICAgICB9XG5cbiAgICAgICAgaW5pdCgpO1xuXG4gICAgICAgIC8vIHJlaW5pdCBvbiB3aW5kb3cgcmVzaXplXG4gICAgICAgIG1VdGlsLmFkZFJlc2l6ZUhhbmRsZXIoaW5pdCk7XG4gICAgfVxuXG4gICAgdmFyIGluaXRPZmZjYW52YXNUYWJzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIGluaXRNZXNzYWdlcygpO1xuICAgICAgICBpbml0U2V0dGluZ3MoKTtcbiAgICAgICAgaW5pdExvZ3MoKTtcbiAgICB9XG5cbiAgICB2YXIgaW5pdE9mZmNhbnZhcyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICB0b3BiYXJBc2lkZS5tT2ZmY2FudmFzKHtcbiAgICAgICAgICAgIGNsYXNzOiAnbS1xdWljay1zaWRlYmFyJyxcbiAgICAgICAgICAgIG92ZXJsYXk6IHRydWUsICBcbiAgICAgICAgICAgIGNsb3NlOiB0b3BiYXJBc2lkZUNsb3NlLFxuICAgICAgICAgICAgdG9nZ2xlOiB0b3BiYXJBc2lkZVRvZ2dsZVxuICAgICAgICB9KTsgICBcblxuICAgICAgICAvLyBydW4gb25jZSBvbiBmaXJzdCB0aW1lIGRyb3Bkb3duIHNob3duXG4gICAgICAgIHRvcGJhckFzaWRlLm1PZmZjYW52YXMoKS5vbmUoJ2FmdGVyU2hvdycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbUFwcC5ibG9jayh0b3BiYXJBc2lkZSk7XG5cbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgbUFwcC51bmJsb2NrKHRvcGJhckFzaWRlKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0b3BiYXJBc2lkZUNvbnRlbnQucmVtb3ZlQ2xhc3MoJ20tLWhpZGUnKTtcblxuICAgICAgICAgICAgICAgIGluaXRPZmZjYW52YXNUYWJzKCk7XG4gICAgICAgICAgICB9LCAxMDAwKTsgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHJldHVybiB7ICAgICBcbiAgICAgICAgaW5pdDogZnVuY3Rpb24oKSB7ICBcbiAgICAgICAgICAgIGluaXRPZmZjYW52YXMoKTsgXG4gICAgICAgIH1cbiAgICB9O1xufSgpO1xuXG4vLyAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbi8vICAgICBtUXVpY2tTaWRlYmFyLmluaXQoKTtcbi8vIH0pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9kb21haW4vY29tbW9uL3RoZW1lL2pzL3NuaXBwZXRzL2Jhc2UvcXVpY2stc2lkZWJhci5qcyJdLCJzb3VyY2VSb290IjoiIn0=