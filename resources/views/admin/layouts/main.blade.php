<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @hasSection('title')
            @yield('title') |
        @endif
        {{ config('app.name') }}
    </title>

    <meta name="author" content="{{ config('config.copyright.name') }} <{{ config('config.copyright.url') }}>">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            },
        });
    </script>
    <!--end::Web font -->

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('admin/css/theme.css') }}">
    <link rel="stylesheet" href="{{ mix('css/vendor.css', 'admin') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css', 'admin') }}">
    <link rel="stylesheet" href="{{ asset('/admin/css/admin.css') }}">
    @stack('css')
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<main id="app" class="m-grid m-grid--hor m-grid--root m-page">

    @include('admin.partials.header')

    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        @include('admin.partials.sidebar')

        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">@yield('title')</h3>

                        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                            <li class="m-nav__item m-nav__item--home">
                                <a href="{{ route('admin.dashboard') }}" class="m-nav__link m-nav__link--icon">
                                    <i class="m-nav__link-icon fa fa-home"></i>
                                </a>
                            </li>

                            @hasSection('breadcrumb')
                                <li class="m-nav__separator">
                                    -
                                </li>

                                @yield('breadcrumb')
                            @endif
                        </ul>
                    </div>

                    <div>
                        @yield('action-button')
                    </div>
                </div>
            </div>

            <div class="m-content">
                <div class="row">
                    <div class="col-md-12" id="app">
                        <alert-notification errors="{{ $errors }}"
                                            flash="{{ session()->get('flash_notification') }}"></alert-notification>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.footer')
</main>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500"
     data-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>

<script src="{{ mix('js/manifest.js', 'admin') }}"></script>
<script src="{{ mix('js/vendor.js', 'admin') }}"></script>
<script src="{{ mix('js/theme.js', 'admin') }}"></script>
<script src="{{ mix('js/app.js', 'admin') }}"></script>
@stack('scripts')
</body>
</html>
