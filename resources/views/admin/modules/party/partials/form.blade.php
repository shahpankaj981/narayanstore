<div>
  <div :class="{'loading': formSubmitting, 'wrapper': formSubmitting}">
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
          @submit.prevent="submit">
      <div class="m-portlet__body" style="padding: inherit;">
        <div class="form-group m-form__group row">
          <div class="col-lg-12">
            <label class="required">Name</label>
            <input type="text" name="name"
                   :class="{ 'input-error' : form.errors.has('name'), 'form-control': loaded }"
                   placeholder="Name"
                   v-model="values.name">
            <span class="m-form__help danger"
                  v-if="form.errors.has('name')"
                  v-text="form.errors.get('name')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label class="required">Street</label>
            <input type="text" name="street"
                   :class="{ 'input-error' : form.errors.has('address.street'), 'form-control': loaded }"
                   placeholder="Street"
                   v-model="values.address.street">
            <span class="m-form__help danger"
                  v-if="form.errors.has('address.street')"
                  v-text="form.errors.get('address.street')"></span>
          </div>
          <div class="col-lg-6">
            <label class="required">City</label>
            <input type="text" name="street"
                   :class="{ 'input-error' : form.errors.has('address.city'), 'form-control': loaded }"
                   placeholder="City"
                   v-model="values.address.city">
            <span class="m-form__help danger"
                  v-if="form.errors.has('address.city')"
                  v-text="form.errors.get('address.city')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label class="required">Country</label>
            <input type="text" name="street"
                   :class="{ 'input-error' : form.errors.has('address.country'), 'form-control': loaded }"
                   placeholder="Country"
                   v-model="values.address.country">
            <span class="m-form__help danger"
                  v-if="form.errors.has('address.country')"
                  v-text="form.errors.get('address.country')"></span>
          </div>
          <div class="col-lg-6">
            <label class="required">ZIP Code</label>
            <input type="text" name="street"
                   :class="{ 'input-error' : form.errors.has('address.zip'), 'form-control': loaded }"
                   placeholder="ZIP Code"
                   v-model="values.address.zip">
            <span class="m-form__help danger"
                  v-if="form.errors.has('address.zip')"
                  v-text="form.errors.get('address.zip')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>Phone</label>
            <input type="text" name="phone"
                   :class="{ 'input-error' : form.errors.has('phone'), 'form-control': loaded }"
                   placeholder="Phone"
                   v-model="values.phone">
            <span class="m-form__help danger"
                  v-if="form.errors.has('phone')"
                  v-text="form.errors.get('phone')"></span>
          </div>
          <div class="col-lg-6">
            <label>Mobile</label>
            <input type="text" name="mobile"
                   :class="{ 'input-error' : form.errors.has('mobile'), 'form-control': loaded }"
                   placeholder="Mobile"
                   v-model="values.mobile">
            <span class="m-form__help danger"
                  v-if="form.errors.has('mobile')"
                  v-text="form.errors.get('mobile')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>Contact Person</label>
            <input type="text" name="phone"
                   :class="{ 'input-error' : form.errors.has('contact_person'), 'form-control': loaded }"
                   placeholder="Contact Person Name"
                   v-model="values.contact_person">
            <span class="m-form__help danger"
                  v-if="form.errors.has('contact_person')"
                  v-text="form.errors.get('contact_person')"></span>
          </div>
          <div class="col-lg-6">
            <label>LUT</label>
            <input type="text" name="lut"
                   :class="{ 'input-error' : form.errors.has('lut'), 'form-control': loaded }"
                   placeholder="LUT"
                   v-model="values.lut">
            <span class="m-form__help danger"
                  v-if="form.errors.has('lut')"
                  v-text="form.errors.get('lut')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>Bank Name</label>
            <input type="text" name="bank"
                   :class="{ 'input-error' : form.errors.has('bank'), 'form-control': loaded }"
                   placeholder="Bank Name"
                   v-model="values.bank">
            <span class="m-form__help danger"
                  v-if="form.errors.has('bank')"
                  v-text="form.errors.get('bank')"></span>
          </div>
          <div class="col-lg-6">
            <label>Account Number</label>
            <input type="text" name="bank_account"
                   :class="{ 'input-error' : form.errors.has('bank_account'), 'form-control': loaded }"
                   placeholder="Bank Account Number"
                   v-model="values.bank_account">
            <span class="m-form__help danger"
                  v-if="form.errors.has('bank_account')"
                  v-text="form.errors.get('bank_account')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>IFSC Number</label>
            <input type="text" name="ifsc_code"
                   :class="{ 'input-error' : form.errors.has('ifsc_code'), 'form-control': loaded }"
                   placeholder="IFSC Code"
                   v-model="values.ifsc_code">
            <span class="m-form__help danger"
                  v-if="form.errors.has('ifsc_code')"
                  v-text="form.errors.get('ifsc_code')"></span>
          </div>
          <div class="col-lg-6">
            <label>Bank Area</label>
            <input type="text" name="bank_area"
                   :class="{ 'input-error' : form.errors.has('bank_area'), 'form-control': loaded }"
                   placeholder="Bank Area"
                   v-model="values.bank_area">
            <span class="m-form__help danger"
                  v-if="form.errors.has('bank_area')"
                  v-text="form.errors.get('bank_area')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>GST Number</label>
            <input type="text" name="gst_number"
                   :class="{ 'input-error' : form.errors.has('gst_number'), 'form-control': loaded }"
                   placeholder="GST Number"
                   v-model="values.gst_number">
            <span class="m-form__help danger"
                  v-if="form.errors.has('gst_number')"
                  v-text="form.errors.get('gst_number')"></span>
          </div>
          <div class="col-lg-6">
            <label>PAN</label>
            <input type="text" name="pan"
                   :class="{ 'input-error' : form.errors.has('pan'), 'form-control': loaded }"
                   placeholder="PAN"
                   v-model="values.pan">
            <span class="m-form__help danger"
                  v-if="form.errors.has('pan')"
                  v-text="form.errors.get('pan')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>IEC Number</label>
            <input type="text" name="iec_number"
                   :class="{ 'input-error' : form.errors.has('iec_number'), 'form-control': loaded }"
                   placeholder="IEC Number"
                   v-model="values.iec_number">
            <span class="m-form__help danger"
                  v-if="form.errors.has('iec_number')"
                  v-text="form.errors.get('iec_number')"></span>
          </div>
          <div class="col-lg-6">
            <label>CIN Number</label>
            <input type="text" name="cin_number"
                   :class="{ 'input-error' : form.errors.has('cin_number'), 'form-control': loaded }"
                   placeholder="CIN Number"
                   v-model="values.cin_number">
            <span class="m-form__help danger"
                  v-if="form.errors.has('cin_number')"
                  v-text="form.errors.get('cin_number')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-12">
            <label>Other Details</label>
            <textarea rows="5" name="other_details"
                      :class="{ 'input-error' : form.errors.has('other_details'), 'form-control': loaded }"
                      placeholder="Details..."
                      v-model="values.other_details"></textarea>
            <span class="m-form__help danger"
                  v-if="form.errors.has('other_details')"
                  v-text="form.errors.get('other_details')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <button class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
    </form>
    <!--end::Form-->
  </div>
</div>
