@extends('admin.layouts.main')

@section('title', 'Clients')

@section('breadcrumb')
    <li class="m-nav__item">
        <a href="{{ route('admin.parties.index') }}" class="m-nav__link">
            <span class="m-nav__link-text">Clients</span>
        </a>
    </li>
    <li class="m-nav__separator">
        -
    </li>
    <li class="m-nav__item">
        <a href="#" class="m-nav__link">
            <span class="m-nav__link-text">Create</span>
        </a>
    </li>
@endsection

@section('content')
    {{--send further props from this create user component--}}
    <party-form inline-template url="{{ route('admin.parties.store') }}">
        <portlet-content header="Client" subheader="Create" isForm="true">
            <template slot="body">
                @include('admin.modules.party.partials.form')
            </template>
        </portlet-content>
    </party-form>
@endsection
