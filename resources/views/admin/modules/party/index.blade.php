@extends('admin.layouts.main')

@section('title', 'Clients')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="" class="m-nav__link">
      <span class="m-nav__link-text">Clients</span>
    </a>
  </li>
@endsection

@section('content')
  <portlet-content header="Clients" subheader="list">
    <template slot="action_button">
      <a href="{{ route('admin.parties.create') }}">
        <button
            class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air"
            title="Add new client"
            data-toggle="tooltip"
        >
          <i class="fa fa-plus"></i>
        </button>
      </a>
    </template>
    <template slot="body">
      <div class="row">
        <div class="col-md-12">
          <vue-table
              source="{{ route('admin.parties.table') }}"
              :columns="['name', 'address', 'mobile', 'contact_person', 'action']"
          ></vue-table>
        </div>
      </div>
    </template>
  </portlet-content>
@endsection
