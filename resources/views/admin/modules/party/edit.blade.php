@extends('admin.layouts.main')

@section('title', 'Clients')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="{{ route('admin.parties.index') }}" class="m-nav__link">
      <span class="m-nav__link-text">Parties</span>
    </a>
  </li>
  <li class="m-nav__separator">
    -
  </li>
  <li class="m-nav__item">
    <a href="#" class="m-nav__link">
      <span class="m-nav__link-text">Edit</span>
    </a>
  </li>
@endsection

@section('content')
  {{--send further props from this create user component--}}
  <party-form inline-template url="{{ route('admin.parties.update', $party->id) }}" method="patch"
               party="{{ json_encode($party) }}">
    <portlet-content header="Client" subheader="Edit" isForm="true">
      <template slot="body">
       @include('admin.modules.party.partials.form')
      </template>
    </portlet-content>
  </party-form>
@endsection
