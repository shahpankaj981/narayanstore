@extends('admin.layouts.main')

@section('title', 'Clients')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="{{ route('admin.parties.index') }}" class="m-nav__link">
      <span class="m-nav__link-text">Clients</span>
    </a>
  </li>
  <li class="m-nav__separator">
    -
  </li>
  <li class="m-nav__item">
    <a href="{{ route('admin.parties.show', $party->id) }}" class="m-nav__link">
      <span class="m-nav__link-text">{{ $party->name }}</span>
    </a>
  </li>
@endsection

@section('content')
  <portlet-content header="Client" subheader="Details">
    <template slot="action_button">
      <edit-button
          url="{{ route('admin.parties.edit', $party->id) }}"
          title="Edit Party Details"
      >
      </edit-button>
    </template>
    <template slot="body">
      <div class="row">
        <div class="col-md-12">
          <table class="table">
            <tr>
              <td><strong> Name </strong></td>
              <td> {{ $party->name }}</td>
            </tr>
            <tr>
              <td><strong> Address </strong></td>
              <td>{{ $party->formatted_address }}</td>
            </tr>
            <tr>
              <td><strong> Phone </strong></td>
              <td>{{ $party->phone ?? "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Mobile</strong></td>
              <td>{{ $party->mobile or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Contact Person</strong></td>
              <td>{{ $party->contact_person or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Bank Name</strong></td>
              <td>{{ $party->bank or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Contact Person</strong></td>
              <td>{{ $party->contact_person or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Bank Name</strong></td>
              <td>{{ $party->bank or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Account Number</strong></td>
              <td>{{ $party->bank_account or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Contact Person</strong></td>
              <td>{{ $party->contact_person or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>IFSC Code</strong></td>
              <td>{{ $party->ifsc_code or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Bank Area</strong></td>
              <td>{{ $party->bank_area or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>GST Number</strong></td>
              <td>{{ $party->gst_number or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>PAN</strong></td>
              <td>{{ $party->pan or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>IEC Number</strong></td>
              <td>{{ $party->iec_number or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>CIN Number</strong></td>
              <td>{{ $party->cin_number or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>LUT</strong></td>
              <td>{{ $party->lut or "N/A" }}</td>
            </tr>
            <tr>
              <td><strong>Other Details</strong></td>
              <td>{{ $party->other_details or "N/A" }}</td>
            </tr>
          </table>
        </div>
      </div>
    </template>
  </portlet-content>
@endsection
