@extends('tenant.layouts.app')

@section('title')
  {{ ucwords($user->name) }} | Edit | Profile
@endsection

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="{{ route('users.profile') }}"
       data-toggle="tooltip"
       title="View profile"
       class="m-nav__link">
      <span class="m-nav__link-text">My Profile</span>
    </a>
  </li>
  <li class="m-nav__separator">-</li>
  <li class="m-nav__item">
    <a href="javascript:" class="m-nav__link" data-toggle="tooltip" title="Edit profile">
      <span class="m-nav__link-text">Edit</span>
    </a>
  </li>
@endsection

@section('content')
  <portlet-content header="Profile" subheader="Edit" isform="true">
    <template slot="body">
      <create-user inline-template action="put" user="{{ json_encode($user) }}"
                   url="{{ route('tenant.users.profile.update', currentTenant()) }}">
        @include('tenant.modules.users.partials.userForm', ['action' => 'edit'])
      </create-user>
    </template>
  </portlet-content>
@endsection
