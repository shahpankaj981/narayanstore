@extends('admin.layouts.main')

@section('title', 'Profile')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="javascript:" class="m-nav__link" data-toggle="tooltip" title="My Profile">
      <span class="m-nav__link-text">My Profile</span>
    </a>
  </li>
@endsection

@section('content')
  <profile inline-template
           changepasswordurl="{{ route('admin.users.changePassword') }}">
    <div>
      <portlet-content header="{{ ucwords($user->name) }}" subheader="Profile">
        <template slot="action_button">
          <a data-toggle="tooltip" title="Change Password" @click="modals.changePassword.show = true"
             class="m-portlet__nav-link btn btn-lg btn-outline-info m-btn--air m-btn m-btn--icon m-btn--icon-only m-btn--pill">
            <i class="fa fa-lock"></i>
          </a>
          <edit-button title="Edit profile"
                       url="{{ route('admin.users.profile.edit') }}"></edit-button>
        </template>
        <template slot="body">
          @include('admin.modules.users.partials.details')
          @include('admin.modules.users.partials.changePasswordModal')
        </template>
      </portlet-content>
    </div>
  </profile>
@endsection
