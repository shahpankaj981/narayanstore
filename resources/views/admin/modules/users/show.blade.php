@extends('admin.layouts.main')

@section('title', 'Users Details')

@section('breadcrumb')
    <li class="m-nav__item">
        <a href="{{ route('admin.users.index') }}" class="m-nav__link">
            <span class="m-nav__link-text">Users</span>
        </a>
    </li>
    <li class="m-nav__separator">
        -
    </li>
    <li class="m-nav__item">
        <a href="{{ route('admin.users.show', $user->id) }}" class="m-nav__link">
            <span class="m-nav__link-text">{{ $user->formatted_full_name }}</span>
        </a>
    </li>
@endsection

@section('content')
    <portlet-content header="User" subheader="Details">
        <template slot="action_button">
            <delete-button
                    submiturl="{{ route('admin.users.change.status', [$user->id, $buttonDetails['change_action']]) }}"
                    title="{{ $buttonDetails['title'] }}"
                    swaltitle="Are you sure ?"
                    swalsuccesstitle="{{ $buttonDetails['success_message'] }}"
                    itagclassname="{{ $buttonDetails['icon'] }}"
                    method="patch"
                    redirecturl="{{ route('admin.users.show', [$user->id]) }}"
            ></delete-button>
            <edit-button
                    url="{{ route('admin.users.edit', $user->id) }}"
                    title="Edit User Details"
            >
            </edit-button>
        </template>
        <template slot="body">
            <div class="row">
                @include('admin.modules.users.partials.details')
            </div>
        </template>
    </portlet-content>
@endsection
