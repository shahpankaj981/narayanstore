@extends('admin.layouts.main')

@section('title', 'Customs')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="{{ route('admin.customs.index') }}" class="m-nav__link">
      <span class="m-nav__link-text">Customs</span>
    </a>
  </li>
  <li class="m-nav__separator">
    -
  </li>
  <li class="m-nav__item">
    <a href="{{ route('admin.customs.show', $custom->id) }}" class="m-nav__link">
      <span class="m-nav__link-text">{{ $custom->custom_ref_no }}</span>
    </a>
  </li>
@endsection

@section('content')
  <portlet-content header="Custom" subheader="Details">
    <template slot="action_button">
      <edit-button
          url="{{ route('admin.customs.edit', $custom->id) }}"
          title="Edit custom Details"
      >
      </edit-button>
    </template>
    <template slot="body">
      <div class="row">
        <div class="col-md-12">
          <table class="table">
            <tr>
              <td class="label"><strong> Custom Ref No. </strong></td>
              <td> {{ $custom->custom_ref_no }}</td>
            </tr>
            <tr>
              <td><strong> Vehicle No. </strong></td>
              <td> {{ $custom->vehicle_no ?? 'Not Available' }}</td>
            </tr>
            <tr>
              <td><strong> Invoices </strong></td>
              <td>
                @forelse($custom->invoices as $invoice)
                  <span class="m-badge m-badge--info" style="min-height:7px; min-width:7px;"></span>
                  <a href="{{ route('admin.invoices.show', $invoice->id) }}" title="View invoice details">
                    {{ $invoice->invoice_number }} &nbsp;
                  </a>

                @empty
                  Not Available
                @endforelse
              </td>
            </tr>
            <tr>
              <td><strong> Parties </strong></td>
              <td>
                @forelse($custom->parties as $party)
                  <span class="m-badge m-badge--info" style="min-height:7px; min-width:7px;"></span>
                  <a href="{{ route('admin.parties.show', $party->id) }}" title="View Party's Details">
                    {{ $party->name }} &nbsp;
                  </a>
                @empty
                  Not Available
                @endforelse
              </td>
            </tr>
            <tr>
              <td><strong> Quality </strong></td>
              <td> {{ $custom->quality ?? "Not Available" }}</td>
            </tr>
            <tr>
              <td><strong> Custom Amount </strong></td>
              <td> {{ $custom->custom_amoount ?? "Not Available" }}</td>
            </tr>
            <tr>
              <td><strong> VAT Amount </strong></td>
              <td> {{ $custom->vat_amount ?? "Not Available" }}</td>
            </tr>
            <tr>
              <td><strong> Clearance Charge </strong></td>
              <td> {{ $custom->clearance_charge ?? "Not Available" }}</td>
            </tr>
            <tr>
              <td><strong> Clearance Date </strong></td>
              <td> {{ $custom->formatted_clearance_date }}</td>
            </tr>
            <tr>
              <td><strong> Bale Quantity </strong></td>
              <td> {{ $custom->bale_quantity ?? "Not Available" }}</td>
            </tr>
          </table>
        </div>
      </div>
    </template>
  </portlet-content>
@endsection
