<div>
  <div :class="{'loading': formSubmitting, 'wrapper': formSubmitting}">
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
          @submit.prevent="submit">
      <div class="m-portlet__body" style="padding: inherit;">
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label class="required">Custom Reference No.</label>
            <input type="text" name="custom_ref_no"
                   :class="{ 'input-error' : form.errors.has('custom_ref_no'), 'form-control': loaded }"
                   placeholder="Custom Reference Number"
                   v-model="values.custom_ref_no">
            <span class="m-form__help danger"
                  v-if="form.errors.has('custom_ref_no')"
                  v-text="form.errors.get('custom_ref_no')"></span>
          </div>
          <div class="col-lg-6">
            <label class="required">Custom Amount</label>
            <input type="text" name="custom_amount"
                   :class="{ 'input-error' : form.errors.has('custom_amount'), 'form-control': loaded }"
                   placeholder="Custom Amount"
                   v-model="values.custom_amount">
            <span class="m-form__help danger"
                  v-if="form.errors.has('custom_amount')"
                  v-text="form.errors.get('custom_amount')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label class="required">Invoices</label>
            <v-select multiple v-model="values.invoices" :options="invoiceList" placeholder="Select Invoices"
                      label="invoice_number" value="id"
                      :class="{'input-error': form.errors.has('invoices') }"></v-select>
            <span class="m-form__help danger"
                  v-if="form.errors.has('invoices')"
                  v-text="form.errors.get('invoices')"></span>
          </div>
          <div class="col-lg-6">
            <label>Parties</label>
            <v-select multiple v-model="values.parties" :options="partyList" placeholder="Select Parties"
                      label="name" value="id"
                      :class="{'input-error': form.errors.has('parties') }"></v-select>
            <span class="m-form__help danger"
                  v-if="form.errors.has('parties')"
                  v-text="form.errors.get('parties')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>Vehicle Number</label>
            <input type="text" name="name"
                   :class="{ 'input-error' : form.errors.has('vehicle_no'), 'form-control': loaded }"
                   placeholder="Vehicle Number"
                   v-model="values.vehicle_no">
            <span class="m-form__help danger"
                  v-if="form.errors.has('vehicle_no')"
                  v-text="form.errors.get('vehicle_no')"></span>
          </div>
          <div class="col-lg-6">
            <label>Quality</label>
            <input type="text" name="quality"
                   :class="{ 'input-error' : form.errors.has('quality'), 'form-control': loaded }"
                   placeholder="Quality"
                   v-model="values.quality">
            <span class="m-form__help danger"
                  v-if="form.errors.has('quality')"
                  v-text="form.errors.get('quality')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label class="required">Clearance Charge</label>
            <input type="text" name="clearance_charge"
                   :class="{ 'input-error' : form.errors.has('clearance_charge'), 'form-control': loaded }"
                   placeholder="Clearance Charge"
                   v-model="values.clearance_charge">
            <span class="m-form__help danger"
                  v-if="form.errors.has('clearance_charge')"
                  v-text="form.errors.get('clearance_charge')"></span>
          </div>
          <div class="col-lg-6">
            <label>Clearance Date</label>
            <input type="date" name="clearance_date"
                   :class="{ 'input-error' : form.errors.has('clearance_date'), 'form-control': loaded }"
                   placeholder="Clearance Date"
                   v-model="values.clearance_date">
            <span class="m-form__help danger"
                  v-if="form.errors.has('clearance_date')"
                  v-text="form.errors.get('clearance_date')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>VAT Amount</label>
            <input type="text" name="vat_amount"
                   :class="{ 'input-error' : form.errors.has('vat_amount'), 'form-control': loaded }"
                   placeholder="VAT Amount"
                   v-model="values.vat_amount">
            <span class="m-form__help danger"
                  v-if="form.errors.has('vat_amount')"
                  v-text="form.errors.get('vat_amount')"></span>
          </div>
          <div class="col-lg-6">
            <label>Bale Quantity</label>
            <input type="text" name="bale_quantity"
                   :class="{ 'input-error' : form.errors.has('bale_quantity'), 'form-control': loaded }"
                   placeholder="Bale Quantity"
                   v-model="values.bale_quantity">
            <span class="m-form__help danger"
                  v-if="form.errors.has('bale_quantity')"
                  v-text="form.errors.get('bale_quantity')"></span>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <button class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
    </form>
    <!--end::Form-->
  </div>
</div>
