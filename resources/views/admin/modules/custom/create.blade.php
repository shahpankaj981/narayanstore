@extends('admin.layouts.main')

@section('title', 'Customs')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="{{ route('admin.customs.index') }}" class="m-nav__link">
      <span class="m-nav__link-text">Customs</span>
    </a>
  </li>
  <li class="m-nav__separator">
    -
  </li>
  <li class="m-nav__item">
    <a href="#" class="m-nav__link">
      <span class="m-nav__link-text">Create</span>
    </a>
  </li>
@endsection

@section('content')
  {{--send further props from this create user component--}}
  <custom-form inline-template
                allinvoices="{{ json_encode($invoices) }}"
                allparties="{{ json_encode($parties) }}"
                url="{{ route('admin.customs.store') }}">
    <portlet-content header="Custom" subheader="Create" isForm="true">
      <template slot="body">
        @include('admin.modules.custom.partials.form')
      </template>
    </portlet-content>
  </custom-form>
@endsection
