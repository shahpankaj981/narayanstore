@extends('admin.layouts.main')

@section('title', 'Customs')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="" class="m-nav__link">
      <span class="m-nav__link-text">Customs</span>
    </a>
  </li>
@endsection

@section('content')
  <portlet-content header="Customs" subheader="list">
    <template slot="action_button">
      <a href="{{ route('admin.customs.create') }}">
        <button
            class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air"
            title="Add new custtom"
            data-toggle="tooltip"
        >
          <i class="fa fa-plus"></i>
        </button>
      </a>
    </template>
    <template slot="body">
      <div class="row">
        <div class="col-md-12">
          <vue-table
              source="{{ route('admin.customs.table') }}"
              :columns="['custom_ref_no', 'vehicle_no', 'custom_amount', 'clearance_date', 'action']"
          ></vue-table>
        </div>
      </div>
    </template>
  </portlet-content>
@endsection
