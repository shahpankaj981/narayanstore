@extends('admin.layouts.main')

@section('title', 'Invoices')

@push('css')
  <link rel="stylesheet" href="{{ asset('admin/css/vendors.bundle.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/css/style.bundle.css') }}">
@endpush

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="{{ route('admin.invoices.index') }}" class="m-nav__link">
      <span class="m-nav__link-text">Invoices</span>
    </a>
  </li>
  <li class="m-nav__separator">
    -
  </li>
  <li class="m-nav__item">
    <a href="{{ route('admin.invoices.show', $invoice->id) }}" class="m-nav__link">
      <span class="m-nav__link-text">{{ $invoice->invoice_number }}</span>
    </a>
  </li>
@endsection

@section('content')
  <div class="m-portlet">
    <div class="m-portlet__body m-portlet__body--no-padding">
      <div class="m-invoice-1">
        <div class="m-invoice__wrapper">
          <div class="m-invoice__head" style="background-image: url(/images/defaults/bg-6.jpg);">
            <div class="m-invoice__container m-invoice__container--centered">
              <div class="m-invoice__logo">
                <a href="#">
                  <h1>
                    INVOICE
                  </h1>
                </a>
                <a href="#">
                  <img src="{{ asset('images/defaults/logo_client_white.png') }}">
                </a>
              </div>
              <span class="m-invoice__desc">
													<span>
														Main Road, Biratnagar
													</span>
													<span>
														Morang, Nepal 977
													</span>
												</span>
              <div class="m-invoice__items">
                <div class="m-invoice__item">
														<span class="m-invoice__subtitle">
															DATE
														</span>
                  <span class="m-invoice__text">
															{{ $invoice->formatted_invoice_date }}
														</span>
                </div>
                <div class="m-invoice__item">
														<span class="m-invoice__subtitle">
															INVOICE NO.
														</span>
                  <span class="m-invoice__text">
															{{ $invoice->invoice_number }}
														</span>
                </div>
                <div class="m-invoice__item">
														<span class="m-invoice__subtitle">
															INVOICE FROM.
														</span>
                  <span class="m-invoice__text">
                    {{ $invoice->party->address->street.' '.$invoice->party->address->city }}
                    <br>
                    {{ $invoice->party->address->country .', '. $invoice->party->address->zip }}
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="m-invoice__body m-invoice__body--centered">
            <div class="table-responsive">
              <table class="table">
                <thead>
                <tr>
                  <th>
                    DESCRIPTION
                  </th>
                  <th>
                    Quantity
                  </th>
                  <th>
                    RATE
                  </th>
                  <th>
                    TAXABLE AMOUNT
                  </th>
                </tr>
                </thead>
                <tbody>
                <?php $total = 0; ?>
                @foreach($invoice->records as $record)
                <tr>
                  <td>
                    {{ $record->description }}
                  </td>
                  <td>
                    {{ $record->quantity }}
                  </td>
                  <td>
                    {{ $record->rate }}
                  </td>
                  <td>
                    {{ $record->quantity * $record->rate }}
                  </td>
                </tr>
                  <?php $total += $record->quantity * $record->rate; ?>
                  @endforeach
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>____________</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>Total</td>--}}
                {{--<td>{{ $total }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>Discount</td>--}}
                {{--<td>{{ $discount = $total * $invoice->discount_rate / 100 }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>Insurance</td>--}}
                {{--<td>{{ $invoice->insurance }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>Postage</td>--}}
                {{--<td>{{ $invoice->postage }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>Transport Amt</td>--}}
                {{--<td>{{ $invoice->transport_amount }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>Misc Amt</td>--}}
                {{--<td>{{ $invoice->miscellaneous_amount }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>____________</td>--}}
                {{--<tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>Total Taxable Amt</td>--}}
                {{--<td>{{ $totalTaxableAmount = $total - $discount + $invoice->insurance +--}}
                {{--$invoice->postage + $invoice->transport_amount + $invoice->miscellaneous_amount }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td></td>--}}
                {{--<td>IGST Charge</td>--}}
                {{--<td>{{ $igst = $totalTaxableAmount * $invoice->igst_rate / 100 }}</td>--}}
                {{--</tr>--}}

                </tbody>
              </table>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <hr>
                <div class="col-md-7 pull-left" style="font-style: italic">Total</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{ $total }}
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <hr>
                <div class="col-md-7 pull-left" style="font-style: italic">Discount</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{ $discount = $total * $invoice->discount_rate / 100 }}
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <div class="col-md-7 pull-left" style="font-style: italic">Insurance</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{ $invoice->insurance }}
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <div class="col-md-7 pull-left" style="font-style: italic">Postage</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{ $invoice->postage }}
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <div class="col-md-7 pull-left" style="font-style: italic">Transport Amount</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{ $invoice->transport_amount }}
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <div class="col-md-7 pull-left" style="font-style: italic">Miscellaneous Amount</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{ $invoice->miscellaneous_amount }}
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <hr>
                <div class="col-md-7 pull-left" style="font-style: italic">Total Taxable Amt</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{ $totalTaxableAmount = $total - $discount + $invoice->insurance +
                  $invoice->postage + $invoice->transport_amount + $invoice->miscellaneous_amount }}
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="padding-top: 1% !important;">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <div class="col-md-7 pull-left" style="font-style: italic">IGST Charge</div>
                <div class="col-md-5 pull-right" style="font-style: oblique;">
                  {{$igst = $totalTaxableAmount * $invoice->igst_rate / 100 }}
                </div>
              </div>
            </div>
          </div>

          <div class="m-invoice__footer">
            <div class="m-invoice__container m-invoice__container--centered">
              <div class="m-invoice__content">
                            <span>
                              BANK TRANSFER
                            </span>
                <span>
                              <span>
                                Account Name:
                              </span>
                              <span>
                                {{ $invoice->party->name }}
                              </span>
                            </span>
                <span>
                              <span>
                                Account Number:
                              </span>
                              <span>
                                {{ $invoice->party->bank_account }}
                              </span>
                            </span>
                {{--<span>--}}
														{{--<span>--}}
															{{--Code:--}}
														{{--</span>--}}
														{{--<span>--}}
															{{--BARC0032UK--}}
														{{--</span>--}}
													{{--</span>--}}
              </div>
              <div class="m-invoice__content">
													<span>
														TOTAL AMOUNT
													</span>
                <span class="m-invoice__price">
														Rs. {{ $totalTaxableAmount + $igst }}
													</span>
                <span>
														Taxes Included
													</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
