@extends('admin.layouts.main')

@section('title', 'Invoices')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="" class="m-nav__link">
      <span class="m-nav__link-text">Invoices</span>
    </a>
  </li>
@endsection

@section('content')
  <portlet-content header="Invoices" subheader="list">
    <template slot="action_button">
      <a href="{{ route('admin.invoices.create') }}">
        <button
            class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air"
            title="Add new invoice"
            data-toggle="tooltip"
        >
          <i class="fa fa-plus"></i>
        </button>
      </a>
    </template>
    <template slot="body">
      <div class="row">
        <div class="col-md-12">
          <vue-table
              source="{{ route('admin.invoices.table') }}"
              :columns="['invoice_number', 'invoice_date', 'party', 'agent_name', 'action']"
          ></vue-table>
        </div>
      </div>
    </template>
  </portlet-content>
@endsection
