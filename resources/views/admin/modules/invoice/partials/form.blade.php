<div>
  <div :class="{'loading': formSubmitting, 'wrapper': formSubmitting}">
    <!--begin::Form-->
    <form class="m-form m-form--fit"
          @submit.prevent="submit" style="margin-top: 4%; padding-bottom: 1% !important;">
      <div class="m-portlet__body" style="padding: inherit;">
        <div class="m-form__section m-form__section--first">
          <div class="m-form__heading">
            <h3 class="m-form__heading-title">Invoice Info</h3>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label class="required">Invoice Number</label>
              <input type="text" name="invoice_number"
                     :class="{ 'input-error' : form.errors.has('invoice.invoice_number'), 'form-control': loaded }"
                     placeholder="Invoice Number"
                     v-model="values.invoice.invoice_number">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.invoice_number')"
                    v-text="form.errors.get('invoice.invoice_number')"></span>
            </div>
            <div class="col-lg-6">
              <label class="required">Invoice Date</label>
              <input type="date" name="invoice_date"
                     :class="{ 'input-error' : form.errors.has('invoice.invoice_date'), 'form-control': loaded }"
                     placeholder="Invoice Date"
                     v-model="values.invoice.invoice_date">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.invoice_date')"
                    v-text="form.errors.get('invoice.invoice_date')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Payment</label>
              <input type="text" name="payment"
                     :class="{ 'input-error' : form.errors.has('invoice.payment'), 'form-control': loaded }"
                     placeholder="Payment"
                     v-model="values.invoice.payment">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.payment')"
                    v-text="form.errors.get('invoice.payment')"></span>
            </div>
            <div class="col-lg-6">
              <label>Payment Date</label>
              <input type="date" name="payment_date"
                     :class="{ 'input-error' : form.errors.has('invoice.payment_date'), 'form-control': loaded }"
                     placeholder="Payment Date"
                     v-model="values.invoice.payment_date">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.payment_date')"
                    v-text="form.errors.get('invoice.payment_date')"></span>
            </div>
          </div>
        </div>

        <div class="m-form__seperator m-form__seperator--dashed"></div>

        <div class="m-form__section m-form__section--second">
          <div class="m-form__heading">
            <h3 class="m-form__heading-title">Client Info</h3>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-12">
              <label class="required">Client</label>
              <select name="party_id" v-model="values.invoice.party_id"
                      :class="{ 'input-error' : form.errors.has('invoice.party_id'), 'form-control': loaded }">
                <option value="" disabled>Select Client</option>
                @foreach ($parties as $id => $party)
                    <option value="{{ $id }}">{{ ucwords($party) }}</option>
                @endforeach
              </select>
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.party_id')"
                    v-text="form.errors.get('invoice.party_id')"></span>
            </div>
          </div>
        </div>

        <div class="m-form__seperator m-form__seperator--dashed"></div>

        <div class="m-form__section m-form__section--third">
          <div class="m-form__heading">
            <h3 class="m-form__heading-title">Transport Info</h3>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-12">
              <label>Transport Name</label>
              <input type="text" name="transport_name"
                     :class="{ 'input-error' : form.errors.has('invoice.transport_name'), 'form-control': loaded }"
                     placeholder="Transport Name"
                     v-model="values.invoice.transport_name">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.transport_name')"
                    v-text="form.errors.get('invoice.transport_name')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Transport Payment</label>
              <input type="text" name="transport_payment"
                     :class="{ 'input-error' : form.errors.has('invoice.transport_payment'), 'form-control': loaded }"
                     placeholder="Transport Payment"
                     v-model="values.invoice.transport_payment">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.transport_payment')"
                    v-text="form.errors.get('invoice.transport_payment')"></span>
            </div>
            <div class="col-lg-6">
              <label>Transport Payment Date</label>
              <input type="date" name="transport_payment_date"
                     :class="{ 'input-error' : form.errors.has('invoice.transport_payment_date'), 'form-control': loaded }"
                     placeholder="Transport Payment Date"
                     v-model="values.invoice.transport_payment_date">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.transport_payment_date')"
                    v-text="form.errors.get('invoice.transport_payment_date')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Lr Number</label>
              <input type="text" name="lr_number"
                     :class="{ 'input-error' : form.errors.has('invoice.lr_number'), 'form-control': loaded }"
                     placeholder="LR Number"
                     v-model="values.invoice.lr_number">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.lr_number')"
                    v-text="form.errors.get('invoice.lr_number')"></span>
            </div>
            <div class="col-lg-6">
              <label>Lr Date</label>
              <input type="date" name="lr_date"
                     :class="{ 'input-error' : form.errors.has('invoice.lr_date'), 'form-control': loaded }"
                     placeholder="LR Number"
                     v-model="values.invoice.lr_date">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.lr_date')"
                    v-text="form.errors.get('invoice.lr_date')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Dispatched Date</label>
              <input type="date" name="dispatched_date"
                     :class="{ 'input-error' : form.errors.has('invoice.dispatched_date'), 'form-control': loaded }"
                     placeholder="Dispatched Date"
                     v-model="values.invoice.dispatched_date">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.dispatched_date')"
                    v-text="form.errors.get('invoice.dispatched_date')"></span>
            </div>
            <div class="col-lg-6">
              <label>Received Date</label>
              <input type="date" name="received_date"
                     :class="{ 'input-error' : form.errors.has('invoice.received_date'), 'form-control': loaded }"
                     placeholder="Received Date"
                     v-model="values.invoice.received_date">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.received_date')"
                    v-text="form.errors.get('invoice.received_date')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Dispatched From</label>
              <input type="text" name="dispatched_from"
                     :class="{ 'input-error' : form.errors.has('invoice.dispatched_from'), 'form-control': loaded }"
                     placeholder="Dispatched From"
                     v-model="values.invoice.dispatched_from">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.dispatched_from')"
                    v-text="form.errors.get('invoice.dispatched_from')"></span>
            </div>
            <div class="col-lg-6">
              <label>Received At</label>
              <input type="text" name="received_at"
                     :class="{ 'input-error' : form.errors.has('invoice.received_at'), 'form-control': loaded }"
                     placeholder="Received At"
                     v-model="values.invoice.received_at">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.received_at')"
                    v-text="form.errors.get('invoice.received_at')"></span>
            </div>
          </div>
        </div>

        <div class="m-form__seperator m-form__seperator--dashed"></div>

        <div class="m-form__section m-form__section--forth">
          <div class="m-form__heading">
            <h3 class="m-form__heading-title">Agent Info</h3>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Agent Name</label>
              <input type="text" name="agent_name"
                     :class="{ 'input-error' : form.errors.has('invoice.ifsc_code'), 'form-control': loaded }"
                     placeholder="Agent Name"
                     v-model="values.invoice.agent_name">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.agent_name')"
                    v-text="form.errors.get('invoice.agent_name')"></span>
            </div>
            <div class="col-lg-6">
              <label>Agent Phone</label>
              <input type="text" name="agent_mobile"
                     :class="{ 'input-error' : form.errors.has('invoice.agent_mobile'), 'form-control': loaded }"
                     placeholder="Agent Phone"
                     v-model="values.invoice.agent_mobile">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.agent_mobile')"
                    v-text="form.errors.get('invoice.agent_mobile')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Agent Charge</label>
              <input type="number" min="0" name="agent_charge"
                     :class="{ 'input-error' : form.errors.has('invoice.agent_charge'), 'form-control': loaded }"
                     placeholder="Agent Charge"
                     v-model="values.invoice.agent_charge">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.agent_charge')"
                    v-text="form.errors.get('invoice.agent_charge')"></span>
            </div>
          </div>
        </div>

        <div class="m-form__seperator m-form__seperator--dashed"></div>

        <div class="m-form__section m-form__section--fifth">
          <div class="m-form__heading">
            <h3 class="m-form__heading-title">Extra Charges & Discounts</h3>
          </div>
          <h3 class="form-section"></h3>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Discount (in %)</label>
              <input type="text" name="discount_rate"
                     :class="{ 'input-error' : form.errors.has('invoice.discount_rate'), 'form-control': loaded }"
                     placeholder="Discount Rate"
                     @change="calculateTotalAmount()"
                     v-model="values.invoice.discount_rate">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.discount_rate')"
                    v-text="form.errors.get('invoice.discount_rate')"></span>
            </div>
            <div class="col-lg-6">
              <label>IGST (in %)</label>
              <input type="text" name="igst_rate"
                     :class="{ 'input-error' : form.errors.has('invoice.igst_rate'), 'form-control': loaded }"
                     placeholder="IGST Rate"
                     v-model="values.invoice.igst_rate">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.igst_rate')"
                    v-text="form.errors.get('invoice.igst_rate')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Insurance Amount</label>
              <input type="text" name="insurance"
                     :class="{ 'input-error' : form.errors.has('invoice.insurance'), 'form-control': loaded }"
                     placeholder="Insurance"
                     @change="calculateTotalAmount()"
                     v-model="values.invoice.insurance">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.insurance')"
                    v-text="form.errors.get('invoice.insurance')"></span>
            </div>
            <div class="col-lg-6">
              <label>Postage</label>
              <input type="text" name="postage"
                     :class="{ 'input-error' : form.errors.has('invoice.postage'), 'form-control': loaded }"
                     placeholder="Postage"
                     @change="calculateTotalAmount()"
                     v-model="values.invoice.postage">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.postage')"
                    v-text="form.errors.get('invoice.postage')"></span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Transport Amount</label>
              <input type="text" name="transport_amount"
                     :class="{ 'input-error' : form.errors.has('invoice.transport_amount'), 'form-control': loaded }"
                     placeholder="Transport Amount"
                     @change="calculateTotalAmount()"
                     v-model="values.invoice.transport_amount">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.transport_amount')"
                    v-text="form.errors.get('invoice.transport_amount')"></span>
            </div>
            <div class="col-lg-6">
              <label>Miscellaneous Amount</label>
              <input type="text" name="miscellaneous_amount"
                     :class="{ 'input-error' : form.errors.has('invoice.miscellaneous_amount'), 'form-control': loaded }"
                     placeholder="Miscellaneous Amount"
                     @change="calculateTotalAmount()"
                     v-model="values.invoice.miscellaneous_amount">
              <span class="m-form__help danger"
                    v-if="form.errors.has('invoice.miscellaneous_amount')"
                    v-text="form.errors.get('invoice.miscellaneous_amount')"></span>
            </div>
          </div>
        </div>

        <div class="m-form__seperator m-form__seperator--dashed"></div>

        <div class="form-group m-form__group row">
          <div class="col-md-12">
            <table class="table">
              <thead>
              <th><strong>Bale No.</strong></th>
              <th style="width: 30%;"><strong>Description</strong></th>
              <th><strong>HSN Code</strong></th>
              <th><strong>Type</strong></th>
              <th><strong>Qty.</strong></th>
              <th><strong>U/M</strong></th>
              <th><strong>Rate</strong></th>
              <th><strong>Taxable Amt.</strong></th>
              <th></th>
              </thead>
              <tbody>
              <tr v-for="(row, index) in values.items" :key="index">
                <td>
                  <span v-if="!row.is_edit" v-text="row.bale_number"></span>
                  <input type="text" v-else v-model="row.bale_number" class="form-control"/>
                </td>
                <td>
                  <span v-if="!row.is_edit" v-text="row.description"></span>
                  <input type="text" v-else v-model="row.description" class="form-control"/>
                </td>
                <td>
                  <span v-if="!row.is_edit" v-text="row.hsn_code"></span>
                  <input type="text" v-else v-model="row.hsn_code" class="form-control"/>
                </td>
                <td>
                  <span v-if="!row.is_edit" v-text="row.product_type"></span>
                  <input type="text" v-else v-model="row.product_type" class="form-control"/>
                </td>
                <td>
                  <span v-if="!row.is_edit" v-text="row.quantity"></span>
                  <input type="text" v-else v-model="row.quantity" class="form-control"/>
                </td>
                <td>
                  <span v-if="!row.is_edit" v-text="row.u_m"></span>
                  <input type="text" v-else v-model="row.u_m" class="form-control"/>
                </td>
                <td>
                  <span v-if="!row.is_edit" v-text="row.rate"></span>
                  <input type="text" v-else v-model="row.rate" class="form-control"/>
                </td>
                <td>
                  <span v-if="!row.is_edit" v-text="row.taxable_amount"></span>
                  <input type="text" v-else v-model="row.taxable_amount" class="form-control"/>
                </td>
                <td nowrap="">
                  <a href="javascript:void(0)" class="btn btn-sm"
                     :class="row.is_edit ? 'btn-success' : 'btn-primary'"
                     @click.prevent="handleRowEditClick(index)">
                    <i class="fa" :class="row.is_edit ? 'fa-save' : 'fa-edit'"></i>
                  </a>
                  <a href="javascript:void(0)"
                     class="btn btn-sm btn-danger"
                     @click.prevent="removeRow(index, row)"
                  ><i class="fa fa-trash"></i></a>
                </td>
                <td>
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" :class="{ 'input-error' : form.errors.has('bale_number'), 'form-control': loaded }"
                         placeholder="Bale No.*"
                         v-model="bale_number">
                  <span class="m-form__help danger"
                        v-if="form.errors.has('bale_number')"
                        v-text="form.errors.get('bale_number')"></span>
                </td>
                <td>
                  <input type="text" :class="{ 'input-error' : form.errors.has('description'), 'form-control': loaded }"
                         placeholder="Description*" v-model="description">
                  <span class="m-form__help danger"
                        v-if="form.errors.has('description')"
                        v-text="form.errors.get('description')"></span>
                </td>
                <td>
                  <input type="text" :class="{ 'input-error' : form.errors.has('hsn_code'), 'form-control': loaded }"
                         placeholder="HSN Code*" v-model="hsn_code">
                  <span class="m-form__help danger"
                        v-if="form.errors.has('hsn_code')"
                        v-text="form.errors.get('hsn_code')"></span>
                </td>
                <td>
                  <select :class="{ 'input-error' : form.errors.has('product_type'), 'form-control': loaded }"
                           v-model="product_type">
                    @foreach($productTypes as $productType)
                      <option value="{{ $productType }}">{{ strtoupper($productType) }}</option>
                      @endforeach
                  </select>
                  {{--<input type="text" :class="{ 'input-error' : form.errors.has('items.'+index+'.product_type'), 'form-control': loaded }"--}}
                         {{--placeholder="Select Product type*" v-model="row.product_type">--}}
                  <span class="m-form__help danger"
                        v-if="form.errors.has('product_type')"
                        v-text="form.errors.get('product_type')"></span>
                </td>
                <td>
                  <input type="text" :class="{ 'input-error' : form.errors.has('quantity'), 'form-control': loaded }"
                         placeholder="Quantity*" v-model="quantity" v-on:change="calculateTaxableAmount()">
                  <span class="m-form__help danger"
                        v-if="form.errors.has('quantity')"
                        v-text="form.errors.get('quantity')"></span>
                </td>
                <td>
                  <input type="text" :class="{ 'input-error' : form.errors.has('u_m'), 'form-control': loaded }"
                         placeholder="U/M" v-model="u_m">
                  <span class="m-form__help danger"
                        v-if="form.errors.has('u_m')"
                        v-text="form.errors.get('u_m')"></span>
                </td>
                <td>
                  <input type="text" :class="{ 'input-error' : form.errors.has('rate'), 'form-control': loaded }"
                         placeholder="Rate*" v-model="rate"  v-on:change="calculateTaxableAmount()">
                  <span class="m-form__help danger"
                        v-if="form.errors.has('rate')"
                        v-text="form.errors.get('rate')"></span>
                </td>
                <td>
                  <input type="text" :class="{ 'input-error' : form.errors.has('taxable_amount'), 'form-control': loaded }"
                         placeholder="Taxable Amount"
                           v-model="taxable_amount">
                  <span class="m-form__help danger"
                        v-if="form.errors.has('taxable_amount')"
                        v-text="form.errors.get('taxable_amount')"></span>
                </td>
                <td>
                  <button type="button" class="btn btn-primary m-btn--sm mt-28" title="Save" data-toggle="tooltip"
                          @click="addNewRow()">ADD
                  </button>
                  {{--<button type="button" class="btn btn-sm btn-danger" v-on:click="removeElement(index);"--}}
                          {{--style="cursor: pointer" title="Remove">--}}
                    {{--<i class="fa fa-trash-o"></i>--}}
                  {{--</button>--}}
                </td>
              </tr>
              </tbody>
            </table>
            <span class="m-form__help danger"
                  v-if="form.errors.has('items')"
                  v-text="form.errors.get('items')"></span>
            {{--<div class="pull-right">--}}
              {{--<button type="button" class="btn btn-sm btn-success" @click="addRow"><i class="fa fa-plus"></i> Add row--}}
              {{--</button>--}}
            {{--</div>--}}
            <hr>
          </div>
        </div>
        <div class="form-group m-form__group row"  style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">Total : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ total }}
              </div>
            </div>
          </div>
        </div>
        <div class="form-group m-form__group row"  style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">Discount : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ values.invoice.discount_rate * total / 100 }}
              </div>
            </div>
          </div>
        </div>
        <div class="form-group m-form__group row" style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <div class="col-md-6 pull-left">
              <strong style="font-style: italic; font-family: 'Helvetica Neue'">Insurance : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ values.invoice.insurance }}
              </div>
            </div>
          </div>
        </div>
        <div class="form-group m-form__group row" style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">Postage : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ values.invoice.postage }}
              </div>
            </div>
          </div>
        </div>
        <div class="form-group m-form__group row" style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">Transport Amt : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ values.invoice.transport_amount }}
              </div>
            </div>
          </div>
        </div>
        <div class="form-group m-form__group row" style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">Misc. Amt : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ values.invoice.miscellaneous_amount }}
              </div>
            </div>
          </div>
        </div>
        <div class="form-group m-form__group row" style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <hr>
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">Total Taxable Amt : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ total_taxable_amount }}
              </div>
            </div>
          </div>
        </div>
        <div class="form-group m-form__group row" style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">IGST : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ total_taxable_amount * values.invoice.igst_rate / 100 }}
              </div>
            </div>
          </div>
        </div>

        <div class="form-group m-form__group row" style="margin-top: -2% !important;">
          <div class="col-md-12">
            <div class="col-md-4 pull-right">
              <hr>
              <div class="col-md-6 pull-left">
                <strong style="font-style: italic; font-family: 'Helvetica Neue'">Grand Total : </strong>
              </div>
              <div class="col-md-6 pull-right">
                @{{ total_taxable_amount + total_taxable_amount * values.invoice.igst_rate / 100 }}
              </div>
            </div>
          </div>
        </div>

        <div class="m-form__seperator m-form__seperator--dashed"></div>

        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <button class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
    </form>
    <!--end::Form-->
  </div>
</div>
