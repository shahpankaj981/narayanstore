@extends('admin.layouts.main')

@section('title', 'Invoices')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="{{ route('admin.invoices.index') }}" class="m-nav__link">
      <span class="m-nav__link-text">Invoices</span>
    </a>
  </li>
  <li class="m-nav__separator">
    -
  </li>
  <li class="m-nav__item">
    <a href="#" class="m-nav__link">
      <span class="m-nav__link-text">Create</span>
    </a>
  </li>
@endsection

@section('content')
  {{--send further props from this create user component--}}
  <invoice-form inline-template url="{{ route('admin.invoices.store') }}">
    <portlet-content header="Invoice" subheader="Create" isForm="true">
      <template slot="body">
        @include('admin.modules.invoice.partials.form')
      </template>
    </portlet-content>
  </invoice-form>
@endsection
