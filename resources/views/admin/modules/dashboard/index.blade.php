@extends('admin.layouts.main')

@section('title', 'Dashboard')

@section('breadcrumb')
  <li class="m-nav__item">
    <a href="" class="m-nav__link">
      <span class="m-nav__link-text">Dashboard</span>
    </a>
  </li>
@endsection

@section('content')
  <div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide"><i class="la la-gear"></i></span>
          <h3 class="m-portlet__head-text">Welcome {{ currentUser()->formatted_full_name }}</h3>
        </div>
      </div>
    </div>

    <div class="m-portlet__body">

      <div class="m-section">
        <div class="m-section__content">
          <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
              <div class="row">
                <h6 class="text-center"> This is your dashboard</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
