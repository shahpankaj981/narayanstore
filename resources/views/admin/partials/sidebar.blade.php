<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
	<i class="la la-close"></i>
</button>

<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<div id="m_ver_menu"
		 class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
		 data-menu-vertical="true"
		 data-menu-scrollable="false" data-menu-dropdown-timeout="500">
		
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			
			<li class="m-menu__item {{ isSubMenuActive('admin.dashboard') }}" aria-haspopup="true">
				<a href="{{ route('admin.dashboard') }}" class="m-menu__link ">
					<i class="m-menu__link-icon fa fa-home"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">
								Dashboard
							</span>
							<span class="m-menu__link-badge">
								<span class="m-badge m-badge--danger">2</span>
							</span>
						</span>
					</span>
				</a>
			</li>

			<li class="m-menu__item  m-menu__item--submenu {{ isSideBarActive('users') }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
				<a href="javascript:" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon fa fa-users"></i>
					<span class="m-menu__link-text">User Management</span>
					<i class="m-menu__ver-arrow fa fa-chevron-right"></i>
				</a>
				
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item {{ isSubMenuActive('admin.users.index') }}" aria-haspopup="true">
							<a href="{{ route('admin.users.index') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Users</span>
							</a>
						</li>
					
					</ul>
				</div>
			</li>
			
			{{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" data-menu-submenu-toggle="hover">--}}
				{{--<a href="#" class="m-menu__link m-menu__toggle">--}}
					{{--<i class="m-menu__link-icon fa fa-universal-access"></i>--}}
					{{--<span class="m-menu__link-text">Permissions Mgmt.</span>--}}
					{{--<i class="m-menu__ver-arrow fa fa-chevron-right"></i>--}}
				{{--</a>--}}
				{{----}}
				{{--<div class="m-menu__submenu ">--}}
					{{--<span class="m-menu__arrow"></span>--}}
					{{--<ul class="m-menu__subnav">--}}
						{{--<li class="m-menu__item" aria-haspopup="true">--}}
							{{--<a href="{{ route('admin.permissions.manage') }}" class="m-menu__link ">--}}
								{{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
									{{--<span></span>--}}
								{{--</i>--}}
								{{--<span class="m-menu__link-text">Manage</span>--}}
							{{--</a>--}}
						{{--</li>--}}
						{{--<li class="m-menu__item  m-menu__item--active" aria-haspopup="true">--}}
							{{--<a href="{{ route('admin.roles.index') }}" class="m-menu__link ">--}}
								{{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
									{{--<span></span>--}}
								{{--</i>--}}
								{{--<span class="m-menu__link-text">Roles</span>--}}
							{{--</a>--}}
						{{--</li>--}}
						{{--<li class="m-menu__item  m-menu__item--active" aria-haspopup="true">--}}
							{{--<a href="{{ route('admin.permissions.index') }}" class="m-menu__link ">--}}
								{{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
									{{--<span></span>--}}
								{{--</i>--}}
								{{--<span class="m-menu__link-text">Permissions</span>--}}
							{{--</a>--}}
						{{--</li>--}}
					{{--</ul>--}}
				{{--</div>--}}
			{{--</li>--}}

			<li class="m-menu__item  m-menu__item--submenu {{ isSideBarActive('parties') }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
				<a href="#" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon fa fa-universal-access"></i>
					<span class="m-menu__link-text">Clients</span>
					<i class="m-menu__ver-arrow fa fa-chevron-right"></i>
				</a>

				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item {{ isSubMenuActive('admin.parties.index') }}" aria-haspopup="true">
							<a href="{{ route('admin.parties.index') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Clients</span>
							</a>
						</li>
						<li class="m-menu__item {{ isSubMenuActive('admin.parties.create') }}" aria-haspopup="true">
							<a href="{{ route('admin.parties.create') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Add New Client</span>
							</a>
						</li>
					</ul>
				</div>
			</li>

			<li class="m-menu__item  m-menu__item--submenu {{ isSideBarActive('invoices') }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
				<a href="#" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon fa fa-address-book"></i>
					<span class="m-menu__link-text">Invoices</span>
					<i class="m-menu__ver-arrow fa fa-chevron-right"></i>
				</a>

				<div class="m-menu__submenu">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item {{ isSubMenuActive('admin.invoices.index') }}" aria-haspopup="true">
							<a href="{{ route('admin.invoices.index') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Invoices</span>
							</a>
						</li>
						<li class="m-menu__item {{ isSubMenuActive('admin.invoices.create') }}" aria-haspopup="true">
							<a href="{{ route('admin.invoices.create') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Add New invoice</span>
							</a>
						</li>
					</ul>
				</div>
			</li>

			<li class="m-menu__item  m-menu__item--submenu {{ isSideBarActive('customs') }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
				<a href="#" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon fa fa-book"></i>
					<span class="m-menu__link-text">Customs</span>
					<i class="m-menu__ver-arrow fa fa-chevron-right"></i>
				</a>

				<div class="m-menu__submenu">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item {{ isSubMenuActive('admin.customs.index') }}" aria-haspopup="true">
							<a href="{{ route('admin.customs.index') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Customs</span>
							</a>
						</li>
						<li class="m-menu__item {{ isSubMenuActive('admin.customs.create') }}" aria-haspopup="true">
							<a href="{{ route('admin.customs.create') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Add New Custom</span>
							</a>
						</li>
					</ul>
				</div>
			</li>

		</ul>
	
	</div>
</div>
