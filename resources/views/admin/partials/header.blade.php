<header class="m-grid__item m-header" data-minimize-offset="200" data-minimize-mobile-offset="200">
	<div class="m-container m-container--fluid m-container--full-height">
		<div class="m-stack m-stack--ver m-stack--desktop">
			
			@include('admin.partials._header_brand')
			
			<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
				<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-topbar__nav-wrapper">
						<ul class="m-topbar__nav m-nav m-nav--inline">
							
							<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
								<a href="#" class="m-nav__link m-dropdown__toggle">
									<span class="m-topbar__userpic">
										<img src="{{ asset('images/defaults/male.png') }}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
									</span>
									<span class="m-topbar__username m--hide">Nick</span>
								</a>
								
								<div class="m-dropdown__wrapper">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
									
									<div class="m-dropdown__inner">
										<div class="m-dropdown__header m--align-center" style="background: url({{ asset('images/defaults/user_profile_bg.jpg') }}); background-size: cover;">
											<div class="m-card-user m-card-user--skin-dark">
												<div class="m-card-user__pic">
													<img src="{{ asset('images/defaults/male.png') }}" class="m--img-rounded m--marginless" alt=""/>
												</div>
												<div class="m-card-user__details">
													<span class="m-card-user__name m--font-weight-500">
														{{ currentUser()->formatted_full_name }}
													</span>
													<a href="mailto:mark.andre@gmail.com" class="m-card-user__email m--font-weight-300 m-link">
														{{ currentUser()->email }}
													</a>
												</div>
											</div>
										</div>
										
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav m-nav--skin-light">
													
													<li class="m-nav__section m--hide">
														<span class="m-nav__section-text">
															Section
														</span>
													</li>
													
													<li class="m-nav__item">
														<a href="{{ route('admin.users.profile') }}" class="m-nav__link">
															<i class="m-nav__link-icon fa fa-user"></i>
															<span class="m-nav__link-title">
																<span class="m-nav__link-wrap">
																	<span class="m-nav__link-text">
																		My Profile
																	</span>
																</span>
															</span>
														</a>
													</li>
													
													<li class="m-nav__separator m-nav__separator--fit"></li>
													
													<li class="m-nav__item">
														<a href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('logout-form').submit()" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
															Logout
														</a>
													</li>
													
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

{!! Form::open([
	'url' => route('auth.logout'),
	'method' => 'post',
	'id' => 'logout-form',
	'style' => 'display:none;'
]) !!}
{!! Form::close() !!}
