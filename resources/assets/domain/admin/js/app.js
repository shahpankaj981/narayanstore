/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("./bootstrap");

import Vue from 'vue';
import VueToastr from '@deveodk/vue-toastr'
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'
/**************************************************
 * View files
 **************************************************/
import UsersList from '../views/Users/UsersList';
import createUser from '../views/Users/CreateUser'
import PartyForm from '../views/Party/PartyForm'
import InvoiceForm from '../views/Invoice/InvoiceForm'
import CustomForm from '../views/Custom/CustomForm'
import PermissionForm from '../views/Users/Permissions/PermissionForm'
import RolePermissionAssociation from '../views/Users/Permissions/RolesAndPermissionsAssociation'
import vSelect from 'vue-select'
import Profile from '../views/Users/Profile'
/**************************************************
 * Components
 **************************************************/
import PortletContent from '../../common/components/Portlets/Content/PortletContent'
import DeleteButton from '../../common/components/Buttons/DeleteButton'
import EditButton from '../../common/components/Buttons/EditButton'
import VueTable from '../../common/components/Datatable/DataTable'
import AlertNotification from '../../common/components/Alert/AlertNotification'
import Modal from '../../common/components/Modal'

Vue.component('v-select', vSelect);

Vue.use(VueToastr, {
    defaultPosition: 'toast-top-right',
    defaultType: 'info',
    defaultTimeout: 6000
});

const app = new Vue({
    el: '#app',
    components: {
      UsersList,
      createUser,
      PortletContent,
      DeleteButton,
      AlertNotification,
      EditButton,
      PermissionForm,
      VueTable,
      RolePermissionAssociation,
      PartyForm,
      InvoiceForm,
      CustomForm,
      Profile,
      Modal
    }
});