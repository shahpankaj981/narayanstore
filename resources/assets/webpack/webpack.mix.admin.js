let mix = require("laravel-mix");

if (!mix.config.production) {
    mix.webpackConfig({devtool: "inline-source-map"}).sourceMaps();
}

mix.
    setPublicPath(path.normalize("public/admin")).
    setResourceRoot("../").
    js("resources/assets/domain/admin/js/theme.js", "js/theme.js").version().
    js("resources/assets/domain/admin/js/app.js", "js/app.js").version().
    extract([
        "jquery", "popper.js", "bootstrap", "js-cookie", "jquery-smooth-scroll", "wnumb",
    ]).
    autoload({
        jquery: ["$", "jQuery", "jquery", "window.jQuery"],
    }).
    sass("resources/assets/domain/admin/sass/vendor.scss", "css/vendor.css").version().
    sass("resources/assets/domain/admin/sass/app.scss", "css/app.css").version().
    copy("resources/assets/domain/common/theme/css", "public/admin/css").
    js("resources/assets/domain/common/theme/js/demo/default/custom/components/forms/widgets/form-repeater.js", "public/admin/js/form-repeater.js");
