<?php

return [

    'route_prefixes' => [
        'api' => env('API_ROUTE_PREFIX', 'api'),
    ],

    'copyright' => [
        'name'       => 'Pankaj Shah',
        'url'        => 'https://shahpankaj.com.np',
        'start_year' => 2018,
    ],

];
