<?php

return [
    'Foreign saree',
    'Cotton saree',
    'Synthetic saree',
    'Print',
    'Shirting',
    'Suiting',
    'Rubia',
    'Poplin',
    'Towel',
    'Leggings',
    'Bedsheet',
    'Kurta salwar',
    'Fals',
    'Net',
    'Dhoti',
    'Lungi',
    'Ganji',
    'Tweed',
    'Mal mal',
    'Astar',
    'Broket',
    'Gamcha',
    'Banarasi saree',
    'Miscellaneous',
];
