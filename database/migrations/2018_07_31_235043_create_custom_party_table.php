<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCustomPartyTable
 */
class CreateCustomPartyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_party', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('custom_id');
            $table->unsignedInteger('party_id');
            $table->timestamps();

            $table->foreign('custom_id')->references('id')->on('customs')->onDelete('cascade');
            $table->foreign('party_id')->references('id')->on('parties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_party');
    }
}
