<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCustomsTable
 */
class CreateCustomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('custom_ref_no');
            $table->string('vehicle_no')->nullable();
            $table->string('quality')->nullable();
            $table->string('custom_amount');
            $table->string('vat_amount')->nullable();
            $table->string('clearance_charge')->nullable();
            $table->date('clearance_date')->nullable();;
            $table->integer('bale_quantity')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customs');
    }
}
