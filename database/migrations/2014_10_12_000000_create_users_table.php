<?php

use App\Constants\DBTable;
use App\Constants\StatusType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateUsersTable
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DBTable::AUTH_USERS,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('username', 190)->unique();
                $table->string('email', 190)->unique();
                $table->string('password', 190);
                $table->rememberToken();

                $table->text('full_name')->nullable();
                $table->boolean('is_first_login')->default(true);
                $table->string('status', 190)->default(StatusType::UNVERIFIED);
                $table->text('metadata')->nullable();

                $table->unsignedInteger('created_by_id')->index()->nullable();
                $table->unsignedInteger('updated_by_id')->index()->nullable();
                $table->unsignedInteger('deleted_by_id')->index()->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('created_by_id')->references('id')->on(DBTable::AUTH_USERS)->onDelete('cascade')->nullable();
                $table->foreign('updated_by_id')->references('id')->on(DBTable::AUTH_USERS)->onDelete('cascade')->nullable();
                $table->foreign('deleted_by_id')->references('id')->on(DBTable::AUTH_USERS)->onDelete('cascade')->nullable();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::AUTH_USERS);
    }
}
