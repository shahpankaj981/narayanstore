<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number');
            $table->date('invoice_date');
            $table->string('payment')->nullable();
            $table->date('payment_date')->nullable();
            $table->unsignedInteger('party_id');
            $table->string('agent_name')->nullable();
            $table->string('agent_mobile')->nullable();
            $table->double('agent_charge')->nullable();
            $table->string('lr_number')->nullable();
            $table->date('lr_date')->nullable();
            $table->string('transport_name')->nullable();
            $table->float('transport_payment')->nullable();
            $table->date('transport_payment_date')->nullable();
            $table->date('dispatched_date')->nullable();
            $table->date('received_date')->nullable();
            $table->string('dispatched_from')->nullable();
            $table->string('received_at')->nullable();
            $table->float('discount_rate')->default(0);
            $table->float('gst_rate')->default(0);
            $table->float('cgst_rate')->default(0);
            $table->float('sgst_rate')->default(0);
            $table->float('postage')->default(0);
            $table->float('insurance')->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('party_id')->references('id')->on('parties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
