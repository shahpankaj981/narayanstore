<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCustomInvoiceTable
 */
class CreateCustomInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('custom_id');
            $table->unsignedInteger('invoice_id');
            $table->timestamps();

            $table->foreign('custom_id')->references('id')->on('customs')->onDelete('cascade');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_invoice');
    }
}
