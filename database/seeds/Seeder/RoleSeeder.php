<?php

use App\Constants\AuthPermission;
use App\Constants\AuthRoles;
use App\Data\Entities\Models\User\Role;

/**
 * Class RoleSeeder
 */
class RoleSeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(AuthRoles::ALL())->each(
            function (string $role) {
                /** @var Role $roleInDb */
                $roleInDb = app(Role::class)->where('name', $role)->first();
                if ( is_null($roleInDb) ) {
                    $roleInDb = app(Role::class)->create(['name' => $role]);
                }
            }
        );
    }
}
